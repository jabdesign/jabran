/*
 Highcharts JS v4.0.4 (2014-09-02)
 Exporting module

 (c) 2010-2014 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(f){var A=f.Chart,t=f.addEvent,B=f.removeEvent,l=f.createElement,o=f.discardElement,v=f.css,k=f.merge,r=f.each,p=f.extend,D=Math.max,j=document,C=window,E=f.isTouchDevice,F=f.Renderer.prototype.symbols,s=f.getOptions(),y;p(s.lang,{printChart:"Print chart",downloadPNG:"Download PNG image",downloadJPEG:"Download JPEG image",downloadPDF:"Download PDF document",downloadSVG:"Download SVG vector image",contextButtonTitle:"Chart context menu"});s.navigation={menuStyle:{border:"1px solid #A0A0A0",
background:"#FFFFFF",padding:"5px 0"},menuItemStyle:{padding:"0 10px",background:"none",color:"#303030",fontSize:E?"14px":"11px"},menuItemHoverStyle:{background:"#4572A5",color:"#FFFFFF"},buttonOptions:{symbolFill:"#E0E0E0",symbolSize:14,symbolStroke:"#666",symbolStrokeWidth:3,symbolX:12.5,symbolY:10.5,align:"right",buttonSpacing:3,height:22,theme:{fill:"white",stroke:"none"},verticalAlign:"top",width:24}};s.exporting={type:"image/png",url:"http://export.highcharts.com/",buttons:{contextButton:{menuClassName:"highcharts-contextmenu",
symbol:"menu",_titleKey:"contextButtonTitle",menuItems:[{textKey:"printChart",onclick:function(){this.print()}},{separator:!0},{textKey:"downloadPNG",onclick:function(){this.exportChart()}},{textKey:"downloadJPEG",onclick:function(){this.exportChart({type:"image/jpeg"})}},{textKey:"downloadPDF",onclick:function(){this.exportChart({type:"application/pdf"})}},{textKey:"downloadSVG",onclick:function(){this.exportChart({type:"image/svg+xml"})}}]}}};f.post=function(b,a,d){var c,b=l("form",k({method:"post",
action:b,enctype:"multipart/form-data"},d),{display:"none"},j.body);for(c in a)l("input",{type:"hidden",name:c,value:a[c]},null,b);b.submit();o(b)};p(A.prototype,{getSVG:function(b){var a=this,d,c,z,h,g=k(a.options,b);if(!j.createElementNS)j.createElementNS=function(a,b){return j.createElement(b)};b=l("div",null,{position:"absolute",top:"-9999em",width:a.chartWidth+"px",height:a.chartHeight+"px"},j.body);c=a.renderTo.style.width;h=a.renderTo.style.height;c=g.exporting.sourceWidth||g.chart.width||
/px$/.test(c)&&parseInt(c,10)||600;h=g.exporting.sourceHeight||g.chart.height||/px$/.test(h)&&parseInt(h,10)||400;p(g.chart,{animation:!1,renderTo:b,forExport:!0,width:c,height:h});g.exporting.enabled=!1;g.series=[];r(a.series,function(a){z=k(a.options,{animation:!1,enableMouseTracking:!1,showCheckbox:!1,visible:a.visible});z.isInternal||g.series.push(z)});d=new f.Chart(g,a.callback);r(["xAxis","yAxis"],function(b){r(a[b],function(a,c){var g=d[b][c],f=a.getExtremes(),h=f.userMin,f=f.userMax;g&&(h!==
void 0||f!==void 0)&&g.setExtremes(h,f,!0,!1)})});c=d.container.innerHTML;g=null;d.destroy();o(b);c=c.replace(/zIndex="[^"]+"/g,"").replace(/isShadow="[^"]+"/g,"").replace(/symbolName="[^"]+"/g,"").replace(/jQuery[0-9]+="[^"]+"/g,"").replace(/url\([^#]+#/g,"url(#").replace(/<svg /,'<svg xmlns:xlink="http://www.w3.org/1999/xlink" ').replace(/ href=/g," xlink:href=").replace(/\n/," ").replace(/<\/svg>.*?$/,"</svg>").replace(/(fill|stroke)="rgba\(([ 0-9]+,[ 0-9]+,[ 0-9]+),([ 0-9\.]+)\)"/g,'$1="rgb($2)" $1-opacity="$3"').replace(/&nbsp;/g,
"Â ").replace(/&shy;/g,"Â­").replace(/<IMG /g,"<image ").replace(/height=([^" ]+)/g,'height="$1"').replace(/width=([^" ]+)/g,'width="$1"').replace(/hc-svg-href="([^"]+)">/g,'xlink:href="$1"/>').replace(/id=([^" >]+)/g,'id="$1"').replace(/class=([^" >]+)/g,'class="$1"').replace(/ transform /g," ").replace(/:(path|rect)/g,"$1").replace(/style="([^"]+)"/g,function(a){return a.toLowerCase()});return c=c.replace(/(url\(#highcharts-[0-9]+)&quot;/g,"$1").replace(/&quot;/g,"'")},exportChart:function(b,a){var b=
b||{},d=this.options.exporting,d=this.getSVG(k({chart:{borderRadius:0}},d.chartOptions,a,{exporting:{sourceWidth:b.sourceWidth||d.sourceWidth,sourceHeight:b.sourceHeight||d.sourceHeight}})),b=k(this.options.exporting,b);f.post(b.url,{filename:b.filename||"chart",type:b.type,width:b.width||0,scale:b.scale||2,svg:d},b.formAttributes)},print:function(){var b=this,a=b.container,d=[],c=a.parentNode,f=j.body,h=f.childNodes;if(!b.isPrinting)b.isPrinting=!0,r(h,function(a,b){if(a.nodeType===1)d[b]=a.style.display,
a.style.display="none"}),f.appendChild(a),C.focus(),C.print(),setTimeout(function(){c.appendChild(a);r(h,function(a,b){if(a.nodeType===1)a.style.display=d[b]});b.isPrinting=!1},1E3)},contextMenu:function(b,a,d,c,f,h,g){var e=this,k=e.options.navigation,q=k.menuItemStyle,m=e.chartWidth,n=e.chartHeight,j="cache-"+b,i=e[j],u=D(f,h),w,x,o,s=function(a){e.pointer.inClass(a.target,b)||x()};if(!i)e[j]=i=l("div",{className:b},{position:"absolute",zIndex:1E3,padding:u+"px"},e.container),w=l("div",null,p({MozBoxShadow:"3px 3px 10px #888",
WebkitBoxShadow:"3px 3px 10px #888",boxShadow:"3px 3px 10px #888"},k.menuStyle),i),x=function(){v(i,{display:"none"});g&&g.setState(0);e.openMenu=!1},t(i,"mouseleave",function(){o=setTimeout(x,500)}),t(i,"mouseenter",function(){clearTimeout(o)}),t(document,"mouseup",s),t(e,"destroy",function(){B(document,"mouseup",s)}),r(a,function(a){if(a){var b=a.separator?l("hr",null,null,w):l("div",{onmouseover:function(){v(this,k.menuItemHoverStyle)},onmouseout:function(){v(this,q)},onclick:function(){x();a.onclick.apply(e,
arguments)},innerHTML:a.text||e.options.lang[a.textKey]},p({cursor:"pointer"},q),w);e.exportDivElements.push(b)}}),e.exportDivElements.push(w,i),e.exportMenuWidth=i.offsetWidth,e.exportMenuHeight=i.offsetHeight;a={display:"block"};d+e.exportMenuWidth>m?a.right=m-d-f-u+"px":a.left=d-u+"px";c+h+e.exportMenuHeight>n&&g.alignOptions.verticalAlign!=="top"?a.bottom=n-c-u+"px":a.top=c+h-u+"px";v(i,a);e.openMenu=!0},addButton:function(b){var a=this,d=a.renderer,c=k(a.options.navigation.buttonOptions,b),j=
c.onclick,h=c.menuItems,g,e,l={stroke:c.symbolStroke,fill:c.symbolFill},q=c.symbolSize||12;if(!a.btnCount)a.btnCount=0;if(!a.exportDivElements)a.exportDivElements=[],a.exportSVGElements=[];if(c.enabled!==!1){var m=c.theme,n=m.states,o=n&&n.hover,n=n&&n.select,i;delete m.states;j?i=function(){j.apply(a,arguments)}:h&&(i=function(){a.contextMenu(e.menuClassName,h,e.translateX,e.translateY,e.width,e.height,e);e.setState(2)});c.text&&c.symbol?m.paddingLeft=f.pick(m.paddingLeft,25):c.text||p(m,{width:c.width,
height:c.height,padding:0});e=d.button(c.text,0,0,i,m,o,n).attr({title:a.options.lang[c._titleKey],"stroke-linecap":"round"});e.menuClassName=b.menuClassName||"highcharts-menu-"+a.btnCount++;c.symbol&&(g=d.symbol(c.symbol,c.symbolX-q/2,c.symbolY-q/2,q,q).attr(p(l,{"stroke-width":c.symbolStrokeWidth||1,zIndex:1})).add(e));e.add().align(p(c,{width:e.width,x:f.pick(c.x,y)}),!0,"spacingBox");y+=(e.width+c.buttonSpacing)*(c.align==="right"?-1:1);a.exportSVGElements.push(e,g)}},destroyExport:function(b){var b=
b.target,a,d;for(a=0;a<b.exportSVGElements.length;a++)if(d=b.exportSVGElements[a])d.onclick=d.ontouchstart=null,b.exportSVGElements[a]=d.destroy();for(a=0;a<b.exportDivElements.length;a++)d=b.exportDivElements[a],B(d,"mouseleave"),b.exportDivElements[a]=d.onmouseout=d.onmouseover=d.ontouchstart=d.onclick=null,o(d)}});F.menu=function(b,a,d,c){return["M",b,a+2.5,"L",b+d,a+2.5,"M",b,a+c/2+0.5,"L",b+d,a+c/2+0.5,"M",b,a+c-1.5,"L",b+d,a+c-1.5]};A.prototype.callbacks.push(function(b){var a,d=b.options.exporting,
c=d.buttons;y=0;if(d.enabled!==!1){for(a in c)b.addButton(c[a]);t(b,"destroy",b.destroyExport)}})})(Highcharts);

function js_yyyy_mm_dd_hh_mm_ss (now) {
  var year = "" + now.getFullYear();
  var month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
  var day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
  var hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
  var minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
  var second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
  return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}
/**
 * A small plugin for getting the CSV of a categorized chart
 */
(function (Highcharts) {
    
     // Options
    var itemDelimiter = ',',  // use ';' for direct import to Excel
        lineDelimiter = '\n';
        
    var each = Highcharts.each;
    Highcharts.Chart.prototype.getCSV = function () {
        var xAxis = this.xAxis[0],
            columns = [],
            line,
            csv = "", 
            row,
            col;
        
        //if (xAxis.categories) {
        //    columns.push(xAxis.categories);
        //    columns[0].unshift("");
        //}
        each (this.series, function (series) {
     
            //skip the navigator series
            if(series.name=='Navigator' || series.name == 'flags') 
          return;

            columns.push(series.xData);
            columns.push(series.yData);

            if( columns[columns.length-2][0] != 'Date Time' )
                columns[columns.length-2].unshift("Date Time");
            if( columns[columns.length-1][0] != series.name )
            {
                var cname=series.name.replace(",", "");
                columns[columns.length-1].unshift(cname);
            }
        });
        var date = null;
        // Transform the columns to CSV
        for (row = 0; row < columns[0].length; row++) {
            line = [];
            for (col = 0; col < columns.length; col++) {
                var value = columns[col][row];
                if( col % 2 == 0 && row != 0) {
                    date = moment(value).utc().zone(_tzo.dst);
                    // date is valid
                    var dateString = date.format("DD[/]MM[/]YY HH:mm")
                    line.push(dateString);
                }
                else {
                    // not a date
                        line.push(value);
                }
            }
            csv += line.join(itemDelimiter) + lineDelimiter;
        }
        columns.length = 0;
        line.length = 0;
        return csv;
    };



    Highcharts.Chart.prototype.getadvancedCSV = function () {
        var xAxis = this.xAxis[0],
            columns = [],
            line,
            csv = "", 
            row,
            col;
        var len;
        var finalColumn=[];
        console.log(this.series)
        if(this.series[0].name!='Navigator'||this.series[0].name!='flags')
        {
            var bestIdx=0;
            var lastsize=0;
            var min=0;
            var max=0;
            $.each(this.series,function(kkey,kvalue){
                console.log(kvalue.xData)
                if(min==0)
                {
                    if(kvalue.xData[0]!=' '&&kvalue.xData[0]!=null&&kvalue.xData[0]!=undefined)
                        min=kvalue.xData[0];

                }
                else
                {
                    if(min>=kvalue.xData[0])
                    {
                        min=kvalue.xData[0];
                    }
                }
                if(max==0)
                {
                    if(kvalue.xData[0]!=' '&&kvalue.xData[0]!=null&&kvalue.xData[0]!=undefined)
                        max=kvalue.xData[kvalue.xData.length-1];
                }
                else
                {
                    if(max<=kvalue.xData[kvalue.xData.length-1])
                    {
                        max=kvalue.xData[kvalue.xData.length-1];
                    }
                }
                
            });
            var chartmeta = this.getChartUrls();
            if(!chartmeta)
            {
                alert('Chart Meta Error');
                return;
            }
            var it=chartmeta.intervalmin    *   (60 * 1000);
            var testdata=[];
            end=(max-min)/it;
            testdata.push(min)
            for(i=0;i<end;i++)
            {
                newdata=min+((i+1)*it);
                testdata.push(newdata)
            }
            
            columns.push(testdata.slice());
            //columns.push(this.series[bestIdx].xData);
            len=testdata.length;
            if( columns[0][0] != 'Date Time' )
                columns[0].unshift("Date Time");
            finalColumn.push(columns[0])

        }
        
        //if (xAxis.categories) {
        //    columns.push(xAxis.categories);
        //    columns[0].unshift("");
        //}
        
        var last='';
        each (this.series, function (series) {
           //console.log(series)
            //skip the navigator series
            if(series.name=='Navigator'||series.name=='flags'||series.name=='Date Time') 
                return;
            else if(series.name.indexOf("TEMP") > -1)
            {
               columns.push(series.yData);
               
               if( columns[columns.length-1][0] != 'TEMP' )
                    columns[columns.length-1].unshift(series.name); 
               finalColumn.push(columns[columns.length-1]);
            }
            else if(series.name.indexOf("RAINFALL") > -1)
            {
               columns.push(series.yData);
               
               if( columns[columns.length-1][0] != 'RAINFALL' )
                    columns[columns.length-1].unshift(series.name); 
               finalColumn.push(columns[columns.length-1]);
            }
            else
            {
                var Td=[];
                myseries=series.xData.slice();
                mydata=series.yData.slice();
                for(var i=0;i<testdata.length;i++)
                {
                    if(testdata[i]==myseries[i])
                        Td.push(mydata[i])
                    else
                    {
                        Td.push(null)
                        myseries.splice(i,0,null);
                        mydata.splice(i,0,null);
                    }
                }
                
                console.log(Td)//series.yData
                columns.push(Td);
                
                /*if( columns[columns.length-2][0] != 'Date Time' )
                    columns[columns.length-2].unshift("Date Time");*/
               
                    //var meta = meterInfo(included[i]);
                
                if( columns[columns.length-1][0] != series.name )
                {
                    var cname=series.name.replace(",", "");
                    columns[columns.length-1].unshift(cname);

                }
            }
        });
       
       var last='';
       var fg={};
       var flag=false;
        for (row = 0; row < columns.length; row++) {
            //console.log(columns[row]);
            if(columns[row][0]!='Date Time'&&columns[row][0].indexOf("TEMP") <= -1&&columns[row][0].indexOf("RAINFALL") <= -1)
            {
                var temp=columns[row][0].split('-')
                var meta = meterInfo(temp[1]);

                if(meta==null)
                {
                    //var number='aggr_'+Math.random()
                    //fg[number]=[];
                    //fg[number].push({'data':columns[row]})
                    finalColumn.push(columns[row])
                    flag=false;
                }
                else if(meta.tag_sector=='weather')
                {
                    finalColumn.push(columns[row])
                    flag=false;
                }
                else
                {
                    flag=true;
                    number=meta.ca||meta.accountnumber||meta.description||'indivudual';
                    if(number!=last)
                    {
                       if(number in fg)
                       {
                            //do nothing
                       }
                       else
                       {
                            fg[number]=[];
                            
                       }
                       var zonedata=[];
                       var sectordata=[];
                       zone=meta.supply_zone||null;
                       sector=meta.tag_sector||null;
                       zonedata.push('zone');
                       sectordata.push('sector');
                       
                       for(i=1;i<=len;i++)
                       {
                            zonedata.push(zone);
                            sectordata.push(sector);
                       }
                       //fg[number].push({'zone':zonedata,'sector':sectordata,'data':columns[row]})
                       columns[row].push('zone '+zone);
                       columns[row].push('sector '+sector);
                       fg[number].push({'data':columns[row]})
                       last=number;
                    }
                    else
                    {
                        var zonedata=[];
                           var sectordata=[];
                           zone=meta.supply_zone||null;
                           sector=meta.tag_sector||null;
                           zonedata.push('zone');
                           sectordata.push('sector');
                           for(i=1;i<=len;i++)
                           {
                                zonedata.push(zone);
                                sectordata.push(sector);
                           }
                           columns[row].push('zone '+zone);
                           columns[row].push('sector '+sector);
                           fg[number].push({'data':columns[row]})
                        //fg[number].push({'zone':zonedata,'sector':sectordata,'data':columns[row]})
                    }
               }
                
            }
            else
            {
               // console.log(columns[row])
            }
        }
        //console.log(fg);
        //console.log(columns);
        if(flag)
        {
            $.each(fg,function(key,value){
                if(value.length>1)
                {
                    var total=[];
                    total.push('Total')
                    for(i=1;i<=len;i++)//value[0]['data'].length
                    {
                        var add=0;
                        $.each(value,function(key1,value1){
                            
                            if(!value1['data'][i])
                            {
                                value1['data'][i]='';
                            }
                            else
                            {
                                add+=value1['data'][i];
                            }
                            
                        })
                        total.push(add);
                    }
                    fg[key].push({'data':total})
                }
            })
            grandTotal=[];
            grandTotal.push('Grand Total')
        }
        for(i=1;i<=len+1;i++)
        {
            var add=0;
            $.each(fg,function(key,value){
                idx=value.length-1
                add+=value[idx]['data'][i];
               
            })
            grandTotal.push(add);
            
        }
        fg['grandTotal']=grandTotal;
        $.each(fg,function(key,value){
            if(key=='grandTotal')
                finalColumn.push(value)
            else
            {
                
                $.each(value,function(key1,value1){
                    $.each(value1,function(key2,value2){
                        finalColumn.push(value2)
                    })
                    if(value.length==1)
                    {
                        var temp=[]
                        for(i=1;i<=len+1;i++)
                        {
                            temp.push(' ')
                        }
                        finalColumn.push(temp)
                    }
                })

            }
        })
       // console.log(finalColumn)
        delete(columns)
        columns=finalColumn;
        var date = new Date();
        // Transform the columns to CSV
        for (row = 0; row < columns[0].length+2; row++) {
            line = [];
            for (col = 0; col < columns.length; col++) {
                var value = columns[col][row];
               // console.log(value)
                if( col == 0 && row != 0) { // if( col % 2 == 0 && row != 0) {
                    date.setTime(value);
                    // date is valid
                    var dateString = js_yyyy_mm_dd_hh_mm_ss(date);
                    line.push(dateString);
                }
                else {
                    // not a date
                        line.push(value);
                       
                }
            }
            csv += line.join(itemDelimiter) + lineDelimiter;
        }
        columns.length = 0;
        line.length = 0;
        return csv;
    }; 

    Highcharts.Chart.prototype.getmetaCSV = function () {   
       var xAxis = this.xAxis[0],
            columns = [],
            line,
            csv = "", 
            row,
            col,
            min = 0,
            max = 0;

         var namerow = ['Customer Name'];
         var address = ['Address'];
         var meta_list = ['device','meter_scheme','move-in','pcode','ca','zonal_cluster','premise_type','premise_no','block','unit','premise_address','inst_no','conn_obj','billing_class','acc_class','bp_name','inst_type','tag_sector','tag_subsector','rate_type','ssic','supply_zone'];
         var props = [];
         var ids = [];
        //  CREATE DATE COLUMNS
        var dates = [];
            _.each(this.series,function(ser,serid){
                if(ser.name == 'Navigator' || ser.name == 'flags')
                    return;
                for(var x=0; x<ser.xData.length; x++)
                    dates.push(ser.xData[x]);
            });
            var xDates = _.uniq(dates);
            var chartmeta = this.getChartUrls();
            if(!chartmeta)
            {
                alert('Chart Meta Error');
                return;
            }
            var date = new Date();
            for(i=0;i<xDates.length;i++)
            {
                newdata=xDates[i];
                date.setTime(newdata);
                var thisDate =  [js_yyyy_mm_dd_hh_mm_ss(date)];
                //  Filling in values
                each(this.series,   function(series){
                    if(series.name == 'Navigator' || series.name == 'flags')
                        return;
                    var dateFlag = false; // One Value for each series
                    series.xData.every(function(d,l){
                        if(d == newdata)
                        {
                            thisDate.push(parseFloat(series.yData[l]));
                            dateFlag = true;
                            return false;
                        }
                        else
                            return true;
                    });
                    if(!dateFlag)
                        thisDate.push('');
                });
                var total = 0;
                _.each(thisDate,function(e){
                    if(!isNaN(e) && typeof(e) != 'string')
                        total += e;
                });
                thisDate.push('Total: '+total);
                columns.push(thisDate);
            }
            
        each (this.series, function (series) {
            if(series.name=='Navigator' || series.name =='flags') 
          return;
        namerow.push(series.name)
        //  GET ID
        var id_explode = series.name.split('-');
        var tid = null;
        if(id_explode.length>2) //  Meta Error
            tid = id_explode[id_explode.length-1];
        else
            tid = id_explode[1];
        var thismeta = meterInfo(tid);
        if(thismeta){
        //  FILL META
        var add = null;
        if(thismeta!=null && thismeta.hasOwnProperty('address'))
            add = thismeta.address.replace(/,/g,' ').replace(/\r\n/g, " ").replace(/\n/g," ");
        address.push(add||'N.A');
        // GET PROPS
        if(thismeta)
        props.push(Object.keys(thismeta));
    }
        ids.push(tid);
  });
  //    CREATE ROWS

  // ALL META
  var flatprops = _.flatten(props,true);
  var totalmeta = _.uniq(flatprops);
  _.each(totalmeta,function(el,idx){
    if(el == 'address' || meta_list.indexOf(el) < 0)
        return;
    var thisel = [el];
    _.each(ids,function(x,y){
        var elmeta = meterInfo(x);
        if(elmeta==null || typeof(elmeta[el]) == 'object')
            thisel.push('N.A');
        else{
        var eldata = elmeta[el] || 'N.A';
        eldata = eldata.toString().replace(/,/g, ' ').replace(/\r\n/g, " ").replace(/\n/g," ");
        thisel.push(eldata||'N.A');
    }
    });
        columns.unshift(thisel);
  });
  // ADDRESS COL
  columns.unshift(address);
  // SERIES NAME
  columns.unshift(namerow);
  //    CREATE CSV
  for (row = 0; row < columns[0].length+1; row++) {
            line = [];
            for (col = 0; col < columns.length; col++) {
                var value = columns[col][row];
                        line.push(value);
             
                }
            csv += line.join(itemDelimiter) + lineDelimiter;
        }
     columns.length = 0;
        line.length = 0;
        return csv;
    };

 Highcharts.Chart.prototype.getcustomerCSV = function () {
       var xAxis = this.xAxis[0],
            columns = [],
            line,
            csv = "", 
            row,
            col,
            min = 0,
            max = 0,
            customers = [];

         var namerow = ['Customer Name'];
         var address = ['Address'];
         var meters = ['Meters'];
         var props = [];
         var ids = [];
         var the_series = this.series;
         var meta_list = ['meter_scheme','move-in','pcode','ca','zonal_cluster','premise_type','premise_no','block','unit','premise_address','inst_no','conn_obj','billing_class','acc_class','bp_name','inst_type','tag_sector','tag_subsector','rate_type','ssic','supply_zone'];
        //  CREATE DATE COLUMNS
        var dates = [];
            _.each(this.series,function(ser,serid){
                if(ser.name == 'Navigator' || ser.name == 'flags')
                    return;
                for(var x=0; x<ser.xData.length; x++)
                    dates.push(ser.xData[x]);
            });
            var xDates = _.uniq(dates);
            var chartmeta = this.getChartUrls();
            if(!chartmeta)
            {
                alert('Chart Meta Error');
                return;
            }
            var date = new Date();
        each (this.series, function (series,n) {
          if(series.name=='Navigator' || series.name =='flags'){
            customers.push('');
            ids.push(null);
            return;
        }
        //  GET ID
        var id_explode = series.name.split('-');
        if(id_explode.length>2) //  Meta Error
        {
            var newid = '';
            for(var xx=0; xx<id_explode.length-1;xx++)
                newid += id_explode[xx];
            ids.push(id_explode[id_explode.length-1]);
            customers.push(newid);
        }
        else{
            ids.push(id_explode[1]);
            customers.push(id_explode[0]);
        }
    });
    
        groupcustomers = _.uniq(customers);
        var idIndex = [];
        var serIndex = [];
        _.each(groupcustomers,function(e,i){
            if(e==''){
                return;
            }
            var idx = [];
            var this_meters = [];
            if(customers.indexOf(e)>=0){
                idIndex.push(ids[customers.indexOf(e)]);
                idx.push(customers.indexOf(e));
                this_meters.push(ids[customers.indexOf(e)]);
                customers[idx[0]] = '';
            }
            while(customers.indexOf(e)>=0){
                idx.push(customers.indexOf(e));
                this_meters.push(ids[customers.indexOf(e)]);
                customers[idx[idx.length-1]] = '';
            }
            serIndex.push(idx);
            var meterText = this_meters.toString().replace(/,/g,';');
            meters.push(meterText);
        });
        groupcustomers = _.compact(groupcustomers);
        _.each(groupcustomers,function(e,i){
            namerow.push(e);
            var thismeta = meterInfo(idIndex[i]);
            //  FILL META
            var add = null;
            if(thismeta!=null && thismeta.hasOwnProperty('address'))
                add = thismeta.address.replace(/,/g,' ').replace(/\r\n/g, " ").replace(/\n/g," ");
            address.push(add||'N.A');
            // GET PROPS
            if(thismeta)
            props.push(Object.keys(thismeta));
        });
        _.each(xDates,function(testel){
            date.setTime(testel);
            var myDate =  [js_yyyy_mm_dd_hh_mm_ss(date)+' (TOTAL)'];
            var thisDate = [];
                _.each(serIndex,function(a,b){
                    _.each(a,function(q,w){
                        var dateFlag=false;
                        the_series[q].xData.every(function(d,l){
                        if(d == testel)
                        {
                            thisDate.push(parseFloat(the_series[q].yData[l]));
                            dateFlag = true;
                            return false;
                        }
                        else
                            return true;
                    });
                    if(!dateFlag)
                        thisDate.push('');
                    });
                    var total = 0;
                    _.each(thisDate,function(e){
                        if(!isNaN(e) && typeof(e) != 'string')
                            total += e;
                    });
                    myDate.push(total);
                    thisDate.splice(0,thisDate.length);
                });
             columns.push(myDate);
            });
  //    CREATE ROWS

  // ALL META
  var flatprops = _.flatten(props,true);
  var totalmeta = _.uniq(flatprops);
  _.each(totalmeta,function(el,idx){
    if(el == 'address' || meta_list.indexOf(el) < 0)
        return;
    var thisel = [el];
    _.each(idIndex,function(x,y){
        var elmeta = meterInfo(x);
        if(elmeta==null || typeof(elmeta[el]) == 'object')
            thisel.push('N.A');
        else{
            var eldata = elmeta[el] || 'N.A';
            eldata = eldata.toString().replace(/,/g, ' ').replace(/\r\n/g, " ").replace(/\n/g," ");
        thisel.push(eldata);
        }
    });
        columns.unshift(thisel);
  });
  columns.unshift(meters);
  // ADDRESS COL
  columns.unshift(address);
  // SERIES NAME
  columns.unshift(namerow);
  //    CREATE CSV
  for (row = 0; row < columns[0].length; row++) {
            line = [];
            for (col = 0; col < columns.length; col++) {
                var value = columns[col][row];
                        line.push(value);
             
                }
            csv += line.join(itemDelimiter) + lineDelimiter;
        }
     columns.length = 0;
        line.length = 0;
        return csv;
 };

 // function to get chart data
  Highcharts.Chart.prototype.getChartUrls = function(){
    for(var i=0; i<charts.length; i++){
        if(charts[i].hasOwnProperty('container')){
        if(this.container.offsetParent === charts[i].container.offsetParent)
            return chartUrls[i];
        }
    }
    return null;
  };
  // function to get chart container
  Highcharts.Chart.prototype.getChartContainer = function(){
    for(var i=0; i<charts.length; i++){
        if(charts[i].hasOwnProperty('container')){
        if(this.container.offsetParent === charts[i].container.offsetParent)
            return this.container.offsetParent;
        }
    }
    return null;
  };
  Highcharts.Chart.prototype.getChartIndex = function(){
    for(var i=0; i<charts.length; i++){
        if(charts[i].hasOwnProperty('container')){
        if(this.container.offsetParent === charts[i].container.offsetParent)
            return i;
        }
    }
    return null;
  };

}(Highcharts));



// Now we want to add "Download CSV" to the exporting menu. We post the CSV
// to a simple PHP script that returns it with a content-type header as a 
// downloadable file.
// The source code for the PHP script can be viewed at 
// https://raw.github.com/highslide-software/highcharts.com/master/studies/csv-export/csv.php

Highcharts.getOptions().exporting.buttons.contextButton.menuItems.push(
{
    text: 'Download CSV',
    onclick: function () {
        console.log(this.getCSV())
        Highcharts.post('csv.php', {
            csv: this.getCSV() 
        });
    }
}/*,{
    text:'CSV by Meters',
    onclick:function(){
        Highcharts.post('csv.php', {
            csv: this.getmetaCSV() 
        });
        }
    },{
    text:'CSV by Customers',
    onclick:function(){
        Highcharts.post('csv.php', {
            csv: this.getcustomerCSV() 
        });
    }   }*/);