var color_arr	=	['#FF0000','#CC3300','#009933','#3333CC'];
var	color_index	=	1;
var	polygons	=	[];
function	loadRegions(){
if(!map)
	console.log('Map not available');
//	bksr
	$.getJSON('data/bksr.json',{},function(e){
		processRegion(e);
	});
//	phfc
	$.getJSON('data/phfc.json',{},function(d){
		processRegion(d);
	});
//	qhpz
	$.getJSON('data/qhpz.json',{},function(f){
		processRegion(f);
	});
//	mnsr
	$.getJSON('data/mnsr.json',{},function(g){
		processRegion(g);
	});
}

function	region(x,y)
	{
		switch(x)
			{
				case	'phfc':
					if(y){
					/*$.getJSON('data/phfc.json',{},function(d){
						processRegion(d,'fcph');
					});
					}
					else
						unloadPoly('fcph');*/
					var ctaLayer = new google.maps.KmlLayer({
					    url: 'https://54.169.13.118/meterintel/apps/meterIntel/data/phfc.kml',
					    map:map
					});
					console.log(ctaLayer);
					  //ctaLayer.setMap(map);
					  var	temp	=	{};
						temp.zone	=	'fcph';
						temp.poly	=	ctaLayer;
						polygons.push(temp);
						++color_index;
					}
					else
						unloadPoly('fcph');
				break;
				default:
					if(y){
					/*$.getJSON('data/'+x+'.json',{},function(d){
						processRegion(d,x);
					});
					}*/
					//find the zone
					var kmlPath = '';
					// for(var ii=0;ii<zonesarr.length;ii++)
					// {
					// 	if(zonesarr[ii].meta.name === x && zonesarr[ii].meta.hasOwnProperty('kml_path'))
					// 		kmlPath = zonesarr[ii].meta.kml_path||'';
					// }
					 // $.each(DMAList,function(key,value){
      //                       if(value.display_name==x)
      //                               kmlPath=value.kml_path;
      //               }) 
				
					var thedma = dma.meta(x);
					if(thedma	 === null){
						Notify('Error','DMA not found in memory','normal',true);
						return;
					}
					kmlPath = thedma.kml_path;
					if(!kmlPath || typeof(kmlPath)=='undefined')
						return;
					var ctaLayer = new google.maps.KmlLayer({
					    url: 'https://54.169.13.118/meterintel/apps/meterIntel/data/'+kmlPath,
					    map:map,
					    suppressInfoWindows: true,
					    _name:x
					});
					google.maps.event.addListener(ctaLayer, 'click', function(event) {
						Notify('Zones',ctaLayer._name,'normal',true);
					});
					  //ctaLayer.setMap(map);
					  console.log(ctaLayer);
					  var	temp	=	{};
						temp.zone	=	x;
						temp.poly	=	ctaLayer;
						polygons.push(ctaLayer);
						++color_index;
					}
					else
						unloadPoly(x);
				break;
			}					
	}

function	processRegion(r,x)	
	{
		var	coords	=	[];
		for(var	i=0;	i<r.length;	i++)
			{
				coords.push(new google.maps.LatLng(r[i].YCoord,r[i].XCoord));
			}
		poly = new google.maps.Polygon({
    paths: coords,
    strokeColor: color_arr[color_index],
    strokeOpacity: 1,
    strokeWeight: 3,
    fillColor: color_arr[color_index],
    fillOpacity: 0.1
  });

  	poly.setMap(map);
	map.setCenter(poly.my_getBounds().getCenter());
	//map.setZoom(13);
	var	temp	=	{};
	temp.zone	=	x;
	temp.poly	=	poly;
	polygons.push(temp);
	++color_index;
	}
	
function	unloadPoly(p){
	for(var	i=0;	i<polygons.length;	i++)
		{
			if(polygons[i]._name	==	p){
				polygons[i].setMap(null);
				polygons.splice(i,1);
				--color_index;
				return;
			}
		}
}
