/*  Simulation (Pipe/Valve/Network/MISC)
                                            */

( function( window, undefined ) {
    "use strict";           // Do it the right way :)
    function Simulator(){  

      var privates = {
        _state:0,           // 0 = Off, 1 = Preparing, 2 = Waiting for Server, 3 = Complete
        _type:null,         // pipe or valve
        _ui:null,           // div id
        elements:[],
        zone:null,
        valves:[],
        colors:['#3FEB28','#D31EEB','#2C2933','#FF7500','#F2115C','#11F2E3'],
        nodecolors:[15,3,22,19,21],
        giselements:[],
        clear:function(ignores){
          ignores = ignores || [];
          ignores.push('colors');
          ignores.push('nodecolors');
          resetObject(privates,ignores);
          privates._state = 0;        }
      };
      this.shout = function(){  console.log(privates);  }

      // simulation button for gis
      this.simButton = function(id,zone,type){
        if(map_context == null || map_context == 'valvesimulation'){
          if(type === 'junction')
            return '<br/><a onclick="sim.addSupply(\''+id+'\',\''+zone+'\',\''+type+'\')">Add Supply</a><br/><a onclick="sim.addTank(\''+id+'\',\''+zone+'\',\''+type+'\')">Add Tank</a><br/><a onclick="sim.addDemand(\''+id+'\',\''+zone+'\',\''+type+'\')">Flush</a>';
          else
            return '<br/><a onclick="sim.initSim(\''+id+'\',\''+zone+'\',\''+type+'\')">Queue for Simulation</a>';
        }
        else
          return '';      
      }

      // initiate
      this.initSim = function(id,zone,type){
          if(privates._state >= 1){  // Redirect to adder module
            this.addSim(id,zone,type);
            return;
          }
          privates.zone = zone;
          privates._type = type;
          privates._state = 1;
          // generate initial ui
          privates._ui = logWindow('generic',null,{name:'Sim'});
          //  exit route
          generiClosure = function(){
            sim.killSim();
            generiClosure = null;
          }
          try{
            buildui.call(this,[id],[zone],type);
          }
          catch(e){
            Notify('Error',e,'normal',true);
            return;
          }
          simEvents.call(this);
          map_context = 'valvesimulation';
          showAction();
      }
      // add elements to simulation
      this.addSim = function(id,zone,type){
          if(privates._state === 0)
          {
            Notify('Warning','Simulation not initiated yet','normal',true);
            return;
          }
          // if(privates._state >= 2)
          // {
          //   Notify('Warning','Simulation in progress, clear queue to add more','normal',true);
          //   return;
          // }
          // if(privates._type != null && type != privates._type)
          // {
          //   Notify('Warning','Simulation is initiated for '+privates._type+' only!','normal',true);
          //   return; 
          // }
          if(privates.zone === null)
            privates.zone = zone;
          if(zone != privates.zone)
          {
            Notify('Warning','Cannot add pipes/valves from multiple zones','normal',true);
            return;
          }
          addElements.call(this,[id],[zone],type);
          console.log($('#'+privates.ui+' .thiswrap .scenario .valvetimeselection:last-child .endtime'));
          // if(type == 'valve'){
          //   $('#'+privates.ui+' .thiswrap .scenario .valvetimeselection:last-child .starttime').timepicker({
          //     showPeriodLabels: false,
          //     minutes: {
          //       interval: 5
          //     }
          //   });
          //   $('#'+privates.ui+' .thiswrap .scenario .valvetimeselection:last-child .endtime').timepicker({
          //     showPeriodLabels: false,
          //     minutes: {
          //       interval: 5
          //     }
          //   });
          // }
      }

      this.addSupply = function(id,zone,type){
        if(privates._state === 0)
        {
          Notify('Warning','Start simulation first','normal',true);
          return;
        }
        if(privates.zone != zone){
          Notify('Warning','Cannot add supply from a different zone','normal',true);
          return;
        }
        $('.scenario').append('<ul class="inline-list lirow"><li><b>Supply</b></li><li>'+id+'</li><li><input data-id="'+id+'" class="typesupply" type="text"/></li><li><div class="crossbox"></div></li></ul>');
      }

      this.addTank = function(id,zone,type){
        if(privates._state === 0)
        {
          Notify('Warning','Start simulation first','normal',true);
          return;
        }
        if(privates.zone != zone){
          Notify('Warning','Cannot add supply from a different zone','normal',true);
          return;
        }
        $('.scenario').append('<ul class="inline-list lirow"><li><b>Tank</b></li><li>'+id+'</li><li><input data-id="t_'+id+'" class="typetank" type="text"/></li><li><div class="crossbox"></div></li></ul>');
      }

      this.addDemand = function(id,zone,type){
        if(privates._state === 0)
        {
          Notify('Warning','Start simulation first','normal',true);
          return;
        }
        if(privates.zone != zone){
          Notify('Warning','Cannot add demand from a different zone','normal',true);
          return;
        }
        $('.scenario').append('<ul class="inline-list lirow"><li><b>Flush</b></li><li>'+id+'</li><li><input data-id="'+id+'" class="typeflush" type="text"/></li><li><div class="crossbox"></div></li></ul>');
      }

      this.killSim = function(){
          gis.clearElements(privates.token);
          gis.clearElements('default');
          privates.clear();
          map_context = null;
          showAction();
      }

      this.start = function(){  
          if(privates.elements.length === 0){
            Notify('Information','You have no elements in queue','normal',true);
            return;
          }
          
          process.call(this,0);
      }

      this.plot = function(id,impact,period){
        process.call(this,5,{elementid:id,impact:impact,period:period});
      }
/*
    Private methods
*/
    var buildui = function(el,zone,type){
        if(privates._ui == null || privates._ui === undefined)
          throw 'Bad UI pointer';
        var div = $('#'+privates._ui+' .thiswrap');
        if(div.length === 0)
          throw 'Cannot find UI element';
        var btn1 = 'hide';
        var btn2 = '';
        if(type === 'valve'){
          btn2 = 'hide';
          btn1 = '';
        }
        var html = '<div class="columns medium-12 thescene"><!--<input placeholder="Start Time" style="font-size:9px;margin-top:10px;margin-bottom:0px;width:45%;display:inline-block;margin-left: 5px;height: 20px;background-color: #353535;border: #000;color: #fff;" type="text" id="simst"/><input type="text" id="simet" placeholder="End Time" style="font-size:9px;margin-bottom:0px;margin-top:10px;width:45%;display:inline-block;margin-left: 5px;height: 20px;background-color: #353535;border: #000;color: #fff;"/>--><ul class="scenario"></ul>\
        <div class="medium-12"><div class="button tiny btnfetch hide">Re-Fetch</div><div class="button tiny btnstrt '+btn2+'">Resolve</div><div class="btnsimstart tiny button '+btn1+'">Start</div><div class="button tiny btnclr">Clear</div></div>\
        <div class="loader medium-12 hide"><img src="images/ajax-loader-black.gif"><small></small></div><div class="impact"></div>';
        $(div).append(html);
        addElements.call(this,el,zone,type);

    }
    var addElements = function(el,z,t){
      var div = $('#'+privates._ui+' .thiswrap .thescene ul.scenario');
      var html = '';
      var cbox = '';
      if(t === 'valve')
        {
          var alreadyadded = false;
          _.each(privates.valves,function(v){
            if(v.valveId === el[0])
              alreadyadded = true;
          });
          if(alreadyadded === true)
            return;
          var uniqid = 'btn'+getRandomInt(1000,100000);
          privates.valves.push({valveId:el[0],state:'Closed',include:true,st:null,et:null,uid:uniqid});
          $('#'+privates._ui+' .thiswrap .scenario').append('<ul class="inline-list lirow served" data-id="'+el[0]+'"><li>'+el[0]+'</li><li>V(Closed)</li><li><div class="crossbox">x</div></ul><div class="valvetimeselection" data-id="'+el[0]+'"><input class="starttime" id="start'+uniqid+'" placeholder="Start"/><input id="end'+uniqid+'" class="endtime" placeholder="End"/></div>');
          $('#start'+uniqid).timepicker({
            showPeriodLabels: false,
            minutes: {
              interval: 5
            }
          });
          $('#end'+uniqid).timepicker({
            showPeriodLabels: false,
            minutes: {
              interval: 5
            }
          });
          return;
        }
      else
      {
        // make sure resolve button is open
        $('#'+privates._ui+' .thiswrap .btnstrt').removeClass('hide');
      }
      _.each(el,function(e,idx){
          if(privates.elements.indexOf(e)<0){
            html += '<ul class="inline-list lirow">'
            html += '<li>'+e+'</li><li>'+t+'</li><li>'+z[idx]+'</li><li class="hide"><div class="crossbox">x</div></ul>';
            privates.elements.push(e);
          }
      });
      $(div).append(html);
    }

    /*  all events related to simulation go here
                                                    */
    var simEvents = function(self){
      self = this;
      $('#'+privates._ui).on('click','.served .crossbox',function(){
        var prnt = $(this).parents('.lirow');
        var _state = $(prnt).hasClass('removed');
        $(prnt).toggleClass('removed');
        if(!_state){
          var id = $(prnt).data('id');
          for(var i=0;i<privates.valves.length;i++){
            if(privates.valves[i].valveId === id)
              privates.valves[i].include = false;
          }
        }
      });
      $('#'+privates._ui).on('click','.btnclr',function(){
        //var temp = privates._state;
        gis.clearElements(privates.token);
        gis.clearElements('default');
        privates.clear(['_ui','_type']);
        privates._state = 1;
        console.log($('#'+privates._ui+' .scenario'));
        $('#'+privates._ui+' .scenario').empty();
        $('#'+privates._ui+' .impact').empty();
        //$('#'+privates._ui+' .btnstrt').removeClass('hide');
        $('#'+privates._ui+' .btnsimstart').removeClass('hide');
      });
      $('#'+privates._ui).on('click','.btnstrt',function(){
        self.start();
      });
      $('#'+privates._ui).on('click','.btnfetch',function(){
        process.call(this,1);
      });
      $('#'+privates._ui).on('click','.btnsimstart',function(){
        if(privates.token){
          gis.clearElements(privates.token);
        }
        process.call(this,1);
      });
      $('#'+privates._ui).on('click','.impact ul',function(){
        var tgl = $(this).hasClass('selected');
        $(this).toggleClass('selected');
        var el = $(this).find('.loaderli');
        var imp = $(this).data('id');
        var period = $(this).parent('.timeline_div').data('id');
        console.log(period);
        if(tgl){
          $(el).find('img').addClass('hide');
          $(el).find('span').remove();
          gis.clearSubElement(privates.token,imp.replace(' ','')+period.replace('-',''));
        }
        else{
          $(el).find('img').removeClass('hide');
           var clr = $(this).data('color');
            process.call(this,4,{
            period:period,
            impact:imp,
            clr:clr,
            el:el
          });
        }
      });
    }

    /*  Push list of affected valves into list 
                                                      */
    var pushValves = function(res){
      var valves = [];
      var idmap = [];
      valvetime.call(this);
      for(var key in res){
        var alreadyavailable = false;
        _.each(res[key],function(v){
          _.each(privates.valves,function(vv){
            if(vv.valveId === v[0])
              alreadyavailable = true;
          });
          if(alreadyavailable)
            return;
          // valves.push({valveId:v[0],state:v[1],include:true})
          valves.push({valveId:v[0],state:'Closed',include:true,st:null,et:null,uid:null})
          idmap.push(privates.zone+':'+v[0]+':'+v[1]);
        });
      }
      privates.valves = privates.valves.concat(valves);
      privates.idmap = idmap;
      var div = $('#'+privates._ui+' .thiswrap .scenario');
      html = '';
      var ids = [];
      _.each(valves,function(v){
          v.uid = 'btn'+getRandomInt(1000,100000);
          ids.push(v.uid);
          html += '<ul class="inline-list lirow served" data-id="'+v.valveId+'"><li>'+v.valveId+'</li><li>valve ('+v.state+')</li><li><div class="crossbox">x</div></ul><div class="valvetimeselection" data-id="'+v.valveId+'"><input class="starttime" id="start'+v.uid+'" placeholder="Start"/><input id="end'+v.uid+'" class="endtime" placeholder="End"/></div>';
      });
      $(div).append(html);
      _.each(ids,function(uniqid){
        $('#start'+uniqid).timepicker({
            showPeriodLabels: false,
            minutes: {
            interval: 5
          }
        });
        $('#end'+uniqid).timepicker({
            showPeriodLabels: false,
            minutes: {
            interval: 5
          }
        });
      });
    }

    var generateidmap = function(){
      var idmap = [];
      valvetime.call(this);
      _.each(privates.valves,function(v){
        if(v.include === false)
          return;
        var simst = v.st.split(':');
        var simet = v.et.split(':');
        if(simst.length<2 || simet.length<2)
          Notify('Simulatior','Time Error','normal',true);
        var st = new Date(); var et = new Date;
        st.setHours(parseInt(simst[0])); et.setHours(parseInt(simet[0]));
        st.setMinutes(parseInt(simst[1])); et.setMinutes(parseInt(simet[1])); 
        idmap.push(privates.zone+':'+v.valveId+':'+v.state+':'+'0'+':'+st.getTime()+':'+et.getTime());
      });
      var flushes = $('.typeflush');
      var supplies = $('.typesupply');
      var tanks = $('.typetank');
      _.each(flushes,function(f){
        var id = $(f).data('id');
        var val = $(f).val();
        idmap.push(privates.zone+':'+id+':adddemand:0:1436324568000:1436929368000:'+val); // dummy timestamps - api legacy :)
      });
      _.each(supplies,function(f){
        var id = $(f).data('id');
        var val = $(f).val();
        idmap.push(privates.zone+':'+id+':addsupply:0:1436324568000:1436929368000:-'+val);
      });
      _.each(tanks,function(t){
        var id = $(t).data('id');
        var val = $(t).val();
        idmap.push(privates.zone+':'+id+':addtank:0:1436324568000:1436929368000:'+val);
      });
      privates.idmap = idmap;
    } 

    var valvetime = function(){
      _.each(privates.valves,function(v){
        if(v.include === false)
          return;
        v.st = $('#start'+v.uid).val();
        v.et = $('#end'+v.uid).val();
      });
    }

    /*  Call server and process response
                                                      */

    var process = function(type,d){
      d = d || null;
      switch(type){

        case 0:   //show participants
          loader.call(this,true,'Resolving Pipes to Valves');
            $.get('controllers/tlp.php',{endpoint:'simulationapi/find_valves',params:JSON.stringify({zoneid:privates.zone,pipes:privates.elements}),apptype:'json'},
            function(d){
              loader.call(this,false);
              if(!d){
                Notify('Error','Server Error - Valve Resolution','normal',true);
                this.killSim();
                return; 
              }
              $('#'+privates._ui+' .thiswrap .btnstrt').addClass('hide');
              $('#'+privates._ui+' .thiswrap .btnsimstart').removeClass('hide');
              privates.elements.splice(0,privates.elements.length);
              pushValves.call(this,d);
              // reset bounds
              bounds = new google.maps.LatLngBounds();
              for(var key in d){
                _.each(d[key],function(v){
                  bounds.extend(new google.maps.LatLng(v[2],v[3]));
                  gis.newElement({
                    type:'valve',
                    id:v[0],
                    desc:'Closed',
                    lat:v[2],
                    lng:v[3],
                    msg:'<h5>Isolated Valve for Simulation</h5><i>ID: '+v[0],
                    token:privates.token||'default',
                    subtoken:'isolatedvalves'
                  });
                });
              }
              map.fitBounds(bounds);
              // privates._state = 2;
              // loader.call(this,1,'Fetching simuation, please wait.');
              // process.call(this,2);
            },'json');
        break;

        case 1:   // Start Simulation
              privates._state = 2;
              loader.call(this,1,'Fetching simuation, please wait.');
              process.call(this,2);
        break;

        case 2: //request simulation
          generateidmap.call(this);
          // var simst = $('#simst').val().split(':');
          // var simet = $('#simet').val().split(':');
          // if(simst.length === 1 || simet.length === 1){
          //   Notify('Warning','Invalid Time Seleciton','normal',true);
          //   loader.call(this,false);
          //   return;
          // }
          var supply = $(".typesupply");
          var demand = $(".typeflush");
          if(supply != null) {
            for(var index = 0 ;index < supply.length; index++) {
              if(supply.val() == "" || supply.val() == "0" || isNaN(supply.val())) {
                      Notify('Warning','Supply must be a number','normal',true);
                      loader.call(this,false);
                      return;
              }
            }
          }
          if(demand != null) {
            for(var index = 0 ;index < demand.length; index++) {
              if(demand.val() == "" || demand.val() == "0" || isNaN(demand.val())) {
                      Notify('Warning','Flush must be a number','normal',true);
                      loader.call(this,false);
                      return;
              }
            }
          }
          // var st = new Date();
          // st.setHours(parseInt(simst[0]));
          // st.setMinutes(parseInt(simst[1]));
          // var et = new Date();
          // et.setHours(parseInt(simet[0]));
          // et.setMinutes(parseInt(simet[1]));
          $.get('controllers/tlp.php',{endpoint:'simulationapi/start',params:'st=1436719074535&et=1436762263106&idmap='+privates.idmap.toString()},
                function(e){
                  if(e.errorCode>0){
                    Notify('Error',e.errorMsg,'normal',true);
                    loader.call(this,false);
                    return;
                  }
                  console.log(e);
                  privates.token = e.response;
                  $('#'+privates._ui+' .thiswrap .btnsimstart').removeClass('hide');
                  loader.call(this,'Simulation Ready, fetching impact..');
                  process.call(this,3);
                },'json');
        break;

        case 3:// get results
            $.get('controllers/tlp.php',{endpoint:'simulationapi/summary',params:'zoneid='+privates.zone+'&simulationtoken='+privates.token},
              function(e){
                if(e.errorCode>0){
                  Notify('Error',e.errorMsg,'normal',true);
                  loader.call(this,false);
                  return;
                }
                loader.call(this,false);
                console.log(e);
                if(typeof(e.response) === 'string')
                  e.response = JSON.parse(e.response);
                $('#'+privates._ui+' .thiswrap .btnsimstart').removeClass('hide');
                var html = '<h5 style="color:#FF7500 !important">Results</h5>';
                var count = 0;
                for(var key in e.response){
                  html += '<div class="timeline_div" data-id="'+key+'"><span class="timeline_title">&rarr; '+key+'</span>';
                  for(var key2 in e.response[key]){
                    var clr = privates.colors[count];
                    var hex = clr;
                    if(clr[0] != '#'){
                      var clr_explode = clr.substr(4,clr.length-5).split(',');
                      hex = rgbToHex.call(self,clr_explode[0],clr_explode[1],clr_explode[2]);
                    }
                    var imp = e.response[key][key2];
                    html += '<ul class="inline-list medium-10" data-id="'+imp.impactType+'" data-color="'+hex+'"><li><div class="crossbox" style="background:'+clr+'"></div></li><li>'+imp.impactType+'</li><li>JNCT:'+imp.jcount+'</li><li>P:'+imp.pcount+'</li><li class="loaderli"><img class="hide" width="15px" style="margin-top:2px" src="images/ajax-loader-black.gif"></li></ul>';  
                    ++count;
                  }
                  html += '</div>';
                  count = 0;
                }

                // _.each(e.response.summary,function(s,idx){
                //   var clr = Highcharts.getOptions().colors[idx];
                //   html += '<ul class="inline-list medium-10" data-id="'+s.impactType+'"><li><div class="crossbox" style="background:'+clr+'"></div></li><li>'+s.display+'</li><li>'+s.count+'</li><li class="loaderli"><img class="hide" width="15px" style="margin-top:2px" src="images/ajax-loader-black.gif"></li></ul>';
                // });
                $('#'+privates._ui+' .impact').empty().append(html);
              },'json');
        break;

        case 4:   // get individual impact detail
            $.get('controllers/tlp.php',{endpoint:'simulationapi/allimpactresult',params:'simulationtoken='+privates.token+'&impacttype='+d.impact+'&period='+d.period},
              function(e){
                $(d.el).append('<span><i>Impact available on map</i></span>');
                $(d.el).find('img').addClass('hide');
                if(e.errorCode>0){
                  Notify(e.errorMsg);
                  return;
                }
                var response = e.response;
                bounds = new google.maps.LatLngBounds();
                if(typeof(response) === 'string')
                  response = JSON.parse(response);
                console.log(response);
                _.each(response,function(r){
                  _.each(r.location,function(loc){
                    bounds.extend(new google.maps.LatLng(loc.latitude,loc.longitude));
                  });
                  var options = {
                    id:r.id,
                    loc:r.location,
                    type:r.type,
                    desc:'',
                    msg:'<h5>'+r.type+'&nbsp;'+r.id+'</h5><a onclick="sim.plot(\''+r.id+'\',\''+d.impact+'\',\''+d.period+'\')">Show Impact</a>',
                    token:privates.token,
                    subtoken:d.impact.replace(' ','')+d.period.replace('-',''),
                    clr:d.clr
                  };
                  if(r.type === 'pipe')
                    options.dim = r.diameter;
                  if(r.type === 'valve'){
                    options.desc = 'open';
                  }
                  if(r.type === 'junction')
                    options.icon = privates.nodecolors[privates.colors.indexOf(d.clr)];
                  gis.newElement(options);
                });
                map.fitBounds(bounds);
              },'json');
        break;

        case 5:
          //impactresult -- add elementid:v
          var cid = logWindow('plot',{});
           $.get('controllers/tlp.php',{endpoint:'simulationapi/impactresult',params:'simulationtoken='+privates.token+'&impacttype='+d.impact+'&elementid='+d.elementid+'&period='+d.period},
            function(e){
              $('#'+cid+ 'img').remove();
              if(e.errorCode>0){
                Notify('Error',e.errorMsg,'normal',true);
                return;
              }
              var response = e.response;
              if(typeof(response) === 'string')
                response = JSON.parse(response);
              // plot the shit
              response.before = sort_object.call(this,response.before);
              response.after = sort_object.call(this,response.after);
              var payloadA = [],payloadB = [];
              for(var ts in response.before){
                payloadA.push([parseInt(ts),response.before[ts]]);
              }
              for(var ts in response.after){
                payloadB.push([parseInt(ts),response.after[ts]]);
              }
              var chart = new Highcharts.Chart({
                chart:{
                  renderTo:cid,
                  type:'column',
                  animation:false
                },
                navigator:{
                  enabled:false
                },
                rangeSelector:{
                  enabled:false
                },
                plotOptions: {
                  series: {
                    marker: {
                        enabled: false
                   }
                 }
                },
                xAxis:  { type:'datetime',minPadding:0.02,maxPadding:0.02},
                yAxis:  [{opposite:false,title:{text:'?'}},{opposite:true,title:{text:''}}],
                title:{
                  text:d.impact+' Simulation Response for '+d.elementid
                },
                series:[{
                  data:payloadA,name:'Before'
                },
                {
                  data:payloadB,
                  name:'After'
                }]
              });

            },'json');
        break;

        default: throw "case not recognized - Process Server Request - Simulator";
      }
    }
    var loader = function(x,txt){
      txt = txt || null;
      if(x){
        $('#'+privates._ui+' .loader').removeClass('hide');
        if(txt)
          $('#'+privates._ui+' .loader small').empty().append(txt);
      }
      else{
        $('#'+privates._ui+' .loader').addClass('hide');
        $('#'+privates._ui+' .loader small').empty();
      }
    }

    var sort_object = function(map) {
      var keys = _.sortBy(_.keys(map), function(a) { return a; });
      var newmap = {};
      _.each(keys, function(k) {
          newmap[k] = map[k];
      });
      return newmap;
    }

    var componentToHex = function(c) {
      var hex = c.toString(16);
      return hex.length == 1 ? "0" + hex : hex;
    }

    var rgbToHex = function(r, g, b) {
      return "#" + componentToHex.call(this,r) + componentToHex.call(this,g) + componentToHex.call(this,b);
    }

/*  End of Declaration  */

    }

  window.Simulator = Simulator;
  } )( window );

  // initiate simulator module
  var sim = new Simulator();