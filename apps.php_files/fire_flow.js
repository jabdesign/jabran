( function( window, undefined ) {

  function  fireFlow(ParentObject){

    // local variables/settings for module
    var myprivates = {
        Parentconfig:{}
    };
    myprivates.Parentconfig=ParentObject;
    myprivates.parsedURL=parseURL(myprivates.Parentconfig.waterwise)
    // myprivates.Objextend(ParentObject);
    /* Public Functions accessible to outside scripts */
    this.shout = function(){  console.log(myprivates);  }

    this.fireButton = function(id,type,zone){
       return '<br /><a onclick="fireflow.LoadApp(\''+id+'\',\''+type+'\',\''+zone+'\')">Fire Flow</a>';
             
    }
    this.LoadApp = function(jID,type,zone){
      // to call another public function
      // this.myroutine();
      //to call another private function
      // myroutine3.call(this);
      myprivates.junctionID=jID;
      myprivates.itemtype=type;
      myprivates.zone=zone;
      buildui.call(this);
      this.shout();
    }

    /* Private funcitons */

    var buildui = function(){
     // events.call(self);
      var self = this;
      myprivates.winid = '#'+logWindow('mindash');
      activeapp = myprivates.winid;
      var html="<style>";
      
      html+="</style>";
      html+='<div class="Editclose closeBTN">X</div><ul class="tabs"><li class="tab-link current" data-tab="tab-3" id="fireflow">Fire Flow</li></ul>';
      html+='<div class="row"><div class="large-6 columns">';
      html+='<div class="row"> <div class="large-12 columns"> <label>Minimum Pressure<input type="text" placeholder="" id="minPressure"/> </label> </div> </div> <div class="row"> <div class="large-12 columns"> <label>Time<input type="text" placeholder="In hours (e.g: 5)"  id="hrtime"/> </label> </div> </div> <div class="row"> <div class="large-12 columns"> <label>Flow Rate<input type="text" placeholder=""  id="flowrate"/> </label> </div> </div> <div class="row"> <div class="large-12 columns"> <div class="button tiny" id="Calculate">Calculate</div> </div> </div> '
      html+='</div><div class="large-6 columns" id="Response"></div></div>';
      $(myprivates.winid).append(html);
      events.call(self);
      


    }

    var events=function(){
      var self = this;
    	$(myprivates.winid+' .closeBTN').click(function(){
          $(myprivates.winid).remove();
          removeTrayObject()
      });
      $(myprivates.winid+' #Calculate').click(function(){
          valid=self.validate();
          if(valid)
          {
          	//myprivates.parsedURL.protocol+'
            var Queryurl='http://'+myprivates.parsedURL.domain+'/simulationapi/fireflow/getMaximumFlow?zone='+myprivates.zone+'&junctionId='+myprivates.junctionID+'&minPressure='+myprivates.minPressure
            if(myprivates.hrtime==0)
            {
              Queryurl+='&requiredFlow='+myprivates.flowrate
            }
            else
            {
              Queryurl+='&requiredTime='+myprivates.hrtime;
            }
            console.log(Queryurl)

            var url='controllers/curlProxy.php?tag=getData';
            process(url, 'POST',{ url : Queryurl}, 
               function(resp) { 
                     if(resp.errorCode=='0')
                     {
                        $('#Response').empty();
                        html='<table style="margin: 100px auto;"> <thead><tr>';
                          $.each(Object.keys(resp.response[0]),function(key,value){
                              switch(value)
                              {
                                case 'junctionId':
                                    title='Junction ID';
                                      break;
                                case 'minPressure':
                                    title='Minimum Pressure';
                                      break;
                                case 'hour':
                                    title='Time in hours';
                                      break;
                                case 'flow':
                                    title='Flow Rate';
                                      break;
                                default:
                                  title=value

                              }
                              html+='<th>'+title+'</th>';
                          })
                        html+='</tr></thead><tbody><tr>';
                          $.each(resp.response[0],function(key,value){
                              html+='<td>'+value+'</td>';
                          })
                        html+='</tr></thead><tbody>';
                        $('#Response').append(html);
                     }
                     else
                     {
                     	$('#Response').empty();
                     	$('#Response').append('<div style=" width:80%; margin: 100px auto;"> Err.. nothing to show</div>');
                     }

                },
                function(e) {
                    console.log(e);
                }
            );
          }
      });
    }


    this.validate=function(){
      myprivates.minPressure=$('#minPressure').val();
      if(!$('#minPressure').val())
      {
        Notify('Fire Flow','Please enter minimum pressure','normal',true)
        return false;
      }
      else if(!$('#hrtime').val())
      {
         myprivates.hrtime=0;
          if(!$('#flowrate').val())
          {
            Notify('Fire Flow','Please enter either Time or flow rate','normal',true)
            return false;
          }
          else
          {
            myprivates.flowrate=$('#flowrate').val();
            return true
          }
      }
      else if(!$('#flowrate').val())
      {
          myprivates.flowrate=0;
          if(!$('#hrtime').val())
          {
            Notify('Fire Flow','Please enter either time or flow rate','normal',true)
            return false;
          }
          else
          {
            myprivates.hrtime=$('#hrtime').val();
            return true
          }
      }
      else if($('#hrtime').val()&&$('#flowrate').val())
      {
      		Notify('Fire Flow','Please enter either one, time or flow rate','normal',true)
            return false;
      }
    }
  }
  window.fireFlow = fireFlow;

})(window);

var fireflow = new fireFlow(Parentconfig);
