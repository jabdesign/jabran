var	map;
var widgetData;
var searchtxtObj;
var isSearchFilter=false;
var firstload = true;
var rootSelection='sensor';
var lastSearchList=[];
var overRideobj=[];
var searchOptions={};
var MarkerInfoData;
var globalSelection=[];
var allmode=false;
var lossoMode=false;
var actionParamObJ={};
var unitTemp=[];
var buildingSelection=false;
var searchTstring;
var Radiochecked = null;
var lastSelection;
var globalSelectObj=[];
var lastknownType=null;
var opt,mapjson={};
var config = {}
var bookmark={}
var onePosition;
var SaveDSearcheData;
var confirm=false;
var minimizemode=false;
var minmizeSelection=null;
var polygon   = [];
var path = null;
var polySelect;
var p_markers = [];
var lasso_toggle=false;
var sh='all';
var User;
var Parentconfig;
var appObject;
var havePermission=[];  
var language ;
var tr;
var hol= [];
var datejson={}
var defaultSettings={"watertype":"potable","subzone":[],"sensor":[], "water":true,"demandForecast":false}
var zoneQuery='{"aggs": {"unique": {"terms": {"field": "supply_zone"}}}}';
var waterQuery='{"aggs": {"unique": {"terms": {"field": "tag_watertype"}}}}';
var options={};
var zonesInfo=[]; //['fcph','qhpz','western','bbsr','eastern','isr','mnsr','krsr','nysr','bksr','bpsr'];
var DMAList={};
var reportPermission;
var noncom={"nw":["wafer_fab","Manufacturing","Comm_Bldg"],"pw":["Mixed_Devt","Retail","Manufacturing","Hospital","Airport","Prison","Comm_Bldg","Hotel","Sch"],"all":["Mixed_Devt","Retail","Manufacturing","Hospital","Airport","Prison","Comm_Bldg","Hotel","Sch","wafer_fab"]};
var Rawprofile;
var role;
var _decouple = false;  // PLOTTER DECOUPLE OPTION
var _stacked = true;  // PLOTTER STACKING OPTION
var _tzo ={} //new Date().getTimezoneOffset()*60*1000;
var browserOffset = new Date().getTimezoneOffset();
var unitsList = [];
var screenobj = {element:null};
var _splitsensors = false;
var _splitstns = false;
var units={};
var sensortype_map={};
var sensortype_displaymap = {};
var derived_map = {};
var AppObj={};
var selectedZones = [];
var loadstate=false;
var autoLoad=false;
var flagsbit = false;
var locSearch=false;
var interval_temp = 60; 
var AMRState=false;
var searchFilter;
var dplist={"dataintelapipub":"pub"};
var parsedURL;
var globalex = true;
//var derivedList=['minnightflow','maxnightflow','totalnightvolume','rate','volume']
$(document).foundation();
//$('.addTray').tooltip();

var derivedList = ['pressure','ph','level','flow','btry'];


//window.onload = loadScript;	
//function loadScript() {
//	
//	 /*var script = document.createElement('script');
//	 script.type = 'text/javascript';
//	 script.src = 'https://maps.googleapis.com/maps/api/js?key='+Parentconfig.mapKey+'&' +
//	  	       'callback=initialize';
//
//                 
//	 document.body.appendChild(script);
//      var script = document.createElement('script');
//      script.type = 'text/javascript';
//      script.src = 'js/ploy.js';
//      document.body.appendChild(script);*/
//      
//     
//}
function HomeControl(controlDiv, map) {
  controlDiv.style.padding = '7px 0px';
  controlDiv.style.margin = '0px 0px 0px -3px';
  
  var controlUI = document.createElement('div');
  controlUI.className = 'maptopbtn';
  controlUI.title = 'Undo Search';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.fontFamily = 'Arial,sans-serif';
  controlText.style.fontSize = '11px';
  controlText.style.paddingLeft = '6px';
  controlText.style.paddingRight = '6px';
  controlText.innerHTML = 'Undo';
  controlUI.appendChild(controlText);


  // Setup the click event listeners: simply set the map to Chicago.
  google.maps.event.addDomListener(controlUI, 'click', function() {
    $('#undosearch').trigger('click')
  });
}
function HomeControl2(controlDiv, map) {

  controlDiv.style.padding = '7px 0px';
  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.className = 'maptopbtn';
  controlUI.title = 'Reset Map';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.fontFamily = 'Arial,sans-serif';
  controlText.style.fontSize = '11px';
  controlText.style.paddingLeft = '6px';
  controlText.style.paddingRight = '6px';
  controlText.innerHTML = 'Reset';
  controlUI.appendChild(controlText);
  
  // Setup the click event listeners: simply set the map to Chicago.
  google.maps.event.addDomListener(controlUI, 'click', function() {
    resetMap()
  });
}
function HomeControl3(controlDiv, map) {

  // Set CSS styles for the DIV containing the control
  // Setting padding to 5 px will offset the control
  // from the edge of the map.
  controlDiv.style.padding = '7px 0px';
  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.className = 'maptopbtn';
  controlUI.title = 'Lasso Select';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.fontFamily = 'Arial,sans-serif';
  controlText.style.fontSize = '11px';
  controlText.style.paddingLeft = '6px';
  controlText.style.paddingRight = '6px';
  controlText.innerHTML = 'Lasso';
  controlUI.appendChild(controlText);

  
  // Setup the click event listeners: simply set the map to Chicago.
  google.maps.event.addDomListener(controlUI, 'click', function(e) {
    Help('lasso',function(e){ 
      // var state=lassoSelect();
      // if(state!=false)
      {
        google.maps.event.trigger(controlUI, 'click');
      }
    });
    lassoToggle(this);
    clearAllempty()
  });
}
function initialize() {
    loadDefaultDetails();
  	google.maps.Polygon.prototype.my_getBounds=function(){
  	    var bounds = new google.maps.LatLngBounds()
  	    this.getPath().forEach(function(element,index){bounds.extend(element)})
  	    return bounds
  	}
    path = new google.maps.MVCArray;
     	var mapOptions = {
  	         center: new google.maps.LatLng(Parentconfig.lat,Parentconfig.lng),
  	         zoom: 12,
  	         panControl: false,
  	         zoomControl: false,
  	         scaleControl: false,
  	         streetViewControl: true,
             zoomControl:true,
                 zoomControlOptions:{position:google.maps.ControlPosition.LEFT_TOP,style:google.maps.ZoomControlStyle.SMALL},
                  mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_CENTER
      },mapTypeControl:true
  	       };

  	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    var homeControlDiv = document.createElement('div');
    var homeControl = new HomeControl(homeControlDiv, map);

    homeControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(homeControlDiv);

    var homeControlDiv2 = document.createElement('div');
    var homeControl2 = new HomeControl2(homeControlDiv2, map);

    homeControlDiv2.index = 1;
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(homeControlDiv2);

    var homeControlDiv3 = document.createElement('div');
    var homeControl3 = new HomeControl3(homeControlDiv3, map);

    homeControlDiv3.index = 1;
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(homeControlDiv3);
    polySelect = new google.maps.Polygon({
              strokeWeight: 3,
              fillColor: '#5555FF',
              strokeColor:'#CC3300'
            });
            polySelect.setMap(map);
            polySelect.setPaths(new google.maps.MVCArray([path]));
            google.maps.event.addListener(map, 'click', addPoint);
            google.maps.event.addListener(polySelect,'click',lassoSelect);
    google.maps.event.addListener(map, 'idle', function() {
     if(loadstate&&autoLoad)
    {
      setTimeout(function(){
        layers(true)
      },3000)
    
     }  
    });

  try{
  	initMarkers();
  }
  catch(e){
    preloader(null);
    Notify('ERROR',e,'normal',true);
  }
    //fetchZones();
}

var myDictionary = {
    fr : {
      'Hello' : 'Bonjour ',
      'Change Language': 'changer de langue',
      'logout': 'déconnexion',
      'Zone': 'Zone'
    },
    cn: {
      'Hello' : '&#24744;&#22909; ',
      'Change Language': '&#26356;&#25913;&#35821;&#35328;',
      'logout': '&#27880;&#38144;',
      'Zone'  : '&#21306;'
    },
  }

Date.prototype.stdTimezoneOffset = function() {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.dst = function() {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}

$(document).ready(function() {
  // Parentconfig=getConfigs();
  GetSession();
  console.log(new Date().dst())
  $.tr.dictionary(myDictionary);
  $("#ActionPanel .closeTray").click(function () {
          // $(".closeTray").parent().toggle("slide", { direction: "left" }, 1000);
          if($(this).parent().hasClass('actve'))
          {
            // $(this).parent().animate({"margin-left": '+=32'});
            $(this).children().removeClass('flpright');
            $(this).parent().removeClass('actve');
            $(this).siblings('.PanelCont').hide();
            $(this).parent().animate({"width": '0px'});
          }
          else
          {
              $(this).parent().animate({"width": '250px'});
              $(this).siblings('.PanelCont').show();
              $(this).children().addClass('flpright');
              $(this).parent().addClass('actve');
              // $('#pollSlider-button').animate({"margin-right": '+=200'});
          }
  });
  $('#search').on('keyup',function(e){
      locSearch=true;
  })
  
 // $('#changeLanguage').click(function(e){
 //    e.preventDefault();
 //    // var changetext='<select id="language"><option value="en">English</option><option value="fr">France</option></select>';
 //    // Notify('Search',changetext,'normal',false);
 //    $('#languageArea').show();
 //    $('#languageArea #language').change(function() {
 //      ln=$(this).val()
 //      changeLn(ln);
 //      //$('#languageArea').hide();
 //    });


 // });
 

    lang=$.cookie('language');
    if(lang)
      language = $.tr.language(lang, true);
    else
      language = $.tr.language('en', true);

    $('#language').val(language);
    changeLn(language)

    $('#commercialType .label').hide();
                  $.each(noncom.pw,function(key,value){
                      $('#commercialType #'+value).show();
                  });
});

function changeLn(ln)
{
      $.tr.language(ln);
      tr = $.tr.translator();
      $.cookie('language', ln);
      $('#hello').html(tr('Hello'));
      $('#home').html(tr('logout'));
      $('#zone').html(tr('Zone'));
      console.log(tr('Change Language'))
      $('#language').html(tr('Change Language'));
}
function GetSession()
{
             
                var sessionurl='api.php?tag=GetSession';
                
                var request = $.ajax({
                  url: sessionurl,
                  type: "GET",
                  dataType: "JSON"
                });
                 
                request.done(function( data ) {
                  if(getCookie('dIntelSession')=='failed')
                  {
                    data={};
                    data.response={}
                    data.response.UserEmail=getCookie('dIntelUser');
                    data.response.UserID=null;
                    data.response.errorCode=0;
                    console.log(getCookie('dIntelcustomer'))
                    data.response.customer=JSON.parse(getCookie('dIntelcustomer'));
                    data.response.role=getCookie('dIntelrole');
                    SessionObject=data;
                    User=data;
                  }
                  else
                  {
                    SessionObject=data;
                    User=data;
                    role=getCookie("dIntelrole");
                    user=getCookie("dIntelUser");
                    if(user==''||!user)
                    {
                      window.location.href= 'index.php'+'?er=Session expired';
                      eraseCookie('dIntelrole');
                      eraseCookie('dIntelUser');
                    }
                    
                  }
                  
                  var sessionState=getCookie("dIntelSession");
                  if(SessionObject.response.errorCode==0||sessionState=='failed')
                  {
			customers=JSON.parse(getCookie('dIntelcustomer'))
//                     Parentconfig=getConfigs();
//                     showPanel()
//                     var selct= getCookie("timezone");
//                     if(selct==''||selct==undefined)
//                     {
//                       setCookie("timezone",Parentconfig.userzone,365);
//                     }
                    
//                     var selct= getCookie("timezone")
//                     _tzo.offset=moment().tz(selct)._offset;
//                     _tzo.browser=moment().zone();
//                     _tzo.dst=moment().tz(selct).format('Z z');
//                     if(sessionState=='failed')
//                     {
//                        role=getCookie("dIntelrole")
//                     }
//                     else
//                     {
//                        role=SessionObject.response.role;
//                     }
                    
// //                    $getMeterUrl=Parentconfig.apirul+'meter/information/all';
//                     _loadApps();
//                     _loadsrmnu()
//                     initialize();
          
                     gConfig(sessionState)
                  
                  }
                  else
                  {
                     window.location.href= 'index.php'+'?er='+SessionObject.response.Desc;
                      eraseCookie('dIntelrole');
                      eraseCookie('dIntelUser');
                  }
                // _getprofile();
                  
                  
                });
                request.fail(function( jqXHR, textStatus ) {
                  alert( "Request failed: " + textStatus );
                });
              
} 
function gConfig(sessionState){
   getConfigs(function(e){
                      Parentconfig=e;
                      // console.log(JSON.stringify(Parentconfig))
                      showPanel()
                      var selct= getCookie("timezone");
                      if(selct==''||selct==undefined)
                      {
                        setCookie("timezone",Parentconfig.userzone,365);
                      }
                      
                      var selct= getCookie("timezone")
                      _tzo.offset=moment().tz(selct)._offset;
                      _tzo.browser=moment().zone();
                      _tzo.dst=moment().tz(selct).format('Z z');
                      if(sessionState=='failed')
                      {
                         role=getCookie("dIntelrole")
                      }
                      else
                      {
                         role=SessionObject.response.role;
                      }
                      
  //                    $getMeterUrl=Parentconfig.apirul+'meter/information/all';
  //                    console.log(Parentconfig.stationindex)

                      _loadApps();
                      _loadsrmnu();
                      initialize();
                      
                    
                    });
}
function _loadsrmnu()
{
  $.each(Parentconfig.markerIcons,function(key,value){
    $('#'+value).show();
  })
}
function checkCookie(name)
{
    return getCookie(name) != null;
}
function eraseCookie(name) {
    setCookie(name,"",-1);
} 
function _loadApps()
{
  console.log(Parentconfig)

  var sessionurl='api.php?tag=getApps';
    var request = $.ajax({
      url: sessionurl,
      type: "GET",
      dataType: "JSON"
    });
                 
    request.done(function( data ) {
      appObject=data;

      if(appObject.response.length>0)
      {
          

          parsedURL=parseURL(Parentconfig.backend)
          var Queryurl=parsedURL.protocol+'://'+parsedURL.domain+'/'+parsedURL.path+'rolepermission/getpermission?rolename='+role;
          var url='controllers/curlProxy.php?tag=getData';
          process(url, 'POST',{ url : Queryurl}, 
              function(data) { 
                  // console.log(data) 
                  if(data.errorCode=="0")
                  {
                     Rawprofile= data.response[role][0];

                     _WaterMenufilter(Rawprofile);
                     _loadCodes();  
                  }
                  else{

                  }   
              },
              function(e) {
                  console.log(e);
              }
          );
         
      }
                
                  
    });
    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
  });
}
function _WaterMenufilter(Rawprofile)
{
       
        var temp={};
        
        console.log(Rawprofile)
        //var Rawprofile='{"id":"78ayuis98fsyiad98efi","user":"Dinesh Vadivel","usertype":"support","apps":[{"name":"Alert Management Control","alias":"alert_management"}],"scripts":[{"name":"Alert Subscription","alias":"alert_subscription"},{"name":"Bookmark","alias":"bookmark"},{"name":"Edit Meta","alias":"edit_meta"},{"name":"Plot Graph","alias":"plot_graph"},{"name":"Pipes","alias":"pipes"},{"name":"Regions","alias":"regions"},{"name":"Reporter","alias":"reporter"},{"name":"Undo Search","alias":"undo_search"},{"name":"Upload Data","alias":"upload_data"},{"name":"GIS","alias":"gis"},{"name":"Stations","alias":"stations"}]}';
        //var Rawprofile='{"id":"78ayuis98fsyiad98efi","user":"Dinesh Vadivel","usertype":"support","apps":[{"name":"Alert Management Control","alias":"alert_management"}],"scripts":[{"name":"Bookmark","alias":"bookmark"},{"name":"Edit Meta","alias":"edit_meta"},{"name":"Plot Graph","alias":"plot_graph"},{"name":"Regions","alias":"regions"},{"name":"Reporter","alias":"reporter"},{"name":"Undo Search","alias":"undo_search"},{"name":"Upload Data","alias":"upload data"}]}';
           profile=Rawprofile;
            //console.log(profile.apps)
            $.each(appObject.response,function(key,value){
              $.each(value,function(key1,value1){
                $.each(value1,function(key2,value2){
                  if(containsObject(value2,profile.apps))
                  {
                    pack=key1;
                    if(pack in temp)
                    {

                    }
                    else
                    {
                      temp[pack]=[];
                    }
                    
                    temp[pack].push(appObject.response[key][key1][key2]);
                    
                  }
                  else if(containsObject(value2,profile.scripts))
                  {
                    pack=key1;
                    if(pack in temp)
                    {

                    }
                    else
                    {
                      temp[pack]=[];
                    }
                    
                    temp[pack].push(appObject.response[key][key1][key2]);
                    
                  }
                });
              });
            })
            if('zones' in profile)
            {
              temp.zones=profile['zones']
            }
            if('sensors' in profile)
            {
              temp.sensors=profile['sensors']
            }
            havePermission.push(temp)
            return havePermission;
            
}
function containsObject(obj, list) {
       try{   var i;
          for (i = 0; i < list.length; i++) {
            
              if (list[i]['alias'] === obj['alias']) {
                  return true;
              }
          }

          return false;
      }
	catch(e){ return false; }
}
// function _loadgraphscript()
// {
//    $.getScript( "apps/graph/plotter.js" )
//       .done(function( script, textStatus ) {
//        //  setActionevents()
//       })
//       .fail(function( jqxhr, settings, exception ) {
//         console.log("Triggered ajaxError handler." );
//     });
// }
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}
function _loadCodes()
{


  $.get('data/publicholidays.json',function(d){
    _.each(d,function(el,idx){
      var st = new Date(el.st).getTime();
      var et = new Date(el.et).getTime();
      if((st-et) <= 86400000)
        hol.push(st);
      else{
        hol.push(st);
        hol.push(et);
      }
    });
    hol.sort();
  },'json');

  $.getJSON('js/units.json',function(d){
    unitsList = d;
  });

  Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    isArray = function(a) {
          return (!!a) && (a.constructor === Array);
    };
    isObject = function(a) {
          return (!!a) && (a.constructor === Object);
    };
    
    // requirejs.config({
    //     baseUrl: 'js',
    //     paths: {
    //         app: '../apps'
    //     }
    // });

    // // Start loading the main app file. Put all of
    // // your application logic in there.
    // requirejs(['app/main']);
      if('Waterwise' in havePermission[0])
      {
          $.each(havePermission[0].Waterwise,function(key,value){
              var showmenu = true;
            // JAB EDIT - Read JSON
            var jsonLoc = value.Appfile.split('/');
            $.ajax({url: 'apps/'+jsonLoc[0]+'/app.json',dataType: 'json',async: false, success: function(data) {
                if(data.hasOwnProperty('trigger'))
                  actionListObj.triggers.push(data.trigger);
                if(data.hasOwnProperty('showinmenu') && data.showinmenu===false)
                  showmenu = false;
                // if(data.hasOwnProperty('button'))
                //   $('#optiArea').append('<div class="mnitems"><div onClick="report(\'demand\');" class="mnitemHold" id=""><div class="mnuicon"><img src="images/dma.png" width="22px" title="Demand Variation"></div><div class="mnutitle">Demand Variation</div></div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div>');

              },
             error:function(){console.log('Warning: App JSON not found [ '+value.AppName+' ]');}
           });   
            if(showmenu){
                  AppObj[value.alias]="apps/"+value.Appfile;
                  var html='<div class="mnitems" style=""><div id="'+value.alias+'" class="mnitemHold iframemnuItem">';
                  html+='<div class="mnuicon"><img src="apps/'+value.icon+'" width="22px" title="Edit"> </div><div class="mnutitle">'+value.AppName+'</div>';
                  html+='</div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div></div>';
                  switch(value.location)
                  {
                     case 'Apps':
                        apLoc='optiArea';
                     break;
                     case 'Alert':
                        apLoc='AlertMenuArea';
                     break;
                  }

                  $('#'+apLoc).append(html);
                  $('#'+value.alias+' .mnutitle').autoEllipsisText();
               }
              // switch(value.alias)
              // {
              //     case 'alert_management':
              //       AppObj[value.alias]="apps/"+value.Appfile;
              //       $('#alertMng').parent().show();
              //     break;
              //     case 'edit_meta2':
              //       AppObj[value.alias]="apps/"+value.Appfile;
              //       var html='<div class="mnitems"  style=""><div id="editMeta2" class="mnitemHold" >';
              //       html+='<div class="mnuicon"><img src="images/editIcon.png" width="22px" title="Edit"> </div><div class="mnutitle">'+value.AppName+'</div>';
              //       html+='</div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div></div>';
              //       $('#optiArea').append(html);
              //       $('#editMeta2 .mnutitle').autoEllipsisText();
              //     break;
              //     case 'usermanagement':
              //       AppObj[value.alias]="apps/"+value.Appfile;
              //           var html='<div class="mnitems" style=""><div id="usermanagement" class="mnitemHold">';
              //           html+='<div class="mnuicon"><img src="images/user.png" width="22px" title="Edit"> </div><div class="mnutitle">'+value.AppName+'</div>';
              //           html+='</div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div></div>';
              //           $('#optiArea').append(html);
              //           $('#usermanagement .mnutitle').autoEllipsisText();
              //     break;
              //     case 'dataDownloader':
              //           AppObj[value.alias]="apps/"+value.Appfile;
              //           var html='<div class="mnitems" style=""><div id="dataDownloader" class="mnitemHold">';
              //           html+='<div class="mnuicon"><img src="apps/'+value.icon+'" width="22px" title="Edit"> </div><div class="mnutitle">'+value.AppName+'</div>';
              //           html+='</div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div></div>';
              //           $('#optiArea').append(html);
              //           $('#dataDownloader .mnutitle').autoEllipsisText();
              //     break;
              //     case 'sensor_quality':
              //       AppObj[value.alias]="apps/"+value.Appfile;
              //           var html='<div class="mnitems" style=""><div id="sensor_quality" class="mnitemHold">';
              //           html+='<div class="mnuicon"><img src="images/user.png" width="22px" title="Edit"> </div><div class="mnutitle">'+value.AppName+'</div>';
              //           html+='</div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div></div>';
              //           $('#optiArea').append(html);
              //           $('#sensor_quality .mnutitle').autoEllipsisText();
              //     break;
              // }
          });
        }
        if('sensors' in havePermission[0])
        {
           $('#sensorparams').empty();
           $('#sensorparams').append('<div class="heading">Sensors</div>');
           $.each(havePermission[0].sensors,function(key,value){
              if(jQuery.inArray(value,derivedList)>=0)
                $('#sensorparams').append('<div class="large-3 label secondary" id="'+value+'">'+value+'</div>')
           })
        }
        if('scripts' in havePermission[0])
        {
          $.each(havePermission[0].scripts,function(key,value){
            var jsonLoc = value.Appfile.split('/');
            $.ajax({url: 'apps/'+jsonLoc[0]+'/app.json',dataType: 'json',async: false, success: function(data) {
                if(data.hasOwnProperty('trigger'))
                  actionListObj.triggers.push(data.trigger);
              },
             error:function(){console.log('Warning: App JSON not found [ '+value.AppName+' ]');}
            });
            loadjscssfile("apps/"+value.Appfile, "js");
            response=checkloadjscssfile("apps/"+value.Appfile, "js");
            fname=value.Appfile.split('/');
            if(response.errorCode==0)
            {
                switch(value.alias)
                    {
                      case 'alert_subscription':
                          $('#subscribe').parent().show();
                        break;
                      case 'bookmark':
                          $('#BookControls').click(function(e){
                              e.preventDefault();
                              _loadbookmarkscript();
                           })
                        break;
                      case 'upload_data':
                          // _uploadData();
                          $('#uploadData').parent().show();
                        break;
                      case 'edit_meta':
                          // _editMeta();
                          $('#editMeta').parent().show();
                        break;
                      
                      case 'plot_graph':
                         setActionevents();
                         // check for sub apps (JabEdit)
                         if(value.hasOwnProperty('subApps'))
                         {
                           _.each(value.subApps,function(s){
                              if(s.hasOwnProperty('trigger')){
                                  	if(s.triggertype=='default')
                                      actionListObj.default.push(s.trigger);
                              }
                           });
                         }
                        break;
                      case 'undo_search':
                           // _undosearch();
                        break;
                      case 'gis':
                          gisTrigger()
                        break;
                      case 'reporter':		
                            reportPermission=value.subApps;
                            if(value.subApps.length==0)
                            {
                              $('#reportId').hide();
                            }
                            $('#reportArea1').empty();
                            $('#reportArea1').append('<div class="row"><div><a href="#" >Report</a></div><div class="large-12 large-centered columns noPadding borBtm" id="triggerArea">');
                            $.each(value.subApps,function(key,value){
                                $('#reportArea1').append('<a href="#" class="button small expand cbtn" id="'+value.alias+'">'+value.name+'</a>');
                            });
                            $('#reportArea1').append('</div></div>');

                        break;
                      case 'gis':
                          $('#gis').show();
                          $('#sensor').show();
                        break;
                      // case 'stations':
                      //     checkStn();
                      //   break;
                      case 'user_permission':
                          // _permission();
                          if(role=='super'||role=='admin')
                          {
                             $('#permission').parent().show();
                          }
                          break;
                      default:
                        console.log('Warning: '+value.alias+' not found');
                    }

            }

          

            });
            
          }
          if(!('Waterwise' in havePermission[0])&&!('scripts' in havePermission[0]))
          {
            $('#showalerts').parent().remove();
            $('#optionSettings').parent().remove();
            $('#BookControls').parent().remove();
          }

        // //  DMA APP - MANUAL LOADING - JAB-EDIT
          $('#optiArea').append('<div class="mnitems"><div onClick="report(\'demand\');" class="mnitemHold" id=""><div class="mnuicon"><img src="images/dma.png" width="22px" title="Demand Variation"></div><div class="mnutitle">Demand Variation</div></div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div>');
          $('#optiArea').append('<div class="mnitems"><div onClick="report(\'zonal\');" class="mnitemHold" id=""><div class="mnuicon"><img src="images/dma.png" width="22px" title="Zonal Analysis"></div><div class="mnutitle">Zonal Analysis</div></div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div>'); 
  $('#optiArea').append('<div class="mnitems"><div onClick="report(\'dailyreport\');" class="mnitemHold" id=""><div class="mnuicon"><img src="images/dma.png" width="22px" title="Daily Report"></div><div class="mnutitle">Daily Report</div></div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div>'); 
  $('#optiArea').append('<div class="mnitems"><div onClick="report(\'sector\');" class="mnitemHold" id=""><div class="mnuicon"><img src="images/dma.png" width="22px" title="Sector Report"></div><div class="mnutitle">Sector Report</div></div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div>'); 


  addAppsButton()
 _addSearchOptions();

 
 _selectAll();

 _listview();
 _othersEvents(); 
  showAction()

}

var filesadded=[] //list of files already added

function checkloadjscssfile(filename, filetype){
  // if (filesadded.indexOf(filename)==-1){
  //     loadjscssfile(filename, filetype)
  //     filesadded.push(filename);
  // }
  // else
  // {
  //       var rt={};
  //       rr
  // }
  var rt={};
  fileT=filename.split('/');
  if (filesadded.indexOf(filename)>-1){
        rt.errorCode=0;
        rt.filename=fileT[2];
  }
  else
  {
        rt.errorCode=99;
        rt.msg='not loaded';
  }
  return rt;
}
function gisTrigger()
{
 
   
 
  $('#gisItems').on('click','#autoloadGIS',function(){
    if($(this).hasClass('onstate'))
    {
      //remove on
      autoLoad=false;
      $(this).removeClass('onstate');
      $(this).children("img").attr("src","images/autoload.png");
      $(this).attr('style','');
    }
    else
    {
      autoLoad=true;
      $(this).addClass('onstate');
      $(this).children("img").attr("src","images/autoOn.png")
      $(this).attr('style','background:#ccc');
    }
  })
  $('#gisItems').on('click','#disableicons',function(){
    if($(this).hasClass('onstate'))
    {
      //remove on
      $(this).removeClass('onstate');
      // $(this).children("img").attr("src","images/autoload.png");
      $(this).attr('style','');
      $.each(markers,function(key,value){
         value.set('visible', true);
      } )
      // setAllMap(map);
      // marker.setMap(null);
    }
    else
    {
      $(this).addClass('onstate');
      // $(this).children("img").attr("src","images/autoOn.png")
      $(this).attr('style','background:#ccc');
      $.each(markers,function(key,value){
       value.set('visible', false);
      } )
      // setAllMap(null);
    }
  })
   $('#settingsArea').on('click','input[name=switch-x]:checked',function(){
          var thistype = $(this).val();
          if(thistype==1)
        autoLoad=true;
      else
        autoLoad=false;
  });
  if(!loadstate)
  {
    $('#gisItems').on('click','#reloadGIS',function(e){
      e.preventDefault();
      layers(true)
    });
  }
}
function loadjscssfile(filename, filetype){
 if (filetype=="js"){ //if filename is a external JavaScript file
  var fileref=document.createElement('script')
  fileref.setAttribute("type","text/javascript")
  fileref.setAttribute("src", filename)
  filesadded.push(filename)
 }
 else if (filetype=="css"){ //if filename is an external CSS file
  var fileref=document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", filename)
 }
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref)
}
function _othersEvents()
{
  $('.resbox #res_select').on('change',function(){
              if(isNaN(parseInt($(this).val()))){
               interval = 1440;
               _interval = $(this).val();
             }
             else{
              interval = parseFloat($(this).val());
              _interval = "";
            }
            interval_temp = interval 
        });
  $('.settingsArea .mnuinfo').on('click','img',function(){
     var id=$(this).parent().siblings().attr('id');
     Help(id);
  })
  /* ----------- DMA BUCKET DIRTY CODE (JAB-EDIT) ------------ */
  $(document).on('click','#zoneSelection .label',function(){
    if($(this).hasClass('regular'))
      selectedZones.push($(this).text());
    else
      selectedZones.splice(selectedZones.indexOf($(this).text()));
    showAction();
  });
  $(document).on('click','.opt_dma',function(){
       var el = $(this).parent().find('.labelspan');
       var id = $(el).data('id');
      var idx = $(el).data('index');
      if(!$(this).prop('checked')){
        removeEmpty();
        refreshTray();
      }
      else
        selectweather(id,idx);
  });
  // high resolution chart shift
  $(document).on('click','.shiftbtn',function(){
    var container = $(this).parents('.plotter');
    if(container.length === 0)
    {
      alert('Cannot find parent object');
      return;
    }
    var chartid = $(container).find('.plotcontainer').attr('id');
    chartobj = null;
    chartindex = null;
    _.each(highresCharts,function(h,idx){
            if(h.id == chartid)
            {
              // found object
              chartobj = h;
              chartindex = idx;
            }
            else
              return;
          });
    if(!chartobj)
    {
      alert('Cannot find chart object');
      return;
    }
    // destroy it
    $(container).find('.closeme').trigger('click');
    if($(this).hasClass('arr_left'))
      highResShift(chartindex,'left');
    if($(this).hasClass('arr_right'))
      highResShift(chartindex,'right');
  });
  //closed valves evaluation
  $(document).on('click','.closedvalvesbtn',function(){
    if(!window.hasOwnProperty('coveragearr'))
    {
      alert('Please initiate network coverage first');
      return;
    }
    var container = $(this).parents('.plotter');
    if(container.length === 0)
    {
      alert('Cannot find parent object');
      return;
    }
    var chartid = $(container).find('.plotcontainer').attr('id');
    chartobj = null;
    _.each(highresCharts,function(h){
            if(h.id == chartid)
            {
              // found object
              chartobj = h;
            }
            else
              return;
          });
    if(!chartobj)
    {
      alert('Cannot find chart object');
      return;
    }
    if(!chartobj.hasOwnProperty('timeselections'))
    {
      alert('No time selections found');
      return;
    }
    var stations = [];
    var time = [];
    for( var key in chartobj.timeselections){
      var temp = key.split('Stn_');
      var stn = temp[1].substring(0,temp[1].length-1);
      stations.push('yvw_'+stn);
      time.push(chartobj.timeselections[key]);
    }
    console.log(stations);
    console.log(time);
    //find missing station
    var missing = [];
    _.each(coveragearr.stations,function(stn){
      if(stations.indexOf(stn)<0){  // convert to backend type and push
        var temp = stn.split('_');
        missing.push('Stn_'+temp[1]);
        //push 0 for time
        time.push(0);
      }
    });
    // convert station names to backend type
    _.each(stations,function(stn,idx){
      var temp = stn.split('_');
      stations[idx] = 'Stn_'+temp[1];
    });
    totalstn = stations.concat(missing);
    var junction = coveragearr.junction;
    var url = 'https://54.252.188.170/ws/networkcoverage/yvw/getCloseValves?junctionId='+junction+'&testTime=0&sensorIds='+totalstn.toString()+'&receivedTimes='+time.toString();
    $.get('controllers/proxyman.php',{ww:true,params:url},function(d){
        console.log(d);
        if(d.errorCode>0){
          alert('Server Error - Closed Valves');
          return;
        }
        var coords = null
        coveragearr.closedValves = [];
        _.each(d.response,function(pt){
          coords = new google.maps.LatLng(pt.latitude,pt.longitude);
          addValve(pt.valveId,coords,'Possibly closed valve','valve',coveragearr.closedValves,'selected');
        });
    },'json'); //ajax
  });
/*  ----------- DMA END ------------------- */
  // PLOT THEME COLOR CHANGE
$(document).on('click','#themecheck',function(){
   var check = $(this).is(':checked');
    ResetOptions(check);
});
  $(document).on('mouseenter','.addTray>ul li',function(){
    //$('.traypop').remove();
    var el = $(this).attr('title');
    var idx = unitObj.ids[el];
/*  -----------dma code change ------------------- */
    var selectData = unitObj.sensorMeta[el];
    idx=selectData._id;
    stnname=selectData.name||selectData.display_name;
    _id=selectData._id;

    var sensors = unitObj.sensorSelection[el];
    var html = '<div class="sensorlistBlock">';
    html+='<div class="sensorTitleBlock"><div class="snsrTitle">'+stnname+'</div><div class="removeSensor" onClick="removeunitID(\''+_id+'\')">remove</div><div class="addNewSensor" onClick="sensorAdd(\''+idx+'\',\''+_id+'\')">Edit</div></div><div class="showSensorList hidedia"><div class="listSensorList"></div><div class="optionBox hidedia"><div class="small-4 label regular" id="addDone" style=" margin-right: 5px;">done</div><div class="small-4 label regular" id="addCancel" style="">cancel</div></div></div><div class="senSelLst">'; //
    i=0;
    _.each(sensors,function(sen){
      html += '<div class="sensorsListblock" id="'+idx+'_'+i+'"><div class="sensortitle">'+sen+'</div><div class="remove" onClick="remSensor(\''+idx+'\','+i+')">remove</div></div>';
      i++;
    });
/*  -----------dma code change ------------------- */
    html += '</div></div>';
    $('.traypop').empty().append(html);
    $('.traypop').removeClass('hide');
    $('.traypop').removeClass('resizeTray');
    var pos = $(this).position();
    var top = pos.top-150;
    $('.traypop').stop( true, true ).animate({top:top+'px',left:pos.left+'px'},'fast');
  });
  $(document).on('mouseleave','.addTray',function(){
    $('.traypop').addClass('hide');
  });
          $(document).on('click','.minme',function(){
      $(this).parents('.plotter, .reporter, .stackedPlotter').addClass('animated fadeOutDownBig');
              if($(this).parents('.plotter').length>0)
              {
                var el = $(this).parents('.plotter');
                var chartid = $(this).parents('.plotter').find('.plotcontainer').attr('id');
                var chartindex = null;
                if($(el).hasClass('highresstyle'))
                  chartindex = findHChart(chartid);
                else{
                  chartindex = findChart(chartid);
                if(chartindex!=null && chartUrls[chartindex].hasOwnProperty('dependants')){
                _.each(chartUrls[chartindex].dependants,function(d){
                  var _id = depchartUrls[d].id;
                  $('#'+_id).addClass('animated fadeOutDownBig');
                  $('#'+_id).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    $('#'+_id).removeClass('fadeOutDownBig').addClass('hide');
                    jsPlumb.repaintEverything();
                  });
                });
              }
            }
          }
               var el2 = $(this).parents('.reporter');
               var el3 = $(this).parents('.stackedPlotter');
               $(this).parents('.plotter, .reporter, .stackedPlotter').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                   //console.log('Animation Ended');
                    $(el).removeClass('fadeOutDownBig').addClass('hide');
                    $(el2).removeClass('fadeOutDownBig').addClass('hide');
                    $(el3).removeClass('fadeOutDownBig').addClass('hide');
               });
    });
                // Window Tray Clear
                $(document).on('click','.panelclear',function(){
                    $('.bottom-panel .minWindow').each(function(ind,el){
                      $(this).find('.cross').trigger('click');
                    });
                });
                $(document).on('click','.closeme',function(){
               var  uniqid    =    $(this).parents('.plotter, .stackedPlotter').data('id').toString();
               var _id = $(this).parents('.plotter').attr('id');
               console.log(uniqid);
               var  index     =    windows.indexOf(uniqid);
               var  stuff     =    uniqid.split(',');
               if($(this).parents('.genericstyle').length>0)
                if(typeof(generiClosure)=='function')
                    generiClosure();
               if($(this).parents('.plotter').find('.plotdependant').length>0)
               {
                  _.each(depchartUrls,function(d){
                    if(d.id == _id)
                    {
                      _.each(d.connections,function(c){
                        jsPlumb.detach(c);
                      });
                      _.each(d.endpoints,function(e){
                        jsPlumb.deleteEndpoint(d.endpoints[0]);
                      });
                    }
                  });
               }
              else{
              var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
              var  chartindex = findChart(chartid);
              if(chartindex){
                if(chartUrls[chartindex].hasOwnProperty('dependants')){
                  _.each(chartUrls[chartindex].dependants,function(d){
                      var cid = depchartUrls[d].id.toString();
                      $('#'+cid).find('.closeme').trigger('click');
                  });
                    _.each(chartUrls[chartindex].endpoints,function(e){
                    jsPlumb.deleteEndpoint(e);
                  });
                }
              }
            }
            $(this).parents('.plotter, .stackedPlotter').addClass('animated slideOutLeft');
            $(this).parents('.plotter, .stackedPlotter').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
              $(this).context.remove();
            });
           $('.minWindow').each(function(x){
              if($(this).data('id')    ==   uniqid){                         
                    if($(this).hasClass('addmode'))
                        addplotobj.reset();
                      $(this).remove();
                    }
               });
               windows.splice(index,1);
          });
          $(document).on('click','.stackheader .txt',function(){
            var stackid = $(this).parents('.stackedPlotter').data('id');
            if(!stackid)
            {
              throw "Err- stacked potter id";
              return;
            }
            stackobj.mode = true;
            stackobj.stack = stackid;
            $('.bottom-panel .minWindow').each(function(ind,el){
              if(stackid == $(el).data('id')){
                $('#stckd'+stackid+' .minme').trigger('click');
                $(el).addClass('addmode'); 
                var pos = $(el).position();
                $('.addTray').animate({left:pos.left},'slow');
               }
            });
          }); 
          //advanced plotter function
          $(document).on('change','.modalcontent .oplist input[type=radio][name=opsel]',function(){
            var op = this.value;
            switch(op){
              case 'overlay':
                $('.modalcontent select').removeAttr('disabled');
                $('.modalcontent #axisselection').prop('disabled',true);
                $('.modalcontent #derivedselection').prop('disabled',true);
                $('input[name="containeroptions"]').prop('disabled',true);
              break;
              case 'diff':
                $('.modalcontent select').prop('disabled',true);
                $('input[name="containeroptions"]').prop('disabled',true);
              break;
              case 'regression':
                $('.modalcontent select').prop('disabled',true);
                $('.modalcontent #serselection').prop('disabled',false);
                $('input[name="containeroptions"]').prop('disabled',true);
              break;
              case 'aggr':
                $('.modalcontent select').removeAttr('disabled');
                $('.modalcontent #ff').prop('disabled',true);
                $('.modalcontent #serselection').prop('disabled',true);
                $('input[name="containeroptions"]').prop('disabled',false);
              break;
              case 'avg':
                $('.modalcontent select').removeAttr('disabled');
                $('.modalcontent #ff').prop('disabled',true);
                $('.modalcontent #serselection').prop('disabled',true);
                $('input[name="containeroptions"]').prop('disabled',false);
              break;
              case 'min':
                $('.modalcontent select').removeAttr('disabled');
                $('.modalcontent #ff').prop('disabled',true);
                $('.modalcontent #serselection').prop('disabled',true);
                $('input[name="containeroptions"]').prop('disabled',false);
              break;
              case 'max':
                $('.modalcontent select').removeAttr('disabled');
                $('.modalcontent #ff').prop('disabled',true);
                $('.modalcontent #serselection').prop('disabled',true);
                $('input[name="containeroptions"]').prop('disabled',false);
              break;
              case 'delta':
                $('.modalcontent select').prop('disabled',true);
                $('.modalcontent #serselection').prop('disabled',false);
                $('input[name="containeroptions"]').prop('disabled',true);
              break;
              case 'derived':
                $('.modalcontent select').prop('disabled',true);
                $('.modalcontent #derivedselection').prop('disabled',false);
                $('input[name="containeroptions"]').prop('disabled',false);
              break;
              default:
                throw "Unknown Selection - advanced plotter functions module";
            }
              $('.modalcontent label').removeClass('disable');
              $('.modalcontent label').each(function(idx,el){
                if($(el).children('input').prop('disabled') || $(el).children('select').prop('disabled'))
                  $(el).addClass('disable');
              });
          });
          $(document).on('click','.plotter .stats ul, .stackedPlotter .stats ul',function(){
            var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id') || $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
            var  chartindex = findChart(chartid);
            var serindex = $(this).index();
            requestStats(chartindex,serindex,this);
          });
          $(document).on('click','.duplicateme',function(){
            var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
            var chartindex = findChart(chartid);
            duplicate(chartUrls[chartindex],0);
          });
          $(document).on('click','.playme',function(){
            var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
            var chartindex = findChart(chartid);
            setupPlayer(chartindex);
            playChart(chartindex);
          });
          $(document).on('click','._extract',function(){
            var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
            var chartindex = findChart(chartid);
            var op = $(this).find('img').attr('src');
            //extractChart(chartUrls[chartindex]);
            if(op == 'images/extract.png'){
              rawchart(chartindex,0);
              $(this).find('img').attr('src','images/compress.png');
            }
            if(op == 'images/compress.png'){
              rawchart(chartindex,1);
              $(this).find('img').attr('src','images/extract.png');
            }
          });
          $('.mnitems .iframemnuItem').on('click',function(e){
              var appid=$(this).attr('id');
             if(appid == 'donotrun' || appid == '' || appid == null || appid == undefined)
		return;
		 showIframeWindow(appid);
              $(this).closest('.settingsArea').hide();
          });
          // $('#alertMng').on('click',function(e){

          //     showIframeWindow('alert_management');
          //     $(this).closest('.settingsArea').hide();
          // });
          // $('#editMeta2').on('click',function(e){

          //     showIframeWindow('edit_meta2');
          //     $(this).closest('.settingsArea').hide();
          // });
          // $('#usermanagement').on('click',function(e){

          //     showIframeWindow('usermanagement');
          //     $(this).closest('.settingsArea').hide();
          // });
          
          // $('#dataDownloader').on('click',function(e){

          //     showIframeWindow('dataDownloader');
          //     $(this).closest('.settingsArea').hide();
          // });
          // $('#sensor_quality').on('click',function(e){

          //     showIframeWindow('sensor_quality');
          //     $(this).closest('.settingsArea').hide();
          // });
          // $('#dmasetup').on('click',function(e){

          //     showIframeWindow('alert_management');
          //     $(this).closest('.settingsArea').hide();
          // });
          
          // DAILY DEMAND (SINGAPORE)
          $(document).on('click','.demanddrop',function(){
            var selection=$('#ActionPanel input[name=dailydemand]:checked').attr('id');
            var opt;
            switch(selection)
            {
              case 'dailydemand-total':
                opt='total';
              break;
              case 'dailydemand-potable':
                opt='potable';
              break;
              case 'dailydemand-newwater':
                opt='newwater';
              break;
            }
            defaultSettings.demandForecast=false;
            if(addseriesobj.mode){
              addplotobj.mode = true;
              addplotobj.chartindex = addseriesobj.chartindex;
              add2plot(6,opt);
            }
            else
              dailyDemand(opt);
          });
          // $(document).on('click','#dailydemand-total',function(){
          //   defaultSettings.demandForecast=false;
          //   if(addseriesobj.mode){
          //     addplotobj.mode = true;
          //     addplotobj.chartindex = addseriesobj.chartindex;
          //     add2plot(6,'total');
          //   }
          //   else
          //     dailyDemand('total');
          // });
          // $(document).on('click','#dailydemand-potable',function(){
          //   defaultSettings.demandForecast=false;
          //   if(addseriesobj.mode){
          //     addplotobj.mode = true;
          //     addplotobj.chartindex = addseriesobj.chartindex;
          //     add2plot(6,'potable');
          //   }
          //   else
          //     dailyDemand('potable');
          // });
          // $(document).on('click','#dailydemand-newwater',function(){
          //   defaultSettings.demandForecast=false;
          //   if(addseriesobj.mode){
          //     addplotobj.mode = true;
          //     addplotobj.chartindex = addseriesobj.chartindex;
          //     add2plot(6,'newwater');
          //   }
          //   else
          //     dailyDemand('newwater');
          // });
          // $(document).on('click','#demandForecast',function(){
          //   stateme=true;
          //   defaultSettings.dmaItems=[];
          //   $.each(unitObj.sensorMeta,function(key,value){
          //       if(value.tag_sector=="dmameter")
          //       {
          //         sensors=value.sensorList;
          //         if(unitObj.sensorSelection[value._id].length==0)
          //         {
          //           Notify('Demand forecast',"Please select sensor type",'normal',true);
          //           stateme=false;
          //         }
          //         else if(unitObj.sensorSelection[value._id].length>1)
          //         {
          //           Notify('Demand forecast',"Please select only one sensor type",'normal',true);
          //           stateme=false;
          //         }
          //         else
          //         {
          //           $.each(sensors,function(ke,val){
          //             if(val._source.sensortype_display===unitObj.sensorSelection[value._id][0])
          //             {
          //               sen=val._source.sensortype_backend
          //             }
          //           })
          //           defaultSettings.dmaItems.push({"id":value._id,"sensor":sen})
          //           stateme=true;
          //         }
                  
          //       }
          //   })
          //   if(stateme)
          //   {
          //     defaultSettings.demandForecast=true;
          //     if(defaultSettings.dmaItems.length>1)
          //     {
          //       Notify('Demand forecast',"Please select only one zone to start demand forecast.",'normal',true);
          //       return false;
          //     }
          //     if(addseriesobj.mode){
          //       addplotobj.mode = true;
          //       addplotobj.chartindex = addseriesobj.chartindex;
          //       add2plot(6,'forecast');
          //     }
          //     else
          //       dailyDemand('forecast');
          //   }
          // });
          // PASCAL VIEW ANALYTICS --------------
          $(document).on('click','#pascalytics',function(event){
            event.stopPropagation();
            if($(event.target).is('span')){
               var id = $(this).data('id');
                    $('#'+id).removeClass('hide');
                    return;
            }
            var meta = unitObj.sensorMeta[unitObj.selectedUnit[0]];
            if(!meta || !meta.station_id)
            {
              Notify('Warning','Error could not find station meta','normal',true);
              return;
            }
            var winid = '#'+logWindow('report');
            activeapp = winid;
            var height = $(winid).height();
            var ctr = $('.newaction #filtering').is(':checked');
            $(winid).append('<div class="Editclose closeBTN">X</div><iframe src="apps/pascalman.php?station='+meta.station_id+'&st='+st+'&et='+et+'&critical='+ctr+'" width="100%" height="'+height+'px" frameborder=0/>');
             $(winid +' .closeBTN').on('click',function(){
                  $(winid).remove();
                  removeTrayObject()
             }); 
          });
          $(document).on('click','._overlay',function(){  // FOR SINGLE PLOTTER (OLD) ONLY
            var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
            var chartindex = findChart(chartid);
            advancedFunction(chartindex);
            // var html = '<h3>Overlay Analysis</h3><label for="serselection">Series<select id="serselection">';
            // // series selection
            // var series_index = null;
            // for(var i=0;i<charts[chartindex].series.length;i++){
            //   if(charts[chartindex].series[i].name == 'Navigator' || charts[chartindex].series[i].name == 'flags')
            //     continue;
            //   else
            //   {
            //     if(series_index==null)
            //       series_index = i;
            //     html += '<option value="'+i+'">'+charts[chartindex].series[i].name+'</option>';
            //   }
            // }
            // html += '</select></label>';
            // var prm = extractParams(chartUrls[chartindex].charthistory[0][1]);
            // var interval_special = prm.interval;
            // // find appropriate xaxis range
            // var xdata = charts[chartindex].series[series_index].xData;
            // var totalrange = xdata[xdata.length-1] - xdata[0];
            // var totalmins = ((totalrange/1000)/60);
            // // get user to choose expansion rate.
            // var idd_min = plotrange_val.indexOf(chartUrls[chartindex].intervalmin);
            // var idd_max = plotrange_val.indexOf(idd_max = closest(totalmins,plotrange_val));
            // if(idd_min<0 || idd_max<0)
            // {
            //   throw 'Range Error - Overlay Analysis';
            //   return;
            // }
            // html += '<label for="ff">Range<select id="ff">';
            // for(var j=idd_min+1; j<idd_max;j++)
            //   html += '<option value="'+plotrange_val[j]+'">'+plotrange[j]+'</option>';
            // html += '</select></label><div class="text-center greyBtn" onClick="overlayRoutine('+chartindex+');">Generate Overlay</div>';
            // $('#appModal .modalcontent').empty().append(html+'</ul>');
            // $('#appModal').foundation('reveal','open');
            // //generateOverlay(chartindex);
          });
          $(document).on('click','.addtoplot',function(){
               resetSel();
               showAction(null);
               if(addseriesobj.mode)
                    addseriesobj.reset();
               addseriesobj.mode = true;
               var el = null;
               var type = null;
               if($(this).parents('.plotter').length>0)
                  el = $(this).parents('.plotter');
                else{
                  el = $(this).parents('.stackplotwrap');
                  type = $(this).parents('.stackedPlotter').data('id');
                }
               var chartid = $(el).children('.plotcontainer').attr('id');
               var  chartindex = findChart(chartid);
               if(chartUrls[chartindex].hasOwnProperty('autoload') && charts[chartindex].autoload == true)
               {
                  alert('Auto Resolution will be forced switch off');
                  charts[chartindex].autoload = false;
               }
               addseriesobj.chartindex = chartindex;
               // peristing chart resolution
               var extract = extractParams(chartUrls[chartindex].charthistory[0][1]);
               persist.interval = extract.interval;
               persist.intervalmin = extract.intervalmin;
               persist.st = extract.st;
               persist.et = extract.et;
               // persistance complete
               var plotid = $(el).data('id')||type;
               $('.bottom-panel .minWindow').each(function(ind,_el){
                 if(plotid == $(_el).data('id')){
                    if(type)
                      $(el).parents('.stackedPlotter').find('.minme').trigger('click');
                    else
                    $(el).find('.minme').trigger('click');
                    $(_el).addClass('addmode');
                    var pos = $(_el).position();
                    $('.addTray').animate({left:pos.left},'slow');
               }
            });
          }); 
            $(document).on('click','.res_btn',function(event){
              var el = null;
              var type = null;
              if($(this).parents('.plotter').length>0)
                el = $(this).parents('.plotter');
              else{
                el = $(this).parents('.stackplotwrap');
                type = $(this).parents('.stackedPlotter').data('id');
              }
              var chartid = $(el).children('.plotcontainer').attr('id');
              var chartindex = findChart(chartid);
              //var list = [['1 Sec',0.01667],['30 Sec',0.5],['1 Min',1],['5 Mins',5],['15 Mins',15],['30 Mins',30],['Hour',60],['6 Hours',360],['12 Hours',720],['Day',1440],['Week','weekly'],['Month','monthly']];
              //var list = [['15 Mins',15],['30 Mins',30],['Hour',60],['6 Hours',360],['12 Hours',720],['Day',1440],['Week','weekly'],['Month','monthly']];
              var list = Parentconfig.alltimeintervals.timeinterval;
              var div = document.createElement('div');
              div.className='plotpad';
              var html='<ul>';
              _.each(list,function(l){
                html += '<li onClick="changeRes('+chartindex+',\''+l[1]+'\','+type+')">'+l[0]+'</li>';
              });
              div.innerHTML=html+'</ul>';
              $(this).parent().append(div);
              var position = $(this).position();
              $('.plotpad').css({'top':position.top+20+'px','left':position.left-15+'px'});
            $('.plotpad').one('mouseleave',function(){
                $(this).remove();
            });
            event.stopImmediatePropagation();
            event.preventDefault();
          });
          // j dropdown - split button
          //$(document).on('click','.split > span',function(){
           // var id = $(this).parent().data('id');
           // $('#'+id).removeClass('hide');
          //});
          $(document).on('mouseleave','.newaction',function(){
            $(this).find('.j-dropdown').addClass('hide');
          });
          // units man
          $(document).on('click','.unitsctrl',function(){
           $('.units_div').remove();
            var chartid=null;
            var el = null;
            if($(this).parents('.plotter').length>0){
               chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
                el = $(this).parents('.plotter').children('.plotcontainer:eq(0)');
            }
            else{
              chartid = $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
              el = $(this).parents('.stackplotwrap').children('.plotcontainer:eq(0)');
            }
            var chartindex = findChart(chartid);
            unitsUI(el,chartindex);
          });
          $(document).on('click','.exportme',function(){
            var chartid = $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
            var  chartindex = findChart(chartid);
            var parentid = chartid.split('-');
            if(chartid==null || chartindex==null)
            {
              alert('ERR - Exporter');
              return;
            }
            var idx = $(charts[chartindex].container).parents('.stackplotwrap').index();
            charts[chartindex].destroy();
            $(this).parents('.stackedPlotter').find('.minme').trigger('click');
            $('#'+parentid[1]+' .stackplotwrap:eq('+idx+')').remove();
            duplicate(chartUrls[chartindex]);
          });
          $(document).on('click','._options',function(){
            if($(this).hasClass('control_selected'))
            {
              $(this).parents('.stackplotwrap').find('.stacktoolbar').addClass('hide');
              $(this).removeClass('control_selected');
            }
            else
            {
              $(this).parents('.stackplotwrap').find('.stacktoolbar').removeClass('hide');
              $(this).addClass('control_selected');
            }
          });
          $(document).on('click','.delme',function(){
            var chartid = $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
            var  chartindex = findChart(chartid);
            var parentid = chartid.split('-');
            var idx = $(charts[chartindex].container).parents('.stackplotwrap').index();
            charts[chartindex].destroy();
            $('#'+parentid[1]+' .stackplotwrap:eq('+idx+')').remove();
          });
          $(document).on('click','.winTitle',function(){
               var  uniqid    =    $(this).parent().data('id').toString();
               var  stuff     =    uniqid.split(',');
               $('.plotter, .reporter, .stackedPlotter').each(function(x){
                    if($(this).data('id')    ==   uniqid)
                         {
                           $(this).addClass('animated fadeInUpBig').removeClass('hide');
                           $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                               $('.plotter, .reporter, .stackedPlotter').removeClass('fadeInUpBig');
                            });
                           // dependants
                      if(!$(this).hasClass('highresstyle')){
                          var chartid = $(this).children('.plotcontainer').attr('id') || null;
                          var  chartindex = findChart(chartid);
                          if(chartindex!=null){
                            if(chartUrls[chartindex].hasOwnProperty('dependants'))
                            {
                              _.each(chartUrls[chartindex].dependants,function(c){
                                $('#'+depchartUrls[c].id).addClass('animated fadeInUpBig').removeClass('hide');
                                $('#'+depchartUrls[c].id).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                                   jsPlumb.repaintEverything();
                                  $('#'+depchartUrls[c].id).removeClass('fadeInUpBig');
                                    });
                              });  
                               //jsPlumb.repaintEverything();
                            }
                          }
                        }
                     }
               });
          });
          $(document).on('click','.box',function(){
               var el = $(this).parent();
               $(el).find('._meterlist').toggleClass('hide');
               $(el).find('.attrlist').toggleClass('hide');
          });
          $(document).on('change','#plotdrop input[type=radio][name=eptype]',function(){
            var idx = +$(this).val();
            selectedep = eptype[idx];
          });
          $(document).on('change','#coupling',function(){
            _splitsensors = !_splitsensors;
            /*if(!_splitsensors && !_splitstns)
              $('#stncoupling').trigger('click');*/
          });
          $(document).on('change','#stncoupling',function(){
            _splitstns = !_splitstns;
            /*if(!_splitstns && !_splitsensors)
              $('#coupling').trigger('click');*/
          });
          $(document).on('click','#stacking',function(){
            _stacked = !_stacked;
            /*if(_stacked){
              stackobj.mode = true;
            }
            else
            {
              if(stackobj.stack)
              {
                alert('This will reset your Add to Stack command');
              }
              stackobj.reset();
              $('.minWindow').removeClass('addmode');
            }*/
          });
          // $('.listSensorList').on('click','.labelspan',function(e){
              
          //     // // itemClass=$(this).parent().parent().attr('class');
          //     // // console.log(itemClass);
          //     // if($(this).siblings().is(':checked'))
          //     //     $(this).siblings().prop('checked',false);
          //     // else
          //     //    $(this).siblings().prop('checked',true);
          //     $(this).trigger('click');
          // });
          // $('#actionsList').on('click','.labelspan',function(e){
          //     e.stopPropagation();
          //     // itemid=$(this).parent().parent().attr('id');
          //     // console.log(itemClass);
          //     if($(this).siblings().is(':checked'))
          //         $(this).siblings().prop('checked',false);
          //     else
          //        $(this).siblings().prop('checked',true);
              
          // });
          $(document).on('click','.sidebar',function(){
               var el = $(this).parent().find('.sidepanel');
               console.log($(el));
               if($(el).hasClass('hide')){
                    $(el).removeClass('hide');
                    $(el).animate({opacity:1},'fast');
               }
               else{
                    $(el).animate({opacity:0},'fast',function(){
                    $(el).addClass('hide');
                    });    
               }
          });
           $(document).on('click','.infobar',function(){
               var el = $(this).parents().find('.sidepanel');
               if($(el).hasClass('hide')){
                    $(el).removeClass('hide');
                    $(el).animate({opacity:1},'fast');
                    $(this).addClass('strong');
               }
               else{
                    $(this).removeClass('strong');
                    $(el).animate({opacity:0},'fast',function(){
                    $(el).addClass('hide');
                   });    
               }
          });
           $(document).on('click','.autoplot_switch',function(){
              var el = $(this).parents('.plotter');
              if(el.length == 0)
                el = $(this).parents('.stackplotwrap');
              var chartid = $(el).children('.plotcontainer').attr('id');
              var chartindex = findChart(chartid);
              if(chartUrls[chartindex].hasOwnProperty('autoload'))
                chartUrls[chartindex].autoload = !chartUrls[chartindex].autoload;
              else{
                chartUrls[chartindex].autoload = true;
                chartUrls[chartindex].datamax = charts[chartindex].xAxis[0].dataMax;
                chartUrls[chartindex].datamin = charts[chartindex].xAxis[0].dataMin;
                // secondary series
                var axs = [];
                _.each(charts[chartindex].series,function(ser){
                  if(ser.name == 'Navigator' || ser.name == 'flags')
                    return;
                  else
                    axs.push(ser.xData);
                });
                axs = _.uniq(_.compact(_.flatten(axs)));
                var sec_series = [];
                _.each(axs,function(x){
                  sec_series.push([x,null]);
                });
                charts[chartindex].addSeries({data:sec_series,name:'flags',showInLegend:false});
              }
              $(this).toggleClass('strong');
           });
           $(document).on('click','.stackbtn',function(){
              var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
              var chartindex = findChart(chartid);
              var res = chart2stack(chartindex);
              if(res)
                $(this).empty().append('Un-Stack').addClass('strong');
              else
                $(this).removeClass('strong').empty().append('Stack All');
           });
           $(document).on('click','.zoom_reset',function(){
              var el = $(this).parents('.plotter');
              if(el.length == 0)
                el = $(this).parents('.stackplotwrap');
              var chartid = $(el).children('.plotcontainer').attr('id');
              var chartindex = findChart(chartid);
              charts[chartindex].zoom();
           });
           $(document).on('click','.zoombtn',function(){
              var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id') || $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
              var chartindex = findChart(chartid);
              //charts[chartindex].zoom();
              var el = $(this).parents('.plotter').children('.plotcontainer');
              if(el.length == 0)
              el =  $(this).parents('.stackplotwrap').children('.plotcontainer');
              $(el[0]).css('height','auto');
              var dims = {height:$(el[0]).height(),width:$(el[0]).width()};
              var maxwidth = $('body').width() - 10;
              chartUrls[chartindex].dimensions = dims;
              charts[chartindex].setSize(maxwidth,500);
              if(screenfull.enabled){
                screenfull.request(el[0]);
              }
           });
           $(document).on(screenfull.raw.fullscreenchange, function () {
              if(screenfull.isFullscreen)
                {
                  screenobj.element = screenfull.element;
                }
              else
              {
                 var el = screenobj.element;
                 var chartid = $(el).attr('id');
                 var chartindex = findChart(chartid);
                 $(el).removeAttr('style');
                 charts[chartindex].setSize(chartUrls[chartindex].dimensions.width,chartUrls[chartindex].dimensions.height);
              }
          });
           
           $(document).on('click','.statsbtn',function(){
              var el = $(this).parents('.plotter');
              if(el.length == 0)
                el = $(this).parents('.stackplotwrap');
              $(el).find('.stats').toggleClass('hide');
              $(this).toggleClass('strong');
           });
          $(document).on('click','.cross',function(){
               var  uniqid    =    $(this).parent().data('id').toString();
               var  index     =    windows.indexOf(uniqid);
               var  stuff     =    uniqid.split(',');
               $('.plotter, .reporter, .stackedPlotter').each(function(x){
                    if($(this).data('id')    ==   uniqid){
                      if($(this).hasClass('genericstyle'))
                      {
                        if(typeof(generiClosure) === 'function')
                          generiClosure();
                      }
                      $(this).remove();
                    }
               });
               if($(this).parent().hasClass('addmode'))
                  addplotobj.reset();
               $(this).parent().remove();
               windows.splice(index,1);
          });
          $(document).on('click','.dayselect .crossme',function(){
            $(this).parent().toggleClass('hide');
          });
          $(document).on('click','.dayselect ul li',function(){
            var days = [];
            $(this).toggleClass('dayselected');
            $('.dayselect li').each(function(a,b){ 
              if($(b).hasClass('dayselected')){
                if(b.innerHTML == 'Pub')
                  days.push('p');
                else
                  days.push(a); 
              }
            });
            var chartid = null;
            var chartindex = null;
            if($(this).parents('.plotter').length>0)
              chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
            else
              chartid = $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
            chartindex = findChart(chartid);

            var tgl = $('.dayselect .daycheck').is(':checked');
            daychecker(charts[chartindex],days,tgl);
          });
          $(document).on('click','.pdfdown',function(){
            var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id') || $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
            var chartindex = findChart(chartid);
            massExport(charts[chartindex]);
          });
/*          $(document).on('click','.swapaxis',function(){
            var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
            var chartindex = findChart(chartid);
            shiftAxis(charts[chartindex],chartindex);
          }); */
          $(document).on('click','.controls a',function(){
            if($(this).parent().find('.inmenu').length != 0){
            $(this).parent().find('.inmenu').toggleClass('hide');
            $(this).toggleClass('strong');
          }

          });
          $('#help').click(function(){
           
           if($('#HelpArea').hasClass('active'))
           {
                $('#HelpArea').animate({top:'30px'}, 200,function() {
                     $('#HelpArea').hide();
                     $('#HelpArea').removeClass('active');
                     $(this).removeClass('onmode')
                }); 
           }
           else
           {
                $('#HelpArea').addClass('active');
                $('#HelpArea').show();
                $(this).addClass('onmode')
                $('#HelpArea').animate({top:'40px'}, 200,function() {
                     $('.helpContent').show();
                });
           }

           })
           $('#settingsArea').on('click','#home',function(){
                console.log(Parentconfig)
                var sessionurl='api.php?tag=DeleteSession';
                
                var request = $.ajax({
                  url: sessionurl,
                  type: "GET",
                  dataType: "JSON"
                });
                 
                request.done(function( data ) {
                  SessionObject=data;
                  if(getCookie('dIntelSession')=='failed')
                  {
                      eraseCookie('dIntelrole');
                      eraseCookie('dIntelUser');
                      eraseCookie('dIntelSession');
                      eraseCookie('dIntelcustomer');
                      eraseCookie('timezone');
                      eraseCookie('language');
                     window.location.href= Parentconfig.baseIP+'/'+Parentconfig.folder+'/'; 
                  }
                  else
                  {
                    eraseCookie('dIntelrole');
                    eraseCookie('dIntelUser');
                    eraseCookie('dIntelSession');
                    eraseCookie('dIntelcustomer');
                    eraseCookie('timezone');
                    eraseCookie('language');
                    if(SessionObject.response.errorCode==0)
                    {

                         window.location.href= Parentconfig.baseIP+'/'+Parentconfig.folder+'/'; 
                    }
                  }
                  
                });
                request.fail(function( jqXHR, textStatus ) {
                  alert( "Request failed: " + textStatus );
                });
              
               
           })

           $('#settings').click(function(e){
              e.preventDefault();
              $('.settingsArea').hide();
             // $('#settingsArea').slideToggle();
              if($(this).hasClass('setslide'))
              {
                  $('#settingsArea').hide();
                  $(this).removeClass('setslide');
              }
              else
              { 
                  $('#settingsArea').show();
                  loadsettings();
                  loadSet();
                  $(this).addClass('setslide');
              }
           });
           $('#helpsettings').click(function(e){
              e.preventDefault();
              $('.settingsArea').hide();
             // $('#settingsArea').slideToggle();
              if($(this).hasClass('setslide'))
              {
                  $('#helpArea').hide();
                  $(this).removeClass('setslide');
              }
              else
              { 
                  $('#helpArea').show();
                  loadsettings();
                  $(this).addClass('setslide');
              }
           });
           $('#BookControls').click(function(e){
              e.preventDefault();
              $('.settingsArea').hide();
             // $('#settingsArea').slideToggle();
              if($(this).hasClass('setslide'))
              {
                  $('#bookmarkArea').hide();
                  $(this).removeClass('setslide');
              }
              else
              { 
                  $('#bookmarkArea').show();
                  loadsettings();
                  $(this).addClass('setslide');
              }
           });
           $('#showalerts').click(function(e){
              e.preventDefault();
              $('.settingsArea').hide();
             // $('#settingsArea').slideToggle();
              if($(this).hasClass('setslide'))
              {
                  $('#alertsArea').hide();
                  $(this).removeClass('setslide');
              }
              else
              { 
                  $('#alertsArea').show();
                  loadsettings();
                  $(this).addClass('setslide');
              }
           });
           $('#optionSettings').click(function(e){
              e.preventDefault();
              $('.settingsArea').hide();
             // $('#settingsArea').slideToggle();
              if($(this).hasClass('setslide'))
              {
                  $('#optiArea').hide();
                  $(this).removeClass('setslide');
              }
              else
              { 
                  $('#optiArea').show();
                  loadsettings();
                  $(this).addClass('setslide');
              }
           });
            $('#reportId').click(function(e){
              e.preventDefault();
               $('.settingsArea').hide();
             // $('#settingsArea').slideToggle();
              if($(this).hasClass('setslide'))
              {
                  $('#reportArea').hide();
                  $(this).removeClass('setslide');
              }
              else
              { 
                  $('#reportArea').show();
                  loadsettings();
                  $(this).addClass('setslide');
              }
           });
           $('#zonesettings').click(function(e){
              e.preventDefault();
              $('.settingsArea').hide();
              if($(this).hasClass('zoneslid'))
              {
                  // $('#zonesettingsArea').slideToggle();
                  $('#zonesettingsArea').hide();
                  // $('#taglist #potable .closeTag').trigger('click');
                  $(this).removeClass('zoneslid');
              }
              else
              { 
                  $('#zonesettingsArea').show();
                  loadZonesettings();
                  $(this).addClass('zoneslid');
              }
              
             
              
           });
           $('.taglist').on('click','.closeTag',function(){
              var closeID=$(this).parent().attr('id');
              var closeParentID=$(this).parent().data('parent');
              $('#'+closeParentID+' #'+closeID).removeClass('regular').addClass('secondary');
               $('#'+closeParentID+' #'+closeID).removeClass('sel');
              $('#'+closeID).removeClass('regular').addClass('secondary');
              $('.taglist #'+closeID).remove();
              //showAction();
          })
           $('.closetext').on('click',function(e){
              e.preventDefault();
              $('.settingsArea').hide();
           })
           // $('#gisItems .Gitems').click(function(e){
           //     e.preventDefault();
           //     selLabel=$(this).attr('id');
           //     if($(this).hasClass('enabled'))
           //     {
           //        $(this).removeClass('enabled')
           //        layers(false,selLabel)
           //     }
           //     else
           //     {
           //       $(this).addClass('enabled')
           //       layers(true,selLabel)
           //     }
           //     //doAction(selLabel)
           // })

           _saveOptions();
}
function loadZonesettings()
{
    $('#zonesettingsArea1').empty();
    $('#taglist #potable').remove();
    $('#taglist #newwater').remove();
    if(settings.water)
    {
      watersettings='<div class="large-12 columns optItems"  id="watertype" ><div class="large-4 columns description" id="greetings">Water Type</div><div class="large-8 columns information"><div class="large-3 label secondary" id="newwater">NeWater</div><div class="large-3 label secondary" id="potable">Potable</div></div> </div>'
      $('#zonesettingsArea1').append(watersettings);
    }
    // var response={};
    //   var pURL='controllers/proxyman.php';
    //   $.ajax({
    //     type: "POST",
    //     url: pURL,
    //     //contentType: "application/json",
    //     data: {search:true,"SearchQuery":zoneQuery},
    //     dataType:"json"
    //   }).done(function( aggRes ) {
    //     if(aggRes.errorCode==0)
    //     {
    //       $.each(aggRes.response.aggregations.unique.buckets,function(){
    //           console.log(this.key)
    //       })//unique
    //     }
    //   });
  
    zoneSettings='<div class="large-12 columns optItems"  id="zoneSelection"><div class="large-4 columns description" id="zone"><span id="zone">'+tr('Zone')+'</span> </div><div class="large-8 columns information"></div></div>';             
    $('#zonesettingsArea1').append(zoneSettings);
    if(settings.water)
    {
      if(settings.watertype=='potable')
      {
          $('#potable').trigger('click');
      }
      else
      {
          $('#newwater').trigger('click');
      }
    }
    if(zonesInfo.length>0)  //JAB EDIT
    {
      tempTxt=''
      $.each(zonesInfo,function(key,value){
        chk = 'secondary';
        if(settings.subzone.indexOf(value)>=0)
          chk = 'regular'
          tempTxt+='<div class="label '+chk+'" id="'+value+'">'+value.toUpperCase()+'</div>';
      });

                  
      $('#zoneSelection .information').html(tempTxt); //'<div class="label secondary" id="yvw">YVW</div>'
    }
    
}
function showIframeWindow(app)
{
  var winid = '#'+logWindow('report');
  activeapp = winid;
  var height = $(winid).height();
  // $(winid).append('<iframe src="'+url+'" width="100%" height="'+height+'px" frameborder=0/>');

  // var html='<div class="controlWindow animated" id="iframewindow"><div class="topbar" style="position: absolute; width: 98% !important; margin-top: 5px;"><div class="selectionMode"><a href="#" class="button tiny" id="SelectionDone">Done</a>&nbsp;<a href="#" class="button tiny" id="Selectioncancel">Cancel</a></div><div class="EditTab"></div><div class="Editclose">X</div></div><div class="content" style="width: 100%; height: 500px; height: 93%;"></div></div>';
  // $('body').append(html);
  url=AppObj[app]
  appAdd='<div class="Editclose closeBTN">X</div><iframe src="'+url+'" seamless sandbox="allow-same-origin allow-scripts allow-popups allow-forms allow-top-navigation" width="100%" height="100%" frameBorder="0" scrolling="no" name="appFrame" id="appFrame" class="frame" style="min-height: 500px;">Browser not compatible. </iframe >'
  // $('#iframewindow .content').append(appAdd);
  // $('#iframewindow .Editclose').on('click',function(){
  //       $('#iframewindow').remove();
  // });
  $(winid).append(appAdd);
  $(winid +' .closeBTN').on('click',function(){
        $(winid).remove();
        removeTrayObject()
  });  
}
function loadDefaultDetails()
{

    settings = $.extend( {}, defaultSettings, options );
    
}
function loadsettings()
{
    $('#settingsArea').on('change','#language',function() {
      ln=$(this).val()
      changeLn(ln);
      //$('#languageArea').hide();
    });

}
function loadSet()
{

  $('#settingsArea1').empty();
    usertext='<div class="large-12 columns optItems"><div class="large-4 columns description" id="greetings"><span id="hello">'+tr('Hello')+'</span> </div><div class="large-8 columns information textalign">'+Parentconfig.user.response.UserEmail+' <br /><div class="label regular logout_btn"><a href="logout.php">logout</a></div></div></div>'
    $('#settingsArea1').append(usertext);
    var LanguageSettings='<div class="large-12 columns optItems"  id="language"><div class="large-4 columns description" id="greetings">Language</div><div class="large-8 columns information"><div class="large-11 large-centered columns"><select id="language"><option value="en">English</option><option value="fr">France</option><option value="cn">Chinese</option></select></div></div></div>';
    $('#settingsArea1').append(LanguageSettings);
    var timezoneList='';
    // tzlist=[];
    // $.getJSON("js/tZones.json",function(result){
    
    //  var selct= getCookie("timezone")

    //   $.each(result.zones,function(key,value){
    //    zn=value.name;

    //    tm=(moment().tz(value.name)._offset/60)*-1;
    //    names='( '+tm+' ) '+zn
    //    tzlist.push({'timezone':tm,'zone':zn})
    //   });
     
    //   var ResponseSorted=_.sortBy(tzlist, function(Filtered){ 
    //     return -Filtered.timezone;
    //   });
    //    console.log(JSON.stringify(ResponseSorted.reverse()))
    // });
   $.getJSON("js/timeZone.json",function(result){

     var selct= getCookie("timezone")

      $.each(result,function(key,value){

       timezoneList+='<option value="'+value.zone+'">( '+value.timezone+' ) '+value.zone+'</option>';
      });
     
     
    var timezoneSettings='<div class="large-12 columns optItems"  id="timezoneset"><div class="large-4 columns description" id="greetings">Time Zone</div><div class="large-8 columns information"><div class="large-11 large-centered columns"><select id="timezone">'+timezoneList+'</select></div></div></div>';
    $('#settingsArea1').append(timezoneSettings);
    $("#timezone").val(selct);   
        $( "#timezone" ).change(function() {
          eraseCookie('timezone');
          console.log($(this).val())
          setCookie("timezone",$(this).val(),365);

          _tzo.offset=moment().tz($(this).val())._offset;
          st=datejson.stime+(_tzo.offset*60*1000);
          et=datejson.etime+(_tzo.offset*60*1000);
          showPanel()
        });
    });
  ischecked = 'checked';
  if(!Highcharts.themetype)
     ischecked = '';
  $('#settingsArea1').append('<label>Plot Theme: Dark &nbsp;<input id="themecheck" type="checkbox" '+ischecked+'/></label>');
   // $.getJSON("js/tZones.json",function(result){
   //  // console.log(result.countries)
   //    $.each(result.zones,function(key,value){
   //     zn=value.name;

   //     tm=moment().tz(value.name)._offset/60;
   //     if(result.countries[value.countries])
   //     {
   //      names='('+tm+')'+result.countries[value.countries].name
   //     }

   //      timezoneList+='<option value="'+zn+'">'+names+'</option>';
   //    });
   // });
    
}
function _saveOptions()
{
    
    $('.settingsArea').on('click','.label',function(){
        var clickBlock=$(this).parent().parent().attr('id');
        switch(clickBlock)
        {
          case 'watertype':
              _toggleLabel('#watertype','.regular',this);
              var itemID=$(this).attr('id');
              if(itemID=='potable')
              {
                  $('#zonesettingsArea #ZoneCluster').remove();
                  _removeTagCollection('settings',['zoneSelection','ZoneCluster']);
                  $('.nw').hide();
                  $('.pt').show();
                  $('#flowMeters .label').removeClass('regular').addClass('secondary');
                  sh='pw'
                  tempTxt=''
                  $.each(zonesInfo,function(key,value){
                      if(jQuery.inArray(value,profile.zones)>=0)
                          tempTxt+='<div class="label secondary" id="'+value+'">'+value.toUpperCase()+'</div>';
                  })
                  $('#zoneSelection .information').html(tempTxt); //'<div class="label secondary" id="yvw">YVW</div>'
                  
                  //$('#zoneSelection .information').html('<div class="label secondary" id="phfc">PHFC</div><div class="label secondary" id="qhpz">QHPZ</div><div class="label secondary" id="western">WESTERN</div><div class="label secondary" id="bbsr">BBSR</div><div class="label secondary" id="eastern">EASTERN</div><div class="label secondary" id="island">ISLAND</div><div class="label secondary" id="murnane">MURNANE</div><div class="label secondary" id="krsr">KRSR</div><div class="label secondary" id="nanyang">NANYANG</div><div class="label secondary" id="bkalang">BKALANG</div> <div class="label secondary" id="bpsr">BPSR</div>');
                  $('#zoneSelection .information').removeClass('newater').addClass('potable');
                  
                  options.watertype='potable';
                  settings = $.extend( {}, defaultSettings, options );
                  $('#commercialType .label').hide();
                  $.each(noncom.pw,function(key,value){
                      $('#commercialType #'+value).show();
                  });
                  $('#residential').show();
                  console.log('hi')
                  //loadDMAZones()
                  //dma.loadzones();
              }
              else
              {
                  _removeTagCollection('settings',['zoneSelection']);
                  $('.pt').hide();
                  $('.nw').show();
                  $('#flowMeters .label').removeClass('regular').addClass('secondary');
                  sh='nw';
                  $('#zoneSelection .information').html('<div class="label secondary" id="Bedok">Bedok</div><div class="label secondary" id="Kranji">Kranji</div><div class="label secondary" id="Seletar">Seletar</div><div class="label secondary" id="Ulu_Pandan">Ulu Pandan</div>');
                  $('#zoneSelection .information').removeClass('potable').addClass('newater');
                  
                  options.watertype='newwater';
                  settings = $.extend( {}, defaultSettings, options );
                  $('#commercialType .label').hide();
                  $.each(noncom.nw,function(key,value){
                      $('#commercialType #'+value).show();
                  });
                  $('#residential').hide();
              }
              _toggleTag(this,'settings');
            break;
          case 'zoneSelection':
              var itemID=$(this).attr('id')
              if($(this).parent().hasClass('potable'))
              {
                
                _selectLabel(this);

                if($(this).hasClass('regular'))
                {
                    _addTag(this,'settings');
                    // region(itemID,1);
                    // addzonetoBucket(this,1)
                    // dma.addtobucket(this,1);
                }
                else
                {
                    _removeTag(this,'settings');
                    // region(itemID,0);
                    // addzonetoBucket(this,0)
                    // dma.addtobucket(this,0);
                }
                
              }
              else
              {
                //_toggleLabel('#zoneSelection', '.regular', this);
                _selectLabel(this);
                selLength=$('#zoneSelection .regular').length;
                if($(this).hasClass('regular'))
                {
                  if(selLength<=1)
                  {
                      
                      cluster=cluster='<div class="large-12 columns optItems"  id="ZoneCluster"><div class="large-4 columns description">Zone Sub Cluster</div><div class="large-8 columns information">'+_getclusterData(itemID)+'</div> </div>';
                      $('#zonesettingsArea').append(cluster);

                  }
                  else
                  {
                      var $elems = $('.taglist').find("[data-parent='ZoneCluster']");
                      var length = $elems.length;
                      if(length>0)
                      {
                            $('.taglist div[data-parent="ZoneCluster"]').remove();
                      }
                      $('#ZoneCluster').empty();
                      $('#ZoneCluster').hide();
                  }
                  _addTag(this,'settings');

                  
                }
                else
                {
                    _removeTag(this,'settings');
                    _removeTagCollection('settings',['ZoneCluster']);
                    $('#ZoneCluster').remove()
                    if(selLength==1)
                    { 
                       selected=$('#zoneSelection .regular').attr('id');
                       cluster=cluster='<div class="large-12 columns optItems"  id="ZoneCluster"><div class="large-4 columns description">Zone Sub Cluster</div><div class="large-8 columns information">'+_getclusterData(selected)+'</div> </div>';
                      $('#zonesettingsArea').append(cluster);                    
                    }
                }
                
                

              }
              
                options.subzone=[];
                $('#zoneSelection .regular').each(function(key,value){
                    options.subzone.push($(this).attr('id'))
                })
              settings = $.extend( {}, defaultSettings, options );
               
            break;
          case 'ZoneCluster':
                _selectLabel(this);
                if($(this).hasClass('regular'))
                {
                    _addTag(this,'settings');
                }
                else
                {
                    _removeTag(this,'settings');
                }
            break;
          default:
              _selectLabel(this);
            break;
        }
        fetchselection();
    });
    
}
function _removeTag(e,from)
{
  
  tagText=$(e).text();
  tagID=$(e).attr('id');
  if(from=='settings'||from=='zone')
  {
    tagParentID=$(e).parent().parent().attr('id');
  }
  else
  {
    tagParentID=$(e).parent().attr('id');
  }
  $('.taglist #'+tagID+'[data-parent="'+ tagParentID +'"]').remove();
}
function _addTag(e,from)
{
  
  tagText=$(e).text();
  tagID=$(e).attr('id');
  if(from=='settings')
  {
    tagParentID=$(e).parent().parent().attr('id');
  }
  if(from=='subzone')
  {
    tagParentID=$(e).parent().attr('id');
    
  }
  if(from=='zone')
  {
    tagParentID=$(e).parent().parent().attr('id');
    tagText=$(e).children('span').text();
  }
  else
  {
    tagParentID=$(e).parent().attr('id');
  }
  labl=tagParentID;
  tag='<div class="tags" id="'+tagID+'" data-parent='+tagParentID+'><div class="labl">'+labl+': '+tagText+'</div><div class="closeTag">x</div></div>';
  $( '.taglist').append(tag);
}
function _toggleTag(e,from)
{
  if(from=='settings')
  {
    tagParentID=$(e).parent().parent().attr('id');
  }
  else if(from=='sidebutton')
  {
    tagParentID=$(e).parent().parent().attr('id'); //.siblings('.sidebutton_gis').children('.sel')
  }
  else
  {
    tagParentID=$(e).parent().attr('id');
  }

  var $elems = $('.taglist').find("[data-parent='" + tagParentID + "']");
  var length = $elems.length;
  if(length>0)
  {
    $('.taglist div[data-parent="'+ tagParentID +'"]').remove();
  }
  tagText=$(e).text();
  tagID=$(e).attr('id');
  
  labl=tagParentID;
  close=true;
  if(tagParentID=='watertype')
  {
    close=false;
  }
  tag='<div class="tags" id="'+tagID+'" data-parent='+tagParentID+'><div class="labl">'+labl+': '+tagText+'</div>';
  if(close)
  {
    tag+='<div class="closeTag">x</div>';
  }
  tag+='</div>';
  $( '.taglist').append(tag);
}
function _removeTagCollection(from,collection)
{
    $.each(collection,function(key,value){
      var $elems = $('.taglist').find("[data-parent='" + value + "']");
      var length = $elems.length;
      if(length>0)
      {
        $('.taglist div[data-parent="'+ value +'"]').each(function(){
         selectid= $(this).attr('id');
         $('#'+value+' #'+selectid).toggleClass('sel')
        })
        $('.taglist div[data-parent="'+ value +'"]').remove();

      }
    })
}
function _getclusterData(itemID)
{
    var opionsList={};
    var clusteritems='';
    opionsList={"Seletar":["Ang Mo Kio"],"Kranji":["Sembawang","Woodlands","Senoko"],"Bedok":["Changi East","Tampines"],"Ulu_Pandan":["Jurong Island","Orchard"]}
    demandCat=itemID;//.replace(/\s/gi, "_")
    $.each(opionsList[demandCat],function(key, value){
      valcat=value.replace(/\s/gi, "_")
      clusteritems+='<div class="label secondary" id="'+valcat+'">'+value+'</div>'
    });
    
    return clusteritems;
}
function _toggleLabel(id,eclass,e)
{
    $(id+' '+eclass).removeClass('regular').toggleClass('secondary');
    $(e).toggleClass('regular').toggleClass('secondary');
}
function _selectLabel(e)
{
    parent=$(e).parent().attr('id')
    selLabel=$(e).attr('id');
    state=$('#'+parent+' #'+selLabel).hasClass("regular")
    if(!state)
    {
      doAction(selLabel)
    }
    else
    {
      reverseAction(selLabel)
    }
    
    $(e).toggleClass('regular').toggleClass('secondary');

}
function doAction(selLabel)
{
  switch(selLabel)
  {
      case 'pipes':
        layers('pipe',true);
      break;
      case 'junction':
        layers('junction',true);
      break;
      case 'valve':
        layers('valve',true);
      break;
  }
}
function reverseAction(selLabel)
{
  switch(selLabel)
  {
      case 'pipes':
        layers('pipe',false);
      break;
      case 'junction':
        layers('junction',false);
      break;
      case 'valve':
        layers('valve',false);
      break;
  }
}

function _animate(id,top,speed,crlevel)
{
  
  if(crlevel!='null'&&crlevel=='.levelOne')
  {
      $('.control_area .levelTwo').hide().css({top: '40px'});
  }
  $('.control_area '+crlevel).hide().css({top: '40px'});
  $(id).show();
  $(id).animate({top: top}, speed,function() {});
}
function removePops(parent,show,hide,toggle)
{
  $(hide).hide();
  $.each(toggle,function(k,va){
    if($(va+' .toggle').length>0)
    {
      var id=$(va+' .regular').attr('id')
      $(va+' .regular').toggleClass('regular').toggleClass('secondary');
      $('#'+id+'_options').hide();
    }
    else
    {
      $(va+' .regular').trigger('click')
    }

  });
  if($('#parent_options_root .regular').length==1&&$('#parent_options_root .regular').attr('id')=='amr')
  {
      if(show=='parent')
        _animate('#'+show+'_options',parent+'px',200,'.levelOne')
  }
  else if($('#parent_options_root .regular').length==1&&$('#parent_options_root .regular').attr('id')=='sensor')
  {
      if(show=='sensor')
        _animate('#'+show+'_options',parent+'px',200,'.levelOne')
  }

  
}
function _addSearchOptions()
{
  SettingsOBJ={};
  var height, parentHt,chHt;
  // SearchTags=Parentconfig.allbasictags.search_tags;
  url=parsedURL.protocol+'://'+parsedURL.domain+'/'+parsedURL.path+'meta/suggestions';
  var pxyUrl='controllers/curlProxy.php?tag=getData'
  searchFilter= $('#search').searchFilter({proxyUrl:pxyUrl,suggestionUrl:url,availableTags:Parentconfig.allbasictags.search_tags});
    
      $('#search').focus(function(e) {
        e.stopPropagation();
          // $('#parent_options_root').show();  
          // $('#parent_options_root').animate({top: '43px'}, 200,function() {
          // });
          height=($('.search_Area').height()+6);    //43px;
          _animate('#parent_options_root',height+'px',200,null);
          $('#Type #sensor').trigger('click');
          
      });
       $('#parent_options_root .label').click(function(){
        parentHt=$('#parent_options_root').height()+height;
        var rootCurrent=$('.regular').attr('id');
        //_toggleLabel('#parent_options_root','.regular',this)
        _selectLabel(this)
        rootSelection=$(this).attr('id');
        rootID=$(this).parent().parent().parent().attr('id');
        switch(rootSelection)
        {
          case 'amr':
              rootSelection='amr';
              lastSelection='amr';
              if($(this).hasClass('regular'))
              {
                _animate('#parent_options',parentHt+'px',200,'.levelOne')  //123px
              }
              else
              {
                removePops(parentHt,'sensor','#parent_options',['#wems_options','#nonresidential_options','#residential_options','#parent_options'])
              }
              firstLoad = true;
              //clearMarkers(true)
             // reloadmarkers()
              //addicons();

            break;
          case 'weather':
              rootSelection='weather';
              lastSelection='weather';
              //_animate('#weather_options','123px',200) 
            break;
          case 'scada':
              rootSelection='scada';
              lastSelection='scada';
              _animate('#scade_options',parentHt+'px',200,'.levelOne')
            break;
          case 'sensor':
              SettingsOBJ.rootSelection='sensor';
              SettingsOBJ.lastSelection='sensor';
              if($(this).hasClass('regular'))
              {
                _animate('#sensor_options',parentHt+'px',200,'.levelOne')
              }
              else
              {
                removePops(parentHt,'parent','#sensor_options',['#sensor_options'])
                
              }
            break;
          case 'gis':
              rootSelection='gis';
              lastSelection='gis';
              _animate('#gis_options',parentHt+'px',200,'.levelOne')
            break;
          default:
            rootSelection='watersource';
            break;
        }
        //_toggleTag(this,'search');
        if($(this).hasClass('regular'))
        {
               _addTag(this,'search');
        }
        else
        {
              _removeTag(this,'search');
        }
        fetchselection();
        console.log(rootSelection);
        //appSettings = $.extend( {}, defaultSettings, SettingsOBJ );
      });

      $('.close_btn').click(function() {
        var closeID=$(this).parent().attr('id');
        $('#'+closeID).animate({top: '43px'}, 200,function() {
            $('#'+closeID).hide(); 
        });   
      });
      $('.root_options .close_btn').click(function(){
               $('.parent_options .close_btn').trigger('click')
               $('.sub_parent_options .close_btn').trigger('click')
      })
      $('.parent_options .close_btn').click(function(){
               $('.sub_parent_options .close_btn').trigger('click')
      })
      $('#predtype').click(function(){
        veecheck=!veecheck
      });
      $('#flagsbtn').click(function(){
        flagsbit=!flagsbit
      });
      $('#excbtn').click(function(){
        globalex = !globalex;
      });
      $('#vee_setup').on('change',function(){
              var val = this.value;
                if(val === 'Raw') { // RAW
                        veecheck = false;
                        pred = false;
                      $('#res_select').attr('disabled',false)
                      interval = interval_temp;
                                }
                    else if(val == 'Raw (Highest Resolution)')
                    {
                      veecheck = false;
                      pred = false;
                      interval_temp = interval;
                      interval = "-1";
                      $('#res_select').attr('disabled',true)
                    }
                    else if(val === 'Valid') { //VALID
                                        veecheck = true;
                                        pred = false;
                      $('#res_select').attr('disabled',false)
                      interval = interval_temp;
                    }
                    else if(val === 'Estimated') { // ESTIMATED
                                        veecheck = true;
                                        pred = true;
                      $('#res_select').attr('disabled',false)
                      interval = interval_temp;
                    }
                    else
                        alert('Illegal Option');
        });
      $('.parent_options').on('click','.label',function(){
          chHt=$('#parent_options').height()+parentHt;
          console.log(chHt)
          tagText=$(this).text();
          tagID=$(this).attr('id');
          tagParentID=$(this).parent().attr('id');
          labl=tagParentID;
          if(!$('#'+tagParentID).hasClass('toggle'))
          {
              _selectLabel(this);
              var $elems = $('.taglist').find('#'+tagID);
              var length = $elems.length;
              if(length>0)
              {
                  closeID=$(this).attr('id'); 
                  closeParentID=$(this).parent().attr('id');
                  $('.taglist').children('#'+closeID).remove();
              }
              else
              {
                    tag='<div class="tags" id="'+tagID+'" data-parent='+tagParentID+'><div class="labl">'+labl+': '+tagText+'</div><div class="closeTag">x</div></div>';
                    $( '.taglist').append(tag);
              }
              _loadSubOptions(tagID);
          }
          else
          {
              id='.parent_options #'+tagParentID;
              _toggleLabel(id,'.regular',this)
              var $elems = $('.taglist').find("[data-parent='" + tagParentID + "']");
              var length = $elems.length;
              if(length>0)
              {
                
                $('.taglist div[data-parent="'+ tagParentID +'"]').remove();
              }
                      
              tag='<div class="tags" id="'+tagID+'" data-parent='+tagParentID+'><div class="labl">'+labl+': '+tagText+'</div><div class="closeTag">x</div></div>';
              $( '.taglist').append(tag);
              _loadSubOptions(tagID);
          }
          fetchselection()
          
      });
      $('.sub_parent_options').on('click','.label',function(){
          _selectLabel(this)
          if($(this).hasClass('regular'))
          {
            _addTag(this,'search');
          }
          else
          {
            _removeTag(this,'search');
          }


          fetchselection()
      });
      function _loadSubOptions(tagID)
      {
          switch(tagID)
          {
            case 'nonresidential':
                _animate('#nonresidential_options',chHt+'px',200,'.levelTwo')
              break;
            case 'residential':
                _animate('#residential_options',chHt+'px',200,'.levelTwo')
              break;
            case 'wems':
                _animate('#wems_options',chHt+'px',200,'.levelTwo') //195px
              break;  
          }
      }


  

      
      

   function onSelectAddress(address, callback) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            callback(results[0].geometry.location);
        } else {
            // alert("Can't find address: " + status);
            callback(null);
        }
    });
    }

    
    $('#search_btn').click(function() {
      // if($('#Type .regular').length==0)
      // {
      //   $('#selectAll').show();
      // }
      // else
      // {
      //   $('#selectAll').hide();
      // }
      preLoader('Loading...','modal',true)
    searchtxtObj=searchFilter.getData()
      // searchtxtObj=$('#search').searchFilter({availableTags: Parentconfig.allbasictags.search_tags}).getdata();
      //&&$('#watertype .regular').length==0
      if(locSearch&&searchtxtObj.length<1&&$('.search_Area input').val()!='')
      {
        
        var places = $('.search_Area input').val()
        onSelectAddress(places, function(location){
          //Do something with location
          preLoader('Loading...','modal',false)
          if (location)
          {
              // map.zom
              // map.panTo(location); //map.panTo(new GLatLng(lat,lon));
              map.setCenter(location);
              map.setZoom(20);
              console.log(location)

          }

        });
        locSearch=false;
        return;
      }
      locSearch=false;
      fetchselection(searchtxtObj);
//      var parseSearch=parseQueryJson(searchOptions);
      var parseSearch=elasticQuery(searchOptions)
      if(minimizemode)
      {
        //minmizeSelection=getActionParams().Searchjson;
        minmizeSelection=null;
      }
      /*if($('#root .regular').size()==0)
               {
                    
                    Notify('Search Filter Error','please select the search filter','normal',true)
                    return false;
               }*/
               
      isSearchFilter=true;
               if(rootSelection=='weather'||rootSelection=='flow')
               {
                    firstload = false;
               }
               else
               {
                    firstload = true;
               }
               if(lastSearchList.length<5)
               {
                    var jsondata=JSON.stringify(searchOptions);
                    lastSearchList.push(jsondata);
               }
               else
               {
                    lastSearchList.shift();
                    var jsondata=JSON.stringify(searchOptions);
                    lastSearchList.push(jsondata);
               }
      // if(settings.subzone.length>0)
      // {
      //   $('#zonesettingsArea #zoneSelection .potable .label').each(function(key){
      //     var value=$(this).attr('id')
      //     region(value,0);
      //   })
      //   $.each(settings.subzone,function(key,value){
      //     region(value,1);
        
      //   });
        
        
      // }
      var response={};
      var pURL='controllers/proxyman.php';
      $.ajax({
        type: "POST",
        url: pURL,
        //contentType: "application/json",
        data: {search:true,"SearchQuery":parseSearch},
        dataType:"json"
      }).done(function( Dataresponse ) {
        response.data=[];

        $.each(Dataresponse.response.hits.hits,function(key,value){
          response.data.push(this._source)
        });
        
        if(objectSize(Dataresponse.response.hits.hits)<=0)
        {
          preLoader('Loading...','modal',false)
          Notify('Data Fetch Error','No data available','normal',true)
          return;
        }
        else {
          //console.log(response)
          clearMarkers(true);
          // zonesarr.splice(0,zonesarr.length);
          // zonesInfo.splice(0,zonesInfo.length);
          MarkerInfoData=response;
          plotIcons(response,false);
          //loadDMAZones();
          //dma.loadzones();
          preLoader('Loading...','modal',false)
        }
      });
    });


// tooltip

   
    

    objsize=objectSize(Parentconfig.urlParams)
    if(objsize>1)
    {
      if('widget' in Parentconfig.urlParams)
      {
        var widdata=Parentconfig.urlParams['widget'];
        var json_str = localStorage.Widget;
        cookieData=JSON.parse(json_str);
        $.each(cookieData,function(key,value){
          
          if(value.hashID==widdata)
          {
            if(value.towhere=='demand')
            {
              widgetData=value;
              forcastData()
            }
          }
        });
      }
    }

}

function _listview()
{
  $('.listview').draggable({ containment: "window", scroll: false });
         $( ".listview" ).on( "drag", function( event, ui ) {

               $(this).css('margin', 'auto');
         } );
          $('.listview').on('click','.Lblock',function(){
               var idx = $(this).data('id');
               console.log($(this).hasClass('selected'))
               if($(this).hasClass('selected')){

                    if(group[idx].length <= 1)
                    {
                         smeter=$(this).data('smeter')
                         removeunitID(smeter)
                         var curid=$(this).attr('id')
                         $('.listview #'+curid).removeClass('selected');
                    }
                    //$(this).animate({width:'100px'}).css('height','20px');
                    //$(this).find('.innerList').remove();
                    if(!allmode)
                    {
                        /* if(group[idx].length==1)
                              unitObj.selectedUnit.splice(unitObj.selectedObj.indexOf(metadata[group[idx][0][3]]._id),1)
                         else
                         {
                             for(var j=0;j<group[idx].length;j++){
                                   unitObj.selectedUnit.splice(unitObj.selectedObj.indexOf(metadata[group[idx][j][3]]._id),1)      
                              }
                         }*/
                    }
                    else
                    {

                    }     
               }
               else{

                   if(group[idx].length > 1){

                          HTMLcontent=generateHTML(idx)
                         $(this).wrapInner( "<div class='Builddisplay'></div>" );
                         $(this).animate({width:'100%'}).css('height','auto'); 
                         var html = '<div id="IN'+idx+'"><div class="ClosecurrentList" id="ClosecurrentList">X</div><div class="innerList">';
                         /*for(var i=0; i<group[idx].length;i++){
                         var name = metadata[group[idx][i][3]].customer_name || metadata[group[idx][i][3]].customername || metadata[group[idx][i][3]].bp_name || metadata[group[idx][i][3]]._id;
                         html += '<li>'+name+'</li>';
                          }*/
                          html+=HTMLcontent;
                        html += '</div></div>';
                        $(this).append(html);

                    }
                    else
                    {
                       smeter=$(this).data('smeter')
                        unitTemp.push(smeter);
                        unitObj.selectedUnit=unitTemp;
                    }

                    $(this).addClass('selected');
                    
               }
               
               console.log(unitObj)
          });
  $('.listview').on('click','#ClosecurrentList',function(e){
               e.stopPropagation();
               removeThis=$(this).parent().attr('id')
               animateparent=$(this).parent().parent().attr('id')
               $('#'+removeThis).remove();
               $('#'+animateparent).animate({width:'100px'}).css('height','20px');
               $('#'+animateparent).removeClass('selected')
  });
          
}


function _selectAll()
{
  $('#selectAll').click(function(){

         // clearMarkers(true)
         //  if(rootSelection=='weather')
         //        firstload = false;
         //  else
         //       firstload = true;
         if($(this).hasClass('onmode'))
         {
               $(this).removeClass('onmode')
         //      plotIcons(MarkerInfoData, false);
               allmode=false;
               delete BuildingSel['allmode'];
               resetObj();
         }
         else
         {
               $(this).addClass('onmode')
         //      plotIcons(MarkerInfoData, true);
               //showAction('allmode','');
               allmode=true;
               BuildingSel['allmode']=true;
         }
         // showAction()
        /* if(!allmode)
         {
               plotIcons(MarkerInfoData, true);
               //showAction('allmode','');
               allmode=true;
         }
         else
         {
               plotIcons(MarkerInfoData, false);
               allmode=false;
         }*/
         //showAction('allmode','');
         selectAll();
     })
}
function addtags()
     {
          //console.log(searchOptions)
          $( '.taglist').empty();
          $.each(searchOptions,function(key,value){
               
               if(key!='text')
               {
                    if(isObject(value))
                    {
                        // console.log(objectSize(value))
                         $.each(value,function(key1,value1){

                               if(isObject(value1))
                               {
                                   /*$.each(value1,function(key2,value2){
                                        console.log(value2)
                                   })*/
                               }
                               else{
                                   tag='<div class="tags" id="'+value1+'" data-parent='+key1+'><div class="labl">'+key1+': '+value1+'</div><div class="closeTag">x</div></div>';
                                   $( '.taglist').append(tag); 
                               } 
                         
                         })    

                    }
                    else
                    {
                         tag='<div class="tags" id="'+value+'" data-parent='+key+'><div class="labl">'+key+': '+value+'</div><div class="closeTag">x</div></div>';
                         $( '.taglist').append(tag);  
                    }
                    
                    
                   
               }
          });
     }  
     

     function clearTags(){
          $('.search_options .label, .sub_search_options .label').each(function(){
               if($(this).hasClass('regular'))
               {
                      $(this).removeClass('regular').addClass('secondary');
                      var thisID=$(this).attr('id');
                      var thisParentID=$(this).parent().attr('id');
                      var $elems = $('.taglist').find('#'+thisID);
                      var length = $elems.length;
                      if(length>0)
                      {
                        /*closeID=$(this).attr('id'); 
                        closeParentID=$(this).parent().attr('id');
                        console.log(closeParentID)*/
                        $('.taglist').children('#'+thisID).remove();
                       // console.log('remove')
                        //$('#'+closeParentID+' #'+closeID).toggleClass('regular').toggleClass('secondary');
                      }
               }
          });

     }

function LoadmetersFactors(meter,id)
{
    point='updatemeta/meterfactor/getMeterFactorData?&meterId='+meter;
    var pURL='api.php?tag=loadMeter';
        $.ajax({
                      type: "POST",
                      url: pURL,
                      data: {"tag":'loadMeter','mtr':meter},
                      dataType:"json",
                      contentType: 'application/x-www-form-urlencoded',
         }).done(function( Dataresponse ) {
              html='';
              $.each(Dataresponse.DATA,function(key,value){
                if(key==Dataresponse.DATA.length-1)
                {
                  say='lastdata';
                }
                else
                {
                  say='';
                }
                if(value.valid_till!="null")
                {
                  Edata=formatEpochToDate(value.valid_till)
                }
                else
                {
                  Edata=null;
                }
                html+='<div class="Fitems entries '+say+'" id="'+value.mtrid+'" data-uid="'+value._id+'"><div class="small-3 columns padd st" id="'+value.valid_from+'">'+formatEpochToDate(value.valid_from)+'</div><div class="small-3 columns padd et" id="'+value.valid_till+'">'+Edata+'</div><div class="small-3 columns fact" id="'+value.meter_factor+'"><input type="text" value="'+value.meter_factor+'" id="Itemfactor" readonly/></div><div class="small-3 columns"></div></div></div>';
              })
               html+='<div class="Fitems" style="display: block; height: 50px;"><div class="small-3 columns" id="start"><input type="text" value="" id="datepicker" readonly/></div><div class="small-3 columns" id="end">&nbsp;</div><div class="small-3 columns" id="meter"><input type="text" value="" id="factor" readonly/></div><div class="small-3 columns" ><div class="AddFactor button tiny" id="addfactor">Add Factor</div><div class="savefactor button tiny" id="savefactor" style="display:none">Save Factor</div></div></div></div>';
              $(id).empty();
              $(id).append(html);
              $('.entries .columns:last-child').append('<div class="savefactor button tiny" id="updateFactor">updateFactor</div><div class="savefactor button tiny" id="updatesaveFactor" style="display:none">Save Factor</div>');
              $( "#datepicker" ).datepicker();
              $('#addfactor').click(function(e){
                e.preventDefault();
                $(id+' input').removeAttr("readonly");
                $(this).hide();
                $('#savefactor').show();

              });
              $('#savefactor').click(function(e){
                e.preventDefault();
                var fromDate = $('#datepicker').val();
        				var factor = $('#factor').val();
        				if(fromDate && factor) {				
        					var fromDateInEpoch = convertDateStringToEpoch(fromDate);
        					var jsonData = {mtrid : meter, meter_factor : factor, valid_from : fromDateInEpoch, valid_till : null};
                  dt=JSON.stringify(jsonData);
        				} else {
        					alert("Please enter Meter Factor");
        				}
                
                var pURL='api.php?tag=saveFactor';
                previousId= $('.Fitems.lastdata').attr('id');
                    $.ajax({
                      type: "POST",
                      url: pURL,
                      data: {"myData":dt,'previousID':previousId},
                      dataType:"json",
                      contentType: 'application/x-www-form-urlencoded',
                    }).done(function( Dataresponse ) {
                        console.log(Dataresponse)
                        Notify('Edit Meta',Dataresponse.result,'normal',false)
                    });

              });
              $('#updateFactor').click(function(e){
                    e.preventDefault();
                    $('.lastdata #updateFactor').hide();
                    $('.lastdata #updatesaveFactor').show();
                    $('.lastdata input').removeAttr("readonly");

              });
              $('#updatesaveFactor').click(function(e){
                    e.preventDefault();
                    Tid=$(this).parent().parent().data('uid');
                    mtrid=$(this).parent().parent().attr('id');
                    st=$('#'+mtrid+' .st').attr('id');
                    et=$('#'+mtrid+' .et').attr('id');
                    fact=$('#'+mtrid+' #Itemfactor').val();
                    var jsData = {_id:Tid, mtrid : mtrid, meter_factor : fact, valid_from : st, valid_till : et};
                    console.log(jsData);
                    dt=JSON.stringify(jsData);
                    var pURL='api.php?tag=updateFactor';
                    $.ajax({
                            type: "POST",
                            url: pURL,
                            data: {"myData":dt,'previousID':''},
                            dataType:"json",
                            contentType: 'application/x-www-form-urlencoded',
                          }).done(function( Dataresponse ) {
                              console.log(Dataresponse)
                              Notify('Edit Meta',Dataresponse.result,'normal',false)
                    }); 

                    $('.lastdata #updateFactor').show();
                    $('.lastdata #updatesaveFactor').hide();
                    $('.lastdata input').attr("readonly","true");
              });
                        
         });
}
function convertDateStringToEpoch(dateString)
{
	var dateValue = dateString.split("/");
	var epoch = new Date(dateValue[2], dateValue[0] - 1, dateValue[1]).getTime() / 1000;
	return epoch;
}

function formatEpochToDate(epoch)
{
	var date = new Date(0); // The 0 there is the key, which sets the date to the epoch
	date.setUTCSeconds(epoch);
	var formattedDate = date.getUTCDate() + '/' + (date.getUTCMonth() + 1)+ '/' + date.getUTCFullYear()
	return formattedDate;
}
var addAppsButton=function (){
  var finalList = _.filter(havePermission[0].scripts, function(num){ return num.location == 'sidebar'; });
  console.log(finalList)
  if(finalList.length>0)
  {
      html='<div class="sideapps sidebutton" style="display:none;"><div class="leftgmapbtn">A\
            <div class="sideAppsitems"><ul>';
            $.each(finalList,function(key,value){
                html+='<li><div class="" id="'+value.alias+'" onclick="'+value.init+'.init(this)" style="  width: 85%;  float: left;"><span >'+value.AppName+'</span></div><div class="" style="  width: 15%; float: right; border-left: 1px solid #666; display:none;" onclick="'+value.init+'.settings()"><img src="images/minisettings.png" width="15"/></div></li>'
            })
      html+='</ul></div>\
          </div></div>';
      var controldiv = document.createElement('div');
      controldiv.innerHTML = html;
      map.controls[google.maps.ControlPosition.LEFT_TOP].push(controldiv);
      var pops=false
      $(document).off().on('click','.sideapps',function(e){
          if(e.target.className=='leftgmapbtn')
          {
            if(pops)
            {
              $('.sideapps .sideAppsitems').removeClass('sel');
            }
            else
            {
              $('.sideapps .sideAppsitems').addClass('sel');
            }
            pops=!pops
          }
      })
  }
 
}
function groupMeterbycustomer(meterList,flag)
{
    var Bool=flag||true;
    var customers = [];
    var groupcustomers = [];
    var cust_id = [];
    var groupids = [];
    var qids = [];
    var quality = [];
    var removed = [];
    var cust = null, meta=null;
    finaldata={};
    if(rootSelection=='weather')
    {
      return 'weather';
    }
    else
    {
      console.log(meterList);
      $.each(meterList,function(key,value){
        meta=value
          if(value.tag_sector=='weather')
          {

          }
          else
          {
             
            cust = meta.bp_name || meta.customer_name || meta.customername || meta.display_customer_name || value._id;
            customers.push(cust);
            cust_id.push(value._id);
          }

      });

    groupcustomers = _.uniq(customers);
    _.each(groupcustomers,function(el,il){
      if(el == ''){
        groupids.push([]);
        return;
      }
      var idx = []; var qx = [];
      var cd = customers.indexOf(el);
      if(cd>=0){
        idx.push(cust_id[cd]);
        customers[cd] = '';
      }
      while(customers.indexOf(el)>=0){
        idx.push(cust_id[customers.indexOf(el)]);
        customers[customers.indexOf(el)] = '';
      }
      groupids.push(idx);
    
    });
    finaldata.customers=groupcustomers;
    finaldata.groupids=groupids;  
    return finaldata;
    }

}
function addicons(){
        $('#more_options_menu').empty();
        $('#more_options_menu #more_option').hide();
        //$('#more_options_menu').append('<div class="sub_menu_close_btn btn_sec">x</div>');
        $('#more_options_menu').append('<br />');
        //<div class="icons"><img src="images/calculator.png" width="20" alt="" id="meter" title="Meter Types"/></div><div class="icons"><img src="images/newspaper-alt.png" width="20" alt="" id="rooms" title="Room Types"/></div>
        $('#more_options_menu').append('<div class="icon_Menu"><div class="icons"><img src="images/map.png" alt="" width="20" id="zone" title="Zone Types"/></div><div class="icons"><img src="images/office.png" width="20" alt="" id="BuildingType" title="Buildings Type"/></div><div class="icons"><img src="images/grid.png" width="20" alt="" id="clusters" title="clusters"/></div><div class="icons"><img src="images/flow-tree.png" width="20" alt="" id="flowMeters" title="flow meters"/></div></div>');
               //<div class="icons"><img src="images/flow-children.png" width="20" alt="" id="compound" title="compound meter options"/></div>
        $('.icon_Menu .icons').click(function() {
            var clickIcon=$(this).children().attr('id');
            var clickheight=$('#'+clickIcon+'_options').data('height');
              $('.sub_search_options').hide(); 
                         //$(this).removeClass('addFindex');
              $('.sub_search_options').animate({top: '220px'}, 200,function() {
              //,height:'100px'
                              
                $('#'+clickIcon+'_options').show();
                $('#'+clickIcon+'_options').animate({top: '230px'}, 200,function() {
                       //$(this).addClass('addFindex');
                });  
              });
           });
}

//  polyselection method
function  addPoint(event){
  if(!lasso_toggle)
    return;
  path.insertAt(path.length, event.latLng);
      var marker = new google.maps.Marker({
          position: event.latLng,
          map: map,
          draggable: true
        });
    p_markers.push(marker);
    marker.setTitle("#" + path.length);
    
    google.maps.event.addListener(marker, 'click', function() {
      
    marker.setMap(null);
      for (var i = 0, I = p_markers.length; i < I && p_markers[i] != marker; ++i);
      p_markers.splice(i, 1);
        path.removeAt(i);
    });

      google.maps.event.addListener(marker, 'dragend', function() { 
      for (var i = 0, I = p_markers.length; i < I && p_markers[i] != marker; ++i);
      path.setAt(i, marker.getPosition());
    });
}
function  lassoSelect(){
  if(p_markers.length <=  2)
    {
      alert('Please make a selection');
      return false;
    }
  for(i=0;i<markers.length;i++)
          {
            if(!markers[i].visible)
              continue;
            var coords  = fetchPos(markers[i]);
            var coordinate  = new google.maps.LatLng(coords[0],coords[1]);
            var isWithinPolygon = polySelect.containsLatLng(coordinate);
//            console.log(isWithinPolygon);
            if(isWithinPolygon)
              {
                // add this group to tray
                var ret = addGroup(markers[i].groupindex); //metaindex
                if(!ret){
                  alert('Selection includes Mixed Types');
                  //resetPoly();
                  return;
                }

              }
                
          }
          //  reset everything  
          resetPoly();
}

function addGroup(index){
  var ret = true;
  lossoMode=true
  _.each(group[index],function(g){
    if(unitObj.selectedUnit.indexOf(g[2])<0){
      if(!checkBeforePush(g[2]))
          ret = false;
      else{
        if(ret){
          unitObj.selectedUnit.push(g[2]);
          unitObj.ids[g[2]]=index;
          unitObj.sensorSelection[g[2]]=[]
          unitObj.sensorMeta[g[2]]=metadata[g[3]]
          unitObj.lastselected.meter=g[2];
          unitObj.lastselected.index=index
          // unitObj.lastselected.meter=unitObj.selectedUnit[unitObj.selectedUnit.length-1];
          // unitObj.lastselected.index=unitObj.ids[unitObj.lastselected.meter]
          // $.each(actionListObj.UnitOptions,function(key2,value2){
          //     if(value2.type.indexOf(lastknownType)>-1)
          //     {
          //       unitObj.sensorSelection[g[2]].push(value2.sensortype_actual)
          //     }
          // })
        }
      }
    }
  });
  refreshTray();
  showAction('unit');
    return ret;
}

function resetPoly(){
  for(j=0;j<p_markers.length;j++)
              p_markers[j].setMap(null);
          polySelect.setMap(null);
          p_markers.splice(0,markers.length);
          polySelect = new google.maps.Polygon({
            strokeWeight: 3,
            fillColor: '#5555FF'
          });
          path = new google.maps.MVCArray;
           polySelect.setPaths(new google.maps.MVCArray([path]));
          polySelect.setMap(map);
          google.maps.event.addListener(polySelect,'click',lassoSelect);
}
function lassoToggle(x){
  lasso_toggle = !lasso_toggle;
  lossoMode=!lossoMode;
  $(x).toggleClass('button_press');
  if(!lasso_toggle)
  {
     $('body #helpUILayout').remove();
     
  }
  if(!lasso_toggle && p_markers.length>=1)
    resetPoly();
}
function  fetchPos(x){
  var rawObject = x.getPosition();
  var coords  = [];
  for (var  i in rawObject)
    if(!isNaN(rawObject[i]))
      coords.push(rawObject[i]);
  return  coords;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    //var path="; path=/"+Parentconfig.folder;
    document.cookie = cname + "=" + cvalue + "; " + expires+path;
}

function conditionalFilter(){

      var condition=searchFilter.getCondition();
      fetchselection(searchtxtObj)
      // if(searchOptions.settings.zoneSelection.zoneSelection.length==0)
      // {
      //   Notify('Data Search!','Please select atleast one zone to perform data search','normal',true);
      //   return false;
      // }
      preLoader('Loading...','modal',true)
      mySearchObject={"text":[],"filterType":[],"parent_options":{"customerType":[]},"weather_options":{"weatherparams":[]},"sensor_options":{"sensorparams":[]},"scade_options":{"flowMeters":[]},"wems_options":{"wemstype":[]},"nonresidential_options":{"commercialType":[]},"residential_options":{"ResidentArea":[]},"settings":{"watertype":{"watertype":["potable"]},"zoneSelection":{"zoneSelection":[]},"ZoneCluster":{"ZoneCluster":[]}}}
      mySearchObject.filterType.push(condition.basetype)
      mySearchObject.settings.watertype.watertype=searchOptions.settings.watertype.watertype
      $.each(searchOptions.settings.zoneSelection.zoneSelection,function(k,v){
        mySearchObject.settings.zoneSelection.zoneSelection.push(v)
      })
      $.each(searchOptions.settings.ZoneCluster.ZoneCluster,function(k,v){
        mySearchObject.settings.zoneSelection.zoneSelection.push(v)
      })
      veecheck=veecheck||false;
      var paramsJs={}
      paramsJs.query=encodeURIComponent(elasticQuery(mySearchObject));
      paramsJs.qtype='source';
      paramsJs.collection='station';
      paramsJs.username=Parentconfig.user.response.UserEmail;
      paramsJs.condition=encodeURIComponent(JSON.stringify(condition.condition));
      paramsJs.st=st;
      paramsJs.et=et;
      paramsJs.intervalmin=interval;
      paramsJs.interval=_interval;
      paramsJs.includepredicted=pred
      paramsJs.exclude='';
      paramsJs.includedays='';
      paramsJs.publicholidayonly=''
      paramsJs.excludedates=''
      paramsJs.turnoffvee=veecheck
      paramsJs.timezone=Parentconfig.userzone;
      paramsJs.includeflag=flagsbit
      paramsJs.turnoffglobalexclusion=globalex
      pt=JSON.stringify(paramsJs)
      var paydata='query='+paramsJs.query+'&qtype=source&collection=station&username='+paramsJs.username+'&condition='+paramsJs.condition+'&st='+paramsJs.st+'&et='+paramsJs.et+'&intervalmin='+paramsJs.intervalmin+'&interval='+paramsJs.interval+'&includepredicted='+paramsJs.includepredicted+'&exclude='+paramsJs.exclude+'&turnoffvee='+paramsJs.turnoffvee+'&timezone='+paramsJs.timezone+'&turnoffglobalexclusion='+paramsJs.turnoffglobalexclusion;
      var Queryurl=parsedURL.protocol+'://'+parsedURL.domain+'/'+parsedURL.path+'node/meta/search/advance/';
      // console.log(paydata)
      var url='controllers/curlProxy.php?tag=postData';
            
      process(
        url, 
        'POST',
        { 
          "dataType":"appJson",'url':Queryurl,'payload':paydata
        }, 
        function(Dataresponse) { 
            response.data=[];

            $.each(Dataresponse.response.hits.hits,function(key,value){
              response.data.push(this._source)
            });
            
            if(objectSize(Dataresponse.response.hits.hits)<=0)
            {
              preLoader('Loading...','modal',false)
              Notify('Data Fetch Error','No data available','normal',true)
              return;
            }
            else {
              removeAllEmpty();
              clearMarkers(true);
              MarkerInfoData=response;
              plotIcons(response,false);
              preLoader('Loading...','modal',false)
            }
        },
        function(e) {
            console.log(e);
        }
    );
}
      

