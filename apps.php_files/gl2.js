( function(window,undefined){
      "use strict";           // Do it the right way :)
    function Excluder(){

      var privates = {
        ui:null,
        st:window.st,
        et:window.et,
        data:[],
        excludes:[],
        excludes_timestamp:[],
        resultant:[],
        saveobj:null,
        potable:null
      };
      this.shout = function (){ console.log(privates); }

      this.init = function(){
        privates.ui=null;
        privates.st = window.st;
        privates.et = window.et;
        privates.data=[];
        privates.excludes=[];
        privates.excludes_timestamp=[];
        privates.saveobj=null;
        privates.resultant=[];
        potable:null;
        load.call(this);
      }

      this.save = function(){
        var div = $('.customermeters li');
        if(div.length === 0){
          Notify('Global Exclusions','No Meters found','normal',true);
          return;
        }
        if(privates.saveobj === null){
          Notify('Global Exclusions','No Time Range found','normal',true);
          return; 
        }
        var o = privates.saveobj;
        var meters = [];
        _.each(div,function(l){
          if($(l).hasClass('removeme'))
            return;
          meters.push($(l).text());
        });
        
        o.meters = meters;
        if(!$('#appModal .modalcontent .showopt').hasClass('hide')){
          o.op = parseInt($('#appModal .modalcontent .showopt input:radio[name=showopt]:checked').val());
          if(isNaN(o.op))
            o.op = 0; // exclusion mode default
        }

        privates.resultant.push(o);
        $('#appModal').foundation('reveal','close');
        switch(o.method){
          case 0:   // Single Day
            $(o.el).addClass('bg');
          break;
          case 1:
            var index = $(o.el).index();
            var total_rows = $('#'+privates.ui+' tr');
            _.each(total_rows,function(tr){
              var el = $(tr).children('td:eq('+index+')');
              var hrs = $(el).find('.col');
              _.each(hrs,function(h){
                $(h).addClass('bg');
              });
            });
          break;
          case 2:
            var hrs = $(o.el).find('.col');
            _.each(hrs,function(h){
                $(h).addClass('bg');
              });
          break;
        }
      }

      this.cancel = function(){
        $('#appModal').foundation('reveal','close');
        // $('.customermeters li').off();
        privates.saveobj = null;
      }

      /*
          privates  
                     */
      var temp = {
        day:'<th><% for(var i=0;i<24;i++) { %>  <%= thisday %> <div class="col"> <%= i %>:00</div> <% } %></th>',
        day2:'<th data-id="<%= thisunix %>"> <%= thisday %><br/><div class="thincl">Include</div><div class="thexc">Exclude</div></th>',
        day3:'<td data-id="<%= thisday %>"><% for(var i=0;i<24;i++) { %> <div class="col"><%= i %></div> <% } %></td>',
        customer:'<tr><td><%= cust %></td>',
        modal:'<h4><%= name %></h4><ul class="customermeters"><% _.each(meters,function(m){ %><li><%= m %></li> <% }) %></ul><p>Start: <%= st %><br/>End: <%= et %></p><div class="button tiny save" onclick="gl.save()">OK</div><div class="showopt hide"><label>Exclude <input name="showopt" type="radio" value="0"/></label><label>Include <input type="radio" name="showopt" value="1"/></label></div><div class="button tiny cncl" onclick="gl.cancel()">CANCEL</div>'
      }; 

      var load = function(){
        var time = {st:st,et:et};
        var self = this;
        preLoader('Loading AMA','modal',true);
        $.post('controllers/sensorquality.php',{params:'ama_daily',type:JSON.stringify(time)},
          //$.getJSON('data/dump.json',
          function(d){
            privates.ui = logWindow('control',null,{display:'Global Exclusions'});
            privates.potable = d.potable;
            $('#'+privates.ui+' .topheader').append('<div class="okbutton button tiny">Save Selection</div>');
            /*  Get Excluded  */
            excluded_ui.call(self);
          },'json');
      }
      var excluded_ui = function(){
          var self = this;
          privates.saveobj=null;
          privates.resultant=[];
          privates.excludes=[];
          privates.excludes_timestamp=[];
          privates.data = [];
          $('#'+privates.ui+' .wrap').remove();
          preLoader('Loading Excludes','modal',true);
          $.getJSON('controllers/proxyman.php',{endpoint:'globalexclusion/getallexclusions',getrequest:true,params:''},function(e){
            //$.getJSON('data/dump2.json',function(e){
            console.log(e);
            preLoader(false);
            if(e.errorCode>0){
              Notify('Global Exclusion','Server Error','normal',true);
              return;
            }
            _.each(e.response,function(r){
              var _idx = privates.excludes.indexOf(r.stationId);
              if(_idx<0)
              privates.excludes.push(r.stationId);
              if(r.et === null)
                r.et = et;
              if(r.st === null)
                r.st = st;
              if(_idx<0)
                privates.excludes_timestamp.push([{st:r.st,et:r.et}]);
              else
                privates.excludes_timestamp[_idx].push([{st:r.st,et:r.et}]);
            });
          /*
            get number of days 
                                */
                var potable = privates.potable;
                var dates = null;
                var cnt = 0;
                for(var key in potable){
                  if(cnt > 0)
                    break;
                  for(var key2 in potable[key]){
                    if(cnt > 0)
                      break;
                    dates = potable[key][key2][0]['incomplete'];
                    ++cnt;
                  }
                }
            /*  days end  */
            var html = '<div class="wrap"><table class="glexclusion_tbl"><thead><tr><th>Customer</th>';
            var incomplete_arr = [];
            _.each(dates,function(d,key){
              html += _.template(temp.day2,{thisday:moment(key).format('MMM Do YYYY'),thisunix:moment(key).unix()*1000});
              incomplete_arr.push(null);
            });
            html += '</thead><tbody>';
            // Headers end <-->
            _.each(potable,function(p,key){
              _.each(p,function(cust,customer){
                var arr = {};
                var arr_count = {};
                var meters = [];
                _.each(cust,function(c){
                  meters.push(c.station);
                  _.each(c.incomplete,function(day,idx){
                      if(idx in arr)
                        arr[idx] = arr[idx] + day;
                      else
                        arr[idx] = day;

                      if(idx in arr_count)
                        arr_count[idx] = arr_count[idx] + 1;
                      else
                        arr_count[idx] = 1;
                  });
                });
                _.each(arr,function(day,idx){
                  if(arr_count.hasOwnProperty(idx))
                    arr[idx] = arr[idx] / arr_count[idx]
                });
                var isred = '';
                var timearr = [];
                _.each(meters,function(m){ 
                  var pdx = privates.excludes.indexOf(m);
                  if(pdx >= 0){
                    console.log('found');
                    isred = 'red';
                    timearr.push(privates.excludes_timestamp[pdx]);
                  } 
                });
                html += '<tr data-id="'+privates.data.length+'"><td class="main '+isred+'">'+customer+'<br/><div class="trincl">Include</div><div class="trexc">Exclude</div></td>';
                _.each(arr,function(a,_hourkey){
                    var bg = 'green';
                    var quality = parseFloat(a).toFixed();
                    if(quality<90)
                      bg = 'red';
                    html += '<td><ul class="glst"><li class="info '+bg+'">'+quality+'</li><li class="boxexc">Exclude</li><li class="boxinc">Include</li></ul>';
                    for(var i=0;i<24;i++){
                      var thishour = (new moment(_hourkey).unix()*1000) + (i*60*60*1000);
                      var isred = '';
                      if(timearr.length>0)
                        isred = findrange.call(self,thishour,timearr);
                      html += '<div class="col '+isred+'" data-id="'+thishour+'">'+i+'</div>';
                    }
                    html += '</td>';
                  });
                privates.data.push(meters);
                html += '</tr>';
              });
            });
            html += '</table></div>';
            $('#'+privates.ui).append(html);
            $('#'+privates.ui+ '.topheader').css('padding-top','0px');
            $('#'+privates.ui+ '.topheader').prepend('<span class="txt noact">Global Exclusion</span>');
            uievents.call(self);
            }); //dump end
      }
      var findrange = function(hour,range){
        var ret = '';
        range = _.flatten(range);
        _.each(range,function(r){
          if(hour >= r.st && hour <= r.et){
            ret = 'red'
            console.log('hour matches');
          }
        });
        return ret;
      }
       var uievents = function(){
        var self = this;
        $('tr td .col').click(function(e){
          // one hour one customer
          var mux = $(this).data('id');
          var hour = mux;
          var hour_end = hour + (1*59*60*1000);
          var idx = $(this).parents('tr').last().data('id');
          var meters = privates.data[idx];
          console.log(meters);
          console.log(hour); 
          console.log(hour_end);
          request.call(self,{st:hour,et:hour_end,meters:meters,el:this},0,0,'show');
          if (!e) var e = window.event;
            e.cancelBubble = true;
          if (e.stopPropagation) e.stopPropagation();
        });

        $('.thincl').click(function(e){
          // all customers one day
          var el = $(this).parent('th').last();
          var day = $(el).data('id');
          var day_end = day + (86399*1000);
          var meters = _.flatten(privates.data);
          request.call(self,{st:day,et:day_end,meters,el:el},1,1);
          if (!e) var e = window.event;
            e.cancelBubble = true;
          if (e.stopPropagation) e.stopPropagation();
        });
        $('.thexc').click(function(){
          var el = $(this).parent('th').last();
          var day = $(el).data('id');
          var day_end = day + (86399*1000);
          var meters = _.flatten(privates.data);
          request.call(self,{st:day,et:day_end,meters,el:el},1,0);
          if (!e) var e = window.event;
            e.cancelBubble = true;
          if (e.stopPropagation) e.stopPropagation();
        });
        $('.trincl').click(function(){
          var idx = $(this).parents('tr').last().data('id');
          var meters = privates.data[idx];
          request.call(self,{st:privates.st,et:privates.et,meters,el:$(this).parents('tr').last()},2,1);
          if (!e) var e = window.event;
            e.cancelBubble = true;
          if (e.stopPropagation) e.stopPropagation();
        });
        $('.trexc').click(function(e){
           var idx = $(this).parents('tr').last().data('id');
          var meters = privates.data[idx];
          request.call(self,{st:privates.st,et:privates.et,meters,el:$(this).parents('tr').last()},2,0);
          if (!e) var e = window.event;
            e.cancelBubble = true;
          if (e.stopPropagation) e.stopPropagation();
        });
        $('.boxinc').click(function(){

        });
        $('.boxexc').click(function(e){
          var idx = $(this).parents('tr').last().data('id');
          var meters = privates.data[idx];
          var prnt = $(this).parents('td').last();
          var myidx = $(prnt).index();
          console.log(myidx);
          var day = $('#'+privates.ui+' th:eq('+myidx+')').data('id');
          var day_end = day + (86399*1000);
          request.call(self,{st:day,et:day_end,meters,el:$(this).parents('td').last()},2,0);
          if (!e) var e = window.event;
            e.cancelBubble = true;
          if (e.stopPropagation) e.stopPropagation();
        });

        $('.boxinc').click(function(e){
          var idx = $(this).parents('tr').last().data('id');
          var meters = privates.data[idx];
          var prnt = $(this).parents('td').last();
          var myidx = $(prnt).index();
          console.log(myidx);
          var day = $('#'+privates.ui+' th:eq('+myidx+')').data('id');
          var day_end = day + (86399*1000);
          request.call(self,{st:day,et:day_end,meters,el:$(this).parents('td').last()},2,1);
          if (!e) var e = window.event;
            e.cancelBubble = true;
          if (e.stopPropagation) e.stopPropagation();
        });

        $('.okbutton').click(function(){
          preLoader('Saving your settings..','modal',true);
          var count = privates.resultant.length;
          _.each(privates.resultant,function(r,idx){
            var ep = '';
            if(r.op === 0)
              ep = 'globalexclusion/saveexclusion';
            else
            if(r.op === 1)
              ep = 'globalexclusion/deleteexclusion';
            if(ep === ''){
              Notify('Global Exclusion','Endpoint Confusions','normal',true);
              return;
            }

            $.ajax({
              url:'controllers/proxyman.php',
              type:'GET',
              data:
                {
                  endpoint:ep,
                  params:'st='+r.start+'&et='+r.end+'&reason=testing&sensortype=CONSUMPTION&stationid='+r.meters.toString()
                },
              async:true,
              success:
                function(e){
                  console.log(e.response + ' -- '+idx);
                  --count;
                  if(count === 0){
                    $('#'+privates.ui+' .wrap *').off();
                    $('#'+privates.ui+' .okbutton').off();
                    $('#'+privates.ui+' .wrap').remove();
                    excluded_ui.call(this);
                    preLoader(false);
                  }
                },
              error:function(){
                --count;
                if(count === 0){
                    $('#'+privates.ui+' .wrap *').off();
                    $('#'+privates.ui+' .wrap').remove();
                    excluded_ui.call(this);
                    preLoader(false);
                  }
              }
            });
           
          });
          
        });
        // $('tr td').click(function(e){
        //   if($(this).hasClass('main')){
        //     alert('not supported');
        //     return;
        //     // all meters for this customer all time range
        //     var idx = $(this).parents('tr').last().data('id');
        //     var meters = privates.customermeters[idx];
        //     request.call(self,{meters:meters});
        //   }
        //   else{
        //     alert('not supported');
        //     return;
        //     // all meters for this customer one day
        //     var idx = $(this).parents('tr').last().data('id');
        //     var meters = privates.customermeters[idx];
        //     var day = $(this).parents('td').last().data('id');
        //     var day_end = parseInt(day) + (24*60*60*1000);
        //     request.call(self,{st:day,et:day_end,meters:meters});
        //   }
        //   if (!e) var e = window.event;
        //     e.cancelBubble = true;
        //   if (e.stopPropagation) e.stopPropagation();
        // });
       
        // $('th').click(function(e){
        //   alert('not supported');
        //   // all customers this day
        //   if (!e) var e = window.event;
        //     e.cancelBubble = true;
        //     if (e.stopPropagation) e.stopPropagation();
        // });
      }

      var request = function(obj,method,op,bit){
        console.log(obj);
        bit = bit || null;
        var st = obj.hasOwnProperty('st') ? obj.st : privates.st;
        var et = obj.hasOwnProperty('et') ? obj.et : privates.et;
        if(!obj.hasOwnProperty('meters')){
          Notify('Global Exclusion','Error - no meters','normal',true);
          return;
        }
        privates.saveobj = {start:st,end:et,el:obj.el,method:method,op:op};
        //privates.saveobj = obj;
        $('#appModal .modalcontent').empty().append(_.template(temp.modal,{st:moment(st).format(),et:moment(et).format(),meters:obj.meters}));
        $('#appModal').foundation('reveal','open');
        $('.customermeters li').off().click(function(){
          $(this).toggleClass('removeme');
        });
        if(bit != null && bit === 'show')
          $('#appModal .modalcontent .showopt').removeClass('hide');
      }

    } // end of constructor
    window.Excluder = Excluder;
     $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', './apps/globalexclusion/style_glexclusion.css') );
     $('#optiArea').append('<div class="mnitems"><div class="mnitemHold" id="excludergui"><div class="mnuicon"><img src="images/dma.png" width="22px" title="Global Exclusions"></div><div class="mnutitle">Global Exclusions</div></div><div class="mnuinfo"><img src="images/info_b.png" width="20px"></div>');
     $(document).on('click','#excludergui',function(){     gl.init();     });
})(window);

window.gl = new Excluder();