// GIS version 2.0
var map_context = null;

( function( window, undefined ) {
    "use strict";
    function  Gis(){
    // cache
    var privates = {
      elements : [],
      controlposition:google.maps.ControlPosition.LEFT_TOP,
      mapcenter_long : null,
      mapcenter_lat : null,
      foreignElements:{},
      map_context : null,
      refreshindex:0,
      autoload:false,
      colorscale: new chroma.scale(['red', 'orange', 'blue', 'green']).out('hex'),
      sw_long:null,
      sw_lat:null,
      ne_long:null,
      ne_lat:null,
      timer:null,
      mutex:false,
      labels:[]
    };

    var layer_types = {};
    var filter_types = {};
    // var layer_types = {
    //   pipe:[{name:'Service',bkp:'service'},{name:'Main',bkp:'main'},{name:'Fish River Main',bkp:'fish_river_main'},{name:'ICC Main',bkp:'Icc_main'}],
    //   valve:[{name:'Air Valves',bkp:'AIRV'},{name:'Scour Valves',bkp:'SCOR'},{name:'Stop Valves',bkp:'STVA'}]
    // };
    // var filter_types = {
    //   pipe:[{name:'Filter By Age',bkp:'byage'},{name:'Filter By Material',bkp:'bymaterial'}]
    // };


    var totallayers = [
      {layer:'pipe',state:null,type:null,operation:null,arr:[],hidden:false,index:Parentconfig.allbasictags.settings.pipe_index},
      {layer:'hydrant',state:null,type:null,operation:null,arr:[],hidden:false,icon:'images/mapicons/hydrant.png',index:Parentconfig.allbasictags.settings.layers_index},
      {layer:'valve',state:null,type:null,arr:[],paths:[],operation:null,hidden:false,index:Parentconfig.allbasictags.settings.layers_index},
      {layer:'junction',state:null,type:null,arr:[],operation:null,hidden:false,icon:'images/mapicons/node_normal.png',index:Parentconfig.allbasictags.settings.layers_index},
      {layer:'reservoir',state:null,type:null,arr:[],operation:null,hidden:false,icon:'images/mapicons/pink.png',index:Parentconfig.allbasictags.settings.layers_index},
      {layer:'meter',state:null,type:null,arr:[],operation:null,hidden:false,icon:'images/mapicons/yellow.png',index:Parentconfig.allbasictags.settings.layers_index},
      {layer:'bulkmeter',state:null,type:null,arr:[],operation:null,hidden:false,icon:'images/mapicons/purple.png',index:Parentconfig.allbasictags.settings.layers_index},
      {layer:'dam',state:null,type:null,arr:[],operation:null,hidden:false,icon:'images/mapicons/iconBlue.png',index:Parentconfig.allbasictags.settings.layers_index},
      {layer:'waterinfopoint',state:null,type:null,operation:null,hidden:false,icon:'images/mapicons/watermeter_p.png',arr:[],index:Parentconfig.allbasictags.settings.layers_index},
      {layer:'water_meter',state:null,type:null,operation:null,hidden:false,icon:'images/mapicons/watermeter_p.png',arr:[],index:Parentconfig.allbasictags.settings.layers_index}
    ];
    var activelayers = [];
    var visenti_pipe_material = ['DUCTILE_IRON','CAST_IRON','STEEL','COPPER','CAST_STEEL','UNLINED_CAST_IRON','ABESTOS_CEMENT','PVC'];
    var visenti_pipe_colors = ['#F00202','#F002B4','#4E02F0','#02F04A','#F2A705','#FF7500','#A605F2','#707880','#12594A'];
    /*
        Init  
              */
    if(map === null || map === undefined){
      alert('Error - Map not found');
      return;
    }

    var layers_enabled = Parentconfig.allbasictags.settings.layer_types;
    //layers_enabled.push('pipe');
    _.each(totallayers,function(t,idx){
      if(layers_enabled.indexOf(t.layer) >= 0)
        activelayers.push(t);
    });
    privates.elements = activelayers;
    if(Parentconfig.allbasictags.settings.hasOwnProperty('layer_sub_type')){
      var subtypes = Parentconfig.allbasictags.settings.layer_sub_type;
      _.each(subtypes,function(s){
        var obj = layer_types;
        obj[s.parent] = [];
        _.each(s.kids,function(k){
          obj[s.parent].push({name:k.toUpperCase(),bkp:k});
        });
      });
    }
    if(Parentconfig.allbasictags.settings.hasOwnProperty('layer_filter_type')){
      var filters_enabled = Parentconfig.allbasictags.settings.layer_filter_type;
      _.each(filters_enabled,function(f){
        var obj = filter_types;
        obj[f.parent] = [];
        _.each(f.kids,function(k){
          obj[f.parent].push({name:k.name,bkp:k.bkp});
        });
      });
    }

    /*  -- Init Ends -- */

    /*  Public Methods  */
    this.shout = function(){ console.log(privates); console.log(layer_types); console.log(filter_types);}

    this.loadLayer = function(name,cb){
      cb = cb || null;
      if(privates.mutex)
      {
        Notify('Please Wait','Loading a different GIS Layer. Kindly wait..','normal',true);
        if(cb!=null && typeof(cb) === 'function')
          cb.call(self);
        return;
      }
      if(!name || name == undefined || name == '')
      {
        Notify('Error','bad layer','normal',true);
        if(cb!=null && typeof(cb) === 'function')
          cb.call(self);
        return;
      }
      var layer_exists = false;
      var layer_number = null;
      _.each(privates.elements,function(el,idx){
        if(el.layer === name){
          layer_exists = true;
          layer_number = idx;
        }
      })
      if(!layer_exists){
        Notify('Error','Layer does not exist','normal',true);
        if(cb!=null && typeof(cb) === 'function')
          cb.call(self);
        return;
      }
      privates.mutex = true;
      //if(privates.elements[layer_number].arr.length>0)
        //clear_arr.call(this,layer_number);
      var server_query = query.call(this,layer_number);
      if(map.getZoom()<10)
        map.setZoom(10);
      preLoader('Loading '+name,'modal',true)
      try{
        fetch.call(this,server_query,name,layer_number,cb);
      }
      catch(e){
        Notify('Error','Unknown Error','normal',true);
        console.log(e);
        privates.mutex = false;
        preLoader(false);
        if(cb!=null && typeof(cb) === 'function')
          cb.call(self);
      }
    }

    this.clearLayer = function(name){
       if(!name || name == undefined || name == '')
      {
        Notify('Error','bad layer','normal',true);
        return;
      }
      var layer_exists = false;
      var layer_number = null;
      _.each(privates.elements,function(el,idx){
        if(el.layer === name){
          layer_exists = true;
          layer_number = idx;
        }
      })
      if(!layer_exists){
        Notify('Error','Layer does not exist','normal',true);
        return;
      }
      if(privates.elements[layer_number].arr.length>0)
        clear_arr.call(this,layer_number);
      if(name === 'pipe'){
        _.each(privates.labels,function(l){l.setMap(null)});
        privates.labels.splice(0,privates.labels.length);
      }
      privates.elements[layer_number].state = null;
    }
   
    this.refresh = function(){
      var x = privates.refreshindex;
      var el = privates.elements;
      for(var i=x;i<el.length;i++){
        if(el[i].state === 1){
          this.loadLayer(el[i].layer,function(){
            ++privates.refreshindex;
            this.refresh();
          });
          return;
        }
      }
      privates.refreshindex = 0;
    }

    this.clearmap = function(x,tgl){
      switch(x)
      {
        case 'markers':
          toggle_arr.call(this,markers,tgl);
        break;
        case 'layers':
          _.each(privates.elements,function(el){
            toggle_arr.call(this,el.arr,tgl);
            el.hidden = tgl;
          });
        break;
        default:
          Notify('Warning','Method not found - Clear Map -- GIS Module','normal',true);
      }
    }

    this.newElement = function(opt){
      if(!opt || typeof(opt) != 'object'){
        Notify('Error - GIS','Parameters not found','normal',true);
        return;
      }
      if(!privates.foreignElements.hasOwnProperty(opt.token))
          privates.foreignElements[opt.token] = {};
      if(!privates.foreignElements[opt.token].hasOwnProperty(opt.subtoken))
          privates.foreignElements[opt.token][opt.subtoken] = [];
      switch(opt.type){
        case 'valve':
          var coords = [new google.maps.LatLng(opt.lat,opt.lng),new google.maps.LatLng(opt.lat,opt.lng)];
          if(!opt.hasOwnProperty('animate'))
            opt.animate = false;
          addValve.call(this,opt.id,coords,opt.desc,null,null,opt.msg,privates.foreignElements[opt.token][opt.subtoken],opt.animate);
        break;

        case 'pipe':
          var coords = [];
          opt.loc.sort(function(a,b){
                return parseInt(a.serialNo) - parseInt(b.serialNo);
              });
          _.each(opt.loc,function(nd){
                coords.push(new google.maps.LatLng(parseFloat(nd.latitude),parseFloat(nd.longitude)));
          });
          addPipe.call(this,opt.id,coords,opt.dim||400,'N.A','N.A','N.A',null,{
            token:privates.foreignElements[opt.token][opt.subtoken],
            clr:opt.clr,
            zindex:9,
            msg:opt.msg
          });
        break;
        default:
          var coords = new google.maps.LatLng(parseFloat(opt.loc[0].latitude),parseFloat(opt.loc[0].longitude));
          addElement.call(this,opt.id,coords,opt.desc,opt.type,opt.type,null,null,{
            token:privates.foreignElements[opt.token][opt.subtoken],
            zIndex:9,
            msg:opt.msg,
            icon:opt.icon
          });
        break;
      }
    }

    this.clearElements = function(token){
      if(!token)
        return;
      if(!privates.foreignElements.hasOwnProperty(token))
        return;
      for(var i in privates.foreignElements[token]){
        _.each(privates.foreignElements[token][i],function(t){
          t.setMap(null);
        });  
      }
      delete privates.foreignElements[token];
    }

    this.clearSubElement = function(token,subtoken){
      if(!token || !subtoken)
        return;
      if(!privates.foreignElements.hasOwnProperty(token) || !privates.foreignElements[token].hasOwnProperty(subtoken))
        return;
        _.each(privates.foreignElements[token][subtoken],function(t){
          t.setMap(null);
        });
        delete privates.foreignElements[token][subtoken];
    }
    

    /*  Private Methods  */

    var query = function(layer_number){
      mapbounds.call(this);
      var jsonObj = null;
      var el = privates.elements[layer_number];
      var name = el.layer;
      if(name === 'pipe' && el.type === null){
          jsonObj={"query":{"filtered":{"query":{"match_all":{}},"filter":{"geo_bounding_box":{"location":{"top_left":{},"bottom_right":{}}}}}},"size":10000000}
          jsonObj.query.filtered.filter.geo_bounding_box.location.top_left.lat=privates.ne_lat;
          jsonObj.query.filtered.filter.geo_bounding_box.location.top_left.lon=privates.sw_long;
          jsonObj.query.filtered.filter.geo_bounding_box.location.bottom_right.lat=privates.sw_lat;
          jsonObj.query.filtered.filter.geo_bounding_box.location.bottom_right.lon=privates.ne_long;
      }
      else{
          if(el.type === null)
            jsonObj={"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"TYPE":name}}]},{"geo_bounding_box":{"location":{"top_left":{},"bottom_right":{}}}}]}]}}}},"size":10000000}
          else
            jsonObj={"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"TYPE_UTILITY":el.type}}]},{"geo_bounding_box":{"location":{"top_left":{},"bottom_right":{}}}}]}]}}}},"size":10000000}
          jsonObj.query.filtered.filter.bool.must[0].and[1].geo_bounding_box.location.top_left.lat=privates.ne_lat;
          jsonObj.query.filtered.filter.bool.must[0].and[1].geo_bounding_box.location.top_left.lon=privates.sw_long;
          jsonObj.query.filtered.filter.bool.must[0].and[1].geo_bounding_box.location.bottom_right.lat=privates.sw_lat;
          jsonObj.query.filtered.filter.bool.must[0].and[1].geo_bounding_box.location.bottom_right.lon=privates.ne_long;
      }
      return JSON.stringify(jsonObj);
    }

    var clear_arr = function(idx){
      var _arr = privates.elements[idx].arr;
      _.each(_arr,function(a){
        a.setMap(null);
      });
      if(privates.elements[idx].hasOwnProperty('paths')){
        var _paths = privates.elements[idx].paths;
        _.each(_paths,function(p){
          p.setMap(null);
        });
        _paths.splice(0,_paths.length);
      }
      _arr.splice(0,_arr.length);

    }

    var toggle_arr = function(x,tgl){
      tgl = tgl || null;
      if(tgl)
        tgl = map;
      _.each(x,function(y){
        y.setMap(tgl);
      });
    }

    // update map specifics
    var mapbounds = function(){
      privates.mapcenter_long = map.getCenter().lng();
      privates.mapcenter_lat = map.getCenter().lat();
      privates.sw_long = map.getBounds().getSouthWest().lng(); 
      privates.sw_lat = map.getBounds().getSouthWest().lat();  
      privates.ne_long = map.getBounds().getNorthEast().lng(); 
      privates.ne_lat = map.getBounds().getNorthEast().lat();
    }

    var fetch = function(query,name,idx,cb){
      var self = this;
      $.post('controllers/proxyman.php',{search:true,SearchQuery:query,index:privates.elements[idx].index},
      function(d){
        if(d.errorCode>0){
          Notify('Error',d.errorMsg,'normal',true);
          privates.mutex = false;
          preLoader(false);
          if(cb!=null && typeof(cb) === 'function')
            cb.call(self);
          return;
        }
        var el = d.response.hits.hits;
        if(el.length === 0){
          Notify('Information','No '+name+' found in this region','normal',true);
          privates.mutex = false;
          preLoader(false);
          if(cb!=null && typeof(cb) === 'function')
            cb.call(self);
          return;
        }
        var coords = null;
        _.each(el,function(point){
          switch(name)
          {
            case 'pipe':
              coords = [];
              var ne=point.location.sort(function(a,b){
                return parseInt(a.sequence) - parseInt(b.sequence);
              });
              _.each(point.location,function(nd){
                coords.push(new google.maps.LatLng(parseFloat(nd.lat),parseFloat(nd.lon)));
              });
              addPipe.call(this,point.meta.id,coords,point.meta.pipeDiameter||80,point.meta.pipeAge||'N.A',point.meta.pipeMaterial||'N.A',point.meta.zone||'N.A',idx);
            break;
            case 'valve':
              coords = [new google.maps.LatLng(point.location[0].lat,point.location[0].lon),new google.maps.LatLng(point.location[0].lat,point.location[0].lon)];
              addValve.call(this,point.meta.id,coords,point.meta.DESCRIPTION,idx,point.meta.zone);
            break;
            default:
              coords = new google.maps.LatLng(point.location[0].lat,point.location[0].lon);
              addElement(point.meta.id,coords,point.meta.DESCRIPTION,point.meta.TYPE,name,idx,point.meta.zone);
            break;  
          }
        });
        privates.mutex = false;
        preLoader(false);
        if(cb!=null && typeof(cb) === 'function')
          cb.call(self);
        else
          privates.refreshindex = 0;
      },'json');
    }

    /* GIS ELEMENTS MAP ROUTINES */
    var addPipe = function(pipeid,pipecoords,pipewidth,pipeage,pipematerial,pipezone,idx,customOptions){
      customOptions = customOptions||null;
      var zidx = null;
      if(customOptions!=null && customOptions.hasOwnProperty('zIndex'))
        zidx = customOptions.zIndex;
      var clr = null;
      if(customOptions!=null && customOptions.hasOwnProperty('clr'))
        clr = customOptions.clr;
      if(idx!=null && privates.elements[idx].operation != null)
        clr = pipeOp.call(this,privates.elements[idx].operation,{pipeage:pipeage,pipematerial:pipematerial});
      var pipepath = new google.maps.Polyline({
        path: pipecoords,
        strokeColor: clr||"#3844F2",
        strokeOpacity: 1.0,
        strokeWeight: pipewidth/100,
        zIndex:zidx||1
      });
      if(idx!=null)
        privates.elements[idx].arr.push(pipepath);
      else
        customOptions.token.push(pipepath);
      pipepath.setMap(map);
      /*
          Pipe Labels
                      */
      if(map.zoom >= 21){
        _.each(pipecoords,function(c,idx){
          if(idx === 0 || idx === pipecoords.length-1 || (idx % 2) === 0)
            return;
          var lbl = new MarkerWithLabel({
             position: c,
             draggable: false,
             raiseOnDrag: false,
             map: map,
             labelContent: pipewidth,
             labelAnchor: new google.maps.Point(22, 0),
             labelClass: "pipe_labels",
             icon:'images/hiddenmarker.png'
          });
          privates.labels.push(lbl);
      });
    }
      var contentString = '<h5><b>Pipe: '+pipeid+'</b></h5>Diameter: <i>'+pipewidth+'mm</i><br/>Installed: <i>'+pipeage+'</i><br/> Material: <i>'+pipematerial+'</i><br/>Supply Zone: <i>'+pipezone+'</i><br/>';
      if(window.hasOwnProperty('sim'))
        contentString+=sim.simButton(pipeid,pipezone,'pipe');
      if(window.hasOwnProperty('netsim'))
        contentString += netsim.button('pipe',pipeid,pipezone);
      if(customOptions!=null && customOptions.hasOwnProperty('msg'))
        contentString = customOptions.msg;
      var info = new google.maps.InfoWindow();
      info.setContent(contentString);
      // set the popup to SOMEWHERE in the middle
      if( pipecoords.length > 1)
         info.setPosition(pipecoords[Math.round(pipecoords.length/2)]);
      else
      info.setPosition(pipecoords[0]);
      google.maps.event.addListener(pipepath, 'click', function() {
        if(infowindow)
          infowindow.close();
       info.open(map); });
      infowindow = info;
  }
  var pipeOp = function(op,options){  // PIPE COLOR BASED FILTERING
      var x,max,min,normalizedVal,override=null;
      if(op == 'byage'){
        x = options.pipeage;
        var current = new Date().getFullYear();
        var critical_val = current - x;
      if(x == 'N.A')
          override = '#000000';
      if(!isNaN(parseInt(x)))
      {
        switch(true){
          case critical_val < 50:
            normalizedVal = '#07E00E';
          break;

          case (critical_val > 50 && critical_val < 75):
            normalizedVal = '#0222F0'
          break;

          case (critical_val > 75 && critical_val < 100):
            normalizedVal = '#FF7500';
          break;

          case (critical_val > 100):
            normalizedVal = '#F00202';
          break;

          default:
            normalizedVal = '#000000';
        }
      }
      // var range = max - min;
      // var ith = (x - min) / range;
      // var range2 = 255;
      // normalizedVal = (ith*range2);
    }
      if(op == 'bymaterial'){
        x = options.pipematerial;
        if(x == 'N.A')
          override = '#000000';
        else{
          var clr_idx = visenti_pipe_material.indexOf(x);
          if(clr_idx<0)
            override = '#000000';
          else
            normalizedVal = visenti_pipe_colors[clr_idx];
        }
      }
      if(normalizedVal)
        return normalizedVal;
      return override;
  }
    /* Valves */
    var addValve = function(valveid,valvecoords,valvestatus,idx,zone,msg,forarr){
      msg = msg || null;
      forarr = forarr || null;
      var valvepath = new google.maps.Polyline({
      path: valvecoords,
      strokeColor: "#666666",
      strokeOpacity: 1.0,
      strokeWeight: 2
      });
      valvepath.setMap(map);
      if(idx != null)
        privates.elements[idx].paths.push(valvepath);
      // valve pipe routine
      var g = google.maps;
      var valveloc;
      var nyc = new google.maps.LatLng(valvecoords[0].lat().toFixed(6), valvecoords[0].lng().toFixed(6));
      if( valvecoords.length > 1 ) {
        var london = new google.maps.LatLng(valvecoords[1].lat().toFixed(6), valvecoords[1].lng().toFixed(6));
        var inBetween = google.maps.geometry.spherical.interpolate(nyc, london, 0.5);
        valveloc = new google.maps.LatLng(inBetween.lat(),inBetween.lng());
      } 
      else
        valveloc = nyc;
    // ----------------

      var contentString;
      if(msg != null)
        contentString = msg;
      else{
        contentString = "<h5><b>Valve</b></h5><br/><b>Status: </b><i>"+ valvestatus +"</i>"; // saying such as "v12345 (closed)"
        if(window.hasOwnProperty('sim'))
          contentString += '<br/>'+sim.simButton(valveid,zone,'valve');
      }

      if(valvestatus.search(/open/i) != -1)
        var iconimage = "images/mapicons/valve_open.png";
      else if(valvestatus.search(/Closed/i != -1))
        var iconimage = "images/mapicons/valve_close.png";
      else
        var iconimage = "images/mapicons/valve_open.png";
      if(infowindow)
        infowindow.close();
      infowindow = new google.maps.InfoWindow();
      infowindow.setContent(contentString);
      infowindow.setPosition(valveloc);
      addMarker_with_popup.call(this,valveloc,forarr||privates.elements[idx].arr,contentString,iconimage,infowindow);
    }
    var addMarker_with_popup = function (location, arrayname, msg, iconimage, info) {
      var image = new google.maps.MarkerImage(iconimage,null,null,new google.maps.Point(7,7));
          var marker = new google.maps.Marker({
                position: location,
                map: map,
                icon:image
            });
            arrayname.push(marker);
            google.maps.event.addListener(marker, 'click', function() {info.open(map);});
    }
    /*  Generic other layers  */

    var addElement = function(valveid,valvecoords,valvestatus,type,name,idx,zone,opt){
      type = type || null;
      name = name || '';
      idx = idx || null;
      zone = zone || null;
      opt = opt || null;
      var contentString='<div style="min-width: 150px; min-height: 50px;">'
      if(valvestatus!=undefined && valvestatus != null)
      {
        contentString +=  '<h5><b>'+name.toUpperCase()+': '+valveid + '</b></h5>';
      }
      else
      {
      //var contentString =  '<h5><b>'+type.toUpperCase()+': '+valveid + '</b></h5>Comments: <i>' + valvestatus +'</i>';
        contentString +=  '<h5><b>'+type.toUpperCase()+'</b></h5>';
      }
      if(opt!=null && opt.hasOwnProperty('msg'))
        contentString += opt.msg;
      else
      {          
         if(type=='junction')
          {
            if(window.hasOwnProperty('fireflow'))
              contentString+=fireflow.fireButton(valveid,'junction',zone);
            if(window.hasOwnProperty('netsim'))
              contentString += netsim.button('junction',valveid,zone);
            if(window.hasOwnProperty('sim'))
              contentString += sim.simButton(valveid,zone,'junction');
          }
      }

      // find id if not given
      if(idx === null && opt!=null){
        _.each(privates.elements,function(el,cnt){
          if(el.layer === type)
            idx = cnt;
        });
      }
      contentString+='</div>';
      
      var iconimage = '';
      if(privates.elements[idx].hasOwnProperty('icon'))
        iconimage = privates.elements[idx].icon;
      if(opt!=null && opt.hasOwnProperty('icon') && opt.icon!=null)
        iconimage = 'images/clustericons/node'+opt.icon+'.png';
      var image = new google.maps.MarkerImage(iconimage,
            new google.maps.Size(60, 60),
            new google.maps.Point(0,0),
            new google.maps.Point(30, 30)
        );
      var nodeMarker = new google.maps.Marker({
          position: valvecoords,
          map: map,
          title: valvestatus,
          icon: iconimage,
          visible:true
      });
      if(opt!=null)
        opt.token.push(nodeMarker);
      else
        privates.elements[idx].arr.push(nodeMarker);
      google.maps.event.addListener(nodeMarker, 'click', function () {
        if(infowindow)
         infowindow.close();
        infowindow = new google.maps.InfoWindow({
          content: contentString    
        });
        infowindow.open(map, nodeMarker);
      });
    }
    var trigger = function(number,el){
      if(privates.elements[number].state === null){
        privates.elements[number].state = 1;
        this.loadLayer(privates.elements[number].layer);
        $(el).addClass('sel');
      }
      else{
        this.clearLayer(privates.elements[number].layer);
        $(el).removeClass('sel');
      }
    }

    /* Generate UI */
    var deploy = function(){
      var self = this;
      var html = '    <div class="GisTray">\
      <div class="trayContent">\
        <div class="row">\
          <div class="large-12 columns" id="gisItems" style="padding: 5px 2px;">';
            html += '<div class="large-3 Gitems" title="Show Assets"><img src="images/layers.svg" width="12px" height="10px"></div>'           
            html += '<div class="large-3 GitemsTrigger" id="reloadGIS" title="Reload Layers"><img src="images/reloadGIS.png" width="10px" style="margin-left:1px;"></div>\
            <div class="large-3 GitemsTrigger" id="autoloadGIS" title="Auto Reload Layers"><img src="images/ticker.png" width="12px"></div>\
            <div class="large-3 GitemsTrigger" id="disableicons" title="Hide Layers"><img src="images/hide.png" width="12px"></div>\
          </div></div></div></div>';
          html += '<div class="expand_gis hide"><ul>';
          _.each(privates.elements,function(el,idx){
              html += '<li data-id="'+idx+'"><span>'+el.layer+'</span>';
              if(el.layer in layer_types || el.layer in filter_types)
              {
                html += '<img class="right arr" src="images/dotarrow.png" width="6px">';
                html += '<ul class="hide">';
                html += '<li class="util">All</li>';
                if(layer_types.hasOwnProperty(el.layer)){
                  for(var key in layer_types[el.layer])
                    html += '<li class="util">'+layer_types[el.layer][key].name+'</li>';
                }
                if(filter_types.hasOwnProperty(el.layer)){
                  html += '<li class="head"><strong>Filter Types</strong></li>';
                  for(var i = 0; i<filter_types[el.layer].length;i++)
                    html += '<li class="gis_filter_radio"><label><input name="gis_'+el.layer+'_filter" type="radio" value="'+filter_types[el.layer][i].bkp+'">'+filter_types[el.layer][i].name+'</label></li>';
                  html += '<li class="gis_filter_radio"><label><input checked name="gis_'+el.layer+'_filter" value="null" type="radio">None</label></li>';
                }
                html += '</ul>';
              }
              html += '</li>';
          });
          html += '</ul></div><div class="expand_hide_gis hide"><ul><li class="_markers">Markers</li><li class="_layers">Layers</li></ul></div>'

  /* PUSH TO GOOGLE MAPS CONTROLS */
          var controldiv = document.createElement('div');
          controldiv.innerHTML = html;
          map.controls[privates.controlposition].push(controldiv);

  /* Pushing Filter Legends to GMAPS  */
    var cdiv = document.createElement('div');
    var chtml = '<div style="margin-bottom:150px"><div class="filter_legends typeage hide"><ul style="margin:0 0 0 2px"><li><div class="legendblock" style="background-color:green"></div>&nbsp; < 50</li>\
    <li><div class="legendblock" style="background-color:blue"></div>&nbsp; < 75</li>\
    <li><div class="legendblock" style="background-color:orange"></div>&nbsp; < 100</li>\
    <li><div class="legendblock" style="background-color:red"></div>&nbsp; > 100</li>\
    <li><div class="legendblock" style="background-color:#000000"></div>&nbsp; unknown</li></ul></div><div class="filter_legends typematerial hide"><ul style="margin:0 0 0 2px">';
    _.each(visenti_pipe_material,function(m,idx){
      chtml += '<li><div class="legendblock" style="background-color:'+visenti_pipe_colors[idx]+'"></div>&nbsp;'+m+'</li>';
    });
    chtml += '</ul></div></div>';
    cdiv.innerHTML = chtml;
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(cdiv);

  /*  ACTIVATE SNAPSHOT CONTROL (CROP) */
          // if(window.hasOwnProperty('Snapshot'))
          //   window.snapshot = new Snapshot();
          // events
          $(document).on('click','.Gitems',function(){
            var swt = $('.expand_gis').hasClass('hide');
            if(swt){
              $('.expand_gis').removeClass('hide');
              return;
            }
             $('.expand_gis').addClass('hide');
             $('.expand_gis ul li ul').addClass('hide');
          });
          // $('.expand_gis').on('mouseleave',function(){
          //   $(this).addClass('hide');
          //   $('.expand_gis ul li ul').addClass('hide');
          // });
          $('.expand_hide_gis').on('mouseleave',function(){
            $(this).addClass('hide');
          });
          $('.expand_gis ul li ul').on('mouseleave',function(){
            $(this).addClass('hide');
          });
          $(document).on('click','.expand_gis li',function(e){
            if($(this).children('ul').length>0){
              $('.expand_gis li ul').addClass('hide');
              $(this).find('ul').removeClass('hide');
            }
            else if($(this).find('span').length>0){
                var index = $(this).data('id');
                trigger.call(self,index,this);
            }
            if (!e) var e = window.event;
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
          });
          $('.expand_gis .gis_filter_radio input:radio').on('change',function(w){
            $('.filter_legends').addClass('hide');
            var layer = $(this).parents('li').last().data('id');
            var filter = $(this).val();
            if(filter === 'byage')
              $('.filter_legends.typeage').removeClass('hide')
            else if(filter === 'bymaterial')
              $('.filter_legends.typematerial').removeClass('hide')
            privates.elements[layer].operation = filter;
          });
          $(document).on('click','.expand_gis ul li ul li.util',function(){
            var layer = $(this).parents('li').last().data('id');
            var idx = $(this).index();
            if($(this).hasClass('sel')){
               self.clearLayer(privates.elements[layer].layer);
               privates.elements[layer].state = null;
               $(this).parents('li').last().removeClass('sel');
               $(this).removeClass('sel');
               return;
            }
            $(this).parents('li').last().removeClass('sel');
            $(this).parents('li').last().find('.util').removeClass('sel');
            $(this).addClass('sel');
            if(idx === 0)
              privates.elements[layer].type = null;
            else
            {
              idx = idx - 1;
              var type = layer_types[privates.elements[layer].layer][idx];
              console.log(type);
              privates.elements[layer].type = type.bkp;
            }
            privates.elements[layer].state = null;
            trigger.call(self,layer,$(this).parents('li').last());
          });
          $(document).on('click','#reloadGIS',function(){self.refresh();});
          $(document).on('click','#disableicons',function(){$('.expand_hide_gis').removeClass('hide');});
          $(document).on('click','.expand_hide_gis ._markers',function(){
            self.clearmap('markers',$(this).hasClass('sel'));
            $(this).toggleClass('sel');
          });
          $(document).on('click','.expand_hide_gis ._layers',function(){
            self.clearmap('layers',$(this).hasClass('sel'));
            $(this).toggleClass('sel');
          });
          $(document).on('click','#autoloadGIS',function(){
            privates.autoload = !privates.autoload;
            $(this).toggleClass('sel');
          });
          google.maps.event.addListener(map, 'dragstart', function() {
            if(privates.timer!=null)
              window.clearTimeout(privates.timer);
          });
          google.maps.event.addListener(map, 'zoom_changed', function() {
            if(privates.timer!=null)
              window.clearTimeout(privates.timer);
          });
          google.maps.event.addListener(map, 'bounds_changed', function() {
            if(privates.timer!=null)
              window.clearTimeout(privates.timer);
          });
          google.maps.event.addListener(map, 'dragend', function() {
            privates.timer = window.setTimeout(function(){
              if(privates.autoload)
                self.refresh();
            },2200);
          });

    }
    /*  End of Private Methods  */
     
     deploy.call(this);
  }

  /*  End of Declaration */
      
    window.gis = new Gis();
} )( window );

//var gis = new Gis();