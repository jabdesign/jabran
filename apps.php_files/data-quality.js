function heatMap2(meterIds,sensorType,startTime,endTime,resolution){
  if(!sensorType || !meterIds || meterIds.length===0)
  {
    alert('Error - Data Quality Parameters');
    return;
  }
  startTime = startTime || st;
  endTime = endTime || et;
  resolution = resolution || interval;
 $.get('controllers/proxyman.php',{endpoint:'dataavailability_dataintel/parkes/'+sensorType,params:'stn='+meterIds.toString()+'&from='+startTime+'&to='+endTime+'&rt='+resolution},
  function(d){
   if(d.length==0){
    alert('No information returned');
    return;
   }
   var uniqid = logWindow('control');
   var plotterdiv = $('#'+uniqid);
   var randomposition = getRandomInt(25,120);
   var currentposition = plotterdiv.position();
   var thistop = currentposition.top;
   $('.plotter:last-child').css({top:parseInt(thistop)+randomposition+'px'});
   var containerid = uniqid+'heatmap';
   $('#'+uniqid).append('<div class="container" id="'+containerid+'"></div>');
   $('#'+uniqid+' .deptitle').empty().append(name);
   //parsing data for series
   var seriesarr = [];
   var yaxis =[];
   var xaxis = [];
   var days = [];
var countIndex = 0;
   $.each(d.response,function(index,value){
    yaxis.push(value.stationname);
    $.each(value.status,function(s,g){
	var per = 0;
	if(g == "available") {
		per = 100;
	}
     
     xaxis.push(s);
     xaxis = _.uniq(xaxis);
     var pos = xaxis.indexOf(s);
     
     var temp = {x:pos,y:countIndex,value:per};
     seriesarr.push(temp);
    });
	countIndex++;
   });
   console.log(seriesarr);
   console.log(yaxis);
   console.log(xaxis);
   var colorScale = new chroma.scale(['red', 'green']).out('hex');
   var html = '<table class="heattable"><thead><tr><th>Customer</th>';
   _.each(xaxis,function(x){
//alert()
    var thisdate = new Date(parseInt(x,10));
    var hours = thisdate.getHours();
    var mins = thisdate.getMinutes();
    var month = thisdate.getMonthName();
    var day = thisdate.getDate();
    var year = thisdate.getFullYear();
    var classtype = '';

    html += '<th title="'+thisdate.toString()+'" class="'+classtype+'" data-id="'+x+'">'+day+'/'+month+'/'+year+' '+hours+':'+mins+'</th>';
   });
   html += '</tr></thead><tbody>';
   // Data Rows
   _.each(yaxis,function(y,yindex){
    var classtype = '';
    //if(checkexclusion(y))
     classtype = '';
    html += '<tr class="'+classtype+'"><td>'+y+'</td>';
     // xaxis
     _.each(xaxis,function(x,xindex){
      var classtype ='';

      var thisdate = new Date(parseInt(x,10));
     /* var testts = thisdate.setHours(o);
      if(mem.generic.dayslist.indexOf(testts)>=0)
       classtype = 'removeme';*/
      _.each(seriesarr,function(ser){
       if(ser.x == xindex && ser.y == yindex){
        var clr = colorScale(ser.value / 100);
        html += '<td class="'+classtype+'" style="background-color:'+clr+'" class="heattd" data-id="'+x+'">'+ser.value+'</td>';
       }
      });
     });
    html += '</tr>';
   });
   html += '</tbody></table>';
   $('#'+containerid).append(html).find('table').tooltip();
   $('#'+uniqid).css('zIndex','100'); // force visibility through reports
      
  },'json');
}
