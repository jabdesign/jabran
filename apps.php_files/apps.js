var activeapp = null;
function dmasetup(){
	var winid = '#'+logWindow('report');
	activeapp = winid;
	var height = $(winid).height();
	$(winid).append('<iframe src="apps/dmaSetup/dma.html" width="100%" height="'+height+'px" frameborder=0/>');
}
// HELPER FUNCTIONS
function requestAppDown(){
	if(!activeapp)
	{
		alert('No Application is running');
		return;
	}
	var height = $('body').height()-100;
	$(activeapp).animate({top:height+'px'},'fast');
}
function requestAppUp(){
	if(!activeapp)
	{
		alert('No Application is running');
		return;
	}
	$(activeapp).animate({top:'none'},'fast');
}
function sessionredirect(){
  window.location.href= '../../index.php'+'?er=Session expired';
}
function requestBucket(){
	var ret = [];
		// convert sensors to backend types
		var sensorSelection = {};
		_.each(unitObj.sensorSelection,function(val,key,obj){	// station
			sensorSelection[key] = [];	// initiate empty array
			var meta = unitObj.sensorMeta[key];
			var backend_name = 'bad_sensor';
			_.each(val,function(sen){		// selected sensors in this station
				_.each(meta.sensorList,function(sen2){
					if(sen2._source.sensortype_display === sen)	// found display name
						backend_name = sen2._source.sensortype_backend;
				});
				sensorSelection[key].push(backend_name);
			});
		});
	_.each(unitObj.selectedUnit,function(key){
		var temp = {};
		temp.sensorList = sensorSelection[key];
		meta = unitObj.sensorMeta[key];
		temp.name = meta.name;
		temp.tag_owner = meta.tag_owner;
		temp.display_name = meta.display_name||meta.name;
		temp._id = key
		ret.push(temp);
	});
	return ret;
}

function requestMeta( ){
	return metadata;
}
function requestVisentiStations()
{
    var VisentiObj=[];
    $.each(metadata,function(key,value){
      if(value.tag_owner=='visenti')
      {
        VisentiObj.push(value)
      }
    })
    return VisentiObj;
}
function requestallStations()
{
    var VisentiObj=[];
    $.each(metadata,function(key,value){
      VisentiObj.push(value)
    })
    return VisentiObj;
}
function groupByActual(SensorList)
{
  var ActualGrouped={};
  $.each(SensorList,function(key,value){
    if(!value.isderived)
    {
      if(!(value.sensortype_actual in ActualGrouped))
      {
          ActualGrouped[value.sensortype_actual]=[]
      }
      ActualGrouped[value.sensortype_actual].push(value)
    }
  })
  return ActualGrouped;
}

function groupData(meterList)
{
    var customers = [];
    var groupcustomers = [];
    var cust_id = [];
    var groupids = {};
    var groupMeta={};
    var qids = [];
    var quality = [];
    var removed = [];
    var cust = null, meta=null;
    gdata={};
    $.each(meterList,function(key,meta){
          cust = meta.bp_name || meta.customer_name || meta.customername || meta.display_customer_name || meta._id;
          cust=cust.replace(/ /g, "_");
          customers.push(cust);
          cust_id.push(meta._id);
          groupMeta[meta._id]=meta

      });

    groupcustomers = _.uniq(customers);
    _.each(groupcustomers,function(el,il){
      if(el == ''){
        // groupids.push([]);
        return;
      }
      if(!(el in groupids))
      {
        groupids[el]={}
      }
      
        var idx = []; var qx = [];
        var cd = customers.indexOf(el);
        if(cd>=0){
          idx.push(cust_id[cd]);
          type=groupMeta[cust_id[cd]].tag_datatype
          customers[cd] = '';
        }
        while(customers.indexOf(el)>=0){
          idx.push(cust_id[customers.indexOf(el)]);
          type=groupMeta[cust_id[customers.indexOf(el)]].tag_datatype
          customers[customers.indexOf(el)] = '';
        }

        groupids[el].type=type
        groupids[el].data=idx;
    });

    gdata.customers=groupcustomers;
    gdata.groupids=groupids; 
    gdata.groupMeta={}
    gdata.groupMeta=groupMeta; 
    return gdata;

}


function requestKill(){
	$(activeapp).remove();
	split=activeapp.split('#')
	$('.bottom-panel div[data-id="'+split[1]+'"]').children('.cross').trigger('click');	
}

function removeTrayObject(){
	split=activeapp.split('#')
	$('.bottom-panel div[data-id="'+split[1]+'"]').children('.cross').trigger('click');
}

function process(url, type, params, successCallback, errorCallback) {
  $.ajax({
          success : successCallback,
          error : errorCallback,
          data : params,
          url : url,
          type : 'POST',
          dataType : 'json'
  });
}