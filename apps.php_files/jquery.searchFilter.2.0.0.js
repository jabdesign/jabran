/*****************************************************
*													 *
*	Author: Dinesh Vadivel							 *
*	Plugin: jquery.searchFilter.2.0.0.js			 *
*	Date:	15-06-2015								 *
*													 *
*													 *
*													 *
*													 *
*													 *
*													 *
*****************************************************/
(function($){
	var SearchFilter = function(element, options)
   	{
   		var elem = $(element);
      	var rand;
       	var UIcompleteData=[];
       	var privates = {};
       	var self=this;
       	privates.obj=[];
        privates.service = new google.maps.places.AutocompleteService();
        privates.currentEntry={};
        privates.or={};
        privates.sensor='sensor';
        privates.advanced=true;
        var newTemp={}
        var backendForm={}
        	backendForm.conditions=[];
        var	advancepop=false;
        var sour=['condition','filterOn','thershold']
   		var settings = $.extend({
       	  text		: "Search",
          speed		: 200,
          map 		: '',
          type		: "dropdown",
          advanced	: false,
          autofill	: true,
          selected	: "",
          overrideObj:"",
          suggestionUrl:"",
          proxyUrl:"",
          availableTags : {},
          displayTags: "#taglist"
       	}, options || {});

   		var generate=function(){
   			console.log(privates)
   			console.log(settings)
   			var self=this;
   			privates.elem=elem;
   			privates.ID='#'+elem.attr('id');
   			privates.sze=0;
			privates.instanceID=Math.floor((Math.random()*9999999)+9999);
			createUI.call(self);

   		}
		var dig=function( blob, vdata, depth ) { 
		  var depth = depth || 0; // start at level zero
		  for( var item in blob ) {
		    if( typeof blob[item] === 'object' ) {
		    	var temp={}
		  		$.each(blob[item],function(k,v){
		  			if(sour.indexOf(k)>-1)
		  			{
		  				temp[k]=v;
		  			}
		  		})
		  		// console.log(temp)
		  		if(!('conditions' in vdata))
		  		{
		  			vdata.conditions=[];
		  		}
				vdata.conditions.push(temp);
		      	dig( blob[item],temp ,++depth ); // descend
		    } else { // simple value, leaf
		    	
		    }

		  }
		  
		}
   		var createUI=function(){
   			var self=this;
   			$(privates.ID).css({'border-width': '0px', 'outline': 'none'})
					.wrap('<div id="'+privates.instanceID+'" class="rare divclearable"></div>')
					.parent()
					.attr('class', $(self).attr('class') + ' divclearable')
					// .append('<a class="tagsLink" href="javascript:"><img src="images/tag.png" width="20px"></a>')
					.append('<a class="clearlink" href="javascript:" style="  position: absolute;right: 0;top: 0;"></a>');
					 windowWidth=$(window).width();
				    logowidth=$('.Logo').width(); 
				    optwidth=$('.optionplaceholder').width(); 
				    final=windowWidth-(logowidth+optwidth);
			// $(privates.ID).before('<div class="taglist myspace" id="taglist" style="width:'+final+'px;"></div>');
			if(!privates.advanced)
			{
				show='style="display:none"'
			}
			else
			{
				show=''
			}
			$(" .search_Button").after( ' <div class="adv_Button" '+show+'><div class="adv_icon" id="adv_btn" >ADVANCED</div></div><div class="tag_Button"><div class="tag_icon" id="taglist_btn">TAGS</div></div>' );
			$('#'+privates.instanceID+' .clearlink').attr('title', 'Click to clear this textbox');
			parentID=privates.instanceID;
			privates.width=$(privates.ID).width();
			uiEvents.call(self);

   		}
   		var newinsertUI=function(id,orstate,level,bool){

   			var self=this;
   			var filterOnlist;
   			multiplelevel=true;
   			keyrand=Math.floor((Math.random()*9999999)+9999);
   			
   			var html='<div class="row collapse addFIlterBox" style="margin-top:10px; " id="addFIlterBox_'+keyrand+'">\
						<div class="small-4 large-4 columns">\
								<select id="filterOn">\
						          <option value="Filter">Filter On</option>'
						   // $.each(filterOnlist,function(key,value){
						   // 		html+='<option value="'+value+'">'+value.toUpperCase()+'</option>'
						   // })
						          
			html+='	</select>\
							</div>\
							<div class="small-4 large-4 columns">\
								<select id="condition">\
						          <option value="Condition">Condition</option>\
						          <option value=">">Greater</option>\
						          <option value="<">Lesser</option>\
						          <option value=">=">Greater or equal</option>\
						          <option value="<=">Lesser or equal</option>\
						        </select>\
							</div>\
							<div class="small-4 large-4 columns">\
								<input type="text" placeholder="Threshold" id="thershold"/>\
							</div>\
							<div class="small-6 large-centered columns" >\
								<span class="alert radius label" style="margin-left: 10px; margin: 7px;" id="AddSearchFilter"> + </span>\
							</div>\
						</div>';
						
			$(id).append(html)
			$('#AddSearchFilter').off().on('click',function(){ //#addFIlterBox_'+keyrand+' 
				var boxparent=$(this).closest('.addFIlterBox').attr('id');
					valid=formValidate(boxparent);
					if(valid)
					{
						randKey=Math.floor((Math.random()*9999999)+9999);
						var html='';
						if(bool=='and')
						{
							html+='<div class="small-12 large-centered columns" style="border-bottom: 1px dotted #666; text-align: center; padding: 5px 0px;">and</div>'
						}
						else
						{
							// html+='<div class="small-12 large-12 columns">\
							//   <span style=" display: table-cell; vertical-align: middle;">or</span>\
							// </div>';
							if(orstate)
								html+='<div class="small-12 large-centered columns" style=" text-align: center; padding: 5px 0px;">or</div>'
						}
						html+='<div class="row collapse entry_Area init '+bool+'" data-type="'+bool+'" data-level="'+level+'" style="margin-top:10px; border: 1px dotted #666; /* border-top:1px dotted #666; border-bottom:1px dotted #666; */" id="entry_Area_'+randKey+'">\
							<div class="row collapse">\
								<div class="small-12 large-12 columns" style="padding:5px;" >\
									'+privates.currentEntry.filterOn+' '+privates.currentEntry.condition+' '+privates.currentEntry.thershold+'\
								</div>\
							</div>';
							// html+='<div class="row collapse entry_Area init '+bool+'" data-type="'+bool+'" data-level="'+level+'" style="margin-top:10px; border: 1px dotted #666; /* border-top:1px dotted #666; border-bottom:1px dotted #666; */" id="entry_Area_'+randKey+'">\
							// <div class="row collapse">\
							// 	<div class="row collapse" style="padding:5px;" >\
							// 		<div class="small-4 large-4 columns">\
							// 			<select id="fon">\
							// 	          <option value="Filter">Filter On</option>\
							// 	        </select>\
							// 		</div>\
							// 		<div class="small-4 large-4 columns">\
							// 			<select id="con">\
							// 	          <option value="Condition">Condition</option>\
							// 	          <option value=">">Greater</option>\
							// 	          <option value="<">Lesser</option>\
							// 	          <option value=">=">Greater or equal</option>\
							// 	          <option value="<=">Lesser or equal</option>\
							// 	        </select>\
							// 		</div>\
							// 		<div class="small-4 large-4 columns">\
							// 			<input type="text" placeholder="Threshold" id="ton"/>\
							// 		</div>\
							// 	</div>\
							// </div>';
							//small-12 large-12 columns
						//	'+privates.currentEntry.filterOn+' '+privates.currentEntry.condition+' '+privates.currentEntry.thershold+'\
							if(multiplelevel)
							{
								html+='<div class="small-12 large-12 columns and" id="entry_Area_sub_'+randKey+'" style=" background:#fefefe; padding:3px;"> <span class="label alert radius and_'+randKey+'" style="opacity:0.9">and</div></div>';
							}
						html+='</div>'
						if(multiplelevel)
						{
							html+='<div class="small-12 large-12 columns or" id="entry_Area_'+randKey+'" style=" background:#fefefe; padding: 10px 0px; "> <span class="label alert radius or_'+randKey+'" style="opacity:0.9">or</div></div>' //width:auto;
						}

						// if ($(this).parent().parent().children('andGroup').length > 0) {
						//      // do something
						//      console.log('have and group')
						//      $(this).parent().parent().parent().children('#andgroup').append(html)
						// }
						// else
						// {
						// 	console.log('create and group')
						// 	var andhtml='<div class="row collapse andgroup"  style="margin-top:10px; border: 1px dotted #666;" id="andgroup"> </div>'
						// 	$(this).parent().parent().before(andhtml);
						// 	$(this).parent().parent().parent().children('#andgroup').append(html)
						// }
						$(this).parent().parent().before(html)
						leng=$( ".entry_Area#entry_Area_"+randKey ).parentsUntil( ".seAdveSearch" ).length;
						console.log(leng)
						if(leng==0)
						{
							ikey="entry_Area_"+randKey;
							// newTemp.or[ikey]=privates.currentEntry;
							newTemp[ikey]={};
							newTemp[ikey].condition=privates.currentEntry.condition
							if(privates.currentEntry.filterOn=='meter')
							{
								var val='consumption'
							}
							else if(privates.currentEntry.filterOn=='customer')
							{
								var val='customerconsumption'
							}
							else
							{
								var val=privates.currentEntry.filterOn
							}
		              		newTemp[ikey].filterOn=val
		             		newTemp[ikey].thershold=privates.currentEntry.thershold
		             				        // console.log(JSON.stringify(newTemp))
		             				        // console.log(privates.currentEntry)
		             		clearPrivateEntry()

						}
						else
						{
							var tempArr=[];
							var tempArridx=[];
							$( ".entry_Area#entry_Area_"+randKey ).parentsUntil( ".seAdveSearch" ).each(function(key){
								tempArr.push($(this).attr('id'))
								tempArridx.push($(this).index())
								console.log($(this).attr('id')+' - '+$(this).index())
								
								console.log($(this).attr('id')+' - '+$(".seAdveSearch").index(this))
							})
							
							checkEval(tempArr.reverse(),privates.currentEntry,leng,"entry_Area_"+randKey,tempArridx.reverse())
							// console.log(JSON.stringify(newTemp))
							clearPrivateEntry()
							// console.log(backendForm.conditions)
						}
						$('#'+boxparent).remove();
						if(multiplelevel)
						{
							$('.entry_Area #entry_Area_sub_'+randKey+'.and ').off().on('click','span.and_'+randKey,function(){
								var id=$(this).parent().attr('id')
								parentID=$(this).parent().parent().attr('id')
								$(this).parent().remove();
								newinsertUI('#'+parentID,true,'sub','and')
								changeSelection()
								// newinsertUI('.and#'+id)
							});
							$('#entry_Area_'+randKey+'.or ').off().on('click','span.or_'+randKey,function(){
								var id=$(this).parent().attr('id');
								parentID=$(this).parent().parent().attr('id')

								$(this).parent().remove();
								if(parentID=='seAdveSearch')
								{
									newinsertUI('.seAdveSearch',true,'base','or')
									changeSelection()
								}
								else
								{
									newinsertUI('#'+parentID,true,'sub','or')
									changeSelection()
								}

							})		
						}
						
							
					}
			})
   		}
   		function clearPrivateEntry()
   		{
   			privates.currentEntry={}
   		}
   		function checkEval(keys,newvalue,len,crkey,indexarray)
		{		
				// console.log(keys)
				// console.log(newvalue)
				// console.log(newTemp)
				var str='newTemp';
		        $.each(keys,function(key,value){
		        	test=eval("(value in "+str+")");
		            if(!test)
		            {
		              eval(str+"[value]={}");
		              str+="['"+value+"']";
		            }
		            else
		            {
		              str+="['"+value+"']";
		            }
		            if(key==len-1)
		            {

		              // type=$('#'+crkey).data('type');
		              // eval(str+"[type]={}");
		              // str+="['"+type+"']";
		              eval(str+"[crkey]={}");
		              str+="['"+crkey+"']";
		              if(newvalue.filterOn=='meter')
		              {
		              	var keyval='consumption';
		              }
		              else if(newvalue.filterOn=='customer')
					  {
						var keyval='customerconsumption'
					  }
		              else
		              {
		              	var keyval=newvalue.filterOn
		              }
		              eval(str+".condition=newvalue.condition");
		              eval(str+".filterOn=keyval");
		              eval(str+".thershold=newvalue.thershold");
		              
		            }
		        })
		        

		}
   		var formValidate=function(e){
   			privates.currentEntry.filterOn=$('#'+e+' #filterOn').val();
   			if(privates.currentEntry.filterOn=='filterOn')
   			{
   				return false;
   			}
   			privates.currentEntry.condition=$('#'+e+' #condition').val();
   			if(privates.currentEntry.condition=='Condition')
   			{
   				return false;
   			}
   			privates.currentEntry.thershold=$('#'+e+' #thershold').val();
   			if(!($.isNumeric(privates.currentEntry.thershold)))
   			{
   				return false;
   			}
   			return true;
   		}
   		
   		var addNewBlock=function(e){
   			randKey=Math.floor((Math.random()*9999999)+9999);
   			var html='<div class="row collapse entry_Area" style="margin-top:10px; border-top:1px dotted #666; border-bottom:1px dotted #666;" id="entry_Area_sub_'+randKey+'">\
							</div>';
			$(e).after(html);
			return '#entry_Area_sub_'+randKey;
   		}
   		var POPUI=function(id){
   			// privates.sensor='sensor';
   			var self=this;
   					// 	var html='<div class="row">\
					// 	<div class="class="small-8 large-8 columns" id="dataSensor">\
					// 	  <div class="regular label" data-id="sensor" id="t1">SENSOR</span></div>\
					// 	  <div class="secondary label" data-id="amr" id="t2">AMR</span></div>\
					// 	  <div class="secondary label" data-id="weather" id="t3">WEATHER</span></div>\
					// 	</div>\
					// 	<div class="class="small-4 large-4 columns">\
					// 	</div>\
					// </div>';
					// html+='<div class="row collapse" style="margin-top:10px; border-top:1px dotted #666; border-bottom:1px dotted #666;" id="addFilter_Btn">\
					// 		<div class="small-12 large-12 columns" >\
					// 			<span class="alert radius label" style="margin-left: 10px; margin: 5px;" id="showSearchFilter"> Add </span>\
					// 		</div>\
					// </div>'; 
					// $(id).append(html);

					$('#dataSensor .label').click(function(){
						var id=$(this).attr('id');
						privates.sensor=$(this).data('id');
						uiEvents.call(self);

						// $('#dataSensor .regular').toggleClass('regular').toggleClass('secondary')
						// $(this).toggleClass('secondary').toggleClass('regular');
						// newinsertUI(id,false,'base','or');

						
					})
					newTemp={}
					newinsertUI(id,false,'base','or')
					changeSelection()

					
   		}
   		var changeSelection=function(){
   			if(privates.sensor=='sensor')
   			{
   				filterOnlist=[];
	   			$.each(havePermission[0].sensors,function(key,value){
	              // if(jQuery.inArray(value,derivedList)>=0)
	                filterOnlist.push(value)
	            })
   			}
   			else if(privates.sensor=='amr')
   			{
   				filterOnlist=['meter','customer']
   			}
   			else if(privates.sensor=='weather')
   			{
   				derivedOnlist=['rainfall','temp','humidity'];
   				filterOnlist=[];
	   			$.each(havePermission[0].sensors,function(key,value){
	              if(jQuery.inArray(value,derivedOnlist)>=0)
	                filterOnlist.push(value)
	            })
   			}

			var html='<option value="Filter">Filter On</option>'
			$.each(filterOnlist,function(key,value){
				html+='<option value="'+value+'">'+value.toUpperCase()+'</option>'
			})
			$('#filterOn').empty();
			$('#filterOn').append(html);
   		}
   		var uiEvents=function(){
   			var sensor = (privates.sensor =='sensor') ? "regular":"secondary";
   			var amr = (privates.sensor =='amr') ? "regular":"secondary";
   			var weather = (privates.sensor =='weather') ? "regular":"secondary";
   			var html='<a href="#" class="arrow" id="ClickAclose"> </a>\
   					<div class="row collapse">\
   						<div class="small-8 large-8 columns" id="dataSensor">\
						  <div class="'+sensor+' label" data-id="sensor" id="t1">SENSOR</span></div>\
						  <div class="'+amr+' label" data-id="amr" id="t2">AMR</span></div>\
						  <div class="'+weather+' label" data-id="weather" id="t3">WEATHER</span></div>\
						</div>\
						<div class="small-4 large-4 columns">\
							<span class="label small warning" id="clear" style="float:right">Clear</span>\
						</div>\
					</div>';
					var btnhtml='<div class="row collapse">\
								<span class="success radius label" style="  float: right;   margin-right: 15px;  margin-top: 10px;" id="advancedSearch_btn"> Search </span>\
							</div>'
					if ($(".advsearch").size()){
						$( '#advsearch').empty();
						$( '#advsearch').append(html+'<div class="row collapse seAdveSearch" id="seAdveSearch"></div>'+btnhtml)	
					}
					else
					{
						$( '.control_area').after( '<div class="advsearch" id="advsearch" style="display:none;">'+html+'<div class="row collapse seAdveSearch" id="seAdveSearch"></div>'+btnhtml+'</div>')
					
					}
					POPUI('.seAdveSearch')
   			$('#adv_btn').off().on('click',function(){
				// if(advancepop)
				// {
				// 	$('#advsearch').hide();
				// }
				// else
				{
					$('#advsearch').show();
				}
				// advancepop=!advancepop;
				advancepop=true;
   			})
   			$('#ClickAclose').off().on('click',function(){
   				$('#advsearch').hide();
   				// advancepop=!advancepop;
   				advancepop=false;
   			});
   			$('#advancedSearch_btn').off().on('click',function(){
					conditionalFilter.call()
			});
   			$('#clear').off().on('click',function(){
   					privates.sensor='sensor';
					uiEvents.call(self);
			});

   			$('.clearlink').off().on('click',function(){
   					// $('#taglist .tags .closeTag').each(function(e){
   					// 	$(this).trigger('click');
   					// })
   					// resetObj()
   					// refreshTray();
   					resetSel()
					$(settings.displayTags).empty();
					$('.control_area .label').removeClass('regular').addClass('secondary')
					$('.control_area .close_btn').trigger('click');
					$('.control_area .sub_close_btn').trigger('click');
					$(privates.ID).val('');
					privates.obj.splice(0,privates.obj.length);
					self.clear()
			});

			$('#taglist_btn').click(function() {
				if ($(".tagsAvaliable").size()){
					$(".tagsAvaliable").remove();
				}
				else
				{
					$( '.control_area').after( '<div class="tagsAvaliable"></div>')
					$.each(settings.availableTags,function(key,val){
						$('.tagsAvaliable').append('<div class="tagsLabelsItem" id="tagsLabelsItem" data-key='+key+'>'+val.label+'</div>')
					})
					$('.tagsAvaliable .tagsLabelsItem').off().on('click',function(){
						selectedID=$(this).data('key');

						privates.terms=[];
						
						privates.terms.push( settings.availableTags[selectedID].label );
						privates.terms.push( "" );
						Itms=privates.terms.join( ": " );
						$(".search_Area "+privates.ID).val(Itms);
						$(".tagsAvaliable").remove();
						privates.ui=settings.availableTags[selectedID]
						loadSuggestions.call()
					})
				}
						
			});
			if(settings.autofill==true)
			{
				$(privates.ID).bind( "keydown", function( event ) {
					if ( event.keyCode === $.ui.keyCode.TAB && $( self ).data( "ui-autocomplete" ).menu.active ) 
		        	{
		          		event.preventDefault();
		        	}
		        	if(event.keyCode === $.ui.keyCode.ENTER)
		       		{
		       			var dat={};
			        	var wwidth=0;
			        	stdata=$(this).val().split(':');
			        	len=stdata.length
			        	if(len==2&&stdata[1]!='')
			        	{
				        	privates.fullText=$(this).val();
				        	dat[stdata[0]]=stdata[1];
				        	dat.randomKey=Math.floor((Math.random()*9999999)+9999);
				        	privates.obj.push(dat);
				        	privates.sze=privates.obj.length;
				        	console.log(privates)

				        	tag='<div class="tags" id="'+dat.randomKey+'" data-sidx="'+stdata[1]+'""><div class="labl">'+$(this).val()+'</div><div class="xmark">x</div></div>';
				        	$(settings.displayTags).append(tag);
				        	$(privates.ID).val('');
				        	$(settings.displayTags).children('.tags').each(function(value){
				        			thisid=$(this).attr('id')
				        			wwidth+=$('#'+thisid).outerWidth(true)+15;
				        	})

				        	$( settings.displayTags+ ' .xmark').off().on('click',function(){
								// console.log(parentID)
								val=$(this).parent().data('sidx')-1;
								$.each(privates.obj,function(key,val){
									if(val===val)
									{
										index=key;
										return false;
									}
									
								})
								$(this).parent().remove();
								privates.obj.splice(index, 1)
							//	animat();
							})
						}	
		       		}
				})
				.autocomplete({
					minLength: 0,
					source: function( request, response ) {
			          // delegate back to autocomplete, but extract the last term
			          privates.input=$(privates.ID).val();
			          privates.converted=self.toProperCase(privates.input);

				 	  var callback = function (predictions, status) {
								if (status != google.maps.places.PlacesServiceStatus.OK) {
								    return;
								}
								  privates.UIcompleteData= $.extend(true,[],settings.availableTags);
						  		  var results = document.getElementById('ui-id-1');

								for (var i = 0, prediction; prediction = predictions[i]; i++) {
								  	temp={};
								  	temp.label=prediction.description;
								  	temp.value=prediction.description;
								  	temp.desc='';
								  	temp.group='address';
								    privates.UIcompleteData.push(temp);
								}
							response( $.ui.autocomplete.filter(
			            		privates.UIcompleteData, extractLast( request.term ) ) ); 
							}
	  				  privates.service.getQueryPredictions({ input: privates.converted}, callback);
	  				  
			        },
			        focus: function() {
			          // prevent value inserted on focus
			          return false;
			        },
			        select: function( event, ui ) {
		        	
			          privates.terms = split( this.value );
			          // remove the current input
			          privates.terms.pop();
			          // add the selected item
			          privates.terms.push( ui.item.label );
			          privates.terms.push( "" );
			          switch(ui.item.group)
			          {
			          	case 'options':
								this.value = privates.terms.join( ": " );
								privates.ui=ui.item;
								loadSuggestions.call()
			          	break;
						case 'address':
			          			this.value = privates.terms.join( "" );
							break;
						default:
			          			this.value = privates.terms.join( "" );
							break;
			          }
			          // add placeholder to get the comma-and-space at the end
			          return false;
			        }
				})
				.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
					switch(item.group)
					{
						case 'options':
							return $( "<li>" )
							.append( '<a><div class="completeIcon"><img src="images/tag.png" width="10px"></div><div class="completeLabel">' + item.label +  "</div></a>" )
							.appendTo( ul );
							break;
						case 'address':
							return $( "<li>" )
							.append( '<a><div class="completeIcon"><img src="images/loc.png" width="10px"></div><div class="completeLabel">' + item.label +  "</div></a>" )
							.appendTo( ul );
							break;
						default:
							return $( "<li>" )
							.append( '<a><div class="completeIcon"><img src="" width="10px"></div><div class="completeLabel">' + item.label +  "</div></a>" )
							.appendTo( ul );
							break;
					}
			      
			    };

			}
   		}

   		self.clear= function()
		{
		    privates.obj=[];
		    privates.ID='#'+elem.attr('id');
		    $(privates.ID).siblings('.taglist').empty();
		    $(privates.ID).val('');
		 	$('#watertype .regular').each(function(e){
		 		widd=$(this).attr('id')
		 		$('#'+widd).removeClass('regular').addClass('secondary')
		 	})
		    return false;
		    // $(privates.ID).css({left:10,width:280})
		};

		var process= function (url, type, params, successCallback, errorCallback) {
		  $.ajax({
		          success : successCallback,
		          error : errorCallback,
		          data : params,
		          url : url,
		          type : 'POST',
		          dataType : 'json'
		  });
		}

		self.toProperCase=function (s)
		{
		  return s.toLowerCase().replace( /\b((m)(a?c))?(\w)/g,
		          function($1, $2, $3, $4, $5) { if($2){return $3.toUpperCase()+$4+$5.toUpperCase();} return $1.toUpperCase(); });
		}
		var split= function( val ) {
		      return val.split( /,\s*/ );
		}

		var extractLast= function( term ) {
		      return split( term ).pop();
		}

		var loadSuggestions=function (term,item){
			$(".suggestionsList").remove();
			
			var finalUrl=settings.suggestionUrl+'?fieldname='+privates.ui.value;
			$( '.control_area').after( '<div class="suggestionsList"></div>')
			$('.suggestionsList').append('<div class="close" id="closeBtn" style=" font-size: 12px;float: right;padding: 2px;">close</div><div class="suggestionGroups"></div><div class="suggestionGroupItems"></div>')
			$('.suggestionGroupItems').append('<div class="suggestionLabelsItem" >Suggestions:<br /> Fetching ....</div>');
			$('#closeBtn').click(function(){
				$(".suggestionsList").remove();
			})
			process(settings.proxyUrl, 'GET',{ url : finalUrl}, function(resp) {
	                	$('.suggestionGroups').empty();
	                	$('.suggestionGroupItems').empty();
	                    if(resp.errorCode=="0")
	                    {
	                     	privates.groupedByInit=split_by_initials(resp.response.suggestions,20,0)
	                     	$.each(Object.keys(privates.groupedByInit).sort(),function(ki,itit){
	                     		items=privates.groupedByInit[itit];
	                     		$('.suggestionGroups').append('<a href="#'+itit+'">'+itit.toUpperCase()+'</a>')
	                     		$('.suggestionGroupItems').append('<div class="suggestionLabelsItemGroup" id="'+itit+'" >'+itit.toUpperCase()+'</div>')
	                     		$.each(items,function(key,value){ //resp.response.suggestions
	                     			$('.suggestionGroupItems').append('<div class="suggestionLabelsItem" id="suggestionLabelsItem" data-g='+itit+' data-key='+key+'>'+value.toLowerCase()+'</div>')
								
	                     		})
	                     	})
	                     	suggestionEvents.call(self)
	                     	$('.suggestionGroupItems #suggestionLabelsItem').off().on('click',function(){
									selectedID=$(this).data('key');
									selectedgroup=$(this).data('g');
									val=privates.groupedByInit[selectedgroup][selectedID];
									
							        Itms=privates.terms.join( ": " );
							        Itms+=val;
							        $(".search_Area "+privates.ID).val(Itms);
							        $(".suggestionsList").remove();
							        simulateKeyPress();
							})
	                    }
	                    else  if(resp.errorCode!="0")
	                    {
	                     	$('.suggestionGroupItems').append('<div class="suggestionLabelsItem" >Suggestions:<br />No suggestion found....</div>')
						}
						else
						{
						  	$('.suggestionGroupItems').append('<div class="suggestionLabelsItem" >Suggestions:<br />Oh no, Something gone wrong.</div>')
						}
					},
	                function(e) {
	                   	 
	                }
	        );

		}
		var simulateKeyPress=function () {
				$(privates.ID).focus();
				// var e = jQuery.Event("keypress");
				// e.keyCode = $.ui.keyCode.ENTER;
				// $( "#"+ElementID).trigger(e);
		}
		var split_by_initials=function (names,ammount,tollerance) {
			tolerance=tollerance||0;
		    total = names.length;
		    var filtered={}
		    var result={};
		    $.each(names,function(key,value){
		    	if(value)
		    	{
			    	val=value.trim();
			    	var pattern = /[a-zA-Z0-9&_\.-]/
			    	if(val[0].match(pattern)) {
			    		intial=val[0];
			    	}
			    	else
			    	{
			    		intial='sym';
			    	}
			    	intal=intial.toLowerCase();
			    	if(!(intal in filtered))
			    		filtered[intal]=[];
			    	
					filtered[intal].push(val);
				}
		    })
		    var count = 0;
		    var key = '';
		    var temp = [];
		    console.log(Object.keys(filtered).sort())
		    $.each(Object.keys(filtered).sort(),function(ky,value){
		    	count += filtered[value].length;
		    	temp = temp.concat(filtered[value])
		        key += value+'-'; 
		        if (count >= ammount || count >= ammount - tollerance) {
		            key = key.substring(0, key.length - 1);
		            result[key] = temp;
		            count = 0;
		            key = '';
		            temp = [];
		        }
		    }) 
		    if(Object.keys(result).length==0)
		    {
		    	return filtered;
		    }
		    else
		    {
		    	return result;
		    }
		    
		}
		var suggestionEvents=function(){
			
		}
   		var checkBrowser=function(){
   			var matched, browser;
	       	jQuery.uaMatch = function( ua ) {
		   	ua = ua.toLowerCase();
		   	var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
	        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
	        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
	        /(msie) ([\w.]+)/.exec( ua ) ||
	        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
	        [];
				return {
			        browser: match[ 1 ] || "",
			        version: match[ 2 ] || "0"
			    };
			};

			matched = jQuery.uaMatch( navigator.userAgent );
			browser = {};
			
			if ( matched.browser ) {
			    browser[ matched.browser ] = true;
			    browser.version = matched.version;
			}
			
			// Chrome is Webkit, but Webkit is also Safari.
			if ( browser.chrome ) {
			    browser.webkit = true;
			} else if ( browser.webkit ) {
			    browser.safari = true;
			}
			
			privates.browser = browser;
   		}





   		checkBrowser.call(this)
   		generate.call(this);
   		

   		$.fn.extend({
		  
		  getData: function() {
		  	getObj=[]
		  	$.each(privates.obj,function(key,value){
		  		delete value.randomKey;
		  		getObj.push(value)
		  	})
		  	return getObj; 
		  },getCondition: function() {
		  	backendForm.conditions=[]
		  	dig( newTemp, backendForm ) ;
		  	var response={}
		  		response.basetype=privates.sensor;
		  		response.condition=backendForm;
		  	return response; 
		  }
		  //, updateData:function(newSeries)
		  // {
		  // 		settings = $.extend({},defaults, newSeries );
		  // 		generate.call(self);
		  // }
		});
   	};

	$.fn.searchFilter = function(options)
   	{
       return this.each(function()
       {
           var element = $(this);
          
           // Return early if this element already has a plugin instance
           if (element.data('searchFilter')) return;

           // pass options to plugin constructor
           var searchFilter = new SearchFilter(this, options);
          
           // Store plugin object in this element's data
           element.data('searchFilter', searchFilter);
       });
   	};
})(jQuery);