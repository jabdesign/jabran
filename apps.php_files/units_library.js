/*	IMPORTANT	
	All units refer to sensortype display name in LOWERCASE
*/
function converter(request,unit1)
	{
		if(unit1 === null)
			return unit1;
		switch(request){
			case 'cubic meter to mgd':
				return unit1*0.000264;
			break;
			case 'mgd to cubic meter':
				return unit1/0.000264;
			break;
			// Celcius
			case 'centigrade to farenheit':
				return ((unit1*9)/5)+32;
			break;
			case 'centigrade to kelvin':
				return unit1 + 273.15;
			break;
			// Farenheit
			case 'farenheit to centigrade':
				return ((unit1-32)*5)/9;
			break;
			case 'farenheit to kelvin':
				return (((unit1 - 32)*5)/9)+273.15;
			break;
			// Kelvin
			case 'kelvin to centigrade':
				return unit1 - 273.15;
			break;
			case 'kelvin to farenheit':
			return ((unit1 - 273.15)*1.8)+32;
			break;
			// Meters (Pressure)
			case 'meter to bar':
			return (unit1/10.197);
			break;
			case 'bar to meter':
			return (unit1*10.197);
			break;
			case 'meter to psi':
			return (unit1 * 1.42197020632);
			break;
			case 'psi to meter':
			return (unit1 / 1.42197020632);
			break;
			// Volts (Battery)
			case 'volts to millivolts':
			return (unit1*1000);
			break;
			case 'millivolts to volts':
			return (unit1/1000);
			break;
			// 	Litres
			case 'litres to m^3':
				return unit1/1000;
			break;
			case 'm^3 to litres':
				return unit1*1000;
			break;
			default:
				throw new Error("Request not interpretable");
			break;
		}	
	}