// Snapshot Tool - JS Visenti
( function( window, undefined ) {
  "use strict";
  function  Snapshot(){

    var privates = {
      controlposition:google.maps.ControlPosition.LEFT_TOP,
      transform:null,
      mapleft:null,
      maptop:null,
      mouseX:null,
      mouseY:null,
      cropmode:null,
      listener:null,
      map:'#map-canvas',
      el:null,
      cropxy:null
    };
    /*  Init  */
    if(!window.hasOwnProperty('html2canvas') || !window.hasOwnProperty('map')){
      Notify('Snapshot Tool','Dependancy Error','normal',true);
      return;
    }

    /*  Public Methods  */
    this.shout = function(){  console.log(privates);  }

    this.snap = function(){
      preLoader('Generating screenshot','modal',true);
      html2canvas($('#map-canvas'),
      {
        useCORS: true,
        logging:false,
        proxy:'../controllers/html2canvas-proxy.php',
        onrendered: function(canvas)
        {
          var ctx = canvas.getContext('2d');
          var img = new Image();
          var dataUrl= canvas.toDataURL('image/png');
          console.log(dataUrl);
          var tempCanvas = document.createElement('canvas');
          var context = tempCanvas.getContext('2d');
          tempCanvas.width = privates.cropxy[0];
          tempCanvas.height = privates.cropxy[1];
          img.onload = function() {
            var sourceX = privates.mouseX;
            var sourceY = privates.mouseY;
            var sourceWidth = privates.cropxy[0];
            var sourceHeight = privates.cropxy[1];
            context.drawImage(img, sourceX, sourceY, sourceWidth, sourceHeight,0 ,0 ,sourceWidth, sourceHeight);
            var image_data = tempCanvas.toDataURL('image/png');
            $.post('./controllers/snapshot_service.php',{img:image_data});
            preLoader(false);
          };
          img.src = dataUrl;
        }
      });
    }

    this.cropmode = function(x){
      var self = this;
      privates.cropmode = x;
      if(privates.cropmode){
         map.setOptions({ draggableCursor: 'crosshair' });
         map.setOptions({
          draggable: false,
          scrollwheel: false
        });        
        $(privates.map).bind('mousedown.snapshot',function(e){
          var parentOffset = $(this).offset(); 
          var relX = e.pageX - parentOffset.left;
          var relY = e.pageY - parentOffset.top;
          privates.mouseX = relX;
          privates.mouseY = relY;
          $('body').append('<div class="snapshot_selection_tool" style="position:absolute;top:'+privates.mouseY+'px;left:'+privates.mouseX+'px;z-index:20;background:transparent;opacity:0.5;border:1px dashed #000;"></div>');
          startcrop.call(self);
        });
        $(privates.map).bind('mouseup.snapshot',function(){
          endcrop.call(self);
        });
      }
    }

    this.newSlide = function(){
      var html = '<div class="snapshot_slide"><ul></ul></div>';
      $('body').append(html);
    }
    this.addImage = function(img){
      $('.snapshot_slide').append('<li><img src="'+img+'"/></li>');
    }

    /* Private Methods */

    var startcrop = function(){
      var self = this;
      privates.el = $('.snapshot_selection_tool');
      $(privates.map).bind('mousemove.snapshot',function(e){
           var parentOffset = $(this).offset(); 
           var relX = e.pageX - parentOffset.left;
           var relY = e.pageY - parentOffset.top;
           cropmove.call(self,relX,relY);
        });
    }

    var endcrop = function(){
      var self = this;
      $(privates.map).unbind('mousemove.snapshot');
      $(privates.map).unbind('mousedown.snapshot');
      $(privates.map).unbind('mouseup.snapshot');
      privates.cropxy = [$(privates.el).width(),$(privates.el).height()];
      $('.snapshot_selection_tool').remove();
      privates.cropmode = null;
      map.setOptions({ draggableCursor: null });
      map.setOptions({
          draggable: true,
          scrollwheel: true
        });
      $('#snapshot_crop_button').removeClass('sel');
      self.snap();
    }

    var cropmove = function(x,y){
      var width = x - privates.mouseX;
      var height = y - privates.mouseY;
      $(privates.el).css({width:width,height:height});
    }

    var setupmap = function(){
      privates.transform = $(".gm-style>div:first>div").css("transform");
      var comp = transform.split(",");
      privates.mapleft = parseFloat(comp[4]);
      privates.maptop = parseFloat(comp[5]);
      $(".gm-style>div:first>div").css({ //get the map container. not sure if stable
        "transform":"none",
        "left":privates.mapleft,
        "top":privates.maptop,
      });
    }

    var resetmap = function(){
      $(".gm-style>div:first>div").css({
          left:0,
          top:0,
          "transform":privates.transform
        });
    }

    // deploy UI
    var deploy = function(){
      var self = this;
      var html = '<div class="sidebutton_gis" id="snapshot_crop_button"><img src="images/crop.png"></div>'; 
      var controldiv = document.createElement('div');
      controldiv.innerHTML = html;
      map.controls[privates.controlposition].push(controldiv);
      //events
      $('#snapshot_crop_button').click(function(){
        $(this).toggleClass('sel');
        self.cropmode($(this).hasClass('sel'));
      });
    }

    /* End of Declaration */
    deploy.call(this);
  }
    window.Snapshot = Snapshot;
} )( window );