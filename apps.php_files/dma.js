// DMA - Supply Zones module
( function( window, undefined ) {
  "use strict";
  function  Dma(){
    var privates = {
      controlposition:google.maps.ControlPosition.LEFT_TOP,
      zones:[],
      dmalist:{},
      watertype:'potable',
      newwaterzones:['Bedok','Kranji','Ulu Pandan','Seletar'],
      newwaterclusters:{"Seletar":["Ang Mo Kio"],"Kranji":["Sembawang","Woodlands","Senoko"],"Bedok":["Changi East","Tampines"],"Ulu_Pandan":["Jurong Island","Orchard"]},
      water:true,
      state:false
    };
    this.shout = function(){console.log(privates)};
    this.init = function(){
      load.call(this);
    }
    this.reload = function(){
      privates.zones = [];
      privates.dmalist = {};
      load.call(this);
    }
    this.addtobucket = function(tagID,state){
       var currentZoneData,MeterID;
       $.each(privates.dmalist,function(key,value){
        if(tagID==value.name)
        {
          currentZoneData=value;
        }
       })
       if(state==1)
       {
        removeEmpty();
        MeterID=currentZoneData._id
        unitObj.selectedUnit.push(MeterID);
        unitObj.sensorSelection[MeterID]=[];
        unitObj.sensorMeta[MeterID]=privates.dmalist[MeterID]
        unitObj.lastselected.meter=MeterID;
        unitObj.lastselected.index=0;
       }
       else
       {
        //remove
        MeterID=currentZoneData._id
        removeunitID(MeterID)
       }
       refreshTray();
       showAction();
    }

    this.meta = function(name){
      var ret = null;
      _.each(privates.dmalist,function(obj){
        if(obj.display_name === name)
          ret = obj;
      });
      return ret;
    }
    this.requestmeta = function(){ return privates.dmalist;  };
    // private routines
    var deploy = function(){
      var self = this;
      var html = '';
      var state = privates.state;
      if(!state){
      html += '<div class="sidebutton_gis">\
        <div class="leftgmapbtn" id="supplyzonebtn">Z</div>';

        if(privates.water)
          html += '<div class="leftgmapbtn" id="supplywaterbtn">W</div></div>';
        html += '<div class="expand_dma hide typezone" id="zoneSelection"><ul>';
      }
        _.each(privates.zones,function(z){
          var zid=z.replace(' ','_');
          html += '<li class="wt_p" data-id="'+zid+'" id="'+zid+'"><span>'+z+'</span></li>';
        });
        _.each(privates.newwaterzones,function(z){
          var zid=z.replace(' ','_');
          html += '<li class="wt_n hide halfwidth" data-id="'+zid+'"  id="'+zid+'"><span>'+z+'</span><img class="right arr" src="images/dotarrow.png" width="6px">';
          if(!state){
            var cc = z.replace(/\s/gi, "_");
            if(cc in privates.newwaterclusters){
              html += '<ul class="hide ZoneCluster" id="ZoneCluster">';
              _.each(privates.newwaterclusters[cc],function(c){
                var subid=c.replace(' ','_');
                html += '<li class="wt_n_s halfwidth" data-id="'+subid+'" id="'+subid+'">'+c+'</li>';
              });
              html += '</ul>';
            }
          }
          html += '</li>';
        });
        if(!state)
          html += '</ul></div>';
      if(state === true){
        $('.expand_dma ul:first-child:eq(0)').empty().append(html);
        return;
      }
        if(privates.water){
          html += '<div class="expand_dma hide typewater" id="watertype"><ul><li data-id="potable" class="sel halfwidth" id="potable">Potable</li><li data-id="newwater" class="halfwidth" id="newwater">NeWater</li></ul></div>';
        }
      var controldiv = document.createElement('div');
      controldiv.innerHTML = html;
      map.controls[privates.controlposition].push(controldiv);
      uievents.call(self);
      privates.state = true;
    }

    var uievents = function(){
      $(document).on('click','#supplyzonebtn',function(){
        $(this).toggleClass('sel');
        $('.expand_dma.typezone').toggleClass('hide');
        $('.expand_dma.typewater').addClass('hide');
        $('#supplywaterbtn').removeClass('sel');
      });
      $(document).on('click','#supplywaterbtn',function(){
        $(this).toggleClass('sel');
        $('.expand_dma.typewater').toggleClass('hide');
        $('.expand_dma.typezone').addClass('hide');
        $('#supplyzonebtn').removeClass('sel');
        // $('.typewater#watertype #potable').trigger('click');
      });
      $(document).on('click','.expand_dma li',function(e){
        var prnt = $(this).parents('.expand_dma').hasClass('typezone');
        if(prnt){
          if($(this).children('ul:first').length>0){
            $('.expand_dma li ul').addClass('hide');
            $(this).find('ul').removeClass('hide');
            $(this).toggleClass('sel');
            var item = $(this).data('id');
            if($(this).hasClass('wt_n')&&item)
            {
             
              if($('#'+item).hasClass('sel'))
              {
                _addTag(this,'zone');
              }
              else
              {
               _removeTag(this,'zone');
                $('#'+item+' ul li.sel').each(function(e){
                  $(this).removeClass('sel');
                })
                $(this).find('ul').addClass('hide');
              }
              options.subzone=[];
              $('.typezone#zoneSelection .wt_n.sel').each(function(key,value){
                    options.subzone.push($(this).attr('id'))
              })
              settings = $.extend( {}, defaultSettings, options );
            }
            // return;
          }
          else
          {
            $(this).toggleClass('sel');
            var state = $(this).hasClass('sel');
            var item = $(this).data('id');
            if($(this).hasClass('wt_p')){
              
              
              console.log(state)
              if($('#'+item).hasClass('sel'))
              {
                _addTag(this,'zone');
                region(item,1);
                dma.addtobucket(item,1);
              }
              else
              {
                _removeTag(this,'zone');
                region(item,0);
                dma.addtobucket(item,0);
              }
              options.subzone=[];
              $('.typezone#zoneSelection .wt_p.sel').each(function(key,value){
                    options.subzone.push($(this).attr('id'))
              })
              settings = $.extend( {}, defaultSettings, options );
             
            }
            else if($(this).hasClass('wt_n_s')){
                var item = $(this).data('id');
                if($('#'+item).hasClass('sel'))
                {
                  _addTag(this,'subzone');
                }
                else
                {
                  _removeTag(this,'subzone');
                }
            }
          }
        }
        else
        {

          $('.expand_dma.typewater li').removeClass('sel');
          $(this).toggleClass('sel');
          var state = $(this).hasClass('sel');
          var item = $(this).data('id');
          privates.water=item
          if(item === 'potable'){
            $('.expand_dma.typezone li.wt_n').addClass('hide');
            $('.expand_dma.typezone li.wt_p').removeClass('hide');
            options.watertype='potable';
            $('#commercialType .label').hide();
            $.each(noncom.pw,function(key,value){
              $('#commercialType #'+value).show();
            });
            $('#residential').show();
            _toggleTag(this,'sidebutton');
            _removeTagCollection('settings',['zoneSelection','ZoneCluster']);
            options.watertype='potable';
            settings = $.extend( {}, defaultSettings, options );
          }
          else{
            $('.expand_dma.typezone li.wt_p').addClass('hide');
            $('.expand_dma.typezone li.wt_n').removeClass('hide');
            options.watertype='newwater';
            $('#commercialType .label').hide();
            $.each(noncom.nw,function(key,value){
                $('#commercialType #'+value).show();
            });
            $('#residential').hide();
            _toggleTag(this,'sidebutton');
            _removeTagCollection('settings',['zoneSelection']);
            options.watertype='newwater';
            settings = $.extend( {}, defaultSettings, options );
            
          }
        }
        if (!e) var e = window.event;
          e.cancelBubble = true;
          if (e.stopPropagation) e.stopPropagation();
      });
      // $(document).on('click','.expand_dma ul li ul li',function(){
      //   $(this).parents('li').last().toggleClass('sel');
      // });
    }

    var load = function(){
      var self = this;
      preLoader('Loading Supply Zones','modal',true);
      $.get('controllers/proxyman.php',{search:true,SearchQuery:encodeURI(dmajson())},function(d){
        preLoader(false);
        if(d.errorCode>0){
          Notify('Err DMA Update',d.errorMsg,'normal',true)
          return;
        }
        var obj = d.response.hits.hits;
        if(obj.length === 0)
          Notify('DMA','No Supply Zones for this network','normal',true);
        $.each(obj,function(key,value){
          privates.zones.push(value._source.name)
          privates.dmalist[value._id]=value._source;
        })
        deploy.call(self);
      },'json');
    }
    
  }
    window.dma = new Dma();
} )( window );