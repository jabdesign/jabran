/*  Leak Library - JS 2015 - Visenti
                                            */

( function( window, undefined ) {
    "use strict";           // Do it the right way :)
    function Leaklibrary(){  
      var privates = {
        controlposition:google.maps.ControlPosition.LEFT_TOP,
        markers:[]
      };
      if(!window.hasOwnProperty('map'))
      {
        Notify('Warning - LeakLibrary','No map found - exitting','normal',true);
        return;
      }
      this.init = function(){
        $.get('controllers/tlp.php',{endpoint:'api/leak/library/all',enduserdetails:false,method:'get'},
          function(d){
            console.log(d);
            if(d.errorCode>0){
              Notify('Server Error',d.errorMsg,'normal',true);
              return;
            }
            _.each(d.response,function(r){
              var latlng = new google.maps.LatLng(parseFloat(r.lat),parseFloat(r.lon));
              var marker  = new google.maps.Marker({
                position: latlng,
                map:  map,
                visible:true,
                optimized:true
              });
              var contentString = '<b>Leak '+r.caseId+'</b><br/><hr style="margin:5px 0"></hr><ul style="margin: 0;font-size: 12px;">';
              var headers = ['size','type','leakCause','casePriority','address','description'];
              _.each(headers,function(h){
                if(r.hasOwnProperty(h))
                  contentString += '<li>'+h+' <i>'+r[h]+'</i></li>';
              });
              contentString += '</ul>';
              if(r.hasOwnProperty('eventTimestamp'))
                contentString += '<small>'+moment(r.timestamp).format()+'</small>';

              google.maps.event.addListener(marker, 'click', function(){
                if (infowindow) 
                  infowindow.close();
                infowindow = new google.maps.InfoWindow({content: contentString, minWidth: 300});
                infowindow.open(map, marker);
              });
              privates.markers.push(marker);
            });
          },'json')
      }

      /*  End of Declaration  */

    }

  window.Leaklibrary = Leaklibrary;
  } )( window );

  // initiate module
  var leaklibrary = new Leaklibrary();