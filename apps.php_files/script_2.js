function setActionevents()
{
	console.log('Actions ');
	$('#ActionPanel').on('click','input[name=opt]:checked,.snrGrp input[name=opt]:checked',function(){
		var thistype = $(this).attr('type');
		parent=$(this).parent().parent().attr('id')
		
		$('#ActionPanel input[name=opt]:checked').each(function(){
			if($(this).attr('type')!=thistype)
				$(this).prop('checked',false);
		});
		// if(parent=='basic')
		// {
		// 	var actSensor=$(this).val()
		// 	var objID=unitObj.lastselected.meter;
		// 	//var objID=unitObj.selectedUnit[unitObj.selectedUnit.length-1];
		// 	if(jQuery.inArray(actSensor,unitObj.sensorSelection[objID])==-1)
		// 	{
		// 		unitObj.sensorSelection[objID].push(actSensor);
		// 	}
		// 	showBucket();
		// }
	});
	$('#ActionPanel ').on('click','.Hdholder img',function(){
		var thisiD=$(this).parent().attr('id');
		if($('#'+thisiD).hasClass('opnState'))
		{
			$(this).attr('src','images/expand.png');
			$('#'+thisiD+' .snrGrp').hide();
			$('#'+thisiD).removeClass('opnState');
		}
		else
		{
			$(this).attr('src','images/collapse.png');
			$('#'+thisiD+' .snrGrp').show();
			$('#'+thisiD).addClass('opnState');
		}
		
	});
	$('#ActionPanel ').bind('click','.Hdholder img',function(){
		var thisiD=$(this).parent().attr('id');
		if($('#'+thisiD).hasClass('opnState'))
		{
			$(this).attr('src','images/expand.png');
			$('#'+thisiD+' .snrGrp').hide();
			$('#'+thisiD).removeClass('opnState');
		}
		else
		{
			$(this).attr('src','images/collapse.png');
			$('#'+thisiD+' .snrGrp').show();
			$('#'+thisiD).addClass('opnState');
		}
		
	});
	$('#ActionPanel').on('click','.Hdholder label input[name=groupHeader]',function(){
			parent=$(this).closest('.Hdholder').attr('id');
			curent=$(this).attr('id')
			if($('#'+curent).prop('checked'))
			{	
				$('#'+parent+' .snrGrp input[name=opt]').each(function(e){
					if(!$(this).prop('checked'))
						$(this).trigger('click')
				});
			}
			else
			{
				$('#'+parent+' .snrGrp input[name=opt]').each(function(e){
					if($(this).prop('checked'))
						$(this).trigger('click')
				});
			}
			
	});
	$('#ActionPanel').on('click','input[name=opt],.snrGrp input[name=opt]',function(){
		parent=$(this).closest('.borBtm').attr('id');
		var parGroup=$(this).closest('.snrGrp').siblings().children().attr('id'); //.attr('id')
		var currenTgrp=$(this).closest('.snrGrp').attr('id')
		var actSensor=$(this).val()
		var objID=unitObj.selectedUnit[unitObj.selectedUnit.length-1];
		if(parent=='basic')
		{
			if(!allmode&&!lossoMode&&!AMRState)
			{
				if($(this).prop('checked'))
				{

					if(jQuery.inArray(actSensor,unitObj.sensorSelection[objID])==-1)
					{
						unitObj.sensorSelection[objID].push(actSensor);
					}
				
				}
				else
				{
					posindex=jQuery.inArray( actSensor, unitObj.sensorSelection[objID] )
					if(posindex>-1)
						unitObj.sensorSelection[objID].splice(posindex, 1);
				}
			}
			else
			{
				preLoader('DataIntel','Please wait..','normal',true);
				if($(this).prop('checked'))
				{
					$.each(unitObj.sensorSelection,function(key,value){

						
						if(key=='undefined')
						{
							delete unitObj.sensorSelection[undefined]
						}
						else
						{
							//  -----------dma code change ------------------- //
							// _meta=metadata[unitObj.ids[key]].sensorList;
							_meta=unitObj.sensorMeta[key].sensorList;
							
							if(jQuery.inArray(actSensor,unitObj.sensorSelection[key])==-1)
							{
								$.each(_meta,function(key1,value1){
									if(value1._source.sensortype_display==actSensor)
									{
										//console.log(actSensor)
										unitObj.sensorSelection[key].push(actSensor);
									}
									else
									{
										return;
									}
								})
								
							}
						}
					});
					
				}
				else
				{
					$.each(unitObj.sensorSelection,function(key,value){
						
						if(key=='undefined')
						{
							delete unitObj.sensorSelection[undefined]
						}
						else
						{
							posindex=jQuery.inArray( actSensor, unitObj.sensorSelection[key] )
							if(posindex>-1)
								unitObj.sensorSelection[key].splice(posindex, 1);
						}
					});
				}
			}
			showBucket();
		}
		if($('#'+parGroup).prop('checked'))
		{
			count=0;
			$('#'+currenTgrp+' label').each(function(){

				if($(this).children().prop('checked'))
				{
					count++;
				}
			})
			if(count==0)
				$('#'+parGroup).prop('checked',false);
			else
				$('#'+parGroup).prop('checked',true);
			
		}
		else
		{
			count=0;
			$('#'+currenTgrp+' label').each(function(){

                                if($(this).children().prop('checked'))
                                {
                                        count++;
                                }
                        })
			if(count==0)
                                $('#'+parGroup).prop('checked',false);
                        else
                                $('#'+parGroup).prop('checked',true);
		}
		preLoader(false);
	});
	// $('.listSensorList').on('click','input[name=opt]',function(){
	// 	showBucket();
	// });
	$('#actionsList').on('click','#plotgraph',function(event){
		event.stopPropagation();
		if($(event.target).is('span')){
			 var id = $(this).data('id');
            $('#'+id).removeClass('hide');
            return;
		}
		console.log(jobpool)
		jobpool.splice(0,jobpool.length);
		joblist.splice(0,joblist.length);
		batch.splice(0,batch.length);
	// convert sensors to backend types
		var sensorSelection = {};
		_.each(unitObj.sensorSelection,function(val,key,obj){	// station
			sensorSelection[key] = [];	// initiate empty array
			/*  -----------dma code change ------------------- */
			var meta = unitObj.sensorMeta[key];
			/*  -----------dma code change ------------------- */
			var backend_name = 'bad_sensor';
			_.each(val,function(sen){		// selected sensors in this station
				_.each(meta.sensorList,function(sen2){
					if(sen2._source.sensortype_display === sen)	// found display name
						backend_name = sen2._source.sensortype_backend;
				});
				sensorSelection[key].push(backend_name);
			});
		});
		console.log(sensorSelection);
		// determine job
if(selectedep == 'search/data' || selectedep == 'forecast/forecast_default'){

	if(_splitsensors==true && _splitstns==true){
		//	CASE INDIVIDUAL PLOTS
		for (var mtr in sensorSelection){
			var axisToggle = 0;
			sensorSelection[mtr] = sortBySensorType(sensorSelection[mtr]);
			_.each(sensorSelection[mtr],function(el,elid){
				//arr.push(el);
				batch.push(1);
				joblist.push(el);
				jobpool.push([mtr]);
				++axisToggle;
			});
		}		
	}
	else if(_splitsensors==true)
	{
		// CASE SPLIT BY SENSORS
		_.each(sensorSelection,function(el){
			joblist.push(el);
		});
		joblist = _.uniq(_.flatten(joblist));
		joblist = sortBySensorType(joblist);
		var switcher = joblist[0];
		_.each(joblist,function(sensor){
			var arr = [];
			for(var mtr in sensorSelection)
			{
				var temp = sensorSelection[mtr];
				console.log(temp.indexOf(sensor));
				if(sensorSelection[mtr].indexOf(sensor)>=0)
					arr.push(mtr);
			}
			jobpool.push(arr);
			if(sensortype_map[sensor] != sensortype_map[switcher])
				batch.push(1);
			else
				batch.push(0);
			switcher = sensor;
		});
	}
	else if(_splitstns==true)
	{
		// CASE SPLIT BY STATIONS
		for (var mtr in sensorSelection){
			var axisToggle = 0;
			var switcher = null;
			sensorSelection[mtr] = sortBySensorType(sensorSelection[mtr]);
			_.each(sensorSelection[mtr],function(el,elid){
				//arr.push(el);
				if(switcher == null)
					switcher = el;
				if(elid === 0 || axisToggle>=2){ 
					batch.push(1);
					if(axisToggle>=2)
						axisToggle = 0;
				}
				else
					batch.push(0);
				joblist.push(el);
				jobpool.push([mtr]);
				if(sensortype_map[el] != sensortype_map[switcher])
					++axisToggle;
				switcher = el;
			});
		}
	}
	else
	{
		// CASE ECONOMY (NO SPLITTING)
		_.each(sensorSelection,function(el){
			joblist.push(el);
		});
		joblist = _.uniq(_.flatten(joblist));
		joblist = sortBySensorType(joblist);
		var switcher = joblist[0];
		var axisToggle = 0;
		_.each(joblist,function(sensor,idx){
			if(sensortype_map[switcher] != sensortype_map[sensor])
				++axisToggle;
			if(axisToggle>=2)
				{
					batch.push(1);
					axisToggle = 0;
				}
			else
				batch.push(0);
			var arr = [];
			for(var mtr in sensorSelection)
			{
				var temp = sensorSelection[mtr];
				//console.log(temp.indexOf(sensor));
				if(sensorSelection[mtr].indexOf(sensor)>=0)
					arr.push(mtr);
			}
			jobpool.push(arr);
			switcher = sensor;
			//batch.push(!(idx%2));
		});
	}
		/*if(joblist.length>=3 && !stackobj.mode)
		{
			alert('Switching to stack to accomodate all sensors');
			stackobj.mode = true;
		}*/
}
	else
	{
		mapjson = {map:[]};
		for(var stn in sensorSelection){
			_.each(sensorSelection[stn],function(sen){
				mapjson.map.push({sensortype:sen,stnids:[stn]});
			});
		}
		console.log(mapjson);
		joblist.push('');
		jobpool.push('');
	}
		if(_stacked){
			stackobj.mode = true;
		}
		Radiochecked = joblist[0];
		// make sure object is clean
		cleanPlotDataObject();
		plotHQ(1);
	})
	$('#actionsList').on('click','#plotaggregate',function(){
		joblist.splice(0,joblist.length);
		$('input[name=opt]:checked').each(function(){
			joblist.push($(this).val());
		});
		if(joblist.length>=3)
		{
			alert('Cannot select more than TWO different datatypes');
			return;
		}
		Radiochecked = joblist[0];
		plotHQ(2);
		console.log('plot aggregate'+ Radiochecked)
	})
	$('#actionsList').on('click','#plotaverage',function(){
		joblist.splice(0,joblist.length);
		$('input[name=opt]:checked').each(function(){
			joblist.push($(this).val());
		});
		if(joblist.length>=3)
		{
			alert('Cannot select more than TWO different datatypes');
			return;
		}
		Radiochecked = joblist[0];
		plotHQ(5);
		console.log('plot average'+ Radiochecked)
	})
	$('#actionsList').on('click','#plotratio',function(){
		joblist.splice(0,joblist.length);
		jobpool.splice(0,jobpool.length);
		batch.splice(0,batch.length);
		// convert sensors to backend types
		var sensorSelection = {};
		_.each(unitObj.sensorSelection,function(val,key,obj){	// station
			sensorSelection[key] = [];	// initiate empty array
			var meta = unitObj.sensorMeta[key];
			var backend_name = 'bad_sensor';
			_.each(val,function(sen){		// selected sensors in this station
				_.each(meta.sensorList,function(sen2){
					if(sen2._source.sensortype_display === sen)	// found display name
						backend_name = sen2._source.sensortype_backend;
				});
				sensorSelection[key].push(backend_name);
			});
		});
		// ECONOMY SPLITTING
		_.each(sensorSelection,function(el){
			joblist.push(el);
		});
		joblist = _.uniq(_.flatten(joblist));
		joblist = sortBySensorType(joblist);
		var switcher = joblist[0];
		var axisToggle = 0;
		_.each(joblist,function(sensor,idx){
			if(sensortype_map[switcher] != sensortype_map[sensor])
				++axisToggle;
			if(axisToggle>=2)
				{
					batch.push(1);
					axisToggle = 0;
				}
			else
				batch.push(0);
			var arr = [];
			for(var mtr in sensorSelection)
			{
				var temp = sensorSelection[mtr];
				if(sensorSelection[mtr].indexOf(sensor)>=0)
					arr.push(mtr);
			}
			jobpool.push(arr);
			switcher = sensor;
		});
		Radiochecked = joblist[0];
		plotHQ(3);
	})
	// Network coverage HIGH RES routine
	$('#actionsList').on('click','#coverage',function(){
		if(!window.hasOwnProperty('coveragearr'))
		{
			alert('Network coverage not found');
			return;
		}
		if(coveragearr.stations.length === 0)
		{
			alert('No stations found for this network');
			return;
		}
		// clear bucket
		$('.addTray .killme').trigger('click'); 
		//switch to highres
		$('.resbox select').val(0);
		interval = 0;
		_interval = "";
		// set up bucket data layer
		var sensor_array = [];
		_.each(coveragearr.stations,function(stn){
			sensor_array.push(['pressure']);
		});
		bucketFill(coveragearr.stations,sensor_array);
		joblist = ['pressure'];
		$.extend(jobpool,coveragearr.stations);
		jobpool = [jobpool];	//single chart mode
		batch = [0];
		Radiochecked = joblist[0];
		plotHQ(1);
	});

	function bucketFill(arr,sensors){
		_.each(arr,function(id,index){
			var meta = meterInfo(id);
			if(!meta)
				return;
			unitObj.selectedUnit.push(id);
			unitObj.sensorMeta[id] = meta;
			// find sensors
			unitObj.sensorSelection[id] = [];
			_.each(sensors[index],function(sen){
				_.each(meta.sensorList,function(senmeta){
					if(senmeta._source.sensortype_backend == sen)
						unitObj.sensorSelection[id].push(senmeta._source.sensortype_display);
				});
			});
		});
		refreshTray();
	}

	$('#reportArea').on("click",'#sectreport',function(){
	 	report('sector');
	})
	$('#reportArea').on("click",'#overview',function(){
		report('sbys');
	});
	$('#reportArea').on("click",'#zonal',function(){
		report('zonal');
	});
	$('#reportArea').on("click",'#dva',function(){
		report('demand');
	});
	$('#reportArea').on("click",'#dailydemand',function(){
		dailyPdf();
	});
	$('#reportArea').on('click','#custreport',function(){
		report('customer');
	});
	$('#reportArea').on('click','#MQA',function(){
		if(allmode!=false || unitObj.selectedUnit.length>0)
			report('health');
		else
			alert('You have no meters selected.');
	});
	
	$('#edit').click(function(){
         controleWindowID= logWindow('control');
         console.log(MarkerInfoData)

     });
	// $('.actionPanel').on('click','input[name="opt"][type="checkbox"]',
	// function(){
	// 	if($('input[name="opt"][type="checkbox"]:checked').length>1)
	// 		$('input[name="opt"][type="checkbox"]:not(:checked)').each(function(){
	// 			$(this).prop('disabled',true);
	// 		});
	// 	else
	// 		$('input[name="opt"][type="checkbox"]:disabled').each(function(){
	// 			$(this).prop('disabled',false);
	// 		});
	// });
	
	/*$('#actionsList #ploreading').click(function(){
	
		var UnitID=unitObj.selectedUnit;
		
		plotme(UnitID,groupbuildIndexID,'MTR')
	});
	$('#actionsList #plotoutflow').click(function(){
	
		var UnitID=unitObj.selectedUnit;
		
		plotme(UnitID,groupbuildIndexID,'OUTFLOW')
	});
	$('#actionsList #plotcons').click(function(){
	
		var UnitID=unitObj.selectedUnit;
		
		plotme(UnitID,groupbuildIndexID,'CONSUMPTION')
	});
	$('#actionsList #zoneDemand').click(function(){
		forcastData()
	});
	$('#actionsList #showinfo').click(function(){
		showMeterInfo();
	});
	$('#actionsList #dailyTotalDemand').click(function(){
		
		plotDailyDemand();
	});
	$('#actionsList #potableDemand').click(function(){
		plotDailyNewDemand();
	});
	$('#actionsList #NewDemand').click(function(){
		plotDailypotableDemand();
	});*/
}
function showBucket()
{
	countList={}
	$.each(unitObj.sensorSelection,function(key,value){
		$.each(value,function(key1,value1){
			if(value1 in countList)
			{
				countList[value1]+=1;
			}
			else
			{
				countList[value1]=1;
			}
			
		});

	});
	$('#bucketList').empty();
	$.each(countList,function(key,value){
		$('#bucketList').append('<div class="bucketSensor">'+value+' x '+key+'</div>')
	})
}

function showMeterInfo(MeterID)
{
        var idx=unitObj.ids[MeterID];
        var mywin = logWindow('vertical');
        var metaD=unitObj.sensorMeta[MeterID];
        html = '<div style="overflow-y:auto;max-height:400px;width:100%;"><ul>';
        for (var k in metaD){
                html += '<li><b>'+k+'</b> : ';
                if(typeof(metaD[k])=='object'){
                        html += '<ul>';
                        for(var w in metaD[k]){
                                        html += '<li><ul>';
                                _.each(metaD[k][w],function(val,key,obj){
                                        if(typeof(val)=='object'){
                                                html += '<li><b>'+key+'</b> : <ul>';
                                                for(var x in val){
                                                        html += '<li><b>'+x+'</b> : '+val[x]+'</li>';
                                                }
                                                html += '</ul></li>';
                                        }
                                        else
                                                html += '<li><b>'+key+'</b> : '+val+'</li>';
                                });
                                html += '</ul></li>';
                }
                        html += '</ul>'
                }
                else
                        html += metaD[k]+'</li>';
        }
        html += '</ul></div>';
        $('#'+mywin).append(html);
}

function showMetaUser(x){
	if(!x)
		return;
	
	var thisarray = [].concat(Parentconfig.allbasictags.sensor_basic_tags,Parentconfig.allbasictags.station_basic_tags);
	var idx=unitObj.ids[x];
        var mywin = logWindow('vertical');
        $('#'+mywin).css({ maxHeight:  '400px' });
        var metaD=unitObj.sensorMeta[x];
       var html = '<div class="right"><small><a onclick="showMeterInfo(\''+metaD._id+'\')"><i>Show All</i></a></small></div><div style="overflow-y:auto;max-height:400px;width:100%;">';
       html += '<h5>Station/Premise</h5><ul>';

    // STATION
    for (var k in metaD){
    	if(thisarray.indexOf(k)>=0)
    	{
    		                html += '<li><b>'+k+'</b> : ';
                if(typeof(metaD[k])=='object'){
                        html += '<ul>';
                        for(var w in metaD[k]){
                                        html += '<li><ul>';
                                _.each(metaD[k][w],function(val,key,obj){
                                        if(typeof(val)=='object'){
                                                html += '<li><b>'+key+'</b> : <ul>';
                                                for(var x in val){
                                                        html += '<li><b>'+x+'</b> : '+val[x]+'</li>';
                                                }
                                                html += '</ul></li>';
                                        }
                                        else
                                                html += '<li><b>'+key+'</b> : '+val+'</li>';
                                });
                                html += '</ul></li>';
                }
                        html += '</ul>'
                }
                else
                        html += metaD[k]+'</li>';
    	}
    }
    html += '</ul><h5>Sensors/Meters</h5><ul>';
    var sensorStuff = metaD.sensorList;
    _.each(sensorStuff,function(sen){
    	html += '<hr/>';
    	var stuff = sen._source;
    	for (var k in stuff){
    	if(thisarray.indexOf(k)>=0)
    	{
    		                html += '<li><b>'+k+'</b> : ';
                if(typeof(stuff[k])=='object'){
                        html += '<ul>';
                        for(var w in stuff[k]){
                                        html += '<li><ul>';
                                _.each(stuff[k][w],function(val,key,obj){
                                        if(typeof(val)=='object'){
                                                html += '<li><b>'+key+'</b> : <ul>';
                                                for(var x in val){
                                                        html += '<li><b>'+x+'</b> : '+val[x]+'</li>';
                                                }
                                                html += '</ul></li>';
                                        }
                                        else
                                                html += '<li><b>'+key+'</b> : '+val+'</li>';
                                });
                                html += '</ul></li>';
                }
                        html += '</ul>'
                }
                else
                        html += stuff[k]+'</li>';
    	}
    }
    });
html += '</ul></div>';
$('#'+mywin).append(html);
}

function MeterInfo(MeterID)
{
	var idx = $('#'+MeterID).data('ind');
	return metadata[idx]
	
}
function meterInfo(MeterID)
{
	for(var i=0; i<globalmeta.length;i++)
	{
			if(globalmeta[i]._id == MeterID)
				return globalmeta[i];
	}
	return null;
}
function remSensor(idx,snsr)
{
	_id=unitObj.sensorMeta[idx]._id;
	console.log(unitObj.sensorSelection[_id])
	if(unitObj.sensorSelection[_id].length>1)
	{
		//just remove pressure
		arrIdx=unitObj.sensorSelection[_id].indexOf(snsr);
		unitObj.sensorSelection[_id].splice (arrIdx, 1);
		$('#'+idx+'_'+snsr).remove();
		
	}
	else
	{
		// remove whole
		removeunitID(_id);
		delete unitObj.ids[_id]
	}
	showBucket();
}
function removeAllEmpty()
{
	$.each(unitObj.sensorSelection,function(k1,v1){
		if(v1.length==0)
		{
			var i = unitObj.selectedUnit.indexOf(k1);
			delete unitObj.sensorSelection[k1];
			delete unitObj.sensorMeta[k1];
			delete unitObj.ids[k1];
			unitObj.selectedUnit.splice(i, 1);
		}
	})
}
function removeEmpty()
{
	if(!allmode&&!lossoMode)
	{
		if(unitObj.selectedUnit.length==0)
		{
			unitObj.lastselected={}
		}
		else
		{
			if(Object.keys(unitObj.lastselected).length>0)
			{
				meter=unitObj.lastselected.meter;
				index=unitObj.lastselected.index;
				var i = unitObj.selectedUnit.indexOf(meter);
				if(unitObj.sensorSelection[meter].length==0)
				{
					delete unitObj.sensorSelection[meter];
					delete unitObj.sensorMeta[meter];
					delete unitObj.ids[meter];
					unitObj.selectedUnit.splice(i, 1);
					console.log(unitObj.lastselected)
				}
			}
		}
	}

	else
	{
		if(unitObj.selectedUnit.length>=0)
		{
			if(Object.keys(unitObj.lastselected).length>0)
                        {
                                meter=unitObj.lastselected.meter;
                                index=unitObj.lastselected.index;
                                var i = unitObj.selectedUnit.indexOf(meter);
                                if(unitObj.sensorSelection[meter].length==0)
                                {
                                        delete unitObj.sensorSelection[meter];
                                        delete unitObj.sensorMeta[meter];
                                        delete unitObj.ids[meter];

                                        unitObj.lastselected.meter=unitObj.selectedUnit[unitObj.selectedUnit.length-1];
										unitObj.lastselected.index=unitObj.ids[unitObj.lastselected.meter]
					 					unitObj.selectedUnit.splice(i, 1);
                                        console.log(unitObj.lastselected)
                                }
                        }
		}

	}
}
function clearAllempty()
{
	$.each(unitObj.sensorSelection,function(key,value){
		console.log(key)
		if(value.length==0)
		{
			delete unitObj.sensorSelection[key];
            delete unitObj.sensorMeta[key];
            delete unitObj.ids[key];
            var i = unitObj.selectedUnit.indexOf(key);
            unitObj.selectedUnit.splice(i, 1);
		}
	})
	if(unitObj.selectedUnit.length==0)
	{
		unitObj.lastselected={}
	}
	else
	{
		unitObj.lastselected.meter=unitObj.selectedUnit[unitObj.selectedUnit.length-1];
		unitObj.lastselected.index=unitObj.ids[unitObj.lastselected.meter]
	}
	showAction();
	if(unitObj.selectedUnit.length>0)
	{
		lastmeter=unitObj.lastselected.meter;
		$.each(unitObj.sensorSelection[lastmeter],function(key,value){
			$('#ActionPanel input[id="act_'+value+'"]').trigger('click'); //.prop('checked',true);
		});
	}
	refreshTray()
}
function sensorAdd(idx,snsr)
{
	_id=unitObj.sensorMeta[idx]._id;
	showAction();
	$.each(unitObj.sensorSelection[_id],function(key,value){
		$('#ActionPanel div[id="'+value+'"]').prop('checked',true);

	})
	$('.sensorsListblock').hide();
	$('.listSensorList').empty();
	$('.showSensorList').parent().parent().addClass('resizeTray')
	$('.showSensorList').removeClass('hidedia');
	addintersectSensor('.listSensorList',unitObj.sensorMeta[idx],'single')
	showBucket();
	unitObj.lastselected.meter=_id;
	unitObj.lastselected.index=idx;
	$('.listSensorList').on('click','input[name=opt]',function(){
			var thistype = $(this).attr('type');
			value= $(this).val();
			if($(this).prop('checked'))
			{
				if(unitObj.sensorSelection[_id].indexOf(value)==-1)
				{
					unitObj.sensorSelection[_id].push(value)
				}
			}
			else
			{
				posindex=jQuery.inArray( value, unitObj.sensorSelection[_id] )
				unitObj.sensorSelection[_id].splice(posindex, 1);
			}
			showBucket();
	});
	// $('#addDone').on('click',function(){
	// 	$('.showSensorList').parent().parent().removeClass('resizeTray');
	// 	$('.showSensorList').addClass('hidedia');
	// 	$('.sensorsListblock').show();

	// });
	// $('#addCancel').on('click',function(){
	// 	$('.showSensorList').parent().parent().removeClass('resizeTray');
	// 	$('.showSensorList').addClass('hidedia');
	// 	$('.sensorsListblock').show();
	// });

}

function addintersectSensor(divElement,meData,type)
{
	sensorList=[];
	_id=meData._id;
	lastType=checkBeforePush(_id);
	$.each(meData.sensorList,function(key,value){
		$.each(actionListObj.UnitOptions,function(key1,value1){
			if(value._source.sensortype_display==value1.sensortype_display)
			{
				sensorList.push(value1);
			}
		});
	});
	$.each(sensorList,function(key, value){
		if(unitObj.sensorSelection[_id].indexOf(this.label)>-1)
			check='checked';
		else
			check='';
		_type = 'checkbox'; 
		//$(divElement).append('<label for="'+this.id+'"><input id="'+this.id+'" type="'+_type+'" name="opt" value="'+this.id+'" class="optionRadios" '+check+'/><div class="labelspan">'+this.label+'</div></label>');

		// if(this.type.indexOf(lastType) > -1)//
			$(divElement).append('<label for="tray_'+this.id+'_list"><input id="tray_'+this.id+'_list" type="'+_type+'" name="opt" value="'+this.id+'" class="optionRadios" '+check+'/><div class="labelspan">'+this.label+'</div></label>');
	})

}
function buildingDetail(data)
{
	var isResident=false;
	$.each(data, function(key,value){
	 		
	 		if(value[5].indexOf('RESIDENTIAL')==0)
	 		{
	 			isResident=true;
	 		}
	});
	if(isResident==true)
	{
		return 'resident';
	}
	else {
		return 'commercial';
	}
	
}

function addwidget(name,data,from)
{
	var arry=[];
	var json_str = localStorage.Widget; //getCookie('Widget')
	if(json_str!=undefined&&json_str!='')
	{
		cookieData=JSON.parse(json_str);
		data.hashID=$.md5(JSON.stringify(data));
		data.name=name;
		data.app='meterIntel';
		data.towhere=from;
		cookieData.push(data);
		var json_str = JSON.stringify(cookieData);
		//deleteCookie('Widget');
		localStorage.removeItem("Widget");
		//setCookie('Widget', json_str, 365);
		localStorage.setItem('Widget', json_str);
		//console.log(getCookie('Widget'))
	}
	else {
		
		data.hashID=$.md5(JSON.stringify(data));
		data.name=name;
		data.app='meterIntel';
		data.towhere=from;
		arry.push(data);
		var json_str = JSON.stringify(arry);
		//setCookie('Widget', json_str, 365);
		localStorage.setItem('Widget', json_str);
	}
	//md5()
}

function forcastData()
{
	forecast2plot();
	return;
	var Percent=0;
	var forecast, unit1;
	var actual, unit2;
	var temp={}
	var ResponseData= new Array();
	
	if(widgetData)
	{
		var url1=widgetData.link1;
		var url2=widgetData.link2;
		var zn=widgetData.zone;
		var ForecastWindowID=zn+'_Demand';
		logWindow('sr',ForecastWindowID);
		$( '<div id="container'+ForecastWindowID+'" class="Graphcontainer"></div>' ).insertAfter( $('#'+ForecastWindowID+' .topheader') );
	}
	else {
		
	
		if(st==undefined||et==undefined)
		{
			//alert('please select the date range');
			Notify('Date Error','Please select the date range','normal',true)
			return false;
		
		}
		var ForecastWindowID=searchOptions.zone_options.zone[0]+'_Demand';
		var zn=searchOptions.zone_options.zone[0];
		logWindow('sr',ForecastWindowID);
		$( '<div id="container'+ForecastWindowID+'" class="Graphcontainer"></div>' ).insertAfter( $('#'+ForecastWindowID+' .topheader') );
		var url1='api.php?tag=getDemand&zone='+searchOptions.zone_options.zone[0]+'&op=forecast&st='+st+'&et='+et+'&z=0';
		var url2='api.php?tag=getDemand&zone='+searchOptions.zone_options.zone[0]+'&op=actual&st='+st+'&et='+et+'&z=0';
	}
		$.ajax({
		    url: url1,
		    dataType: "json",
		 	type: "GET",
		    success: function( response ) {
		       forecast=response.response.data;
		       var unit=response.response.unit;
		       		$.ajax({
		    		    url: url2,
		    		    dataType: "json",
		    		 	type: "GET",
		    		    success: function( response ) {
		    		       actual=response.response.data;
		    		       unit2=response.response.unit;
		    		      
		    		       //plotgraph(forecast,actual,unit,ForecastWindowID);	
		    		       plotforecast(f,a,u,id);
		    		       $('#widget').click(function(){
		    					temp.link1=url1;
		    		       		temp.link2=url2;
		    		       		temp.zone=zn;
		    		       		addwidget(ForecastWindowID,temp,'demand')
		    		       });
		    		    }
		    	})
		       
		    }
	})
}

function plotgraph(forecast,actual,unit,WindowID)	
{	
	
	
	var editData=forecast.substring(1,forecast.length-1);
	var editDataComps=editData.split("],");
	var dataArray=new Array();
	
	
	var editData1=actual.substring(1,actual.length-1);
	var editDataComps1=editData1.split("],");
	var dataArray1=new Array();
	
	for(var i=0;i<editDataComps.length;i++){
		var xyTemp=new Array();
		var eachComps=editDataComps[i].split(",");
		xyTemp.push(parseFloat(eachComps[0].substring(eachComps[0].indexOf("[")+1)));
		if(i!=editDataComps.length-1){
		xyTemp.push(parseFloat(eachComps[1]));
		}
		else{
			xyTemp.push(parseFloat(eachComps[1].substring(0,eachComps[1].indexOf("]"))));
		}
		dataArray.push(xyTemp);
	}
	
	for(var i=0;i<editDataComps1.length;i++){
		var xyTemp1=new Array();
		var eachComps1=editDataComps1[i].split(",");
		xyTemp1.push(parseFloat(eachComps1[0].substring(eachComps1[0].indexOf("[")+1)));
		if(i!=editDataComps.length-1){
		xyTemp1.push(parseFloat(eachComps1[1]));
		}
		else{
			xyTemp1.push(parseFloat(eachComps1[1].substring(0,eachComps1[1].indexOf("]"))));
		}
		dataArray1.push(xyTemp1);
	}
	
		Highcharts.setOptions({
			global: {
			    useUTC: false
			}
		});
		
		chart = new Highcharts.Chart
			({
				    chart: {
				        renderTo: 'container'+WindowID,
				        type: 'line',
						zoomType:'x',
						marginRight:60,
						reflow: false
						
				    },
				    colors: ['#4572A7','#AA4643', '#89A54E'],
				    credits: {
				                enabled: false
				            },
				    title: {
				        text: "Demand per 15 Min"
				    },
				    plotOptions:{
						line: {
					        marker: {
					            enabled: false
					        }
					    },
						series:{
							borderWidth:0,
							shadow:false
						}
					}, 
			    	yAxis: [{
				        min: 0,
				        title: {
				            text: unit
				        }
				    },
				    	{
				            title: {
				                text: ' '
				            },
				    opposite: true
				        }],
					xAxis:{
							type:'datetime',
							
							min:st,
							//max:datejson.end,   
							title:{
							text:null
							}
							
					},
			    tooltip: {
			           crosshairs:[true,true],
					   formatter: function() {
					    var strDate = new Date(this.x).toDateString();
					    var strTime = new Date(this.x).toTimeString();
					    var sampletime = strDate + " " + strTime.substring(0,strTime.indexOf(" GMT")); // dont include the timezone info 
				            return 'Time:<b>'+sampletime+'</b><br>Value: <b>'+this.y.toFixed(2) +'</b>';
				
				        }
			    }, 
				legend:{	
					enabled:true,
					floating:true,
					align: 'right',
					verticalAlign: 'top',
				},
				series:[{
					name:'Forecast',
					data:[]
				},
				{
					name:'Actual',
					data:[]
				}]
			});
		chart.series[0].setData(dataArray);
		chart.series[1].setData(dataArray1);
		$( "#"+WindowID).on( "resize", function( event, ui ) {
			var wd=parseInt(ui.size.width)-40;
			var ht=parseInt(ui.size.height)-40;
			chart.setSize(wd, ht, doAnimation = true);
		} );
		
		
		
}
function objectSize(the_object) {
  /* function to validate the existence of each key in the object to get the number of valid keys. */
  var object_size = 0;
  for (key in the_object){
    if (the_object.hasOwnProperty(key)) {
      object_size++;
    }
  }
  return object_size;
}

function parseQueryJson(searchOptions)
{
	var jsonQuery={};
	var temp={};
	jsonQuery.query={};
	jsonQuery.size=100000;
	jsonQuery.query.filtered={};
	jsonQuery.query.filtered.query={};
	jsonQuery.query.filtered.query.match_all={};
	jsonQuery.query.filtered.filter={};
	jsonQuery.query.filtered.filter.bool={};
	jsonQuery.query.filtered.filter.bool.must=[];
	
	Beforeand={};
	Beforeand.or=[] //and
	$.each(searchOptions,function(key, value){
		if(key=='filterType')
		{
			$.each(value,function(key1, value1){
				switch(value1)
				{
					case 'watersource':
						returnobj=_getWatersourceFilters();
						$.each(returnobj,function(key,value1){
							Beforeand.or.push(value1); //and
						})
						//Beforeand.and.push(returnobj);
					break;
					case 'weather':
						returnobj=_getWeatherFilters();
						$.each(returnobj,function(key,value1){
							Beforeand.or.push(value1); //and
						})
					break;
					case 'amr':
						// Beforeand.or=[]
						// delete Beforeand.and
                        returnobj=_getAMRFilters();
                        $.each(returnobj,function(key,value1){
                                Beforeand.or.push(value1); //and
                        })
                    break;
					case 'scada':
						returnobj=_getScadaFilters();
						$.each(returnobj,function(key,value1){
							Beforeand.or.push(value1); //and
						})
					break;
					case 'sensor':
						returnobj=_getSensorFilters();
							$.each(returnobj,function(key,value1){
								Beforeand.or.push(value1); //and
							})
						

						
					break;
					case 'gis':
						// returnobj=_getGISFilters();
						// Beforeand.and.push(returnobj);
					break;
				}
			});
		}
		else if(key=='text')
		{
			returnobj=_getTextFilters();
			$.each(returnobj,function(key,value){
						Beforeand.or.push(value); //and
			})
		}
		else if(key=='settings')
		{
			
			if(searchOptions.filterType.indexOf('watersource')>-1)
			{

				returnobj=_loadSettingsFilter(true);
				$.each(returnobj,function(key,value){
					Beforeand.or.push(value); //and
				})

			}
			else //if(rootSelection=='sensor')
			{
				returnobj=_loadSettingsFilter(false);
				$.each(returnobj,function(key,value){
					Beforeand.or.push(value); //and
				})
			}
		}
		
		//var tempObj=_objCreate(key,value)
	});
	jsonQuery.query.filtered.filter.bool.must.push(Beforeand)
	
	return JSON.stringify(jsonQuery);
	

}
function _loadSettingsFilter(state)
{
	temparray=[];
	parentObj2={}
	objElements={'zoneSelection':['zoneSelection'],'ZoneCluster':['ZoneCluster'],'watertype':['watertype']};
	//wsLength=searchOptions.settings.watertype.watertype.length;
	wsLength=settings.watertype.length;
	if(wsLength>0)
	{
		
		//watertype=searchOptions.settings.watertype.watertype[0];
		watertype=settings.watertype;
		if(state)
		{
			
			option='zoneSelection';
			child=objElements[option][0];
			tempObj={};
			tempObj.or=[]
			mid={};
			mid.term={}
			label='tag_watertype';
			mid.term[label]=watertype;
			tempObj.or.push(mid)
			temparray.push(tempObj);
		}
		//if(searchOptions.settings[option][child].length>0)
		if(settings.subzone.length>0)
		{
			tempObj={};
			tempObj.or=[]
			// $.each(searchOptions.settings[option][child],function(key,value){
			$.each(settings.subzone,function(key,value){	
				mid={};
				mid.term={}
				if(watertype=='newwater')
				{
					label='demand_cluster';
					value=value.toLowerCase();
					var splitdata=value.split('_');
				}	
				else
				{
					label='supply_zone';
					 var splitdata=value;
				}
				
				mid.term[label]=splitdata;
				tempObj.or.push(mid)
				parentObj2.has_parent={}
				parentObj2.has_parent.type=Parentconfig.stationIndex;//"station_info";
				parentObj2.has_parent.query={"match_all":{}};
				
				tempObj.or.push(parentObj2)

			})
			temparray.push(tempObj);
		}
		if(watertype=='newwater')
		{
			option='ZoneCluster';
			child=objElements[option][0];
			if('ZoneCluster' in searchOptions.settings&&'ZoneCluster' in searchOptions.settings[option] && searchOptions.settings[option][child].length>0)
			{
				tempObj={};
				tempObj.or=[]
				//$.each(searchOptions.settings[option][child],function(key,value){
				$.each(settings.subzone,function(key,value){
					
					mid={};
					mid.term={}
					label='zonal_cluster';
					value=value.toLowerCase();
					var splitdata=value.split('_');
					switch(splitdata)
					{
						case "fcph":
							vl="phfc";
						break;
						default:
							vl=splitdata;
						break;
					}
					mid.term[label]=vl;
					tempObj.or.push(mid)


				})
				delete temparray[1];
				temparray.push(tempObj);
				
			}
		}
	}
	else
	{
		//watertype=searchOptions.settings.watertype.watertype[0];
		if(state)
		{

			watertype=settings.watertype;
			option=watertype+'_options';
			tempObj={};
			tempObj.or=[]
			mid={};
			mid.term={}
			label='tag_watertype';
			mid.term[label]=watertype;
			tempObj.or.push(mid)
			parentObj2.has_parent={}
			parentObj2.has_parent.type=Parentconfig.stationIndex;//"station_info";
			parentObj2.has_parent.query={"match_all":{}};
				
			tempObj.or.push(parentObj2)
			temparray.push(tempObj);

		}
	}
	
	return temparray;
}
function _getAMRFilters()
{
	var CustomerLength=searchOptions.parent_options.customerType.length;
	if(CustomerLength==0)
	{
		var parentObj2={};
        	var temparray=[];
        	var tempObj={};
        	tempObj.or=[];
		mid={};
        	mid.term={}
        	label='tag_datatype';
        	mid.term[label]='amr';
        	tempObj.or.push(mid)
        	
		parentObj2.has_parent={}
   		parentObj2.has_parent.type=Parentconfig.stationIndex;//"station_info";
    		parentObj2.has_parent.query={"match_all":{}};
      		tempObj.or.push(parentObj2)
		temparray.push(tempObj);
		return temparray;
	}
	else
	{
		 var parentObj2={};
        var temparray=[];
        var filpush=[]
		var dt=searchOptions.parent_options.customerType[0];

		var watertype=searchOptions.settings.watertype.watertype[0];
		switch(dt)
		{
			case 'nonresidential':
				childobj='nonresidential_options';
				childType='commercialType';
				childLabel='tag_sector'
			break;
			case 'residential':
				childobj='residential_options';
                childType='ResidentArea';
				childLabel='meter_scheme'
			break;
			case 'wems':
				childobj='wems_options';
                childType='wemstype';
				childLabel='meter_type'
			break;
		}

        var tempObj={};
        tempObj.and=[];
        mid={};
        mid.term={}
        label='tag_watertype';
        mid.term[label]=watertype;
        filpush.push(mid)
        tempObj.and.push(mid);
        // temparray.push(tempObj);
		// var tempObj={};
        // tempObj.or=[];
	 	mid={};
        mid.term={}
        label='tag_category';
        mid.term[label]=dt;
        filpush.push(mid)
        tempObj.and.push(mid);
		childLength=searchOptions[childobj][childType].length;
		if(childLength>0)
		{
			// temparray.push(tempObj);
			// var tempObj={};
        	// tempObj.or=[];
        	var tmid={};
            tmid.or=[];
			list=searchOptions[childobj][childType];
			$.each(list,function(key,value){
				
				vl=value.toLowerCase();
				if(vl.indexOf('_')>=0)
				{
				  	dat=vl.split("_")
				}
				else
				{
					dat=vl
				}
				mid={};
				mid.term={}
                label=childLabel;
                mid.term[label]=dat;
                tmid.or.push(mid)
                
			});
			filpush.push(tmid);
			tempObj.and.push(tmid);
		}
		temparray.push(tempObj);
		var tempObj={};
        tempObj.or=[];
		mid={};
		parentObj2.has_parent={}
                parentObj2.has_parent.type=Parentconfig.stationIndex;//"station_info";
                parentObj2.has_parent.query={"filtered":{}}
                parentObj2.has_parent.query.filtered.query={"match_all":{}};
                parentObj2.has_parent.query.filtered.filter={"and":[]}
                parentObj2.has_parent.query.filtered.filter.and=filpush;
                // tempObj.or.push(parentObj2)
                temparray.push(parentObj2);
       		return temparray;
	}

}
function _getScadaFilters()
{
	tempObj={};
	tempObj.or=[];
	var dt=searchOptions.scade_options.flowMeters;
	len=dt.length;
	if(len>0)
	{
		$.each(dt,function(key,value){
			mid={};
			mid.term={}
			label='tag_sector';
			value2=value.toLowerCase();//.split('_');
			mid.term[label]=value2;
			tempObj.or.push(mid)
		});
		temparray.push(tempObj);
	}
	else
	{
		mid={};
		mid.term={}
		label='tag_category';
		mid.term[label]='pub_flowmeter';
		tempObj.or.push(mid)
		temparray.push(tempObj);
	}
	return temparray;
}
function _getSensorFilters(list)
{
	tempObj={};
	tempObj.or=[];
//	parentObj={};
	parentObj2={}
	var dt=searchOptions.sensor_options.sensorparams;
	len=dt.length;
	if(len>0)
	{
		$.each(dt,function(key,value){
			mid={};
			mid.term={}
			label='sensortype_actual';
			value2=value.toLowerCase();//.split('_');
			mid.term[label]=value2;
			tempObj.or.push(mid)
		});

		

	/*	parentObj.has_child={};
		parentObj.has_child.type="sensor_info";
		parentObj.has_child.query={"match":{}};
		$.each(dt,function(key,value){
			console.log(value)
			parentObj.has_child={};
			parentObj.has_child.type="sensor_info";
			parentObj.has_child.query={"match":{}};
			parentObj.has_child.query.match.sensortype_actual=value;
			tempObj.or.push(parentObj)
		});
		parentObj2.has_parent={}
		parentObj2.has_parent.type="station_info";
		parentObj2.has_parent.query={"match_all":{}};
		
		tempObj.or.push(parentObj2)
		temparray.push(tempObj);*/
		$.each(dt,function(key,value){
			console.log(value)
			parentObj={};
			parentObj.has_child={};
			parentObj.has_child.type=Parentconfig.sensorIndex; //"sensor_info";
			parentObj.has_child.query={"match":{}};
			parentObj.has_child.query.match.sensortype_actual=value;
			tempObj.or.push(parentObj)
		});
		parentObj2.has_parent={}
		parentObj2.has_parent.type=Parentconfig.stationIndex;//"station_info";
		parentObj2.has_parent.query={"match_all":{}};
		
		tempObj.or.push(parentObj2)
		temparray.push(tempObj);		

	}
	else
	{
		mid={};
		mid.term={}
		label='tag_category';
		mid.term[label]='sensorstation';
		tempObj.or.push(mid)
		parentObj={};
		parentObj.has_parent={};
		parentObj.has_parent.type=Parentconfig.stationIndex; //"station_info";
		parentObj.has_parent.query={"match_all":{}};
		// $.each(dt,function(key,value){
		// parentObj.has_child.query.match.sensortype_actual=value
		// });
		tempObj.or.push(parentObj)
		temparray.push(tempObj);
	}
	return temparray;
}
function _getWatersourceFilters()
{
	
	temparray=[];
	objElements={'residential_options':['ResidentArea'],'nonresidential_options':['commercialType'],'wems_options':['wemstype']};
	wsLength=searchOptions.parent_options.customerType.length;
	if(wsLength>0)
	{
		customer=searchOptions.parent_options.customerType[0];
		option=customer+'_options';
		child=objElements[option][0];
		tempObj={};
		tempObj.or=[]
		mid={};
		mid.term={}
		label='tag_category';
		mid.term[label]=customer;
		tempObj.or.push(mid);
		temparray.push(tempObj);
		if(searchOptions[option][child].length>0)
		{
			tempObj={};
			tempObj.or=[]
			$.each(searchOptions[option][child],function(key,value){
				
				mid={};
				mid.term={}
				label='tag_sector';
				value=value.toLowerCase();
				var splitdata=value.split('_');
				mid.term[label]=splitdata;
				tempObj.or.push(mid)
			})
			temparray.push(tempObj);
		}
		
		
		
	}
	else
	{
		/*enable once we had the extra tag for stations*/

		// tempObj={};
		// tempObj.or=[]
		// mid={};
		// mid.term={}
		// label='tag_category';
		// mid.term[label]=rootSelection;
		// tempObj.or.push(mid);
		// temparray.push(tempObj);
	}
	return temparray;
}
function _getTextFilters()
{
	temparray=[];
	$.each(searchOptions.text,function(key,value){
		$.each(value,function(key1,value1){
			tempObj={};
			$.each(Parentconfig.allbasictags.search_tags,function(key,value){
				if(value.value==key1)
				{
					switch(value.operator)
					{
						case 'and':
							TObj=elasticAnd(value.value,value1,1);
							temparray.push(TObj);
						break;
						case 'or':
							TObj=elasticOr(value.value,value1);
							temparray.push(TObj);
						break;
						case 'SensorFilter':
							TObj=elasticSensorFilters(value.value,value1,2);
                    		temparray.push(TObj);
						break;
						case 'AndMultiple':
							vdata=value1.split(',');
		                	// var TObj=elasticAndMultiple('supply_zone',vdata);
		                	TObj=elasticAndMultiple(value.value,vdata,1);
		                    temparray.push(TObj); 
						break;
						case 'elasticAndMultiple':
							vdata=value1.split(',');
		                	// var TObj=elasticAndMultiple('supply_zone',vdata);
		                	TObj=elasticAndMultiple(value.value,vdata,1);
		                    temparray.push(TObj); 
						break;
						case 'range':
							TObj=elasticRange(value.value,value1);
							temparray.push(TObj);
						break;
						default:


					}
				}
			})
			// switch(key1)
			// {
			// 	case 'neighbourhood':
			// 		TObj=elasticAnd('neighbourhood',value1,1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'address':
			// 		TObj=elasticAnd('address',value1,1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'stationName':
   //                  TObj=elasticAnd('name',value1,1);
   //                  temparray.push(TObj);
   //              break;
   //              case 'sensorName':
   //              	TObj=elasticSensorFilters('sensortype_display',value1,2);
   //                  temparray.push(TObj);
   //              break;
   //              case 'tagSector':
   //                  TObj=elasticAnd('tag_sector',value1,1);
   //                  temparray.push(TObj);
   //              break;
   //              case 'displayname':
   //                  TObj=elasticAnd('display_name',value1,1);
   //                  temparray.push(TObj);
   //              break;
   //              case 'tagCategory':
   //                  TObj=elasticAnd('tag_category',value1,1);
   //                  temparray.push(TObj);
   //              break;
   //              case 'supplyZone':
   //              	vdata=value1.split(',');
   //              	// var TObj=elasticAndMultiple('supply_zone',vdata);
   //              	TObj=elasticAndMultiple('supply_zone',vdata,1);
   //                  temparray.push(TObj); 
   //              break;
   //              case 'stationId':
   //                  TObj=elasticAnd('station_id',value1,1);
   //                  temparray.push(TObj);
   //              break;
   //               case 'sensors':
   //               	vdata=value1.split(',');
   //                  TObj=elasticAndMultiple('sensortype_actual',vdata,2);
   //                  temparray.push(TObj);
   //              break;
			// 	case 'size':
			// 		TObj=elasticOr('size',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'wemp':
			// 		TObj=elasticOr('meter_type',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'postalCode':
			// 		TObj=elasticOr('postcode',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'accountName':
			// 		TObj=elasticAnd('bp_name',value1,2);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'premiseType':
			// 		TObj=elasticOr('premise_type',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'accClass':
			// 		TObj=elasticRange('acc_class',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'subSector':
			// 		TObj=elasticOr('tag_subsector',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'accountNumber':
			// 		TObj=elasticOr('tru_ca',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'ID':
			// 		TObj=elasticOr('_id',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'DeviceID':
			// 		TObj=elasticOr('device',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'SSIC':
			// 		TObj=elasticOr('ssic',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'site':
			// 		TObj=elasticAnd('site',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'meterScheme':
			// 		TObj=elasticOr('meter_scheme',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'billingClass':
			// 		TObj=elasticOr('billing_class',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'linkAddress':
			// 		TObj=elasticOr('link_address',value1);
			// 		temparray.push(TObj);
			// 	break;
			// 	case 'owner':
			// 		TObj=elasticOr('tag_owner',value1);
			// 		temparray.push(TObj);
			// 	break;
			// }
		})
	})
	return temparray;
}

function elasticRange(label,value1)
{
	var txtData=value1.split('-');
	var parentObj2={};
	tempObj.or=[];
	mid={};
	
	if(txtData.length>1)
	{
		mid.range={}
		mid.range[label]={}
		mid.range[label].from=parseInt(txtData[0]);
		mid.range[label].to=parseInt(txtData[1]);
		tempObj.or.push(mid);
	}
	else
	{
		mid.term={}
		mid.term[label]=parseInt($.trim(list));
		tempObj.or.push(mid);
	}
	parentObj2.has_parent={}
	parentObj2.has_parent.type=Parentconfig.stationIndex; //"station_info";
	parentObj2.has_parent.query={"match_all":{}};
	tempObj.or.push(parentObj2)
	return tempObj;
}
function elasticOr(label,value1)
{
	var parentObj2={};
	tempObj.or=[]
	mid={};
	mid.term={}
	label=label;
	value2=value1.toLowerCase();
	mid.term[label]=$.trim(value2);
	tempObj.or.push(mid);
	parentObj2.has_parent={}
	parentObj2.has_parent.type=Parentconfig.stationIndex;//"station_info";
	parentObj2.has_parent.query={"match_all":{}};
	tempObj.or.push(parentObj2)
	return tempObj;
}
function elasticOrMultiple(label,val)
{
	var bidObj=[]
	$.each(val,function(ke,value1){
	var parentObj2={};
	var tempObj={}
	tempObj.or=[]
	var mid={};
	mid.term={}
	label=label;
	value2=value1.toLowerCase();
	mid.term[label]=$.trim(value2);
	tempObj.or.push(mid);
	parentObj2.has_parent={}
	parentObj2.has_parent.type=Parentconfig.stationIndex; //"station_info";
	parentObj2.has_parent.query={"match_all":{}};
	tempObj.or.push(parentObj2)
	bidObj.push(tempObj);
	})
	return bidObj;
}
function elasticAndMultiple(label,val,type){
	var parentObj2={};
	var tempObj={}
	tempObj.or=[]
	$.each(val,function(ke,value1){
		var txtData=$.trim(value1).split(' ');
		var tpObj={};
		tpObj.and=[]
		$.each(txtData,function(k1,v1){
			var mid={};
			mid.term={}
			label=label;
			value1=v1.toLowerCase();
			mid.term[label]=value1;
			tpObj.and.push(mid);
			
		})
		tempObj.or.push(tpObj)
		
	})
	
		
	if(type==2)
	{
		parentObj2.has_child={}
		parentObj2.has_child.type=Parentconfig.sensorIndex; //"sensor_info";
		parentObj2.has_child.query={"match_all":{}};
	}
	tempObj.or.push(parentObj2)
	parentObj2.has_parent={}
	parentObj2.has_parent.type=Parentconfig.stationIndex; //"station_info";
	parentObj2.has_parent.query={"match_all":{}};
	tempObj.or.push(parentObj2)

	return tempObj;
}
function elasticAnd(label,value1,type)
{
	
	var parentObj2={};
	var txtData=$.trim(value1).split(' ');
	tempObj.or=[]
	tpObj={};
	tpObj.and=[]
	$.each(txtData,function(k1,v1){
		mid={};
		mid.term={}
		label=label;
		value1=v1.toLowerCase();
		mid.term[label]=value1;
		tpObj.and.push(mid);
	})
	tempObj.or.push(tpObj)
	parentObj2.has_parent={}
	parentObj2.has_parent.type=Parentconfig.stationIndex; //"station_info";
	parentObj2.has_parent.query={"match_all":{}};
	tempObj.or.push(parentObj2)
	return tempObj;
}
function elasticSensorFilters(label,value1)
{
	var parentObj2={};
	var parentObj={};
	var txtData=$.trim(value1).split(' ');
	tempObj.or=[]
	tpObj={};
	tpObj.and=[]

	var txtData=$.trim(value1).split(' ');
	$.each(txtData,function(key,value){
			mid={};
			mid.term={}
			value1=value1.toLowerCase();//.split('_');
			mid.term[label]=value1;
			tempObj.or.push(mid)
			
	
			parentObj.has_child={};
			parentObj.has_child.type=Parentconfig.sensorIndex; //"sensor_info";
			parentObj.has_child.query={"match":{}};
			parentObj.has_child.query.match[label]=value1;
			tempObj.or.push(parentObj)
			
	}); //});		
	parentObj2.has_parent={}
	parentObj2.has_parent.type=Parentconfig.stationIndex;//"station_info";
	parentObj2.has_parent.query={"match_all":{}};
		
	tempObj.or.push(parentObj2)
	// temparray.push(tempObj);		

	
	return tempObj;
}
function _getWeatherFilters()
{
	var parentObj2={};
	temparray=[];
	tempObj={};
	tempObj.or=[]
	mid={};
	mid.term={}
	label='tag_category';
	mid.term[label]=rootSelection;
	tempObj.or.push(mid)
	parentObj2.has_parent={}
        parentObj2.has_parent.type=Parentconfig.stationIndex;//"station_info";
        parentObj2.has_parent.query={"match_all":{}};

        tempObj.or.push(parentObj2)
	temparray.push(tempObj);
	return temparray;
}

function getActionParams()
{
	var currentSearch;
	actionParamObJ.startDate=st;
	actionParamObJ.endDate=et;
	/*if(st==undefined||et==undefined)
	{
		alert('please select the date');
		return 0;
	}*/
	if(lastSearchList.length>=1)
    {
        var lstItem=JSON.parse(lastSearchList[lastSearchList.length-1]);
		currentSearch=elasticQuery(lstItem);
		
	}
	else
	{
		var jsonQuery={};
		var temp={};
		jsonQuery.query={};
		jsonQuery.size=100000;
		jsonQuery.query.filtered={};
		jsonQuery.query.filtered.query={};
		jsonQuery.query.filtered.query.match_all={};
		currentSearch=JSON.stringify(jsonQuery);
	
	}
	actionParamObJ.Searchjson=currentSearch;
	actionParamObJ.exclude=excludeMarkers;
	//if(!allmode)
	actionParamObJ.include=unitObj.selectedUnit;
	//else
	//actionParamObJ.include='';
	//searchTstring=searchTstring.replace(/,(\s+)?$/, ''); 
	actionParamObJ.searchtxt=searchTstring;
	return actionParamObJ;
}
function defaultjson()
{
		var jsonQuery={};
		var temp={};
		jsonQuery.query={};
		jsonQuery.size=100000;
		jsonQuery.query.filtered={};
		jsonQuery.query.filtered.query={};
		jsonQuery.query.filtered.query.match_all={};
		// sensor list
		jsonQuery.query.filtered.filter={};
		jsonQuery.query.filtered.filter.bool={};
		jsonQuery.query.filtered.filter.bool.must=[];
		//if(jQuery.inArray("amr",Parentconfig.markerIcons)==-1)
		{
		jsonQuery.query.filtered.filter.bool.must={"and":[{"or":[{"term":{"tag_category":"sensorstation"}},{"has_parent":{"type":Parentconfig.stationIndex,"query":{"match_all":{}}}}]}]};
		//jsonQuery.query.filtered.filter.bool.must={"and":[{"or":[{"term":{"tag_category":"sensorstation"}},{"has_parent":{"type":Parentconfig.stationIndex,"query":{"match_all":{}}}}]}]};
		console.log( Parentconfig.stationIndex);
		}
		return JSON.stringify(jsonQuery);
}


function dmajson(){
		return '{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"tag_sector":["dmameter"]}},{"has_parent":{"type":"'+Parentconfig.stationIndex+'","query":{"match_all":{}}}}]}]}]}}}},"size":100000}';
}
function reloadDMA(){
	// fetch new
	$.get('controllers/proxyman.php',{search:true,SearchQuery:encodeURI(dmajson())},function(d){
		if(d.errorCode>0){
			alert(d.errorMsg);
			throw "Err DMA Update";
			return;
		}
		var obj = d.response.hits.hits;
		_.each(obj,function(o){
			var zone = o._source;
			var zone_name = zone._id;
				// find this guy in meta
				var found = false;
				_.each(zonesarr,function(z){
					if(z.meta._id === zone_name){
						metadata[z.idx] = zone;
						found = true;
					}
				});
				if(!found){	//new guy
					metadata.push(zone);
					zonesarr.push({idx:(metadata.length-1),meta:zone});
					zonesInfo.push(zone.name);
				}
		});
	loadZonesettings();
	},'json');
}



function Notify(title,msg,type,autoclose,callback)
{
	var header=title||null;
	var content=msg||null;
	var popType=type||'normal';
	$('#notify').remove();
	$('#notify_Overlay').remove();

	if(popType=='model')
	{
		$( "body" ).append(' <div class="overlay" id="notify_Overlay"></div>');
	}
	else
	{
		
	}
	HTMLcontent='<div class="notify" id="notify"><div class="Notify_title">'+header+'</div><div class="textContent">';
	//HTMLcontent+=content+'</div>';
	HTMLcontent+=content+'</div>';
	$('#notify .textContent').empty();
	$('#notify .textContent').html(content)
	if(autoclose==false)
	{
		switch(popType)
		{
			case 'normal':
				HTMLcontent+='<div class="closeArea"><div class="popClose">Close</div></div>';
			break;
			case 'confirmation':
				HTMLcontent+='<div class="closeArea"><div class="popYes">Yes</div><div class="popNo">No</div></div>';
			break;		
		}
	}
	HTMLcontent+='</div>';
	$( "body" ).append(HTMLcontent);
	$('#notify .popYes').click(function(e){
		$('#notify').remove();
		$('#notify_Overlay').remove();
		confirm=true;
		return true;
	})
	$('#notify .popNo').click(function(e){
		$('#notify').remove();
		$('#notify_Overlay').remove();
		confirm=false;

		return false;
	})
	if(autoclose==true)
	{
		$('#notify').delay(2000).fadeOut( 1000, function() {
    		$('#notify_Overlay').remove();
  		});
	}
	$('#notify .popClose').click(function(){
		$('#notify').remove();
		$('#notify_Overlay').remove();
	})
	if (callback && typeof(callback) === "function") {
		$('#notify .popYes').bind('click', callback);
		$('#notify .popNo').bind('click', callback);
	}
}


function preLoader(text,type,state)
{
	if(state)
	{
		$('#uiLoadingResponse').remove();
		$('#uiLoadingResponse_Overlay').remove();
		HTMLcontent='<div id="uiLoadingResponse" class="uiLoadingResponse">';
		HTMLcontent+='<div class="preLoadText" >'+text+'</div>'
		HTMLcontent+='<div class="preLoadimage" style="width:64px; margin:0 auto;"><img src="images/Preloader_3.gif" width="64"/></div>'
		HTMLcontent+='</div>'
		switch(type)
		{
			case 'modal':
				$( "body" ).append(' <div class="uiLoadingResponse_Overlay" id="uiLoadingResponse_Overlay"></div>');
			break;
			case 'normal':
			break
			default:

		}
		$( "body" ).append(HTMLcontent);
		
	}
	else
	{
		$('#uiLoadingResponse').remove();
		$('#uiLoadingResponse_Overlay').remove();
	}

	    // <div id="uiLoadingResponse" class="uiLoadingResponse"></div>
}
function Help(about,callback)
{
	$('#helpUILayout').remove();
	if(about)
	{
		HTMLcontent='<div id="helpUILayout" class="helpUILayout">';
		HTMLcontent+='<div class="newhelpContent"></div>'
		HTMLcontent+='</div>'
		$( "body" ).append(HTMLcontent);
		$('#helpUILayout').append('<div class="closeBTN" id="HelpClose">X</div>');
		srcIMg=$('#'+about+' img').attr('src')

		switch(about)
		{
			case 'lasso':
				$('#HelpClose').remove();
				$('#helpUILayout').addClass('movetopClass');
				ctext='<span class="helpTitle">'+about.toUpperCase()+'</span> <br /> <span  class="helpdesc">Select a region using mouse and click done </span>'
				ctHtml='<div class="hcText">'+ctext+' </div><div class="hcbutton"><a href="#" class="button tiny" style="margin:0px !important;"  id="done">Done</a></div>';
				$('.newhelpContent').append(ctHtml)
			break;
			case 'dataDownloader':
				$('#helpUILayout').addClass('updateClass');
				$('.newhelpContent').append('<div class="hcText"><span class="helpTitle"><img src="'+srcIMg+'" width="20px"/> &nbsp;Data Downloader</span><br /><span  class="helpdesc">will let u download the visenti sensor data.</span></div>')
			break;
			case 'usermanagement':
				$('#helpUILayout').addClass('updateClass');
				$('.newhelpContent').append('<div class="hcText"><span class="helpTitle"><img src="'+srcIMg+'" width="20px"/> &nbsp;User Management</span><br /><span  class="helpdesc">will let u create and manage the users info.</span></div>')
			break;
			case 'edit_meta2':
				$('#helpUILayout').addClass('updateClass');
				$('.newhelpContent').append('<div class="hcText"><span class="helpTitle"><img src="'+srcIMg+'" width="20px"/> &nbsp;Management Station and Sensors</span><br /><span  class="helpdesc">will let u create and manage the meta information of stations and sensors.</span></div>')
			break;
			case 'sensor_quality':
				$('#helpUILayout').addClass('updateClass');
				$('.newhelpContent').append('<div class="hcText"><span class="helpTitle"><img src="'+srcIMg+'" width="20px"/> &nbsp;Sensor Quality</span><br /><span  class="helpdesc"></span></div>')
			break;
			case 'sensor_quality':
				$('#helpUILayout').addClass('updateClass');
				$('.newhelpContent').append('<div class="hcText"><span class="helpTitle"><img src="'+srcIMg+'" width="20px"/> &nbsp;Sensor Quality</span><br /><span  class="helpdesc"></span></div>')
			break;
			case 'dmasetup':
				$('#helpUILayout').addClass('updateClass');
				$('.newhelpContent').append('<div class="hcText"><span class="helpTitle"><img src="'+srcIMg+'" width="20px"/> &nbsp;Zone Creator</span><br /><span  class="helpdesc">will let u to create zone boundaries and its properties.</span></div>')
			break;
			default:
				$('.newhelpContent').append(about)
				$('#helpUILayout').append('<div class="closeBTN" id="HelpClose">X</div>');
		}
		$('#HelpClose').on('click',function(){
			$('#helpUILayout').remove();
		})
		if (callback && typeof(callback) === "function") {
			// $('#helpUILayout #done').bind('click', function(){
			// 	$('#helpUILayout').remove();
			// 	callback(true);
			// })
			
	    	$('#helpUILayout #done').bind('click', callback);
	    }
	}
	
}
function sortBySensorType(x){
	var temp = x[0]||null;
	var ret = [];
	var solved = [];
	_.each(x,function(el){
		if(solved.indexOf(el)>=0)
			return;
		ret.push(el);
		solved.push(el);
		_.each(x,function(el2){
			if(solved.indexOf(el2)>=0)
				return;
			if(sensortype_map[el2] == sensortype_map[el]){
				ret.push(el2);
				solved.push(el2);
			}
		});
	});
	return ret;
}