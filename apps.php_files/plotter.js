// m3m
var defaultSensor = "bad_sensor";
var __tr = 80000;		// Max points handled by plots
var min=null,max=null;
var hol= [];
var veecheck=false;
var chartplayer = [];
var joblist = [];
var jobpool = [];
var batch = [];
var _datagroupingoption = false;
var extremes_timer = null;
var extremes_event = null;	
var global_color_arr = [];
// Push relative triggers to portal
//	-- Plotter Button (force first index)
actionListObj.triggers.unshift({"label":"Plot","id":"plotgraph","dynamic":false,"mode":"default","type":["AMR","WEMS","WEATHER","FLOW",null,"STN"]});
//	-- Compound Analysis
actionListObj.triggers.push({"label":"Compound Analysis","id":"plotratio","dynamic":false,"mode":"default","type":["AMR"]});

var stackobj = {mode:null,stack:null,idx:null,reset:function(){
	for(var key in this)
		if(typeof(this[key])!='function')
			this[key]=null;
}};
var rangeobj = {st:null,et:null,mode:false,reset:function(){
	for(var key in this)
		if(typeof(this[key])!='function')
			this[key]=null;
}};
var persist={st:null,et:null,intervalmin:null,interval:null,reset:function(){
	for(var key in this)
		if(typeof(this[key])!='function')
			this[key]=null;
}};

Highcharts.setOptions({
    global: {

//	useUTC:true,
        timezoneOffset:_tzo.offset//+(_tzo.browser*-1))
	/*	getTimezoneOffset: function (timestamp) {
                var zone = getCookie('timezone'),
                    timezoneOffset = moment.tz.zone(zone).parse(timestamp);
                console.log(timezoneOffset)
                return timezoneOffset;
            }*/
    },
    tooltip:{useHTML:false,
   	        xDateFormat: '%e/%b/%Y - %H:%M:%S:%L',
   	        shared: true
        },
    legend:{maxHeight: 80},
    plotOptions:{
    	series:{turboThreshold:__tr}
    }

});
/* CUSTOM SYMBOL PATHS 
	for plotter module	*/
Highcharts.SVGRenderer.prototype.symbols.plus = function (x, y, w, h) {
    return ['M', x, y + h / 2, 'L', x + w, y + h / 2, 'M', x + w / 2, y, 'L', x + w / 2, y + h, 'z'];
};
Highcharts.SVGRenderer.prototype.symbols.rightarrow = function(x,y,w,h){
	w = w+1;
	h = h+1;
	return ['M',x,y,'L',(x+w/2)+3,h-2,x,(h+y)-2];
};
Highcharts.SVGRenderer.prototype.symbols.leftarrow = function(x,y,w,h){
	w = w+1;
	h = h+1;
	return ['M',x+w,y,'L',(x+w/2)-3,h-2,w+x,(h+y)-2];
};
Highcharts.SVGRenderer.prototype.symbols.cross = function(x,y,w,h){
	return ['M',x,y,'L',(x+w),h+y,'M',w+x,y,'L',x,y+h];
};
Highcharts.SVGRenderer.prototype.symbols.column = function(x,y,w,h){
	return ['M',x,y,'L',x,h+y,'M',x+w/2,y+h/3,'L',x+w/2,y+h,'M',x+w,y+h/2,'L',x+w,y+h];
};
Highcharts.SVGRenderer.prototype.symbols.line = function(x,y,w,h){
	return ['M',x,y+h,'L',x+w/5,y+h/3,x+w/2,y+h/2,x+w*3/4,y,x+w,y+(h/10)];
};
Highcharts.SVGRenderer.prototype.symbols.mymenu = function(x,y,w,h){
	var offset = w/8;
	return ['M',x+offset,y+h/2,'L',x+w/20+offset,y+h/2,'M',x+w/3+offset,y+h/2,'L',x+(w/20)+(w/3)+offset,y+h/2,'M',x+(2*w/3)+offset,y+h/2,'L',x+(w/20)+(2*w/3)+offset,y+h/2];
};
Highcharts.SVGRenderer.prototype.symbols.auto = function(x,y,w,h){
	y = y + h/5;
	return ['M',x,y+h/2,'L',x,y,x+w/2,y,'M',x+w,y,'L',x+w,y+h/2,x+w/2,y+h/2];
};
Highcharts.SVGRenderer.prototype.symbols.expand = function(x,y,w,h){
	return ['M',x+w/2,y,'L',x,y,x,y+h/2,'M',x+w/2,y+h,'L',x+w,y+h,x+w,y+h/2,'M',x+w,y+h,'L',x,y];
};
if (Highcharts.VMLRenderer) {
    Highcharts.VMLRenderer.prototype.symbols.plus = Highcharts.SVGRenderer.prototype.symbols.plus;
    Highcharts.VMLRenderer.prototype.symbols.rightarrow = Highcharts.SVGRenderer.prototype.symbols.rightarrow;
    Highcharts.VMLRenderer.prototype.symbols.leftarrow = Highcharts.SVGRenderer.prototype.symbols.leftarrow;
    Highcharts.VMLRenderer.prototype.symbols.cross = Highcharts.SVGRenderer.prototype.symbols.cross;
    Highcharts.VMLRenderer.prototype.symbols.column = Highcharts.SVGRenderer.prototype.symbols.column;
    Highcharts.VMLRenderer.prototype.symbols.line = Highcharts.SVGRenderer.prototype.symbols.line;
    Highcharts.VMLRenderer.prototype.symbols.mymenu = Highcharts.SVGRenderer.prototype.symbols.mymenu;
    Highcharts.VMLRenderer.prototype.symbols.auto = Highcharts.SVGRenderer.prototype.symbols.auto;
    Highcharts.VMLRenderer.prototype.symbols.expand = Highcharts.SVGRenderer.prototype.symbols.expand;
}
/* SYMBOLS END */
function	plotme(x,y,m,src,callback,optional_ep){
	if(interval === 0)	//TEMPORARY HIGHRES ROUTE
	{
		var win = logWindow('highres');
		// if(stackobj.mode)
		// {
		// 	if(!stackobj.stack){
		// 		var stack = logWindow('stacked');
		// 		var id = stack.split('container-stck');
		// 		stackobj.stack = id[1];
		// 	}
		// 	$('#container-stck'+stackobj.stack).append($('#'+win));
		// }
		var id = x;
		highplot('highres-'+win,x);
		if(x.length>1)
			jobpool.splice(0);
		return;
	}
	src = src || null;
	var customId = null;
	callback = callback || null;
	di = 'false';
	cnt = '';
	var m = m || '';
	if(st==undefined||et==undefined||st==''||et=='')
	{
		Notify('Date Error','Please select the date range','normal',true)
		return false;
	}
	var ep;
	var included = null;
	var genericHTML = '';
	var container_type = 'plot';
	if(stackobj.mode)
		container_type = 'stacked';
	if(stackobj.stack){
		if($('#stckd'+stackobj.stack).length == 0)
		{
			alert('Stack not found in memory.');
			stackobj.reset();
			return;
		}
		var len = charts.length;
		customId = 'container'+len+'-stckd'+stackobj.stack;
		$('.minWindow.addmode .winTitle').trigger('click');
	}
	else
		customId = logWindow(container_type,x);
	if(stackobj.mode){
		genericHTML = '<div class="stackplotwrap clearfix">'+controlmarkup(3);
		genericHTML += '<div id="'+customId+'" class="plotcontainer columns medium-10"><img src="images/ajax-loader-black.gif"></div><div class="sidepanel hide"></div><div class="stats hide columns medium-12"></div><div class="dayselect hide"><div class="crossme right">x</div><input type="checkbox" class="daycheck" id="daycheck" class="left"/><label for="daycheck"></label><ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thr</li><li>Fri</li><li>Sat</li><li>Pub</li></ul></div><div style="clear:both"></div></div>';
	}
	var uid = customId.split('-');
	var	data	=	metadata[y];
	var sjson = ''; // Search Query
	if(allmode === true){
		if(_splitstns==false && selectedep === 'search/data'){
			var otherParams = getActionParams();
			sjson = otherParams.Searchjson;
			x = '';
		}
	}
	var _prm = src || "st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+sjson+"&include="+x+"&exclude=&interval="+_interval+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&turnoffglobalexclusion='+!globalex;
	if(selectedep != 'search/data')
	{
		_prm += '&mapjson='+JSON.stringify(mapjson);
	}
	var extract = extractParams(_prm);
	x = x || extract.include;
	if(typeof(x)=='string')
		x = x.split(',');
	ep = optional_ep || selectedep;
	included = x;
	x = x.toString();
	var ctrl = controlmarkup();
	if(src){
	persist.st = parseInt(extract.st);
	persist.et = parseInt(extract.et);
	}
	if($('#'+uid[1]).hasClass('plotter')) {
		$('#'+uid[1]).find('.topheader').empty().append(ctrl);
		dateEvent(uid[1]);
	}
	if(genericHTML){
		if(stackobj.idx!=null)
		{
			if(stackobj.idx == 0)
				$('#'+uid[1]+' .thiswrap').prepend(genericHTML);
			else
				$('#'+uid[1]+' .thiswrap > .stackplotwrap:eq(' + (stackobj.idx - 1) + ')').after(genericHTML);
			dateEvent(uid[1]+' .thiswrap .stackplotwrap:eq('+stackobj.idx+')');
			$('#'+uid[1]+' .thiswrap').animate({scrollTop:$('#'+customId).offset().top},'fast');
		}
		else{
			$('#'+uid[1]+' .thiswrap').append(genericHTML);
			dateEvent(uid[1]+' .thiswrap .stackplotwrap:last-child');
			var animation_offset = $('#'+uid[1]+' .thiswrap #'+customId).parent('.stackplotwrap').offset().top - $('#'+uid[1]+' .thiswrap').offset().top + $('#'+uid[1]+' .thiswrap').scrollTop();
			$('#'+uid[1]+' .thiswrap').animate({scrollTop:animation_offset},'fast');
		}
	}
	var containerId = customId || 'container'+x;
	if(findChart(containerId)){
		Notify('Chart Error','This chart is already plotted','normal',true)
		if(stackobj.mode)
			$('.stackplotwrap:last-child').remove();
		else
		$('.plotter:last-child').remove();
		stackobj.reset();
		return;
	}
	var _ep=[];
	var derived_insert = {};
	$.post('controllers/proxyman.php',{endpoint:"data/"+ep,params:_prm,di:di,contenttype:cnt}, //"+parms.Searchjson+"
		function(d)
			{
				console.log(d);
				if(d.errorCode>0 || d.errorCode === null)
				{
					Notify('Warning',d.errorMsg,'normal',true);
					d.response = {data:[]}
					//plotcomplete(1);
				}
				var chartmeters = []; seriesurl = [];
				var erroneous = [];
				$('#'+containerId+' img').remove();
				if(!d.response)
							d.response = {meters:[{data:d.data}]};
						if(!d.response.meters)
							d.response.meters = [{data:d.response.data}];
						var payload = [];var stats = [];
						if(d.response.meters.length === 0){
							Notify('Plotter','No Data','normal',true);
							return;
						}
						for(var dd=0; dd<d.response.meters.length; dd++){	
						_ep.push("data/"+ep);
						chartmeters.push(d.response.meters[dd].mtrid || null);
						seriesurl.push("st="+extract.st+"&et="+extract.et+"&intervalmin="+extract.intervalmin+"&includepredicted="+extract.includepredicted+"&sensortype="+extract.sensortype+"&search=&exclude=&include="+d.response.meters[dd].mtrid+"&interval="+extract.interval+"&turnoffvee="+extract.turnoffvee);
						var thisguy = null;
						if(d.response.meters[dd].hasOwnProperty('meta')){
							thisguy = d.response.meters[dd].meta._id;
						derived_insert = merge_options(derived_insert,checkDerivations(thisguy));
						}
						else
						{
							var meters_list = extract.include.split(',');
							_.each(meters_list,function(ml){
								derived_insert = merge_options(derived_insert,checkDerivations(thisguy));
							});
						}
						//errorneous points
					//	if(extract.hasOwnProperty('turnoffvee')&&extract.turnoffvee=="true"){
						var badpoints = detectBad(d.response.meters[dd]);
						_.each(badpoints,function(bad){
							erroneous.push(bad);
						});
					//}
						var arr = [];
						payload.push(arr);
						var stuff = d.response.meters[dd].data || null;
						try{
							stats.push(d.response.meters[dd].statistics);
							if(d.response.meters[dd].hasOwnProperty('quality'))
							{
								if(typeof(d.response.meters[dd].quality) === 'number')
									stats[dd].quality = d.response.meters[dd].quality;
								else
									stats[dd].quality = d.response.meters[dd].quality.quality;
							}
						}
						catch(e){
							console.log('Err(Statistics) '+e);
						}					
						d.data = stuff;
						var isDigital = false;
						if(sensortype_map[m] == 'digital')
							isDigital = true;
				for(var	i=0;	i<d.data.length;	i++)
					{
						if(isDigital && i != 0){
							var	temp	=	{};
							if(!isNaN(d.data[i].value))
								temp.y	=	parseFloat(d.data[i].value);
							else
								temp.y = payload[dd][i-1].y;
						//		continue;
							temp.x	=	d.data[i].ts||null;
							payload[dd].push(temp);
							delete(temp);	
						}	
						else
						{		
							var	temp	=	{};
							if(!isNaN(d.data[i].value))
								temp.y	=	parseFloat(d.data[i].value);
							else
								temp.y = null;
						//		continue;
							temp.x	=	d.data[i].ts||null;
							payload[dd].push(temp);
							delete(temp);
						}
					}
				}
				var seriesarr = []; 
				var navseries = [];
				//var flagarr = [];
				var operation = ep.split('/');
				operation = operation[operation.length-1];
				for(var i=0; i<payload.length; i++)
				{
					var isstep = false;
					if(sensortype_map[m] == 'digital')
						isstep = true;
					if(d.response.meters[i]!=undefined && d.response.meters[i].hasOwnProperty('meta'))
                        meta = d.response.meters[i].meta._source
                    else
                 		meta = meterInfo(included[i]);
					var thisname = "Unknown";
					//if(operation == 'data'){	// REGULAR PLOT
						if(meta){
							if(sensortype_displaymap[meta._id] == undefined){
								Notify('Plotter','Station '+meta._id+' meta error for'+m,'normal',true);
								continue;
							}
							if(sensortype_displaymap[meta._id][m]==undefined){
								Notify('Plotter','Station '+meta._id+' meta error for'+m,'normal',true);
								continue;
							}
							var __txt = sensortype_displaymap[meta._id][m].substr(0,sensortype_displaymap[meta._id][m].length-1)+'-'+meta.supply_zone+')';
							if(meta.tag_category != undefined && meta.tag_category == 'residential')
								thisname = __txt;
							else
								thisname = __txt+'('+units[m]+')';
						}
						else
							thisname = m+':'+units[m];
					if(operation != 'data')
						thisname = thisname+' :: '+operation;
				// load annotations
					var rawannt = {sensors:[]};
						try{
							rawannt = loadAnn("st="+extract.st+"&et="+extract.et+"&intervalmin="+extract.intervalmin+"&includepredicted="+extract.includepredicted+"&sensortype="+extract.sensortype+"&search=&include="+thisname+"&exclude=",true);
						}
						catch(e){
							console.log('ERROR LOAD ANN');
						}
				// generate anntoations
				var ret = [];
					for(var g=0;g<rawannt.sensors.length;g++){
						if(rawannt.sensors[g].id == thisname){
							for(var h=0; h<rawannt.sensors[g].data.length; h++)
								ret.push({x:rawannt.sensors[g].data[h].ts,title:'A', text:rawannt.sensors[g].id+'<br/>(MSG): '+rawannt.sensors[g].data[h].annotation.replace(/(?:\r\n|\r|\n)/g, '<br />')})
						}
					}
					//seriesarr.push({data:flags(ret),width:16,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags',onSeries:'ID'+i,useHTML:true});
					seriesarr.push({data:ret,width:18,showInLegend:false,type:'flags',shape : 'squarepin',name:'flags',onSeries:'ID'+i,useHTML:true});
					var temp = {};
					temp.data = payload[i];
					temp.name = thisname;
					temp.id = 'ID'+i;
					temp.stack = 0;
					temp.step = isstep;
					temp.dataGrouping =  {
                    enabled: _datagroupingoption
                }
                	if(navseries.length==0)
                		navseries = payload[i];
					seriesarr.push(temp);
					delete temp;
				}
				if(erroneous.length>0)
				seriesarr.push({data:erroneous,width:16,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags',onSeries:'ID'+i,useHTML:true});
		try{
				var	chart = new Highcharts.Chart({ //Stock
					chart: {
					renderTo: containerId,
					type:'line',
					zoomType: 'x',
					marginTop:40
				},
				legend: {
		            enabled: true
		        },
				navigator: {
				 	adaptToUpdatedData: false,
				 	//data:[],
			    	enabled: true,
			    	height: 15,
			    	xAxis:{
						labels: {
							enabled: false
						}
			    	},
			    	margin:5
			    },
			    scrollbar: {
                	liveRedraw: true,
                	enabled:false
            	},
			    rangeSelector:
			    {
				    buttonTheme:{
				    	style:
				    	{
				 			fontSize:12
				    	}
				    },
			    	allButtonsEnabled: false,
					inputEnabled: false,
			    	selected: 5,
			    	enabled:false,
		            inputBoxHeight: 12,
		            inputStyle: {
		               fontSize:9
		            },
		            labelStyle: {
		            	fontSize:12
		            }
		    	},
			plotOptions: {
            	series: {
				  lineWidth:1,
				  animation:false,
				  shadow:false,
				  stickyTracking:false,
                marker: {
                    enabled: false
                },
                    	states: {
			      hover: {
				     enabled: false
				    }
			      },
                cursor:'pointer',
                point: {
                    events: {
                        click: function(d) 
                        		{
                        			annotate(this);
	                    		}
	                	}
                	}	          
		        }
		    },
					xAxis:	{	type:'datetime',minPadding:0.02,maxPadding:0.02,ordinal:false,
						events : {
		                    afterSetExtremes :function(){ window.clearTimeout(extremes_timer);
		                    					extremes_event = this;
		                    					extremes_timer = setTimeout(function(){
		                    						//console.log('is this thing working');
		                    						Extremes(extremes_event);
		                    					},250);
		                    				}
		                	}
		                	/*,
		                	minRange:3600*1000*/
	                },
					yAxis:	[{opposite:false,title:{text:units[m]}},{opposite:true,title:{text:''}}],
					title: {
						text:''//'Meter '+x
					},
			series:seriesarr,
	        exporting: {
	        	enabled:true,
	            buttons: {
	            	delBtn:{
	            		symbol:'cross',
	            		symbolSize:9,
	            		onclick:function(){
	            			var  chartindex = this.getChartIndex();
	            			var el = $(charts[chartindex].container).parents('.stackplotwrap');
	            			if(el.length == 0)
	            			{
	            				el = $(charts[chartindex].container).parents('.plotter');
	            				charts[chartindex].destroy();
	            				$(el).find('.closeme').trigger('click');
	            			}
	            			else{
	            			var chartid = $(el).children('.plotcontainer').attr('id');
				            var parentid = chartid.split('-');
				            var idx = $(charts[chartindex].container).parents('.stackplotwrap').index();
				            charts[chartindex].destroy();
				            $('#'+parentid[1]+' .stackplotwrap:eq('+idx+')').remove();
				        	}
	            		}
	            	},
	            	contextButton:{
	            		menuItems:[{
	            			text: 'Download CSV',
						    onclick: function () {
						        Highcharts.post('csv.php', {
						            csv: this.getCSV() 
						        });
						    }
	            		},
	            		{
	            			text:'Download PDF',
	            			onclick:function(){
	            				massExport(charts[this.getChartIndex()]);
	            			}
	            		}]
	            	},
	                overlayButton: {
	                   symbol: 'circle',
	                   onclick:function(){
	                   	var chartindex = this.getChartIndex();
	                   	advancedFunction(chartindex);
	                   },
	                   symbolSize:12
	               },
	               addButton:{
	               	symbol:'plus',
	               	symbolSize:12,
	               	onclick:function(){
	               		var thisparent = $(this.getChartContainer()).parent();
	               		$(thisparent).find('.addtoplot').trigger('click');
	               	}
	               },
	               testButton:{
	               	symbol:'rightarrow',
	               	symbolSize:11,
	               	symbolFill: 'transparent',
	               	onclick:function(){
	               		move(1,this.getChartIndex());
	               	}
	               },
	                testButton2:{
	               	symbol:'leftarrow',
	               	symbolSize:11,
	               	symbolFill: 'transparent',
	               	onclick:function(){
	               		move(0,this.getChartIndex());
	               	}
	               },
	              testButton4:{
	              	symbol:'auto',
	              	symbolFill: 'transparent',
	              	// onclick:function(){
	              	// 	autorefresh(this.getChartIndex());
	              	// }
	              	menuItems: [{
	              			text:'Auto Refresh',
	              			onclick:function(){
	              	 			autorefresh(this.getChartIndex());
	              			 }
	              		},
	              		{
	              			text:'Manual Refresh',
	              			onclick:function(){
	              				duplicate_nondes(this.getChartIndex(),true);
	              			}
	              		}]
	              },
	              // lineButton:{
	              // 	symbol:'line'
	              // },
	              // columnButton:{
	              // 	symbol:'column'
	              // },
	              	// zoomresetBtn:{
	              	// 	symbol:'expand',
	              	// 	symbolSize:10,
	              	// 	symbolFill: 'transparent',
	              	// 	onclick:function(){
				            // charts[this.getChartIndex()].zoom();
	              	// 	}
	              	// },
	                testButton3:{
	               	symbol:'mymenu',
	               	symbolSize:20,
	               	menuItems:[{
	               		text:'Statistics',
	               		onclick:function(){
	               		  var el = $(charts[this.getChartIndex()].container).parents('.plotter');
			              if(el.length == 0)
			              	el = $(charts[this.getChartIndex()].container).parents('.stackplotwrap');
			              $(el).find('.stats').toggleClass('hide');
			              $(this).toggleClass('strong');
	               		}
	               	},
	               	{
	               		text:'Series Information',
	               		onclick:function(){
	               		var el = $(charts[this.getChartIndex()].container).parents('.stackplotwrap');
	               		if(el.length == 0)
	               			el = $(charts[this.getChartIndex()].container).parents('.plotter');
	               		var el_panel = $(el).find('.sidepanel');
			               if($(el_panel).hasClass('hide')){
			                    $(el_panel).removeClass('hide');
			                    $(el_panel).animate({opacity:1},'fast');
			                    //$(this).addClass('strong');
			               }
			               else{
			                    //$(this).removeClass('strong');
			                    $(el_panel).animate({opacity:0},'fast',function(){
			                    $(el_panel).addClass('hide');
			                   });    
			               }
	               		}
	               	},
	               	{
	               		text:'Calendar',
	               		onclick:function(){
	               			var ci = charts[this.getChartIndex()].container;
	               			if($(ci).parents('.plotter').length>0)
								$(ci).parents('.plotter').find('.dayselect').toggleClass('hide');
							else
								$(ci).parents('.stackplotwrap').find('.dayselect').toggleClass('hide');
	               		}
	               	},
	               	{
	               		text:'Stack/Un-Stack Series',
	               		onclick:function(){
			              var chartindex = this.getChartIndex();
			              var res = chart2stack(chartindex);
	          			}
	          		},
	               	{
	               		text:'Distraction Free Mode',
	               		onclick:function(){

			              var chartindex = this.getChartIndex();
			              var el = $(charts[chartindex].container).parents('.plotter').children('.plotcontainer');
			              if(el.length == 0)
			              el =  $(charts[chartindex].container).parents('.stackplotwrap').children('.plotcontainer');
			              $(el[0]).css('height','auto');
			              var dims = {height:$(el[0]).height(),width:$(el[0]).width()};
			              var maxwidth = $('body').width() - 10;
			              chartUrls[chartindex].dimensions = dims;
			              charts[chartindex].setSize(maxwidth,500);
			              if(screenfull.enabled){
			                screenfull.request(el[0]);
			              }
	               		}
	               	},
	               	{
	               		text:'Window Chart',
	               		onclick:function(){
				            var  chartindex = this.getChartIndex();
				            var el = $(charts[chartindex].container).parents('.stackplotwrap');
				            if(el.length==0){
				            	alert('Already a window chart');
				            	return;
				            }
				            var chartid = $(el).children('.plotcontainer').attr('id');
				            var parentid = chartid.split('-');
				            if(chartid==null || chartindex==null)
				            {
				              alert('ERR - Exporter');
				              return;
				            }
				            var idx = $(charts[chartindex].container).parents('.stackplotwrap').index();
				            charts[chartindex].destroy();
				            $(this).parents('.stackedPlotter').find('.minme').trigger('click');
				            $('#'+parentid[1]+' .stackplotwrap:eq('+idx+')').remove();
				            duplicate(chartUrls[chartindex]);
	               		}
	               	}]
	               	// onclick:function(){
	               	// 	var el = this.container;
	               	// 	var prnts = $(el).parents('.stackplotwrap');
	               	// 	// if(prnts.length==0){
	               	// 	// 	prnts = $(el).parents('.plotter');
	               	// 	// 	$(prnts).find('.controlsUL').toggleClass('hide');
	               	// 	// }
	               	// 	// else
	               	// 	// 	$(prnts).find('.stacktoolbar').toggleClass('hide');
	               		
	               	// }
	               }

	            }
   		    }
   		});
	}
		catch(e){
			console.log(e);
		}
					charts.push(chart);
					var clr = [];
					for(var s=0; s<chart.series.length; s++){
						if(chart.series[s].name == 'flags' || chart.series[s].name == 'Navigator')
							continue;
						clr.push(chart.series[s].color)
					}
					if(operation == 'data')
						buildLegend(containerId,clr,x,1,stats);
					else
						buildLegend(containerId,clr,x,2,stats);
					chartUrls.push({"meter":x,"chartmeters":[chartmeters.toString()],"seriesurl":seriesurl,"st":extract.st,"et":extract.et,"intervalmin":extract.intervalmin,"includepredicted":extract.includepredicted,type:m,names:["Meter "+extract.include],sensorType:[extract.sensortype],endpoint:_ep,params:["st="+extract.st+"&et="+extract.et+"&intervalmin="+extract.intervalmin+"&includepredicted="+extract.includepredicted+"&sensortype="+extract.sensortype+"&search=null&include="+extract.include+"&exclude=null"],type2:null,case:1,charthistory:[["data/"+ep,_prm]],units:[units[m],''],derivations:derived_insert});
					if(stackobj.mode){
						if(stackobj.stack)
							$('.minWindow').removeClass('addmode');
					}
					checkReg(chart);
					if(typeof(callback)=='function')
						callback();
					plotcomplete(1);
			},'json');

}

function plotaggr(src,sen,callback,thisep){
	src = src || null;
	sen = sen || null;
	thisep = thisep || null;
	if(!src){
		if(!aggregateobj.mode && !allmode && !averageobj.mode)
			return;
		if(!Radiochecked){
			//alert('Error no radio selection');
			Notify('Action Error','Error no radio selection','normal',true)
			return;
		}
	}
	var operation = 'Aggr ';
	if(thisep)
		operation = 'Avg ';
	var m;
	m = Radiochecked;
	var customId = null;
	var m = m || defaultSensor;
	var pp = getActionParams();
	var searchjson;
	var _ep=[thisep||"data/search/aggregate"];
	if(allmode){
		searchjson = pp.Searchjson;
		pp.include = '';
	}
	else
		searchjson = '';
	var _prm = src || "st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()+"&interval="+_interval+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&turnoffglobalexclusion='+!globalex;
	var extract = extractParams(_prm);
		m = extract.sensortype;
	if(st==undefined||et==undefined)
	{
		//alert('Please select time range for date');
		Notify('Date Error','Please select the date range','normal',true)
		return false;
	}
	customId = 'Aggr'+getRandomInt(0,100)
	logWindow('plot',customId);
	console.log(customId);
	var html = controlmarkup();
	$('.plotter:last-child').find('.topheader').empty().append(html);
	dateEvent(customId);
	var containerId = 'container'+customId;
	if(findChart(containerId)){
		//alert('This chart is already plotted');
		Notify('Chart Error','This chart is already plotted','normal',true)
		$('.plotter:last-child').remove();
		return;
	}
		
	proxyUrl='controllers/proxyman.php';
	//Proxydata="data/"+x+"/meter?st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred;
	$.get('controllers/proxyman.php',{endpoint:_ep[0],params:_prm},
		function(d)
			{
				var title;
				if(pp.searchtxt == undefined || src!=null)
					title = 'Total'
				else{
					var titexpl = pp.searchtxt.split(',');
					title = titexpl[titexpl.length-1];
				}
				// Title Selection
				var meterlist = [];
				if(src){
					var list = extract.include.split(',');
					if(list.length>0){
					_.each(pp.include,function(mm){
						var info = meterInfo(mm);
						var name = info.bp_name || info.customer_name || info.customername || null;
						meterlist.push(name);
					});
					var groupmeters = _.uniq(meterlist);
					if(groupmeters.length == 1)
						title = groupmeters[0];
					}
				}
				else{
					if(pp.include.length>0 && allmode == false){
					_.each(pp.include,function(mm){
						var info = meterInfo(mm);
						var name = info.bp_name || info.customer_name || info.customername || null;
						meterlist.push(name);
					});
					var groupmeters = _.uniq(meterlist);
					if(groupmeters.length == 1)
						title = groupmeters[0];
					}		
				}
				if(thisep)
					title = operation+" - "+m;	// for avg
				var annotations = loadAnn("st="+extract.st+"&et="+extract.et+"&intervalmin="+extract.intervalmin+"&includepredicted="+extract.includepredicted+"&sensortype="+m+"&search="+extract.search+"&include="+extract.include.toString()+"&exclude="+extract.exclude.toString());
				var flagobj = {data:flags(annotations),width:16,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags',onSeries:'aggr',useHTML:true};
				$('#'+containerId+' img').remove();
				var stuff = d.response.meters[0].data || null;
				var stats = [d.response.meters[0].statistics];
				stats[0].quality = ((d.response.meters[0]||{}).quality.quality||{});
				d.data = stuff;
				if(!d.data){
					$('#'+containerId).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>')
					return;
				}
				var	payload	=	[];
				for(var	i=0;	i<d.data.length;	i++)
					{
						var	temp	=	{};
						temp.y	=	d.data[i].value || null;
						temp.x	=	d.data[i].ts || null;
				//		temp.id = 'ID'+i;
						payload.push(temp);
						delete(temp);
					}
				//$('.plotter').css('display','block');
				var	chart = new Highcharts.Chart({
					chart: {
					renderTo: containerId,
					type:'line',
					zoomType: 'xy'
				},
				 navigator: {
			    enabled: true,
			    height: 15,
			    xAxis:{
				labels: {
					enabled: false
				}
			    }},
			    rangeSelector: {
		    	allButtonsEnabled: true,
				inputEnabled: true,
		    	selected: 1,
		    	enabled:true
		    	},
				plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		},
                cursor:'pointer',
                point: {
                    events: {
                        click: function(d) {
                        	annotate(this);
	                        }
	                    }
	                }
		            }
		        },
				xAxis:	{	type:'datetime',
				events: {
	    			setExtremes: function(e) {
	    			}
	    		}},
					yAxis:	[{opposite:false,title:{text:units[m]}},{opposite:true,title:{text:''}}],
					title: {
						text:''
					},
					series:	[{data:payload,turboThreshold:__tr,name:title,stack:0,id:'aggr'},flagobj]
				});
					charts.push(chart);
					buildLegend(containerId,[chart.series[0].color],pp.include,2,stats)
					chartUrls.push({"meter":null,"chartmeters":[pp.include.toString()],"seriesurl":["st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()],"st":st,"et":et,"intervalmin":interval,"includepredicted":pred,type:m,names:["Meter "+operation],sensorType:[m],endpoint:_ep,params:["st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()],type2:null,case:2,exclude:pp.exclude.toString(),include:pp.include.toString(),search:searchjson,charthistory:[[_ep[0],"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()+"&interval="+_interval+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&turnoffglobalexclusion='+!globalex]],units:[units[m],'']});
					checkReg(chart);
					if(aggregateobj.mode)
					aggregateobj.reset();
				if(typeof(callback)=='function')
					callback();
				plotcomplete(1);
			},'json');

}
//	DEPRECATED
function plotFlow(){
	var url = "https://waterwise.visenti.com/waterwise2/fcph/support/getdata_support.php?op=eventmon&ev=0&ad=0&ut=stdout&starttime="+st+"&endtime="+et+"&avg=1h&nodeid="+tanksobj.id+"&sensorname=OUTFLOW";
	var cid = "Flow"+tanksobj.id;
	var cid2 = "container"+cid;
	if(findChart(cid)){
		//alert('This chart is already plotted');
		Notify('Chart Error','This chart is already plotted','normal',true)
		return;
	}
	var winid = logWindow('plot',cid);
	//$('#'+cid).append('<div id="'+cid2+'" class="plotcontainer columns medium-11 forceCenter"></div>');
	var html = controlmarkup();
	$('#'+cid+' .topheader').append(html);
	$.get('controllers/proxyman.php',{params:url,ww:'true'},function(d)
				{
					if(!d)	{
						//alert('Error - Bad Data Received');
						Notify('Data Alert!','Error - Bad Data Received','normal',true)
						$('.plotter:last-child').remove();
						return;
					}
					console.log(d);
					var payload = [];
					var stuff = d.split('|');
					for(var i=0; i<stuff.length; i++)
					{
						var extracted = stuff[i].substring(1, stuff[i].length-1);
						var tankdata = extracted.split(',');
						if(isNaN(parseInt(tankdata[0])) || isNaN(parseFloat(tankdata[1])))
							continue;
						var temp = {x:parseInt(tankdata[0]),y:parseFloat(tankdata[1]),id:'ID'+i};
						payload.push(temp);
					}
					console.log(payload);
					var	chart = new Highcharts.Chart({
					chart: {
					renderTo: cid2,
					type:'line',
					 zoomType: 'xy'
					},
				 navigator: {
			    	enabled: true,
			    	height: 15,
			    	xAxis:{
					labels: {
						enabled: false
					}
			    	}},
			    rangeSelector: {
		    	allButtonsEnabled: true,
				inputEnabled: true,
		    	selected: 1,
		    	enabled:true
		    	},
					plotOptions: {
	            	series: {
	                marker: {
	                    enabled: false
	                		}
			            }
			        },
					xAxis:	{	type:'datetime'},
					yAxis:[{opposite:false,title:{text:'CMH'}},{opposite:true,title:{text:''}}],
					title: {
						text:'Reservoir Flow '+tanksobj.id
					},
					series:	[{data:payload,turboThreshold:__tr,name:'Flow '+tanksobj.id},{data:flags(),width:18,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags'}]
				});
					charts.push(chart);
					buildLegend(cid2,[chart.series[0].color],tanksobj.id,1);
					chartUrls.push({"meter":tanksobj.id,"st":st,"et":et,type:'tank',names:['Flow '+tanksobj.id],type:'tankflow',type2:null,case:3});
					tanksobj.reset();
				});

}
// DEPRECATED
function plotWeather(m){
	if(!weatherobj.id){
		//alert('No Selection Found');
		Notify('Selection Error','Please select a marker first','normal',true)
		return;
	}
	var un = units[m];
	var cid = m+weatherobj.id;
	var cid2 = "container"+cid;
	if(findChart(cid)){
		//alert('This chart is already plotted');
		Notify('Chart Error','This chart is already plotted','normal',true)
		return;
	}
	var winid = logWindow('plot',cid);
	//$('#'+cid).append('<div id="'+cid2+'" class="plotcontainer columns medium-11 forceCenter"></div>');
	var html = controlmarkup();
	$('#'+cid+' .topheader').append(html);
	var ep;
	if(m == 'RAIN')
		ep = "rainfile";
	if(m == "TEMP")
		ep = "tempfile";
	$.get('controllers/proxyman.php',{endpoint:'data/'+ep+'?st='+st+'&et='+et+'&sample='+weatherobj.id},function(d)
			{
				if(!d || !d.data)	{
					$('#'+cid2).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>');
					return;
				}
				if(d.data.length == 0)
				{
					$('#'+cid2).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>');
					return;	
				}
				console.log(d);
				var	payload	=	[];
				for(var	i=0;	i<d.data.length;	i++)
					{
						var	temp	=	{};
						
						if('temp' in d.data[i]){
							temp.y	=	d.data[i].temp;
						}
						else
						{
							temp.y	=	d.data[i].rainfall;
						}
						temp.x	=	d.data[i].ts;
						payload.push(temp);
						delete(temp);
					}
					console.log(payload);
		



		var	chart = new Highcharts.Chart({
						chart: {
						renderTo: cid2,
						type:'line',
						zoomType: 'xy'
					},
				 navigator: {
			    	enabled: true,
			    	height: 15,
			    	xAxis:{
					labels: {
						enabled: false
					}
			    	}},
			    rangeSelector: {
		    	allButtonsEnabled: true,
				inputEnabled: true,
		    	selected: 1,
		    	enabled:true
		    	},
					plotOptions: {
	            	series: {
	                marker: {
	                    enabled: false
	                		}
			            }
			        },
					xAxis:	{	type:'datetime'	},
					yAxis:	[{opposite:false,title:{text:un}},{opposite:true,title:{text:''}}],
					title: {
						text:m+' '+weatherobj.id
					},
					series:	[{data:payload,turboThreshold:__tr,name:m+' '+weatherobj.id}]
				});
					charts.push(chart);
					buildLegend(cid2,[chart.series[0].color],weatherobj.id,1);
					chartUrls.push({"meter":weatherobj.id,"st":st,"et":et,type:m,names:[m+' '+weatherobj.id],endpoint:['data/tempfile?st='+st+'&et='+et+'&sample='+weatherobj.id],params:[null],type2:null,case:4});
					weatherobj.reset();
			},'json');
}

function chart2line(x){
	var series = charts[charttypeobj.chartindex].series[x];
	series.update({type:'line'});
}
function chart2column(x){
	var series = charts[charttypeobj.chartindex].series[x];
	series.update({type:'column'});
}
function chart2stack(x){
	var ret = null;
	if(charts[x].options.plotOptions.series.hasOwnProperty('stacking') && charts[x].options.plotOptions.series.stacking=='normal')
	{
		charts[x].options.plotOptions.series.stacking = null;
		for(var i=0; i<charts[x].series.length; i++){
			if(charts[x].series[i].name == 'flags' || charts[x].series[i].name == 'Navigator')
				continue;
			charts[x].series[i].update({stack:(charts[x].series.length+i)});
		}
		ret = false;
	}
	else
	{
		charts[x].options.plotOptions.series.stacking = 'normal';
		for(var i=0; i<charts[x].series.length; i++){
			if(charts[x].series[i].name == 'flags' || charts[x].series[i].name == 'Navigator')
				continue;
			charts[x].series[i].update({stack:0});	
		}
		ret = true;
	}
	charts[x].redraw();
	return ret;
}

/*	---- DEPRECATED ------ 
-----------------------------	*/
function replot(idx){
	if(chartUrls[idx].names.length>1)
	{
		//alert('Cannot change base parameters of multiple plots');
		Notify('Parameter Error','Cant change base parameters of multiple plots','normal',true)
		return;
	}
	charts[idx].destroy();
	replotobj.index = idx;
	var mid = chartUrls[idx].meter;
	var cid = replotobj.id;
	var met = replotobj.et || chartUrls[idx].et || et; 
	var mst = replotobj.st || chartUrls[idx].st || st;
	var mint = replotobj.res || interval;
	var mpred = replotobj.pred || pred;
	var stype = chartUrls[idx].sensorType;
	var msearch = chartUrls[idx].search;
	var minclude = chartUrls[idx].include;
	var mexclude = chartUrls[idx].exclude;
	// construct url
	var data,endpoint;
	switch(chartUrls[idx].case){
		case 1:  	//	Single Plot

/*		endpoint = "data/"+mid;
		data = "st="+mst+"&et="+met+"&intervalmin="+mint+"&includepredicted="+mpred+"&sensorType="+stype;*/
		break;
		case 2: 	//	Aggregate 
		endpoint = "data/search/aggregate"
		data = "st="+mst+"&et="+met+"&sensortype="+stype+"&intervalmin="+mint+"&includepredicted="+mpred+"&search="+msearch+"&include="+minclude+"&exclude="+mexclude;
		break;
		case 5: 	//	Average
		endpoint = "data/search/average";
		data = "st="+mst+"&et="+met+"&sensortype="+stype+"&intervalmin="+mint+"&includepredicted="+mpred+"&search="+msearch+"&include="+minclude+"&exclude="+mexclude;
		break;
	}
	$.get('controllers/proxyman.php',{endpoint:endpoint,params:data},
		function(d)
			{
				console.log(d);
				if(!d.response)
					console.log('legacy');
				if(typeof(d.response.meters)=='undefined')
				var stuff = d.response.data || null;
				d.data = stuff;
				var	payload	=	[];
				if(chartUrls[idx].type != 'tank'){
				for(var	i=0;	i<d.data.length;	i++)
					{
						var	temp	=	{};
						temp.y	=	d.data[i].value;
						temp.x	=	d.data[i].ts;
						payload.push(temp);
						delete(temp);
					}
				}
				else // This is a TANK. (NO JSON)
				{
					var stuff = d.split('|');
					for(var i=0; i<stuff.length; i++)
					{
						var extracted = stuff[i].substring(1, stuff[i].length-1);
						var tankdata = extracted.split(',');
						if(isNaN(parseInt(tankdata[0])) || isNaN(parseFloat(tankdata[1])))
							continue;
						var temp = {x:parseInt(tankdata[0]),y:parseFloat(tankdata[1])};
						payload.push(temp);
					}
				}
				//$('.plotter').css('display','block');
				var	chart = new Highcharts.Chart({
					chart: {
					renderTo: cid,
					type:'line',
					 zoomType: 'xy'
				},
				plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
		            }
		        },
					xAxis:	{	type:'datetime'},
					yAxis:	{
						title	:	{	text:"Consumption"
							}
					},
					title: {
						text:'Aggr '
					},
					series:	[{data:payload,turboThreshold:__tr,name:'Aggr'}]
				});
					charts[replotobj.index]	=	chart;
					replotobj = {};
					chartUrls[idx].st = mst;
					chartUrls[idx].et = met;
					chartUrls[idx].intervalmin = mint;
					chartUrls[idx].includepredicted = pred;
			},'json');
}

function avg2plot(src,sen,callback){
	plotaggr(src,sen,callback,'data/search/average');
	return;
	src = src || null;
	sen = sen || null;
	if(!src){
		if(!averageobj.mode && !allmode)
			return;
		if(!Radiochecked){
			//alert('Error no radio selection');
			Notify('Action Error','Error no radio selection','normal',true)
			return;
		}
	}
	var m = sen;
	var customId = null;
	customId = m+getRandomInt(0,100)
	logWindow('plot',customId);
	console.log(customId);
	var html = controlmarkup();
	$('.plotter:last-child').find('.topheader').empty().append(html);
	var containerId = 'container'+customId;
	if(findChart(containerId)){
		//alert('This chart is already plotted');
		Notify('Chart Error','This chart is already plotted','normal',true)
		$('.plotter:last-child').remove();
		return;
	}
	dateEvent(customId);	
	proxyUrl='controllers/proxyman.php';
	//Proxydata="data/"+x+"/meter?st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred;
	var pp = getActionParams();
	var searchjson;
	if(allmode){
		searchjson = pp.Searchjson;
		pp.include = '';
	}
	else
		searchjson = '';
	var _prm = src || "st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()+"&interval="+_interval+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&turnoffglobalexclusion='+!globalex;
	var extract = extractParams(_prm);
		m = extract.sensortype;
	if(st==undefined||et==undefined)
	{
		Notify('Date Error','Please select the date range','normal',true)
		return false;
	}
	$.get('controllers/proxyman.php',{endpoint:"data/search/average",params:_prm},
		function(d)
			{
				var title;
				if(pp.searchtxt == undefined)
					title = 'Avg'
				else{
					var titexpl = pp.searchtxt.split(',');
					title = titexpl[titexpl.length-1];
				}
				console.log(d);
				
				var stuff = d.response.meters[0].data || null;
				var stats = [d.response.meters[0].statistics];
				stats[0].quality = d.response.meters[0].quality.quality;
				d.data = stuff;
				if(!d.data){
					$('#'+containerId).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>')
					return;
				}
				var	payload	=	[];
				for(var	i=0;	i<d.data.length;	i++)
					{
						var	temp	=	{};
						if(!d.data[i].hasOwnProperty('value'))
								temp.y=null;
							else
								temp.y	=	d.data[i].value;
						temp.x	=	d.data[i].ts;
						payload.push(temp);
						delete(temp);
					}
				//$('.plotter').css('display','block');
				var	chart = new Highcharts.Chart({
					chart: {
					renderTo: containerId,
					type:'line',
					 zoomType: 'xy'
				},
				 navigator: {
			   	enabled: true,
			   	height: 15,
			   	xAxis:{
				labels: {
					enabled: false
				}
			   	}},
			    rangeSelector: {
		    	allButtonsEnabled: true,
				inputEnabled: true,
		    	selected: 1,
		    	enabled:true
		    	},
				plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
		            }
		        },
							xAxis:	{	type:'datetime'},
					yAxis:	[{opposite:false,title:{text:units[m]}},{opposite:true,title:{text:''}}],
					title: {
						text:'Average Plot'
					},
					series:	[{data:payload,turboThreshold:__tr,name:m+' '+title,stack:0},{data:flags(),width:18,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags'}]
				});
					buildLegend(containerId,[chart.series[0].color],pp.include,2,stats);
					charts.push(chart);
					chartUrls.push({"meter":null,"st":st,"et":et,"chartmeters":[pp.include.toString()],"seriesurl":[_prm],type:m,names:[m+" Avg"],sensorType:[m],endpoint:["data/search/aggregate"],params:["st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()],type2:null,case:5,"sensorType":[m]});
	//	chartUrls.push({"meter":null,"chartmeters":[pp.include.toString()],"seriesurl":["st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()],"st":st,"et":et,"intervalmin":interval,"includepredicted":pred,type:m,names:["Meter "],sensorType:[m],endpoint:_ep,params:["st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()],type2:null,case:2,exclude:pp.exclude.toString(),include:pp.include.toString(),search:searchjson,charthistory:[["data/search/aggregate","st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()+"&interval="+_interval+"&turnoffvee="+veecheck]]});				
					checkReg(chart);
					if(weatherobj.mode)
						weatherobj.reset();
					if(averageobj.mode)
						averageobj.reset();
					if(typeof(callback)=='function')
						callback();
					plotcomplete();
			},'json');

}

/*	------	NOT NEEDED HERE -------	
--------------------------------------	*/

function dailyDemand(w){
	
	if(defaultSettings.demandForecast)
	{
		var m = inc=defaultSettings.dmaItems[0].sensor
		var ep = 'data/forecast/default';
		inc=defaultSettings.dmaItems[0].id

		var watertype='&search=&include='+inc+'&exclude=&interval=';
	}
	else
	{
		var m = 'SGDEMAND';
		var ep = 'singaporedata/demand';
		var watertype = '&watertype='+w; //cluster
	}
	var cluster = '';
	// if($('#DemandCluster .regular').length>0)
	// {
	// 	cluster = '&include=';
	// 	var txt=[];
	// 	$('#DemandCluster .regular').each(function(i,e){
	// 		txt.push($(this).text().toLowerCase().replace(/\s+/g, ''));
	// 	});
	// 	cluster = cluster+txt.toString();
	// }
	
	if(isZone(w))
	{
		m = 'ACTUAL';
		ep = ep+'/'+wwMap(w).toLowerCase()+'/0';
		watertype = '&search=&include=&exclude=&interval=';
	}
	
	// var customId = 'DD'+getRandomInt(0,100);
	// logWindow('plot',customId);
	// var containerId = 'container'+customId;

	var containerId = logWindow('plot',null);
	var expl = containerId.split('-');
	var customId = expl[1]||'';

	var html = controlmarkup();
//	$('#'+customId).append('<div class="plotcontainer columns medium-11 forceCenter" id="'+containerId+'"></div><div class="stats hide columns medium-12"></div>');
	$('#'+customId+' .topheader').append('&nbsp;&nbsp;Singapore Demand');
	$('#'+customId+' .topheader').append('<div class="hide">'+html+'</div>');
	/*var et = new Date().getTime();
	var st = et - 86400000;*/
	$.get('controllers/proxyman.php',{endpoint:ep,params:'st='+st+'&et='+et+'&includepredicted='+pred+'&intervalmin='+interval+'&sensortype='+m+watertype+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&turnoffglobalexclusion='+!globalex},function(d){
		if(d.errorCode>0 || d.errorCode === null)
		{
			alert(d.errorMsg||'Err Demand Plotter');
			$('#'+customId +' .plotcontainer img').remove();
			$('#'+customId).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>');
			return;
		}
		if(typeof(d.response)!='object')
			d.response = JSON.parse(d.response);
			d.data = d.response.data || d.response.meters[0].data || null;
			if(!d.data){
				$('#'+customId+' .plotcontainer img').remove();
				$('#'+customId).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>');
				return;
			}
			if(d.data.length==0)
			{
				$('#'+customId+' .plotcontainer img').remove();
				$('#'+customId).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>');
				return;	
			}
			var payload = [];
			for(var i=0; i<d.data.length; i++)
			{
				var temp = {};
				temp.y = d.data[i].value;
				temp.x = d.data[i].ts;
				payload.push(temp);
				delete payload;
			}
		var chart = new Highcharts.Chart({
					chart: {
					renderTo: containerId,
					type:'line',
					zoomType: 'x',
					animation:false
				},
				 navigator: {
			    	enabled: true,
			    	height: 15,
			    	xAxis:{
					labels: {
						enabled: false
					}
			    	}},
			    rangeSelector: {
		    	allButtonsEnabled: true,
				inputEnabled: true,
		    	selected: 1,
		    	enabled:true
		    	},
				plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
		            }
		        },
					xAxis:	{	type:'datetime'},
					yAxis:	[{opposite:false,title:{text:units[m]}},{opposite:true,title:{text:''}}],
					title: {
						text:''
					},
				exporting:{
					enabled:true,
					buttons:{
						addButton:{
	               	symbol:'plus',
	               	symbolSize:12,
	               	onclick:function(){
	               		var thisparent = $(this.getChartContainer()).parent();
	               		$(thisparent).find('.addtoplot').trigger('click');
	               			}
	               		}
					}
				},
				series:	[{data:payload,turboThreshold:__tr,name:'Daily Demand '+w+' (SG)'}]
				});
					charts.push(chart);
					chartUrls.push({"meter":null,"chartmeters":[],"st":st,"et":et,"intervalmin":interval,"includepredicted":pred,type:m,names:['Daily Demand'],sensorType:[m],endpoint:[ep],params:["st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred],type2:null,case:10,seriesurl:['st='+st+'&et='+et+'&includepredicted='+pred+'&intervalmin='+interval+'&sensortype='+m+watertype+"&turnoffvee="+veecheck+'&includeflag='+flagsbit],units:[units[m],''],charthistory:[[ep,'st='+st+'&et='+et+'&includepredicted='+pred+'&intervalmin='+interval+'&sensortype='+m+watertype+"&turnoffvee="+veecheck+'&includeflag='+flagsbit]]});
	},'json');
}

/*	---- DEPRECATED	-----
----------------------------	*/
function forecast2plot(z){ 
	var zone = wwMap(z).toLowerCase();
	url_f = "https://waterwise.visenti.com/waterwise2/"+zone+"/support/getDemandPredictionData_support.php?op=dailyforecast&z=0&st="+st+"&et="+et;
	url_a = "https://waterwise.visenti.com/waterwise2/"+zone+"/support/getDemandPredictionData_support.php?op=dailyactual&z=0&st="+st+"&et="+et;
	var customId = 'Forecast'+getRandomInt(0,100);
	logWindow('sr',customId);
	var containerId = 'container'+customId;
	var html = controlmarkup();
	$('#'+customId).append('<div class="plotcontainer columns medium-11 forceCenter" id="'+containerId+'"></div>');
	$('#'+customId+' .topheader').append(html);
	var seriesF,seriesA,unitsF,unitsA;
	var ppp=null;
	$.get('controllers/proxyman.php',{params:url_a,ww:'true'},
		function(d){
			console.log(d);
			var stuff = JSON.parse(d.data);
			console.log(stuff);
			/*chart.addSeries({
				name:'Daily Actual '+zone.toUpperCase(),
				data:stuff,
				yAxis:0
			});*/
	var	chart = new Highcharts.Chart({
					chart: {
					renderTo: containerId,
					type:'line',
					 zoomType: 'xy'
				},
					 navigator: {
			    	enabled: true,
			    	height: 15,
			    	xAxis:{
					labels: {
						enabled: false
					}
			    	}},
			    rangeSelector: {
		    	allButtonsEnabled: true,
				inputEnabled: true,
		    	selected: 1,
		    	enabled:true
		    	},
		    		plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
                	}
                },
					xAxis:	{	type:'datetime'},
					yAxis:	[{opposite:false,title:{text:'MGD'}},{opposite:true,title:{text:''}}],
					title: {
						text:'Zonal Demand'
					},
					series:	[{name:'Daily Forecast - '+zone.toUpperCase(),data:stuff,yAxis:0}]
				});
		charts.push(chart);
		chartUrls.push({"st":st,"et":et,"names":['forecast'],type:'OUTFLOW',type2:null,'seriesurl':url_a,'chartmeters':[zone]});
		console.log(charts)
		},'json');
}

function controlmarkup(x){
var x = x || null;
var res = 'Unknown';
var int_min = persist.intervalmin||interval;
var int_true = persist.interval||_interval;
if(int_true==""){
	if(int_min == 60)
		res = 'Hour';
	if(int_min == 1440)
		res = 'Day';
	if(int_min == 720)
		res = '12 Hours';
	if(int_min == 360)
		res = '6 Hours';
	if(int_min == 30)
		res = '30 Mins';
	if(int_min == 15)
		res = '15 Mins';
	if(int_min == 5)
		res = '5 Mins';
	if(int_min == 1)
		res = '1 Min';
	if(int_min == 0.5)
		res = '30 Secs';
	if(int_min == 0.01667)
		res = '1 Sec';
}
else
{
	res = int_true.charAt(0).toUpperCase() + int_true.substring(1);
}
if(x){
	if(x == 1)	//	VERTICAL CONTROLS 	((**** DEPRECATED ****))
	{
		return '<div class="verticalControls"><ul><!--<li class="playme"><img src="images/play.png" width="20px"></li>-->\
		<li class="_extract"><img src="images/extract.png" width="20px"></li>\
		<li class="_overlay"><img src="images/overlay.png" width="20px"></li>\
		<li class="duplicateme"><img src="images/duplicate.png" width="20px"></li>\
		<li class="unitsctrl"><img src="images/units.png" width="20px"></li></ul></div>';
	}
	if(x == 2)	//	OVERLAY toolbar		((**** DEPRECATED ****))
	{
		return '<div class="toolbar"><ul class="inline-list">\
				<li class="selected" id="raw">Raw Values</li>\
				<li id="min">Min</li>\
				<li id="max">Max</li></ul></div>';
	}
	if(x == 3)	//	STACKED PLOTS
	{
		return '<div class="stacktoolbar"><ul>\
		<li class="controls"><a class="zoom_reset">Reset Zoom</a></li>\
		<li class="controls"><strong>Resolution</strong> <a class="res_btn" data-id="'+res+'">'+res+'</a> <strong>Range</strong> <a class="rng_btn" data-id="range"><span class="_timerange"> '+$('#date-range1').val()+'</span></a></li>\
	<!--	<li class="controls"><a class="statsbtn">Statistics</a></li> -->\
	<!--	<li class="controls"><a class="infobar">Info</a></li>	-->\
		<li class="controls"><a class="autoplot_switch">AutoResolution</a></li>\
		<li class="controls"><img src="images/swap.png" width="20px" class="_img swapaxis"></li>\
	<!--	<li class="controls"><img src="images/calendar.png" width="20px" class="dayonday _img"></li> -->\
	<!--	<li class="controls"><img src="images/magnify.png" width="14px" class="_img zoombtn"></li> -->\
		<li class="controls"><img src="images/linechart.png" width="20px" class="_img chartopt lineopt"/></li><li class="controls"><img src="images/barchart.png" width="20px" class="_img chartopt baropt"/></li>\
		<li class="controls"><img src="images/unitsbold.png" width="20px" class="_img unitsctrl"></li>\
	<!--	<li class="controls"><img src="images/download.png" width="20px" class="_img pdfdown"></li> -->\
	<!--	<li class="controls"><img src="images/export.png" width="18x" class="_img exportme"></li>	-->\
	<!--	<li class="controls"><img src="images/del.png" width="18x" class="_img delme"></li> -->\
		</ul></div><div class="_plus addtoplot" title="Add to this chart">+</div><div class="_options"><ul><li></li><li></li><li></li></ul></div><div class="tag_refresh"></div>';
	}
}
var res = 'Inconclusive';
var int_min = persist.intervalmin||interval;
var int_true = persist.interval||_interval;
if(int_true==""){
	if(int_min == 60)
		res = 'Hour';
	if(int_min == 1440)
		res = 'Day';
	if(int_min == 720)
		res = '12 Hours';
	if(int_min == 360)
		res = '6 Hours';
	if(int_min == 30)
		res = '30 Mins';
	if(int_min == 15)
		res = '15 Mins';
	if(int_min == 5)
		res = '5 Mins';
	if(int_min == 1)
		res = '1 Min';
	if(int_min == 0.5)
		res = '30 Secs';
	if(int_min == 0.01667)
		res = '1 Sec';
}
else
{
	res = int_true.charAt(0).toUpperCase() + int_true.substring(1);
}

	//var html = '<ul class="controlsUL"><li class="controls"><strong>Resolution:</strong> '+res+'<!--<a class="plotagain" data-id="res,1440"></a>--></li><li><strong>Range:</strong><span class="_timerange"> '+$('#date-range1').val()+'</span> <!--<a class="plotagain" data-id="range">Change</a>--></li>';
	var html = '<ul class="controlsUL"><li class="controls"><a class="zoom_reset">Reset Zoom</a></li>';
	html += '<li class="controls"><strong>Resolution: </strong><a class="res_btn" data-id="'+res+'">'+res+'</a></li><li><strong>Range:</strong><a class="rng_btn" data-id="range"><span class="_timerange"> '+$('#date-range1').val()+'</span></a></li>';
	html += '<li class="controls"><a class="addtoplot hide">+ Add</a></li>';
//	html += '<li class="controls"><a class="infobar">Info</a></li>';
//	html += '<li class="controls"><a class="stackbtn">Stack</a></li>';
	html += '<li class="controls"><a class="autoplot_switch">AutoRes</a></li>';
	//html += '<li class="controls"><a class="zoombtn">Zoom</a></li>';
//	html += '<li class="controls"><a class="statsbtn">Statistics</a></li>';
	//html += '<li class="controls hide"><a class="regressionBtn">Trend</a></li>';
	//html += '<li class="controls hide strong"><a class="mgd2m3">MGD</a></li>';
	html += '<li class="controls hide"><img src="images/swap.png" width="20px" class="_img swapaxis"></li>';
	//html += '<li class="controls"><img src="images/magnify.png" width="14px" class="_img zoombtn"></li>';
//	html += '<li class="controls"><img src="images/download.png" width="20px" class="_img pdfdown"></li>';
	if(	(et - st)	>= 604800000){	//	WEEK APPLICABLE
		html += '<li class="controls"><img src="images/calendar.png" width="20px" class="dayonday _img"></li>';
	}
	html += '<li class="controls"><img src="images/linechart.png" width="25px" class="_img chartopt lineopt"/><img src="images/barchart.png" width="25px" class="_img chartopt baropt"/></li>';
	html += '</ul>';
	return html;
}
function buildLegend(id,clr,inc,_case,stats){
	var el = $('#'+id).parent().find('.sidepanel');
	var el2 = $('#'+id).parent().find('.stats');
	var html = '<div class="boxlist">';
	var html2 = '';
	var data = getActionParams();
	data.searchtxt = data.searchtxt || 'Default';
	if(typeof(inc)=='string')
	var mlist = inc.split(',');
else
	var mlist = inc;
	if(!data.searchtxt)
	data.searchtxt = 'No Search';
	for(var j=0; j<clr.length; j++){
		html += '<div class="mywrap"><div class="box" style="background-color:'+clr[j]+';"></div><ul class="attrlist">';
		if(stats != null){
		try{
		html2 += '<ul title="Refresh Stats"><li class="box" style="background-color:'+clr[j]+';"></li>';	
		//	Add Stats
		stats[j].quality = typeof(stats[j].quality)=='object' ? stats[j].quality = 0 : stats[j].quality;
		html2 += '<li>Percentage Change <span class="val">'+stats[j].diff+'%</span></li><li>Max <span class="val">'+stats[j].max+'</span></li><li>Min <span class="val">'+stats[j].min+'</span></li><li>Mean <span class="val">'+stats[j].mean+'</span></li><li>Variation <span class="val">'+stats[j].sd+'</span></li></li><li>Data Quality <span class="val">'+stats[j].quality+'%</ul>';
		}
		catch(e){
			console.log('Error '+e);
			html2 += '</ul>';
		}
		}
		var src = data.searchtxt.split(',');
		for(var i=0; i<src.length; i++)
			html += '<li>'+src[i]+'</li>';
		html += '</ul><ul class="_meterlist hide">'; // search attr list complete
		switch(_case){
			case 1: 	//	REGULAR PLOT
			html += '<li>'+mlist[j]+'</li></ul></div>';
			break;
			case 2: 	//	AGGR
				for(var t=0; t<mlist.length;t++)
					html += '<li>'+mlist[t]+'</li>';
				html += '</ul></div>';
		}
	}
	html += '</div>';	
	$(el).append(html);
	$(el2).append(html2);
}
function modLegend(chartindex,clr,inc,_case,stats){
	inc = inc || null;
	if(!charts[chartindex].container){
		//alert('Error modlegend no container');
		Notify('Legend Error','Error modlegend no container','normal',true);
		addseriesobj.reset();
		return;
	}
	if(!charts[chartindex].container.offsetParent){
		//alert('Error modlegend no offsetparent');
		Notify('Legend Error','Error modlegend no offsetparent','normal',true)
		addseriesobj.reset();
		return;
	}
	var prt = charts[chartindex].container.offsetParent;
	var el = $(prt).parent().find('.sidepanel .boxlist');
	var el2 = $(prt).parent().find('.stats');
	var data = getActionParams();
	var html = ''; var html2 = '';
	data.searchtxt = data.searchtxt || 'Default';
	if(typeof(inc)=='string')
	var mlist = inc.split(',');
else
	var mlist = inc;
	if(!data.searchtxt)
	data.searchtxt = 'No Search';
	for(var j=0; j<clr.length; j++){
		html += '<div class="mywrap"><div class="box" style="background-color:'+clr[j]+';"></div><ul class="attrlist">';
		try{
		if(!$.isEmptyObject(stats)){
		html2 += '<ul title="Refresh Stats"><li class="box" style="background-color:'+clr[j]+';"></li>';
		//	Add Stats
		stats[j].quality = typeof(stats[j].quality)=='object' ? stats[j].quality = 0 : stats[j].quality;
		html2 += '<li>Percentage Change <span class="val">'+stats[j].diff+'%</span></li><li>Max <span class="val">'+stats[j].max+'</span></li><li>Min <span class="val">'+stats[j].min+'</span></li><li>Mean <span class="val">'+stats[j].mean+'</span></li><li>Variation <span class="val">'+stats[j].sd+'</span></li></li><li>Data Quality <span class="val">'+stats[j].quality+'%</ul>';
			}
		}
		catch(e){
			console.log('Error '+e);
			html2 += '</ul>';
		}
		var src = data.searchtxt.split(',');
		for(var i=0; i<src.length; i++)
			html += '<li>'+src[i]+'</li>';
		html += '</ul>';
		if(mlist){
		html += '<ul class="_meterlist hide">'; // search attr list complete
		switch(_case){
			case 1: 	//	REGULAR PLOT
			html += '<li>'+mlist[j]+'</li></ul></div>';
			break;
			case 2: 	//	AGGR
				for(var t=0; t<mlist.length;t++)
					html += '<li>'+mlist[t]+'</li>';
				html += '</ul>';
			}
		}
		html += '</div>';
	}
	$(el).append(html);
	$(el2).append(html2);
}
function checkBuildSel()
{
	console.log(BuildingSel)
	if('allmode' in BuildingSel)
	{
		if(BuildingSel['allmode']==true)
		{
			return true;
		}
		else
		{
			Notify('Selection Error','Must Select the non residential building fully for get the sales data','normal',true)
			return false;
		}
	}
	else
	{
		size=Object.size(BuildingSel);
		if(size>0)
		{
			i=0;
			$.each(BuildingSel,function(key,value){
				if(value==true)
				{
					i++;
				}
			});
			if(i==size)
			{
				return true;
			}
			else
			{
				Notify('Selection Error','Must Select the non residential building fully for get the sales data','normal',true)
				return false
			}
		}
		else
		{
			return true;
		}
		;
		
	}
	
}

function plotHQ(x){
	if(st==undefined||et==undefined)
	{
			Notify('Date Error','Please select the date range','normal',true);
			return false;
		
	}
	if(!Radiochecked && Radiochecked!='')
	{
		Notify('Warning','Selection ERR','normal',true);
		return;
	}
	if(!addseriesobj.mode){	// NEW PLOTS
switch(x){
	case 1:
		addplotobj.mode = true;
		plotme(jobpool[0],groupbuildIndexID,Radiochecked);
	break;
	/* 	ALL OTHER CASES ARE NOW DEPRECATED	*/
	case 2:
		aggregateobj.mode = true;
		plotaggr();
	break;
	case 5: // AVG
		averageobj.mode = true;
		if(Radiochecked=='rain')
		avg2plot(null,'RAINFALL');
		if(Radiochecked == 'temperature')
		avg2plot(null,'TEMP');
		if(Radiochecked == 'humidity')
		avg2plot(null,'HUMIDITY');
		if(Radiochecked == 'consumption')
			avg2plot(null,'CONSUMPTION');
		if(Radiochecked == 'reading')
			avg2plot(null,'MTR');
		if(Radiochecked == 'outflow')
			avg2plot(null,'OUTFLOW');
		if(Radiochecked == 'mnf')
			avg2plot(null,'MIN_NIGHT_FLOW');
		if(Radiochecked == 'mnc')
			avg2plot(null,'MIN_NIGHT_CONSUMPTION');
		if(Radiochecked == 'sales')
			avg2plot(null,'SPSALES');
	break;
	case 3: //RATIO
	if(Radiochecked == 'rain')
		Radiochecked = 'RAINFALL';
	if(Radiochecked == 'temperature')
		Radiochecked = 'TEMP';
	if(Radiochecked == 'humidity')
		Radiochecked = 'HUMIDITY';
	if(Radiochecked == 'consumption')
		Radiochecked = 'CONSUMPTION';
	if(Radiochecked == 'reading')
		Radiochecked = 'reading';
	if(Radiochecked == 'sales')
		Radiochecked = 'SPSALES';
	if(Radiochecked == 'outflow')
		Radiochecked = 'OUTFLOW';
	if(Radiochecked == 'mnf')
		Radiochecked = 'MIN_NIGHT_FLOW';
	if(Radiochecked == 'mnc')
		Radiochecked = 'MIN_NIGHT_CONSUMPTION';
	ratioPlot2(Radiochecked);
	break;
	default:
		//alert('Illegal Case');
		Notify('Action Error','Illegal Case','normal',true);
		joblist.splice(0,joblist.length);
		jobpool.splice(0,jobpool.length);
	break;
	}
	}
else 	// ADD 2 PLOT
	{
		switch(x){
			case 1:
					addplotobj.mode = true;
					addplotobj.chartindex = addseriesobj.chartindex;
					add2plot(1,Radiochecked);
			break;
			// ALL OTHER CASES DEPRECATED
			case 2: //AGG
				aggregateobj.mode = true;
				aggregateobj.chartindex = addseriesobj.chartindex;
				if(Radiochecked == 'reading')
					add2plot(2,'MTR');
				if(Radiochecked == 'consumption')
					add2plot(2,'CONSUMPTION');
				if(Radiochecked == 'outflow')
					add2plot(2,'OUTFLOW');
				if(Radiochecked == 'mnf')
					add2plot(2,'MIN_NIGHT_FLOW');
				if(Radiochecked == 'mnc')
					add2plot(2,'MIN_NIGHT_CONSUMPTION');
				if(Radiochecked == 'sales')
					add2plot(2,'SPSALES');
			break;
			case 5: // AVG
				averageobj.mode = true;
				averageobj.chartindex = addseriesobj.chartindex;
				if(Radiochecked == 'reading')
					add2plot(5,'MTR');
				if(Radiochecked == 'consumption')
					add2plot(5,'CONSUMPTION');
				if(Radiochecked=='rain')
					add2plot(5,'RAINFALL');
				if(Radiochecked=='temperature')
					add2plot(5,'TEMP');
				if(Radiochecked == 'outflow')
					add2plot(5,'OUTFLOW');
				if(Radiochecked == 'mnf')
					add2plot(5,'MIN_NIGHT_FLOW');
				if(Radiochecked == 'mnc')
					add2plot(5,'MIN_NIGHT_CONSUMPTION');
				if(Radiochecked == 'sales')
					add2plot(5,'SPSALES');
			break;
			case 3: // RATIO
				add2plot(8);
			break;
			}
	}
}
function findMeter(id){
	for(var i=0; i<markers.length;i++){
		if(markers[i].metaindex == id)
			return i;
			}
	}

function ratioPlot(m){
	if(st==undefined||et==undefined||st==''||et=='')
	{
		alert('Please select a time range');
		return false;
	}
	var ep;
	var included = null;
	//var myid = 'Ratio'+getRandomInt(0,100);
	//var myid = logWindow('plot',jo);
	var containerId = 'container'+myid;
	var html = '';
	var ep = 'search/data';
	var x = unitObj.selectedUnit;
	var	included = x;
		x = x.toString();
	var ctrl = controlmarkup();
	html += ctrl;
	$('#'+myid).find('.topheader').empty().append(html);
	if(findChart(containerId)){
		alert('This chart is already plotted');
		$('#'+myid).remove();
		return;
	}
	var parms=getActionParams();
	$.get('controllers/proxyman.php',{endpoint:"data/"+ep,params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search=&include="+x+"&exclude="}, //"+parms.Searchjson+"
		function(d)
			{
				console.log(d);
				$('#'+containerId+' img').remove();
				if(!d || !d.response || d.errorCode!="0")
				{
					alert(d.errorMsg);
					return;
				}
				var payload = [];
				var pairs = [];
				var solved = [];

				// find number of stacks
				for(var i=0; i<d.response.meters.length;i++){
					// is it a master?
					if(solved.indexOf(d.response.meters[i].mtrid) > -1)
						continue;
					var ismaster = d.response.meters[i].mtrid.match(/mas/);
					var thisid,otherid;
					if(ismaster){
						thisid = d.response.meters[i].mtrid.split('mas');
						otherid = thisid[0]+'mby';
					}
					else // it's a sub
					{
						thisid = d.response.meters[i].mtrid.split('mby');
						otherid = thisid[0]+'mas';
					}
					// push the pair
					pairs.push([d.response.meters[i].mtrid,otherid]);
					solved.push(otherid);
				}
				console.log(pairs);
				console.log(solved);
				// lets make series
				for(var j=0; j<pairs.length; j++)
				{
					var count = 0;
					while(count<2){
						for(var k=0; k<d.response.meters.length;k++){
							if(d.response.meters[k].mtrid == pairs[j][count])	//lets add this one
							{
								var series = [];
								for(var l=0; l<d.response.meters[k].data.length;l++){
									var temp = {};
									temp.y = d.response.meters[k].data[l].value || getRandomInt(0,50);
									temp.x = d.response.meters[k].data[l].ts;
									series.push(temp);
									delete temp;
								}
								var temp = {name:d.response.meters[k].mtrid,stack:j,data:series,turboThreshold:__tr};
								payload.push(temp);
							}
						}
					++count;
					}
				}	// make the chart now
				console.log(payload);
				var	chart = new Highcharts.Chart({
					chart: {
					renderTo: containerId,
					type:'line',
					 zoomType: 'xy'
				},
				 navigator: {
			    	enabled: true,
			    	height: 0
			    },
						plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
		            },
		        column: {
                stacking: 'normal'
            	}

		        },
					xAxis:	{	type:'datetime'},
					yAxis:	[{opposite:false,title:{text:units[m]}},{opposite:true,title:{text:''}}],
					title: {
						text:'Ratio Comparison'
					},
				//	series:	[{data:payload,turboThreshold:10000,name:x}]
					series:payload
				});
					charts.push(chart);
					chartUrls.push({"meter":null,"st":st,"et":et,"intervalmin":interval,"includepredicted":pred,type:m,names:["Ratio"],sensorType:[m],endpoint:["data/"+ep],params:["st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search=null&include="+x+"&exclude=null"],type2:null,case:1});
			},'json');
}

function ratioPlot2(m){
	if(st==undefined||et==undefined||st==''||et=='')
	{
		Notify('Warning','No Time Selection Found!','normal',true);
		return;
	}
	if(!m || m=='' || m==undefined){
		Notify('Warning','No Sensor Type Found!','normal',true);
		return;
	}
	var ep;
	var included = null;
	//var myid = 'Ratio'+getRandomInt(0,100);
	var containerId = logWindow('wide',jobpool[0]);
	var expl = containerId.split('-');
	var myid = expl[1]||'';
	var html = '';
	var ep = 'search/data';
	var x = jobpool[0];
	var	included = x;
		x = x.toString();
	//var ctrl = controlmarkup();
	html += '&nbsp&nbsp;Compound Analysis';
	$('#'+myid).find('.topheader').empty().append(html);
	if(findChart(containerId)){
		alert('This chart is already plotted');
		$('#'+myid).remove();
		return;
	}
	var parms=getActionParams();
	$.get('controllers/proxyman.php',{endpoint:"data/"+ep,params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search=&include="+x+"&exclude="+"&interval="+_interval}, //"+parms.Searchjson+"
		function(d)
			{
				console.log(d);
				$('#'+containerId+' img').remove();
				if(!d || !d.response || d.errorCode!="0")
				{
					Notify('Warning',d.errorMsg,'normal',true);
					return;
				}
				var handy_ids = [];
				var handy_dev = [];
				_.each(d.response.meters,function(m){
					var info = m.meta._source;
					handy_ids.push(m.mtrid);
					handy_dev.push(info.device);
				});
				var payload = [];
				var pairs = [];
				var solved = [];
				var customers = [];

				// check if we have compounsds
			for(var i=0; i<d.response.meters.length;i++){
				if(solved.indexOf(d.response.meters[i].mtrid) > -1)
					continue;
				var meta = d.response.meters[i].meta._source;
				if(meta.hasOwnProperty('component_id_new')){	// we got one
					var explode;
					explode = meta.component_id_new.split('_');
				//	if(meta.tag_category == 'wems,nonresidential'){	// WEMS BULK
						// var idx0 = handy_dev.indexOf(explode[0]);
						// var idx1 = handy_dev.indexOf(explode[1]);
						// explode[0] = handy_ids[idx0];
						// explode[1] = handy_ids[idx1];
				//	}
					solved.push(explode[0]);
					solved.push(explode[1]);
					var cname = meta.bp_name || meta.customer_name || meta.customername || meta.display_customer_name || d.response.meters[i].mtrid;
					if(customers.indexOf(cname)<0){
						customers.push(cname);
						pairs.push([explode]);
					}
					else
					{
						var idx = customers.indexOf(cname);
						//var newpairs = pairs[idx].concat(explode);
						pairs[idx].push(explode);
					}
				}
			}
				console.log(pairs);
				console.log(solved);
				// lets make series
				if(pairs.length>0){
				for(var j=0; j<pairs.length; j++)// total customers
				{
					for(var count=0; count<pairs[j].length; count++){	// total bulks
						for(var k=0; k<d.response.meters.length;k++){	
							if(d.response.meters[k].mtrid == pairs[j][count][0])	//lets add this one
							{
								// find other meter of the compound
								var mtr2=null;
								for(var ee=0;ee<d.response.meters.length;ee++)
								{
									if(d.response.meters[ee].mtrid == pairs[j][count][1])
										mtr2 = d.response.meters[ee];
								}
								if(!mtr2)
									continue;

								var series = [];
								var ser2 = [];
								var cummulative = 0;
								var cummulative2 = 0;
								var aggrobj = [];
								for(var l=0; l<d.response.meters[k].data.length;l++){
									var tempobj = {};
								//	temp.y = d.response.meters[k].data[l].value || getRandomInt(0,50);
								//if(mtr2 && mtr2.data.length<l)
									//temp.y = (d.response.meters[k].data[l].value || 0) + (mtr2.data[l].value||0);
								//			else
									tempobj.y = d.response.meters[k].data[l].value;
									tempobj.x = d.response.meters[k].data[l].ts;
									cummulative = cummulative + (tempobj.y||0);
									aggrobj.push({y:tempobj.y,x:tempobj.x});
									series.push(tempobj);
									//delete temp;
								}
								var temp = {name:customers[j]+'-'+d.response.meters[k].meta._source.device,stack:j,data:series,turboThreshold:__tr};
								if(mtr2 && mtr2.data.length<=l){
									for(var f=0;f<mtr2.data.length;f++){
										var tempobj = {};
										tempobj.y = mtr2.data[f].value||0;
										tempobj.x = mtr2.data[f].ts;
										aggrobj[f].y = aggrobj[f].y + tempobj.y;
										cummulative2 = cummulative2 + tempobj.y;
										ser2.push(tempobj);
									}
									var temp2 = {name:customers[j]+'-'+mtr2.meta._source.device,stack:j,data:ser2};
									//payload.push(temp2);
								}
								//payload.push(temp);
								payload.push({name:customers[j]+' ['+d.response.meters[k].meta._source.device+':'+mtr2.meta._source.device+']',stack:j,data:aggrobj})
							// PIE CHARTS DISABLED
//								var total = cummulative2+cummulative;
//								var per1 = (cummulative/total)*100;
//								var per2 = (cummulative2/total)*100;
//								insertPie(myid,customers[j],[[d.response.meters[k].meta._source.device,per1],[mtr2.meta._source.device,per2]]);
							// Bulk Analysis Bar Chart
							insertBar(myid,customers[j],[[d.response.meters[k].meta._source.device,series],[mtr2.meta._source.device,ser2]]);
							}
						}
					}
				}
				}	// end pairs
			//	var stacknumber = pairs.length+1;

			//NON PAIRED
				// for(var e=0;e<d.response.meters.length;e++){
				// 	var stacknumber = pairs.length+1+e;
				// 	if(solved.indexOf(d.response.meters[e].mtrid) > -1)
				// 		continue;
				// 	var series = [];
				// 	for(var p=0;p<d.response.meters[e].data.length;p++){
				// 		var temp = {};
				// 		temp.y = d.response.meters[e].data[p].value;
				// 		temp.x = d.response.meters[e].data[p].ts;
				// 		series.push(temp);
				// 		//delete temp;
				// 	}
				// 	var temp = {name:d.response.meters[e].meta._source.device,stack:stacknumber,data:series,turboThreshold:__tr};
				// 	payload.push(temp);
				// }
			//NON PAIRED ENDS--
				// make the chart now
				var	chart = new Highcharts.Chart({
					chart: {
					renderTo: containerId,
					type:'column',
					 zoomType: 'xy'
				},
				exporting:{enabled:false},
				navigator: {
			    	enabled: false,
			    	height: 15,
			    	xAxis:{
					labels: {
						enabled: false
					}
			    	}},
			    rangeSelector: {
		    	allButtonsEnabled: false,
				inputEnabled: true,
		    	selected: 1,
		    	enabled:false
		    	},
						plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
		            },
		        column: {
                stacking: 'percent'
            	}
		        },
		        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        		},
					xAxis:	{	type:'datetime'},
					yAxis:	[{opposite:false,title:{text:'%'}},{opposite:true,title:{text:''}}],
					title: {
						text:''
					},
				//	series:	[{data:payload,turboThreshold:10000,name:x}]
					series:payload
				});
					charts.push(chart);
					chartUrls.push({"meter":null,"st":st,"et":et,"intervalmin":interval,"includepredicted":pred,type:m,names:["Ratio"],sensorType:[m],endpoint:["data/"+ep],params:["st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search=null&include="+x+"&exclude=null"],type2:null,case:1});
			},'json');
}
// RatioPlots - PIES
function insertPie(id,name,data){
	var rand = getRandomInt(500,40000);
	$('#'+id+' .extraregion').append('<div id="pie'+rand+'" class="regionpie"></div>');
		var chart=new Highcharts.Chart({
			chart: {
			renderTo: 'pie'+rand},
			title:{text:''},
			subtitle:{text:name},
			exporting: {
         		enabled: false
			},
			//series:{data:[['mem.generic.pies',50],['oye',50]]}
			  series: [{
            type: 'pie',
            data: data
        }]
});
}
function insertBar(id,name,_data){
	var rand = getRandomInt(500,40000);
	$('#'+id+' .extraregion').append('<div id="bar'+rand+'" class="regionbar"></div>');
		var chart=new Highcharts.Chart({
			chart: {
			renderTo: 'bar'+rand,
			type: 'column'},
			title:{text:''},
			plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
		            },
		        column: {
                stacking: 'percent'
            	}
		        },
			subtitle:{text:name},
			tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        		},
			xAxis:	{	type:'datetime'},
			yAxis:	{title:{text:'%'}},
			exporting: {
         		enabled: false
			},
		    series: [{data: _data[0][1],stack:1,name:_data[0][0],color:Highcharts.getOptions().colors[3]},{data:_data[1][1],stack:1,name:_data[1][0],color:Highcharts.getOptions().colors[4]}]
});
}

function flags(x){
	//public holidays
//	if(	(et - st)	< 604800000)
/*		return new Array();
	var startrange = new Date(st);
	var endrange = new Date(et);
	var days_sun = [];
	var days_sat = [];
	var days_pub = [];
	var day = startrange.moveToDayOfWeek(0);
	while(day.between(startrange,endrange))
	{
		days_sun.push(day.getTime());
		day = day.moveToDayOfWeek(0);
	}
	startrange = new Date(st);
	endrange = new Date(et);
	var count = 0;
	var pday = new Date(hol[count]);
	while(pday.getTime()<=endrange.getTime()){
	if(pday.between(startrange,endrange)){
		days_pub.push(pday.getTime());
	}
	++count;
	pday = new Date(hol[count]);
	if(count==hol.length)
		break;
	}
	var day2 = startrange.moveToDayOfWeek(6);
	while(day2.between(startrange,endrange))
	{
		days_sat.push(day2.getTime());
		day2 = day2.moveToDayOfWeek(6);
	}
	// make series
	var data = [];
	for(var i=0; i<days_sun.length; i++)
	{
		var temp = {x:days_sun[i], title:'S',text: 'Sunday'};
		data.push(temp);
		delete temp;
	}
	for(var h=0;h<days_pub.length; h++)
	{
		var temp = {x:days_pub[h],title:'P',text:'Public Holiday'};
		data.push(temp);
		delete temp;
	}
	for(var j=0; j<days_sat.length; j++)
	{
		var temp = {x:days_sat[j], title:'S',text: 'Saturday'};
		data.push(temp);
		delete temp;
	}*/

	return x;
}

function daychecker2(chart,days){
	console.log(days);
	for(var i=0; i<chart.series.length; i++){
		// only elligible series
		if(chart.series[i].name == 'flags' || chart.series[i].name == 'Navigator')
			continue;
		for(var j=0; j<chart.series[i].data.length; j++){
		 var proc = chart.series[i].data[j].realVal || null;
			var ts = new Date(chart.series[i].data[j].x).getDay();
			if(days.indexOf(ts) < 0){
				if(proc == null){
				chart.series[i].data[j].realVal = chart.series[i].data[j].y;
				}
				chart.series[i].data[j].update(y = null,false);
			}
			else
			{	if(proc)
				chart.series[i].data[j].update(y =chart.series[i].data[j].realVal,false);
				console.log(proc);
			}
			
		}
	}
	chart.redraw();
}

function daychecker(chart,days,tgl){
	console.log(days);
	var startrange;
	var endrange;
	var days_pub = [];
	// FORCING SHOW-ONLY MODE
	tgl = true;
	if(days.indexOf('p')>=0)
	{
		startrange = new Date(st);
		endrange = new Date(et);
		var count = 0;
		var pday = new Date(hol[count]);
		while(pday.getTime()<=endrange.getTime()){
		if(pday.between(startrange,endrange)){
			days_pub.push(pday.getTime());
		}
		++count;	
		pday = new Date(hol[count]);
		if(count==hol.length)
			break;
		}
	}
	for(var i=0; i<chart.series.length; i++){
		// only elligible series
		if(chart.series[i].name == 'flags' || chart.series[i].name == 'Navigator')
			continue;
		for(var j=0; j<chart.series[i].data.length; j++){
		 var proc = chart.series[i].data[j].realVal || null;
			var ts = new Date(chart.series[i].data[j].x).getDay();
			var ts2 = chart.series[i].data[j].x;
			if(days.indexOf(ts) < 0){
				if(proc == null){
				chart.series[i].data[j].realVal = chart.series[i].data[j].y;
				}
				if(chart.series[i].data[j].graphic)
			//	chart.series[i].data[j].graphic.attr({ stroke: '#FFFFFF'});
					chart.series[i].data[j].update({borderWidth:3, borderColor: '#FFFFFF'});
				if(tgl)
				chart.series[i].data[j].update(y = null,false);
			}
			else
			{	
				if(chart.series[i].data[j].graphic){
			//	chart.series[i].data[j].graphic.attr({ stroke: '#FF0000', 'stroke-width':2});  		
				chart.series[i].data[j].update({borderWidth:3, borderColor: '#FF0000',strokeWidth:3});
			}
				if(tgl&&proc)
				chart.series[i].data[j].update(y =chart.series[i].data[j].realVal,false);
			}
			//PUBLIC HOLIDAYS
			if(days.indexOf('p')>=0)
			{
				if(days_pub.indexOf(ts2)>=0){
					if(chart.series[i].data[j].graphic)
				chart.series[i].data[j].update({borderWidth:3, borderColor: '#FF0000'});  		
				if(tgl&&proc)
				chart.series[i].data[j].update(y =chart.series[i].data[j].realVal,false);
				}
			}			
		}
	}
	chart.redraw();
}

function requestStats(chartindex,serindex,el){
	if(!chartindex)
		return;
	var max = charts[chartindex].xAxis[0].max;
    var min = charts[chartindex].xAxis[0].min;
    var weekdays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    var chartmeta = chartUrls[chartindex] || null;
    var days=[];
    $(charts[chartindex].container.offsetParent).parent().find('.dayselect ul li.dayselected')
    .each(function(a,b){ 
        if($(b).hasClass('dayselected')){
           	if(a != 7)
                days.push(weekdays[a]); 
        }
    });
    //console.log(days);
    if(days.length==0)
    	days = weekdays;
    if(!chartmeta)
    {
    	alert('Chart meta unavailable // ERR');
    	return;
    }
    var ep,url,baseurl;
    ep = chartUrls[chartindex].endpoint[serindex];
    if(chartUrls[chartindex].chartmeters[serindex]	==	null){
    //	ep = 'data/search/aggregate';
    	baseurl = chartUrls[chartindex].seriesurl[serindex];
    }
   	else{
   	//	ep = 'data/search/data';
   	  //  baseurl = chartUrls[chartindex].seriesurl[serindex]+chartUrls[chartindex].chartmeters[serindex];
   	   baseurl = chartUrls[chartindex].seriesurl[serindex];
   	}
   	var splitA = baseurl.split('st=');
   	url = splitA[0]+'st='+min.toFixed();
   	var splitB = splitA[1].split('&').slice(2).join('&');
   	url += '&et='+max.toFixed()+'&'+splitB;

$.get('controllers/proxyman.php',{endpoint:ep,params:url+'&includedays='+days.toString()},
function(d)
	{ 
		if(d.errorCode>0){
			alert(d.errorMsg);
			return;
		}
		var stats = d.response.meters[0].statistics||null;
		$(el).find('li').each(function(f,e){
			if(f == 0)
				return;
			$(e).remove();
		});
	var html2 = '';
	try{
	if(stats)
	html2 += '<li>Max <span class="val">'+stats.max.toFixed(2)+'</span></li><li>Min <span class="val">'+stats.min.toFixed(2)+'</span></li><li>Mean <span class="val">'+stats.mean.toFixed(2)+'</span></li><li>Variation <span class="val">'+stats.sd.toFixed(2)+'</span></li></li>';
	if(d.response.meters[0].quality)
	html2 += '<li>Data Quality <span class="val">'+d.response.meters[0].quality.quality.toFixed(2)+'%</li>';
}
	catch(e){
		console.log('ERR STATS '+e);
	}
	$(el).append(html2);
	},'json');
}
function annotate(d){
	$('.annBox').remove();
	annotateObj.reset();
	var idx = d.index;
	var name = d.series.name;
	var chart = d.series.chart;
	var ts = d.x;
	var cont = document.createElement('div');
	cont.className = 'annBox text-center';
	cont.setAttribute('data-id' , ts);
	var date = new Date(ts);
// 	var index=chart.series.indexOf(d.series);
// 	if(index<0)
// 		alert('Err Series Index');
// 	if(index>1)
// 	var idx = index - 2; // INCASE FLAGS IS STILL AROUND
// else var idx = index;
// 	console.log(idx);
	// find exact series index
	var index = null;
	var max_idx = chart.series.indexOf(d.series);
	_.each(chart.series,function(ser,idx){
		if(ser.name == 'Navigator' || ser.name == 'flags')
			return;
		if(idx>max_idx)
			return;
		if(index==null)
			index = 0;
		else
			++index;
	});
	var stuff = chart.getChartUrls();
	console.log(stuff);
	annotateObj.ts =ts;
	cont.innerHTML = '<select><option>'+name+'</option></select><textarea placeholder="Enter text here"></textarea><small>'+date+'</small><div class="button tiny text-center" onclick="saveAnnotation()">Submit</div><div class="button tiny text-center" onclick="clearAnn()">Cancel</div>';	
	$(chart.container.offsetParent).parent().append(cont);

}
function saveAnnotation(){
	if(!annotateObj.ts){
		alert('ERR TS');
		return;
	}
	var cs = $('.annBox option:selected').text();
	var ts = annotateObj.ts;
	var txt = $('.annBox textarea').val();
	$.get('controllers/proxyman.php',{endpoint:'annotate/save',params:'customername='+cs+'&st='+ts+'&et='+ts+'&annotation='+txt},function(d){
		if(d && d.result == 'save success')
			alert('Save Successfull');
			$('.annBox').remove();
			annotateObj.reset();

	},'json');
}
function loadAnn(url,bit){
	//return [];
	//$.get('controllers/proxyman.php',{endpoint:'annotate/load',params:url},function(d){
		var e = AjaxRequest('controllers/proxyman.php?endpoint='+encodeURIComponent('annotate/load')+'&params='+encodeURIComponent(url));
			var d = null;
			try{	
				var stripped = e[0];
				d = JSON.parse(decodeURIComponent(stripped));	}
			catch(e){
				//console.log('ERR LOAD ANN');
				Notify('Plotter','Cannot Load Annotations - Server Error','normal',true);
				return {sensors:[]};
			}			
		if(!d)
			return;
		bit = bit || null;
		if(bit)
			return d;
		var ret = [];
		for(var i=0; i<d.customers.length; i++)
			for(var j=0; j<d.customers[i].data.length;j++)
				ret.push({x:d.customers[i].data[j].ts,title:'A',text:d.customers[i].customername+'<br/> (MSG): '+d.customers[i].data[j].annotation.replace(/(?:\r\n|\r|\n)/g, '<br />')});

		return ret;
	//},'json')
}
function clearAnn(){
	$('.annBox').remove();
}
function wwMap(z){
	var zone = z.toUpperCase();
	switch(zone){
	case 'PHFC':
		return 'FCPH';
	case 'QHPZ':
		return 'QHPZ'
	case 'MURNANE':
		return 'MNSR';
	case 'BKALANG':
		return 'BKSR';
	case 'ISLAND':
		return 'ISR';
	case 'NANYANG':
		return 'NYSR';
	default:
		return z;
	}
}
function isZone(z){
	var zone = z.toUpperCase();
	switch(zone){
	case 'PHFC':
		return 'FCPH';
	case 'QHPZ':
		return 'QHPZ'
	case 'MURNANE':
		return 'MNSR';
	case 'BKALANG':
		return 'BKSR';
	case 'ISLAND':
		return 'ISR';
	case 'NANYANG':
		return 'NYSR';
	case 'FCPH':
		return 'FCPH';
	case 'MNSR':
		return 'MNSR';
	case 'BKSR':
		return 'BKSR';
	case 'ISR':
		return 'ISR';
	case 'NYSR':
		return 'NYSR';
	case 'BBSR':
		return 'BBSR';
	default:
		return null;
	}
}

/*	------	DEPRECATED	------	
---------------------------------*/
function mgdconvert(chart,chartindex,key,sensor,unit,op){
	if(!chart)
		return;
	//var key = 'M^3';
	//var stype = 'CONSUMPTION';
	var stype = sensor;
	var coef = 0.000264;
	var chartobj = chartUrls[chartindex];
	var axis = null;
	var both = false;
	if(chartobj.type==stype && chartobj.type2==stype)
		both=true;
	if(chartobj.type==stype)
		axis = 0;
	if(chartobj.type2==stype)
		axis = 1;
	if(axis==null){
		alert('Err: Axis not legible');
		return;
	}
	_.each(chart.series,function(el,id){
		if(el.name=='Navigator' || el.name == 'flags')
			return;
		if(el.yAxis.options.title.text == key){
			//	conver to MGD
			_.each(el.data,function(d){
				if(op)
					d.update(parseFloat(d.y*coef),false);
				else
					d.update(parseFloat(d.y/coef),false);
			});
		}
	});
	if(!both)
	chart.yAxis[axis].update({
        title:{               
  			text: unit	}
  		});
	else{
		chart.yAxis[0].update({
        title:{               
  			text: unit	}
  		});
  		chart.yAxis[1].update({
        title:{               
  			text: unit	}
  		});
	}
	if(axis)
		chartUrls[chartindex].type2 = stype;
	else
		chartUrls[chartindex].type = stype;
	chart.redraw();
	checkReg(chart);
}
function shiftAxis(chartindex,seriesid,op){
	var chart = charts[chartindex];
	var oldAxis = [null,null];
	var serCount=0;
	_.each(chart.series,function(s){
		if(s.name == 'Navigator' || s.name == 'flags')
			return;
		++serCount;
		var axisFlag = s.yAxis.options.index;
		var axisTitle = s.yAxis.options.title.text;
		if(!oldAxis[axisFlag])
			oldAxis[axisFlag]=axisTitle;
	});
	chart.series[seriesid].update({yAxis:op});
	if(oldAxis[1]==null)
		chart.yAxis[op].update({	title:{	text:oldAxis[0]	}});
	if(!chartUrls[chartindex].type2)
		chartUrls[chartindex].type2 = chartUrls[chartindex].type;
}

function autoplot(obj,cb){
	var history = obj.charthistory;
	if(!history||history.length==0)
		return;
	var i = 0;
	var extract = extractParams(history[i][1])
	persist.interval = extract.interval;
	persist.intervalmin = extract.intervalmin;
	var ep = history[i][0];
	var splits = ep.split('/');
	splits.splice(0,1);
	plotme(null,undefined,obj.sensorType[0],history[0][1],cb,splits.toString().replace(',','/'));
	// 		switch(history[i][0]){
	// 			case 'data/search/data':
	// 				plotme(null,undefined,obj.sensorType[0],history[0][1],cb,'search/data');
	// 			break;
	// 			case 'data/search/aggregate':
	// 				//plotaggr(history[0][1],obj.sensorType[0],cb);
	// 				plotme(null,undefined,obj.sensorType[0],history[0][1],cb,'search/aggregate');
	// 			break;
	// 			case 'data/search/average':
	// 				//plotaggr(history[0][1],obj.sensorType[0],cb);
	// 				plotme(null,undefined,obj.sensorType[0],history[0][1],cb,'search/average');
	// 			break;
	// 			default:
	// 				alert('Operation not supported yet');
	// 				return;
	// 			break;
	// }
}

function autoplot2(obj,step,cb){
	if(step==0)
		return;
	var i=step;
	var history = obj.charthistory;
	if(!history||history.length==0)
		return;
	var ep = history[i][0];
	var splits = ep.split('/');
	splits.splice(0,1);
						var chart_num = chartUrls.length-1;
						addplotobj.reset();
						addplotobj.mode = true;
						addplotobj.chartindex = chart_num;
						// find correct sensortype
						var ex = extractParams(history[i][1]);
						var stype = ex.sensortype;
						// go for it.
						add2plot(1,stype,history[i][1],cb,splits.toString().replace(',','/'));
	// switch(history[i][0]){
	// 				case 'data/search/data':
	// 					console.log('adding plot');
	// 					var chart_num = chartUrls.length-1;
	// 					addplotobj.reset();
	// 					addplotobj.mode = true;
	// 					addplotobj.chartindex = chart_num;
	// 					// find correct sensortype
	// 					var ex = extractParams(history[i][1]);
	// 					var stype = ex.sensortype;
	// 					// go for it.
	// 					add2plot(1,stype,history[i][1],cb,'search/data');
	// 				break;
	// 				case 'data/search/aggregate':
	// 					console.log('adding aggr');
	// 					var chart_num = chartUrls.length-1;
	// 					aggregateobj.reset();
	// 					aggregateobj.mode = true;
	// 					aggregateobj.chartindex = chart_num;
	// 					var ex = extractParams(history[i][1]);
	// 					var stype = ex.sensortype;
	// 					add2plot(1,stype,history[i][1],cb,'search/aggregate');
	// 				break;
	// 				case 'data/search/average':
	// 					console.log('adding avg');
	// 					var chart_num = chartUrls.length-1;
	// 					aggregateobj.reset();
	// 					aggregateobj.mode = true;
	// 					aggregateobj.chartindex = chart_num;
	// 					var ex = extractParams(history[i][1]);
	// 					var stype = ex.sensortype;
	// 					add2plot(1,stype,history[i][1],cb,'search/average');
	// 				break;
	// 				default:
	// 					alert('Operation not supported yet');
	// 					return;
	// 				break;
	// }
}
function duplicate(obj,s){
	console.log(s);
	var steps;
	if(s>=obj.charthistory.length)
		return;
	if(s>0){
		steps = s;
		autoplot2(obj,steps,function(){
			console.log('callback running 2 step = '+s);
			if(steps<obj.charthistory.length)
				duplicate(obj,steps+1);
		});
	}
	else{
		steps = 0;
			autoplot(obj,function(){
			console.log('callback running');
			duplicate(obj,1);
		});
	}
}

function extractChart(obj){
	if(obj.seriesurl.length==1)
		return;
	_.each(obj.seriesurl,function(x){
		var url = extractParams(x);
		plotme(null,undefined,url.sensortype,x,function(){
			return false;
			var ran = 10;
			var ran2 = 10;
			var pltrs = $('.plotter');
			_.each(pltrs,function(el){
				var offset = $(el).offset();
				$(el).css('top',ran+offset.top+'px');
				$(el).css('left',ran2+offset.left+'px');
			});		
		});
	});	
}

function extractParams(obj){
	var ret = {};
	var explode = obj.split('&');
	_.each(explode,function(ex){
		var parts = ex.split('=')
		ret[parts[0]] = parts[1]||'';
	});
	return ret;
}
function retractParams(obj){
	var ret = '';
	var count = 0;
	for(var key in obj){
		if(count>0)
			ret = ret + '&';
		if(obj[key] === 'undefined')
			obj[key] = '';
		ret = ret + key+'='+obj[key];
		++count;
	}
	return ret;
}
function dateEvent(x){
	$('#'+x+' .rng_btn').dateRangePicker({
	  startOfWeek: 'monday',
	  format:'DD/MM/YYYY HH:mm',
	  separator:' - ',
	  time: {
	   		enabled: true
	   	},
	  shortcuts : 
	   	{
	   			'prev-days': [3,5,7],
	   			'prev': ['week','month','year'],
	   			'next-days':null,
	   			'next':null
	   	},
	  getValue: function()
	  {
	   return this.innerHTML;
	  },
	  setValue: function(s)
	  {
	   $(this).find('._timerange').empty().append(' '+s);
	  },
	 /* startDate: rangeobj.st||st,
      endDate: rangeobj.et||et,*/
	}).on('datepicker-change',function(event,obj)
	   { 
	   		var procdate=[];
	   		var tempDates=obj.value.split(' - ');
	   		$.each(tempDates, function(key, value){
	   		 		dateSplit=value.split(' ');
	   		 		var	ts	=	dateSplit[0].split('/');
	   		 		var	ts2	=	dateSplit[1].split(':');
	   		 		procdate.push(new Date(ts[2],ts[1]-1,ts[0],ts2[0],ts2[1],0).getTime()-(((_tzo.offset*-1)+_tzo.browser)*60*1000));	
	   		 })
	   		 rangeobj.st=procdate[0];
	   		 rangeobj.et=procdate[1];
	   		 rangeobj.mode=true;
	   }).on('datepicker-apply',function(obj)
		{
			//rangeobj.mode=true;
		}).on('datepicker-close',function(obj)
	   {
	   		if(!rangeobj.mode)
	   			return;
	   		if(rangeobj.st==null||rangeobj.et==null){
	   			rangeobj.reset();
	   			return;
	   		}
	   		var el = null;
	   		var type = null;
	   		/*if($(obj.target.offsetParent).hasClass('plotter'))
	   			el = $(obj.target).parents('.plotter');
	   		else
	   		{
	   			el = $(obj.target).parents('.stackplotwrap');
	   			type = $(obj.target.offsetParent).data('id');
	   		}*/
	   		if($(obj.target).parents('.plotter').length>0){
	   			el = $(obj.target).parents('.plotter:eq(0)');
	   		}
	   		else
	   		{
	   			el = $(obj.target).parents('.stackplotwrap:eq(0)');
	   			type = $(obj.target).parents('.stackedPlotter:eq(0)').data('id');
	   		}
	   		var chartid = $(obj.target).parents('.plotter').children('.plotcontainer').attr('id') || $(obj.target).parents('.stackplotwrap').children('.plotcontainer').attr('id');
	   		//var chartid = $(obj.target.offsetParent).children('.plotcontainer').attr('id');
            var chartindex = findChart(chartid);
            if(chartindex==null)
            {
            	rangeobj.reset();
            	alert('Err - Chart not found');
            	return;
            }
            _.each(chartUrls[chartindex].charthistory,function(el,ed){
            	var stuff = extractParams(el[1]);
            	if(stuff.st==rangeobj.st && stuff.et==rangeobj.et)
            		rangeobj.mode=false;
            	stuff.st = rangeobj.st;
            	stuff.et = rangeobj.et;
            	chartUrls[chartindex].charthistory[ed][1] = retractParams(stuff);
            });
            if(!rangeobj.mode){
            	rangeobj.reset();
	   			return;
            }
            persist.st=rangeobj.st;
            persist.et=rangeobj.et;
            rangeobj.reset();
            //
            if(type != null)	// Request is from a stack
			{
				stackobj.mode = true;
				stackobj.stack = type;
				stackobj.idx = $(charts[chartindex].container).parents('.stackplotwrap').index();
				charts[chartindex].destroy();
				$('#stckd'+type+' .stackplotwrap:eq('+stackobj.idx+')').remove();
			}
			else{
				charts[chartindex].destroy();
				$(el).find('.closeme').trigger('click');
			}
		   	duplicate(chartUrls[chartindex],0);
		   	
	   });
	var newst = new Date((persist.st||st)+(((_tzo.offset*-1)+_tzo.browser)*60*1000));
	var newet = new Date((persist.et||et)+(((_tzo.offset*-1)+_tzo.browser)*60*1000));
	$('#'+x+' .rng_btn').data('dateRangePicker').setDateRange(newst,newet);
}
function changeRes(chartindex,x,type){
	type = type || null;
	if(!chartUrls[chartindex])
		return;
	var calloff=false;
	var interval=null
	var _interval=null;
	if(isNaN(parseInt(x)))
	{
        interval = 1440;
       _interval = x;
    }
    else{
	   	interval = parseFloat(x);
    	_interval = "";
    }
    var _st,_et;
	_.each(chartUrls[chartindex].charthistory,function(el,ed){
            	var stuff = extractParams(el[1]);
            	if(stuff.intervalmin==interval && stuff.interval==_interval)
            		calloff=true;
            	stuff.intervalmin = interval;
            	stuff.interval = _interval;
            	chartUrls[chartindex].charthistory[ed][1] = retractParams(stuff);
            });
	if(calloff){
		alert('No change');
		return;
	}
	persist.intervalmin = interval;
	persist.interval = _interval;
	if(type != null)	// Request is from a stack
	{
		stackobj.mode = true;
		stackobj.stack = type;
		stackobj.idx = $(charts[chartindex].container).parents('.stackplotwrap').index();
		charts[chartindex].destroy();
		$('#stckd'+type+' .stackplotwrap:eq('+stackobj.idx+')').remove();
	}
	duplicate(chartUrls[chartindex],0);
}
function detectBad(res){
	var ret = [];
	if(!res)
		return ret;
	for(var i=0;i<res.data.length;i++){
		var txt = 'No reason available.';
		if(res.data[i].hasOwnProperty('badReason'))
			txt = res.data[i].badReason;
		if(res.data[i].isBad)
			ret.push({x:res.data[i].ts,title:'B',text:res.meta._source.name||res.meta._source.display_name+'<br/>'+txt.replace(/(?:\r\n|\r|\n)/g, '<br />')});		
	}
	return ret;
}
function plotcomplete(type){
	persist.reset();
	joblist.splice(0,1);
	jobpool.splice(0,1);
	batch.splice(0,1);
	if(joblist.length>0)
	{
		_decouple = batch[0];
		Radiochecked = joblist[0];
		//unitObj.selectedUnit = jobpool[0];
		if(addseriesobj.mode)
			addseriesobj.reset();
		if(!_decouple){
			addseriesobj.mode=true;
			addseriesobj.chartindex = charts.length-1;	//Latest Chart created
		}
		if(_stacked && stackobj.mode && !stackobj.stack && _decouple)
			stackobj.stack = stackedPlots[stackedPlots.length-1];	// Stack and Decouple on new
		plotHQ(type);
	}
	else
	{
		if(stackobj.mode)
			stackobj.reset();
		//autoArrange();
		persist.reset();
	}
}
function setupPlayer(x){
	//	non realtime
	if(!chartUrls[x] || !charts[x])
		return;
	// check for existing status
	for(var i=0; i<chartplayer.length;i++)
		if(chartplayer[i].chartindex == x)
		{
			if(chartplayer[i].urls.length>0){
				var ex = extractParams(chartplayer[i].urls[0][1]);
				var diff = (ex.et - ex.st)/2;
				ex.st = parseInt(ex.et);
				ex.et = parseInt(ex.et) + diff;
				var newurl = retractParams(ex);
				var temp = [];
			}
		}
	var series = chartUrls[x].charthistory;
	var today = new Date().getTime();
	var ret = false;
	var obj = {chartindex:x,urls:[]}
	today = today + (6*3600*1000);	// 6 hours buffer
	for(var i=0; i<series.length; i++)
	{
		var url = extractParams(series[i][1]);
		if(url.et > today){
			ret = true;
		}
		else{
		var timespan = (url.et - url.st)/2;
		url.st = parseInt(url.et);
		url.et = parseInt(url.et) + timespan;
		var newurl = retractParams(url);
		var temp = [series[i][0],newurl];
		obj.urls.push(temp);
		}
	}
	if(ret)
	{
		alert('This chart has reached last data');
		return;
	}
	chartplayer.push(obj);
}
function playChart(x){
	for(var i=0; i<chartplayer.length;i++)
		if(chartplayer[i].chartindex == x && chartplayer[i].urls.length>0)
		{
			_.each(chartplayer[i].urls,function(u,ud){
				var ep = u[0];
				var params = u[1];
				$.get('controllers/proxyman.php',{endpoint:ep,params:params},
					function(d){
						console.log(d);
						if(d.errorCode>0){
							return;
						}
						var mtrobj = null;
						var dataobj = null;
						var seriesindex = null;
						for(var k=0;k<1;k++){
							mtrobj = d.response.meters[k].mtrid;
							dataobj = d.response.meters[k].data;
						}
						for(var l=0;l<charts[x].series.length;l++)
						{
							if(charts[x].series[l].name == 'flags' || charts[x].series[l].name == 'Navigator')
								continue;
							var name = charts[x].series[l].name.split('-');
							if(name[1]==mtrobj){
								seriesindex = l;
								break;
							}
						}
						if(!seriesindex || !mtrobj || !dataobj)
						{
							alert('Err - Null Pointer');
							return;
						}
						var count = dataobj.length;
						var time = 0;
						if(count == 0)
							return;
						//while(count > 0){
							
							time = time + 1000;
							var intervalID = setInterval(function(){
								console.log('doing it');
								count = count - 1;
								charts[x].series[seriesindex].addPoint([dataobj[0].ts,dataobj[0].value],true,true);
								dataobj.splice(0,1);
								if(count == 0){	// EOD
									console.log('End of Data - Restarting..');
									console.log(x);
									clearInterval(intervalID);
								}
							},1000);
						u.push(intervalID);
					},'json');
			});
			break;
		}
}

function unitsMatch(orig,othr){
	if(!orig || !othr)
		return false;
	for(var i=0; i<unitsList.length; i++){
		if(unitsList[i].indexOf(orig)>=0 && unitsList[i].indexOf(othr)>=0)
			return true;
	}
	return false;
}
function convertUnits(units_old,units_new,data){
	if(!unitsMatch(units_old,units_new))
		return data;
	var request = units_old.toLowerCase()+' to '+units_new.toLowerCase();
	_.each(data,function(d,ind){
		var val = converter(request,d.value);
		d.value = val;
	});
	return data;
}
function unitsUI(x,chartindex){
	var html = '<div class="units_div">';
	var axis = ['',''];
	var disable = "";
	_.each(charts[chartindex].yAxis,function(y){
		axis[y.options.index] = y.options.title.text;
	});
	if(!axis[1])
		disable = "disabled";
	// get units list
	var list = [[],[]];
	for(var k=0;k<axis.length;k++){
		for(var i=0;i<unitsList.length;i++)
		{
			if(unitsList[i].indexOf(axis[k])>=0)
			{
				list[k] = unitsList[i];
			}
		}
	}	
	html += '<select id="selection0" class="left"><option val="'+axis[0]+'">'+axis[0]+'</option>';
	_.each(list[0],function(l){
		if(l == axis[0])
			return;
		html += '<option val="'+l+'">'+l+'</option>';
	});
	html += '</select>';
	html += '<select id="selection1" class="right" '+disable+'><option val="'+axis[1]+'">'+axis[1]||'None'+'</option>';
	_.each(list[1],function(l){
		if(l == axis[1])
			return;
		html += '<option val="'+l+'">'+l+'</option>';
	});
	html += '</select>';
	html += '</div>';
	$(x).append(html);
	$('.units_div').one('mouseleave',function(){
      $(this).remove();
    });
    $('.units_div select').change(function(){
    	var axis = parseInt(_.compact($(this).attr('id').split('selection')).toString());
    	console.log(axis);
    	var unit_new = $(this).val();
    	console.log(unit_new);
    	// old unit
    	var el = null;
    	var chartid = null;
    	if($(this).parents('.plotter').length>0)
    		chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
    	else
    		chartid = $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
        var  chartindex = findChart(chartid);
        var unit_old = null;
        _.each(charts[chartindex].yAxis,function(y){
        	if(y.options.index == axis)
        		unit_old = y.options.title.text;
        });
        if(!unit_old)
        	throw new Error('Chart units not found');
        console.log(unit_old);
        // convert lah
        var request = unit_old.toLowerCase()+' to '+unit_new.toLowerCase();
        _.each(charts[chartindex].series,function(ser,ser_ind){
        	if(ser.name == 'flags' || ser.name == 'Navigator')
        		return;
        	if(ser.yAxis.options.index != axis)
        		return;
        	if(ser.yAxis.options.title.text != unit_old)
        		return;
        	_.each(ser.data,function(d){
        		d.update(converter(request,d.y),false);
        	});        	
        });
  		charts[chartindex].yAxis[axis].update({
  			title:{
  				text:unit_new
  			}
  		});
  		charts[chartindex].redraw();
  		chartUrls[chartindex].units[axis]=unit_new;
    });
}
 	/* 		   ............................................................
               :  			ADVANCED PLOTTER ANALYTICS SECTION 			  :
               :..........................................................:
    */

function compute(chartindex){
	var op = $('.modalcontent .oplist input[type=radio][name=opsel]:checked').val() || null;
	if(!charts[chartindex] || !op){
		$('.modalcontent .ajax').addClass('hide');
		$('#appModal').foundation('reveal','close');
		return;
	}
	try{
		switch(op){
			case 'diff':
				$('.modalcontent .ajax').addClass('hide');
				$('#appModal').foundation('reveal','close');
				applyReg(charts[chartindex],chartindex);
			break;
			case 'regression':
				var series_index = parseInt($('#serselection').val()); 
	   			var series_name = $('#serselection option:selected').text() || 'unknown series';
	   			computeRegression(chartindex,series_index,series_name);
	   			$('.modalcontent .ajax').addClass('hide');
				$('#appModal').foundation('reveal','close');
			break;
			case 'overlay':
				overlayRoutine(chartindex);
			break;
			case 'delta':
				arithmatic(chartindex,1,{op:op});
			break;
			case 'min':
				arithmatic(chartindex,0,{op:op});
			break;
			case 'max':
				arithmatic(chartindex,0,{op:op});
			break;
			case 'avg':
				arithmatic(chartindex,0,{op:op});
			break;
			case 'aggr':
				arithmatic(chartindex,0,{op:op});
			break;
			case 'derived':
			$('#appModal').foundation('reveal','close');

				var display = $('#derivedselection option:selected').text();
				var bkp = chartUrls[chartindex].derivations[display].bk;
				var inc = chartUrls[chartindex].derivations[display]._id;
				var newchart = parseInt($('#containeroptions input[name="containeroptions"]:checked').val());
				//derivedCharts(chartindex,display,bkp);
				//makeChart({isStacked:isStacked,stackel:stackel,name:op.toUpperCase()+' for '+thisunit,xtype:'datetime',m:thisunit,series:series_arr,charttype:'column'});
				var prm = extractParams(chartUrls[chartindex].seriesurl[0]);
				$.get('controllers/proxyman.php',{endpoint:'data/search/data',params:"st="+prm.st+"&et="+prm.et+"&intervalmin="+prm.intervalmin+"&includepredicted="+prm.includepredicted+"&sensortype="+bkp+"&search=&include="+inc+"&exclude=&interval="+prm.interval+"&turnoffvee="+veecheck+'&includeflag='+flagsbit},
					function(d){
						if(d.errorCode>0){
							alert(d.errorMsg);
							return;
						}
						var series_arr = [];
						_.each(d.response.meters,function(stuff){
							var this_series = [];
							_.each(stuff.data,function(dd){
								var temp = {};
								temp.x = dd.ts;
								temp.y = dd.value || null;
								this_series.push(temp);
							});
							series_arr.push(this_series);
						});
					var el = $(charts[chartindex].container).parents('.plotter');
					if(el.length == 0)
						el = $(charts[chartindex].container).parents('.stackplotwrap');
					if(newchart)
						makeChart({isStacked:true,stackel:el,name:display,xtype:'datetime',m:units[bkp],series:series_arr[0],charttype:'line'});	
					else{
						var axis,updateAxis;
						//find logical axis
						if(units[chartUrls[chartindex].type] != units[bkp])
							axis = 1;
						else
							axis = 0;
						// check if chart's axis are suitable
						if(chartUrls[chartindex].type2 != null){
							if(units[bkp] != units[chartUrls[chartindex].type] && units[bkp] != units[chartUrls[chartindex].type2]){
								alert('Axis Full - Cannot Add to this chart');
								return;
							}
							updateAxis = false;
						}
						else
							updateAxis = true;
						// add to the chart
						charts[chartindex].addSeries({
							name:display,
							data:series_arr[0],
							yAxis:axis
						});
						if(updateAxis)
							charts[chartindex].yAxis[axis].update({
		                title:{               
        					text: units[bkp]	}
    					});
					}
					},'json');
			break;
			default:
				$('.modalcontent .ajax').addClass('hide');
				$('#appModal').foundation('reveal','close');
				alert('Comming Soon');
			break;
		}
	}
	catch(e){
		$('.modalcontent .ajax').addClass('hide');
		$('#appModal').foundation('reveal','close');
		throw e;
	}
}

//	arithmatic operations on charts
function arithmatic(chartindex,mode,params){
   if(!charts[chartindex] || !params || isNaN(mode))
	   {
	   		throw('Err - Arithmatic');
	   		return;
	   }
	   var op = params.op;
	   var thischart = charts[chartindex];
	   var series_arr = [];
	   var isStacked = false;
	   var stackel = null;
	   var newchart = parseInt($('#containeroptions input[name="containeroptions"]:checked').val());
	   if($(thischart.container.offsetParent).parent().hasClass('stackplotwrap'))
		{
			isStacked = true;
			stackel = $(charts[chartindex].container.offsetParent).parent();
		}
	   if(mode == 0){	//	axis based operations
	   		var axis = parseInt($('#axisselection').val());
	   		if(isNaN(axis))
	   			axis = params.axis;
	   		var thisunit = '';
	   		if(isNaN(axis))
	   		{
	   			throw "Axis Not Found";
	   			return;
	   		}
	   		var series_list = []; var xdata = [];
	   		_.each(thischart.series,function(ser){
	   			if(ser.name == 'flags' || ser.name == 'Navigator')
	   				return;
	   			if(ser.yAxis.options.index == axis){
	   				series_list.push(ser);
	   				xdata.push(ser.xData);
	   				if(thisunit == '')
	   					thisunit = ser.yAxis.options.title.text;
	   			}
	   		});
	   		xdata = _.uniq(_.compact(_.flatten(xdata)));
	   		var ydata = [];
	   		_.each(xdata,function(x,idx){
	   			var temp = [];
	   			_.each(series_list,function(ser){
	   				temp.push(ser.yData[idx]);
	   			});
	   			var new_val = null;
	   			if(op == 'min')
					new_val = _.min(temp);
				else if(op == 'max')
					new_val = _.max(temp);
				else if(op == 'avg')
					new_val = temp.reduce(function(a, b){return a+b;})/temp.length;
				else if(op == 'aggr')
					new_val = temp.reduce(function(a, b){return a+b;});
	   			series_arr.push([x,new_val])
	   		});
	   	if(newchart)
	   		makeChart({isStacked:isStacked,stackel:stackel,name:op.toUpperCase()+' for '+thisunit,xtype:'datetime',m:thisunit,series:series_arr,charttype:'column'});
	   	else
	   	{
			// add to the chart
						charts[chartindex].addSeries({
							name:op.toUpperCase()+' for '+thisunit,
							data:series_arr,
							yAxis:axis
						});
	   	}
	   }
	   else if(mode == 1){	// series based operations
	   		var series_index = parseInt($('#serselection').val()); 
	   		var series_name = $('#serselection option:selected').text() || 'unknown series';
	   		var xdata = thischart.series[series_index].xData;
	   		var ydata = thischart.series[series_index].yData;
	   		var thisunit = thischart.series[series_index].yAxis.options.title.text;
	   		switch(op){
	   			case 'delta':
	   				_.each(ydata,function(y,idx){
	   					if(ydata[idx] == undefined || ydata[idx] == null)
	   						return;
	   					if(ydata[idx+1] == undefined || ydata[idx+1] == null)
	   						return;
	   					// otherwise
	   					var tempval = ydata[idx+1] - y;
	   					series_arr.push([xdata[idx],tempval]);
	   				});
	   		if(newchart)
    			makeChart({isStacked:isStacked,stackel:stackel,name:'Delta for '+series_name,xtype:'datetime',m:thisunit,series:series_arr,charttype:'line'});
	   		else
	   			{
						charts[chartindex].addSeries({
							name:'Delta for '+series_name,
							data:series_arr,
							yAxis:series_index
						});
	   			}
	   			break;
	   		}
	   }
	$('.modalcontent .ajax').addClass('hide');
	$('#appModal').foundation('reveal','close');
	
}

/* New Overlay mechanism */
function overlayRoutine(chartindex){
	// preliminary data
	var isStacked = false;
	var stackel = null;
	if($(charts[chartindex].container.offsetParent).parent().hasClass('stackplotwrap'))
	{
		isStacked = true;
		stackel = $(charts[chartindex].container.offsetParent).parent();
	}
	var chartobj = chartUrls[chartindex];
	var series_index = parseInt($('#serselection').val());
	var series_name = $('#serselection option:selected').text() || 'unknown series';
	var range = $('#ff').val() || null;
	var rangeinmillis = parseInt(range)*60*1000;
	var m = chartobj.sensorType[0];
	var xdata = charts[chartindex].series[series_index].xData;
	var ydata = charts[chartindex].series[series_index].yData;
	// bucketing
	var breakpoint = 0;
	var startpoint = 0;
	var series_arr = [];
	var bucket_start = xdata[0];
	var bucket_end = 0;
	var count = 0;
	// total iterations
	var itr = Math.floor((xdata[xdata.length-1] - xdata[0])/rangeinmillis);
	while(count<itr){
		bucket_end = bucket_start+rangeinmillis;
		breakpoint = xdata.indexOf(closest(bucket_end,xdata));
		// get series name intellignetly
		var range_name = rangeName(bucket_start,bucket_end);
		if(breakpoint>=0){
			// fill up data
			var temp = [];
			var _cnt = 1;
			for(var x=startpoint;x<breakpoint;x++){
				temp.push([_cnt,ydata[x]]);
				++_cnt;
			}
			series_arr.push({data:temp,name:range_name});
			bucket_start = bucket_end;
			startpoint = breakpoint;
		}
		++count;
	}
	// make new chart
	var uniqid = getRandomInt(0,500);
	var tits = '';
	if(isStacked)
	{
		$(stackel[0]).append('<div class="row depcont'+uniqid+' depcont" id="'+uniqid+'"><div style="height:260px" id="container'+uniqid+'"></div></div>');
		tits = 'Overlay Analysis for '+series_name;
	}
	else
		logWindow('dependant',uniqid);
	if(!isStacked){
		$('#'+uniqid+' .deptitle').empty().append('Overlay Analysis');
		//$('#'+uniqid).append(controlmarkup(2));
	}
	var customId = 'container'+uniqid;
				var	chart = new Highcharts.Chart({
					chart: {
					 renderTo: customId,
					 type:'line',
					 zoomType: 'xy',
					 animation:false
				},
			plotOptions: {
           	series: {
            marker: {
                enabled: false
               		},
                cursor:'pointer'
                }	          
		    },
		    legend: {
            enabled: false
        	},
        	exporting:{
        		buttons:{
        		contextButton:{enabled:false},
        		delbtn:{
							symbol:'cross',
							symbolSize:10,
							onclick:function(){
								var thisid = $(this.container).attr('id');
								var chartindex = null;
								_.each(depcharts,function(d,idx){
									if(d.hasOwnProperty('container')){
										if($(d.container).attr('id') == thisid)
											chartindex = idx;
									}
								});
								if(!depchartUrls[chartindex]){
									throw "Dependant Chart not found";
									return;
								}
								var el = $(this.container).parents('.depcont');
								if(el.length == 0){
									el = $(this.container).parents('.plotter');
									$(el).find('.closeme').trigger('click');
								}
								depcharts[chartindex].destroy();
								$(el).remove();
							}
						},
						colBtn:{
							symbol:'column',
							symbolSize:10,
							onclick:function(){
								_.each(this.series,function(s){
									if(s.name == 'Navigator' || s.name == 'flags')
										return;
									s.update({type:'column'});
								});
							}
						},
						lineBtn:{
							symbol:'line',
							symbolSize:10,
							onclick:function(){
								_.each(this.series,function(s){
									if(s.name == 'Navigator' || s.name == 'flags')
										return;
									s.update({type:'line'});
								});
							}
						},
        		minButton:{
        			text:'Minimum',
        			onclick:function(){
        				revalOverlay(this,'min');
        			}
        		},
        		maxButton:{
        			text:'Maximum',
        			onclick:function(){
        				revalOverlay(this,'max');
        			}
        		},
        		avgButton:{
        			text:'Average',
        			onclick:function(){
        				revalOverlay(this,'average');
        			}
        		},
        		aggrButton:{
        			text:'Aggregate',
        			onclick:function(){
        				revalOverlay(this,'aggregate');
        			}
        		}
        	}
        	},
					xAxis:	{	type:'category',minPadding:0.02,maxPadding:0.02},
					yAxis:	[{opposite:false,title:{text:units[m]}},{opposite:true,title:{text:''}}],
					title: {
						text:tits,
						align:'left',
						x:35,
						style:{
			                fontSize:12,
			                color:'#888'
			            }
					},
						series:series_arr
				});
					$('.modalcontent .ajax').addClass('hide');
					$('#appModal').foundation('reveal','close');
					depcharts.push(chart);
					depchartUrls.push({id:uniqid,parent:chartindex,connections:[],endpoints:[],charthistory:[],operation:'raw',container:customId,range:range,sensorType:m});
					var child_index = depchartUrls.length-1;
					connect(chartindex,uniqid,child_index);
}

function revalOverlay(x,op){
	if(!x)
		return;
	var thisid = $(x.container).attr('id');
	var chartindex = null;
	_.each(depcharts,function(d,idx){
		if(d.hasOwnProperty('container')){
			if($(d.container).attr('id') == thisid)
				chartindex = idx;
		}
	});
	if(!depchartUrls[chartindex] || !depcharts[chartindex] || !op)
		return;
	if(depchartUrls[chartindex].operation == op)
		return;
	var parentEl = $($(x.container)).parents('.stackplotwrap:eq(0)');
	// get data from chart
	var xAxis = [];
	var yAxis = [];
	var name = [];
	var chart = depcharts[chartindex];
	_.each(chart.series,function(ser){
		xAxis.push(ser.xData);
		yAxis.push(ser.yData);
		name.push(ser.name);
	});
	var days = [];
	var op_arr = [];
	xAxis = _.uniq(_.flatten(xAxis));
	_.each(xAxis,function(x,idx){
		var thisday = [];
		_.each(yAxis,function(y){
			thisday.push(y[idx]);
		});
		/*	operational arithmatic	*/
		if(op == 'min')
		var min_val = _.min(thisday);
		else if(op == 'max')
		var min_val = _.max(thisday);
		else if(op == 'average')
		var min_val = thisday.reduce(function(a, b){return a+b;})/thisday.length;
		else if(op == 'aggregate')
		var min_val = thisday.reduce(function(a, b){return a+b;})
	/* 					------~~~~~-------				*/
		var thisname = name[thisday[0]];
		var thiddate = new Date(name);
		op_arr.push({x:x,y:min_val});
	});
	// make new chart
	var thisname = 'Overlay '+op.toUpperCase();
	var uniqid = getRandomInt(100,10000);
	$(parentEl).append('<div class="depcont'+uniqid+' depcont"><div id="container'+uniqid+'" style="height:200px"></div></div>');
	var customId = 'container'+uniqid;
	var res = depchartUrls[chartindex].resolution;
	var m = depchartUrls[chartindex].sensorType;
	var	dpchart = new Highcharts.Chart({
		chart: {
				renderTo: customId,
				type:'column',
				zoomType: 'xy',
				animation:false
			},
			plotOptions: {
           	series: {
            marker: {
                enabled: false
               		},
                cursor:'pointer'
                }	          
		    },
		    legend: {
            enabled: false
        	},
			xAxis:	{	type:'category',minPadding:0.02,maxPadding:0.02,labels: {
                enabled:true
            }},
					yAxis:	[{opposite:false,title:{text:units[m]}},{opposite:true,title:{text:''}}],
					title: {
						text:thisname,
						align:'left',
						x:35,
						style:{
			                fontSize:12,
			                color:'#888'
			            }
					},
			exporting:{
					buttons:{
						contextButton:{enabled:false},
						delbtn:{
							symbol:'cross',
							symbolSize:10,
							onclick:function(){
								var thisid = $(this.container).attr('id');
								var chartindex = null;
								_.each(depcharts,function(d,idx){
									if(d.hasOwnProperty('container')){
										if($(d.container).attr('id') == thisid)
											chartindex = idx;
									}
								});
								if(!depchartUrls[chartindex]){
									throw "Dependant Chart not found";
									return;
								}
								var el = $(this.container).parents('.depcont');
								depcharts[chartindex].destroy();
								$(el).remove();
							}
						},
						colBtn:{
							symbol:'column',
							symbolSize:10,
							onclick:function(){
								_.each(this.series,function(s){
									if(s.name == 'Navigator' || s.name == 'flags')
										return;
									s.update({type:'column'});
								});
							}
						},
						lineBtn:{
							symbol:'line',
							symbolSize:10,
							onclick:function(){
								_.each(this.series,function(s){
									if(s.name == 'Navigator' || s.name == 'flags')
										return;
									s.update({type:'line'});
								});
							}
						}
					}
				},
				series:[{data:op_arr,name:op}]		
			});
	depcharts.push(dpchart);
	depchartUrls.push({});
	/*
		THESE DEPENDANT CHARTS ARE NOW REGISTERED FOR LATER USAGE
	*/
}

function connect(original_idx,child_id,child_index){
	if(!chartUrls[original_idx].hasOwnProperty('endpoints'))
		chartUrls[original_idx].endpoints = [];
	if(!chartUrls[original_idx].hasOwnProperty('connections'))
		chartUrls[original_idx].connections = [];
	if(!chartUrls[original_idx].hasOwnProperty('dependants'))
		chartUrls[original_idx].dependants = [child_index];
	else
		chartUrls[original_idx].dependants.push(child_index);
	var orig = null;
	var isStacked = false;
	if($(charts[original_idx].container.offsetParent).parent('.plotter').length>0)
		orig = $(charts[original_idx].container.offsetParent).parent('.plotter');
	else{
		return;
	}
	if(!orig){
		throw "Chart container not found";
		return;
	}
	var conn1 = jsPlumb.addEndpoint(orig,{anchor:'Left',paintStyle:{ fillStyle:'transparent',radius:6,outlineColor:'#888888', outlineWidth:3,lineWidth:2,strokeStyle:'#888888' }});
	chartUrls[original_idx].endpoints.push(conn1);
	var child = $('#'+child_id);
	var conn2 = jsPlumb.addEndpoint(child,{anchor:'Left',paintStyle:{ fillStyle:'transparent',radius:6,outlineColor:'#888888', outlineWidth:3,lineWidth:2,strokeStyle:'#888888'},deleteEndpointsOnDetach:true});
	var plumCon	=	jsPlumb.connect({source:conn1,	target:conn2,scope:$('body'),paintStyle :{ strokeStyle:"#888888", lineWidth:2, dashstyle: '3 3'},connector:['Flowchart',{cornerRadius:3,gap:5}]});
	depchartUrls[child_index].connections.push(plumCon);
	depchartUrls[child_index].endpoints.push(conn2);
}

function highplot(id,_unit,params){ 	// HARD CODED SHIT BELOW _
	params = params || null;
	var lbl = ['xAxis'];
	var finaldata = [];
	var datastack = [];
	var ajaxCheck = 0;
	// _.each(_unit,function(u){
	// 	var ext = u.toString().split('_');
	// 	lbl.push('Stn_'+ext[1]);
	// });
	var g = new Dygraph(document.getElementById(id),
			[],{pointClickCallback: function(e, p) {
					var el = e.target.offsetParent;
					var chartid = $(el).parent('.plotcontainer').attr('id');
					var parentel = $('#'+chartid).parents('.plotter');
					var statsel = $(parentel).find('.stats');
					var chartobj = null;
					_.each(highresCharts,function(h){
						if(h.id == chartid)
						{
							// found object
							chartobj = h;
						}
						else
							return;
					});
					if(!chartobj)
						throw "Error - Cannot find chart object";
					var thename = p.name.replace(/\ /g, '_').replace(/\(/g, '_').replace(/\)/g, '_');
					if(chartobj.hasOwnProperty('timeselections'))
					{
						if(chartobj.timeselections.hasOwnProperty(p.name))
						{
								$(statsel).find('#'+thename).empty();
								chartobj.timeselections[p.name] = p.xval;
						}
						else
							chartobj.timeselections[p.name] = p.xval;
					}
					else
					{
						chartobj.timeselections = {};
						chartobj.timeselections[p.name] = p.xval;
						$(statsel).find('.button').removeClass('hide');
					}
					if($(parentel).find('.stats').find('#'+thename).length>0)
						$(parentel).find('.stats').find('#'+thename).append('<li>'+p.name+'</li><li>'+new Date(p.xval)+'</li>');
					else
					$(parentel).find('.stats').append('<ul id="'+thename+'"><li>'+p.name+'</li><li>'+new Date(p.xval)+'</li></ul>');
              		console.log(p);
              },labels:[],
			drawXGrid:false,
			axisLineWidth:0.1,
			xAxisLabelWidth:60,
			axisLabelColor:'#999999',
			axisLabelFontSize:12
	});
	var offset = (_tzo.browser - _tzo.offset);
	console.log('Offset '+offset);
	var offset_millis = offset*60*1000;
	// ajax loop start
	_.each(_unit,function(x){
	++ajaxCheck;
	var ext = x.toString().split('_');
	var unit = ext[1];
	var meta = meterInfo(x);
	var _name = meta.display_name;
	var deployment = meta.supply_zone||null;
	if(!deployment)
		alert('Supply Zone not defined for '+_name);
	var testurl = Parentconfig.legacy_url+deployment+'/getdatahighres_support.php?st='+st+'&et='+et+'&id='+unit+'&rt=rt&un=un&prg=py';
	if(params)
		testurl = Parentconfig.legacy_url+deployment+'/getdatahighres_support.php?st='+params.st+'&et='+params.et+'&id='+unit+'&rt=rt&un=un&prg=py';
	if( (et - st) > (10*60*60*1000) )
	{
		alert('Maximum time range allowed is 10 minutes');
		return;
	}
	(function(name){
	$.get('controllers/proxyman.php',{ww:true,params:testurl},function(d){
		--ajaxCheck;
		lbl.push(name);
		if(!d)
		{
			alert('Err- High Res');
			return;
		}
		var stuff = d.split('|');
		var data = [];
		var str = '';
		_.each(stuff,function(x){
			str = x.substring(1,x.length-1);
			var el = str.split(',');
			var ts = new Date(parseInt(el[0])+offset_millis);
			if(!el[0] || !el[1]){
				console.log('whoops!');
				return;
			}
			
			if(isNaN( ts.getTime() ))
			{
				console.log('Bad TS');
				return;
			}
			data.push([ts||null,parseFloat(el[1])||null]);
		});
		datastack.push(data);
		console.log(data);
		// var thisobj = {id:id,data:data,source:g,labels:['xAxis','yAxis']}
		// highresCharts.push(thisobj);
		if(ajaxCheck == 0){
			plotcomplete(1);
			finalidx = 0;
			// get max length
			var maxlen = 0;
			var proper_id = 0;
			_.each(datastack,function(d,id){
				if(d.length > maxlen){
					maxlen = d.length;
					// proper_id = id;
					//finaldata = datastack[id];
				}
			});
			//datastack.move(proper_id,0);
			finaldata = datastack[0];
			if(datastack.length>1){
				for(var i=1;i<datastack.length;i++){
					var ds = datastack[i];
					for(var x=0;x<maxlen;x++){
						var idx = x;
						var d = ds[x]||null;
						if(!finaldata[idx]){
							// resolve unequal arrays issue
							var temp = finaldata[idx-1];
							if(d == null)
								d = ds[x-1];
							try{
							temp[0] = d[0];
							}
							catch(e){
								console.log(temp);
								console.log(d);
							}
							finaldata.push(temp);
						}
						if(!d)
						{
							ds[x] = ds[x-1]||ds[x-2]||ds[x-3];
							if(ds[x]===null||ds[x]===undefined){
								console.log(ds); console.log(x);
							}

							ds[x][1] = null;
							d = ds[x];
						}
							finaldata[idx].push(d[1]||null);
					}
				}
			}
			console.log(finaldata);
			g.updateOptions({'file':finaldata,'labels':lbl});
			if(params)
				highresCharts.push({id:id,data:finaldata,source:g,labels:lbl,includes:_unit,params:params});
			else
				highresCharts.push({id:id,data:finaldata,source:g,labels:lbl,includes:_unit,params:{st:st,et:et}});
			return;
		}
		}); //ajax
	})(_name);
	});	//each
}
function updateme(index,unit){
	var unitid = unit[0].toString().split('_');
	var testurl = Parentconfig.legacy_url+Parentconfig.deployment+'getdatahighres_support.php?st='+st+'&et='+et+'&id='+unitid[1]+'&rt=rt&un=un&prg=py';
	$.get(testurl,function(d){
		if(!d)
		{
			alert('Err- High Res');
			return;
		}
		// extrapolate
		var stuff = d.split('|');
		var mydata = [];
		_.each(stuff,function(x){
			str = x.substring(1,x.length-1);
			var el = str.split(',');
			mydata.push(parseFloat(el[1])||null);
		});
		// push to existing
		_.each(highresCharts[index].data,function(i,idx){
			i.push(mydata[idx]||null);
		});
		highresCharts[index].labels.push('Stn_'+unitid[1]);
		highresCharts[index].source.updateOptions({'file':highresCharts[index].data,'labels':highresCharts[index].labels});
	});
}

function computeRegression(chartindex,ser,name){
	var seriesarr = [];
	var axis = +charts[chartindex].series[ser].yAxis.options.index;

	_.each(charts[chartindex].series[ser].xData,function(x,idx){
		seriesarr.push([x,charts[chartindex].series[ser].yData[idx]]);
	});
	var d = fitData(seriesarr);
	charts[chartindex].addSeries({data:d.data,name:'Regression ('+name+')',yAxis:axis});
}
function advancedFunction(x){
	if(x==null)
		return;
	var chartindex = x;
	var count = 0;
	var title = [];
	var _reg = 'disabled';
	var _arth = 'disabled';
	var _reg2 = 'disabled';
	for(var xx=0; xx<charts[chartindex].series.length; xx++){
		if(charts[chartindex].series[xx].name=='Navigator' || charts[chartindex].series[xx].name=='flags')
			continue;
		else{
			++count;
			title.push(charts[chartindex].series[xx].yAxis.options.title.text || null);
		}
	}
	title = _.uniq(title);
	if(count==2 && title.length==1)		//	ONE AXIS - 2 SERIES
		_reg = '';
	if(count>1){
		_arth = '';
	}
//	if(title.length == 1)	// ONE AXIS
		_reg2 = '';

	var html = '<ul class="inline-list oplist"><li><label>Overlay<input type="radio" name="opsel" value="overlay" checked></label></li><li><label>Difference (A-B)<input type="radio" name="opsel" value="diff" '+_reg+'></label><li><label>Linear Regression<input type="radio" name="opsel" value="regression" '+_reg2+'></label></li><li><label>Delta<input type="radio" name="opsel" value="delta"></label></li><li><label>Aggregate<input type="radio" name="opsel" value="aggr" '+_arth+'></label></li><li><label>Average<input type="radio" name="opsel" value="avg" '+_arth+'></li><li><label>Min<input type="radio" name="opsel" value="min" '+_arth+'></label></li><li><label>Max<input type="radio" name="opsel" value="max" '+_arth+'></label></li><li><label>Derived Computations<input type="radio" name="opsel" value="derived"/></label></li></ul>';
	html += '<label for="serselection">Series<select id="serselection">';
	// series selection
            var series_index = null;
            for(var i=0;i<charts[chartindex].series.length;i++){
              if(charts[chartindex].series[i].name == 'Navigator' || charts[chartindex].series[i].name == 'flags')
                continue;
              else
              {
                if(series_index==null)
                  series_index = i;
                html += '<option value="'+i+'">'+charts[chartindex].series[i].name+'</option>';
              }
            }
            html += '</select></label>';
	var prm = extractParams(chartUrls[chartindex].charthistory[0][1]);
	            var xdata = charts[chartindex].series[series_index].xData;
            var totalrange = xdata[xdata.length-1] - xdata[0];
            var totalmins = ((totalrange/1000)/60);
    var interval_special = prm.interval;
						// get user to choose expansion rate.
            var intmin = chartUrls[chartindex].intervalmin;
            if(intmin === '-1')
            {
            	Notify('Warning','Cannot overlay unknown resolution, Please use RAW/VALID/ESTIMATED','normal',true);
            }
            if(!isNaN(intmin))
            	intmin = parseInt(chartUrls[chartindex].intervalmin);
            var idd_min = plotrange_val.indexOf(intmin);
            var idd_max = plotrange_val.indexOf(idd_max = closest(totalmins,plotrange_val));
            if(idd_min<0 || idd_max<0)
            {
              throw 'Range Error - Overlay Analysis';
              return;
            }
            html += '<label for="ff">Range<select id="ff">';
            for(var j=idd_min+1; j<idd_max;j++)
              html += '<option value="'+plotrange_val[j]+'">'+plotrange[j]+'</option>';
              html += '</select></label><label for="axisselection">Axis<select disabled id="axisselection">';
    // Axis selection
    	_.each(charts[chartindex].yAxis,function(tits,idx){
    		if(tits.options.title.text)
    			html += '<option value="'+tits.options.index+'">'+tits.options.title.text+'</option>';
    	});
    		html += '</select></label>';
 		//	derived computations options
    		html += '<label for="derivedselection">Derived Computations<select disabled id="derivedselection">';
    		_.each(chartUrls[chartindex].derivations,function(val,key,obj){
    			html += '<option value="">'+key+'</option>';
    		});
    		html += '</select></label><div class="left" id="containeroptions"><label class="left">New&nbsp;<input name="containeroptions" type="radio" value="1" disabled/></label><label class="left">Parent&nbsp;<input name="containeroptions" type="radio" value="0" disabled/></div> <div class="text-center greyBtn" onClick="compute('+chartindex+');">Compute</div>';
            $('#appModal .modalcontent').empty().append(html+'</ul>');
            $('#appModal').foundation('reveal','open');
            $('.modalcontent label').each(function(idx,el){
                if($(el).children('input').prop('disabled') || $(el).children('select').prop('disabled'))
                  $(el).addClass('disable');
              })
}

/*	DEPENDANT CHARTS MAKER	*/
function makeChart(params){
	var uniqid = getRandomInt(500,1000);
	var tits = params.name;
	if(params.isStacked)
		$(params.stackel[0]).append('<div class="depcont'+uniqid+' depcont"><div id="container'+uniqid+'" style="height:200px"></div></div>');
	else
		logWindow('dependant',uniqid);
	if(!params.isStacked){
		//$('#'+uniqid+' .deptitle').empty().append('Overlay Analysis');
		//$('#'+uniqid).append(controlmarkup(2));
	}
	var customId = 'container'+uniqid;
	var	dpchart = new Highcharts.Chart({
		chart: {
				renderTo: customId,
				type:params.charttype,
				zoomType: 'xy',
				animation:false
			},
			plotOptions: {
           	series: {
            marker: {
                enabled: false
            },
            states:{
            	hover:{
            		enabled:false
            	}
            },
			lineWidth:1,
	   	    animation:false,
	     	shadow:false,
		    stickyTracking:false,
            cursor:'pointer'
                }	          
		    },
		    legend: {
            enabled: false
        	},
        	exporting:{
					buttons:{
						contextButton:{enabled:false},
						delbtn:{
							symbol:'cross',
							symbolSize:10,
							onclick:function(){
								var thisid = $(this.container).attr('id');
								var chartindex = null;
								_.each(depcharts,function(d,idx){
									if(d.hasOwnProperty('container')){
										if($(d.container).attr('id') == thisid)
											chartindex = idx;
									}
								});
								if(!depchartUrls[chartindex]){
									throw "Dependant Chart not found";
									return;
								}
								var el = $(this.container).parents('.depcont');
								depcharts[chartindex].destroy();
								$(el).remove();
							}
						},
						colBtn:{
							symbol:'column',
							symbolSize:10,
							onclick:function(){
								_.each(this.series,function(s){
									if(s.name == 'Navigator' || s.name == 'flags')
										return;
									s.update({type:'column'});
								});
							}
						},
						lineBtn:{
							symbol:'line',
							symbolSize:10,
							onclick:function(){
								_.each(this.series,function(s){
									if(s.name == 'Navigator' || s.name == 'flags')
										return;
									s.update({type:'line'});
								});
							}
						}
					}
				},
				xAxis:	{	type:params.xtype,minPadding:0.02,maxPadding:0.02,labels: {
                enabled:true
            }},
					yAxis:	[{opposite:false,title:{text:params.m}},{opposite:true,title:{text:''}}],
					title: {
						text:tits,
						align:'left',
						x:35,
						style:{
			                fontSize:12,
			                color:'#888'
			            }
					},
						series:[{data:params.series,name:tits}]
			});
	depcharts.push(dpchart);
	depchartUrls.push({});
}

function Extremes(e) {
         var chartindex = e.series[0].chart.getChartIndex();
         var range_st = new Date(Math.round(e.min));
         var range_et = new Date(Math.round(e.max));
         $(charts[chartindex].container).parents('.stackplotwrap').find('.rng_btn').data('dateRangePicker').setDateRange(range_st,range_et);
         rangeobj.mode = false;
         updateChartMeta(chartindex,Math.round(e.min),Math.round(e.max),null);
         if(!chartUrls[chartindex].hasOwnProperty('autoload'))
         	return;
         if(chartUrls[chartindex].autoload == false)
         	return;
         var thischart = charts[chartindex];
         var st = Math.round(e.min);
         var et = Math.round(e.max);
		 thischart.showLoading('Loading data from server...');
		 var charturl = chartUrls[chartindex].charthistory[0][1];
		 var params = extractParams(charturl);
		 params.st = st;
		 params.et = et;
		 var diff = et - st;
		 var series_index = null;
		 _.each(thischart.series,function(ser,idx){
		 	if(series_index!=null)
		 		return;
		 	if(ser.name == 'flags' || ser.name == 'Navigator')
		 		return;
		 	series_index = idx;
		 });
		 if(series_index == null){
		 	throw 'ERR Series Index';
		 }
		 if(diff >= (6*30*24*60*60*1000))	//WEEKLY
		 {
		 	// if(params.interval == 'weekly')
		 	// 	return;
		 	params.intervalmin = 1440;
		 	params.interval = 'weekly'
		 }
		 else if(diff >= (2*30*24*60*60*1000)){ //Daily
		 	// if(params.interval == '' && params.intervalmin == 1440)
		 	// 	return;
		 	params.intervalmin = 1440;
		 	params.interval = ''
		 }
		 else if(diff >= (15*24*60*60*1000)){	//HOURLY
		 	// if(params.interval == '' && params.intervalmin == 60)
		 	// 	return;
		 	params.intervalmin = 60;
		 	params.interval = ''
		 }
		 else if(diff >= (24*60*60*1000)){	// Half Hour
		 	params.intervalmin = 30;
		 	params.interval = '';
		 }
		 else if(diff >= 6*60*60*1000){	//15 Mins
		 	params.intervalmin = 15;
		 	params.interval = '';
		 }
		 else if(diff >= 60*60*1000) // 5 MINUTES
		 {
		 	// if(params.intervalmin == 5 && params.interval == '')
		 	// 	return;
		 	params.intervalmin = 1;
		 	params.interval = ''
		 }
		 else
		 {
		 	params.intervalmin = 0.5;
		 	params.interval = '';
		 }
		var _prm = retractParams(params);
		var payload = [];
		$.get('controllers/proxyman.php',{endpoint:"data/search/data",params:_prm,di:'false',contenttype:''},function(d){
			if(d.errorCode>0){
				alert(d.errorMsg);
				charts[chartindex].hideLoading();
				return;
			}
						if(!d.response)
							d.response = {meters:[{data:d.data}]};
						if(!d.response.meters)
							d.response.meters = [{data:d.response.data}];
						var units_switch = false;
						var sensortype = params.sensortype;
						//series_axis = charts[chartindex].series[series_index].yAxis.options.index;
						series_axis = 0;
						// CHECK FOR UNITS CHANGE
						if(chartUrls[chartindex].units[series_axis]!=units[sensortype]	&&	chartUrls[chartindex].units[series_axis]!='')	//units have been changed
						{
							if(unitsMatch(units[sensortype],chartUrls[chartindex].units[series_axis]))
								units_switch = [units[sensortype],chartUrls[chartindex].units[series_axis]];
						}
						var stuff = d.response.meters[0].data;
						if(units_switch)
						{
							var newdata = convertUnits(units_switch[0],units_switch[1],stuff);
						}
						for(var dd=0; dd<d.response.meters.length; dd++){	
							var arr = [];
							payload.push(arr);
							var stuff = d.response.meters[dd].data || null;
							d.data = stuff;
							for(var	i=0;	i<d.data.length;	i++)
								{
									var	temp	=	{};
									if(!isNaN(d.data[i].value))
										temp.y	=	parseFloat(d.data[i].value);
									else
										temp.y = null;
									temp.x	=	d.data[i].ts||null;
									payload[dd].push(temp);
									delete(temp);
								}
						}
				var seriesarr = []; 
				for(var i=0; i<payload.length; i++)
				{				
					var temp = {};
					temp.data = payload[i];
					charts[chartindex].series[series_index].setData(payload[i]);
					seriesarr.push(temp);
					delete temp;
				}
				charts[chartindex].hideLoading();
				var res = findInterval(params.intervalmin,params.interval);
				_.each(charts[chartindex].yAxis,function(f){
					if(f.options.title.text == null || f.options.title.text == '')
			return;
					//f.update({ title:{text:units[chartUrls[chartindex].type]+' per '+res }});
				});
		},'json');
		updateChartMeta(chartindex,null,null,{interval:params.interval,intervalmin:params.intervalmin});
		
		var int_min = params.intervalmin;
		var int_true = params.interval;
		var res = findInterval(int_min,int_true);
		var el = $(charts[chartindex].container).parents('.plotter');
		if(el.length == 0)
			el = $(charts[chartindex].container).parents('.stackplotwrap');
		$(el).find('.res_btn').attr('data-id',res);
		$(el).find('.res_btn').empty().append(res);
}
/* AUTO ARRANGE Plots*/
function autoArrange(){
	var bodywidth = $('body').width();
	var bodyheight = $('body').height();
	var plotterwidth = $('.plotter:eq(0)').width();
	var plotterheight = $('.plotter:eq(0)').height();
	var columns = Math.floor(bodywidth/plotterwidth);
	var rows = Math.floor(bodyheight/plotterheight); 
	var total = (columns)*(rows);
	var numberofplots = [null];
	var x = 0;
	var y = 0;
	// find visible charts
	$('.plotter').each(function(id,el){
		if(!$(this).hasClass('hide'))
			numberofplots.push(id);
	});
	for(var i=1; i<=total; i++){
		if(numberofplots[i] == undefined)
			return;
		var el = $('.plotter:eq('+numberofplots[i]+')');
		$(el).css({top:y+'px',left:x+'px'});
		if(i == columns){	// NEXT ROW
			x = 0;
			y = y + plotterheight;
		}
		else if(i%columns == 0 && i != 0){	//NEXT ROW
			x = 0;
			y = y + plotterheight;	
		}
		else
			x = x + plotterwidth;
	}
}

function move(x,chartindex){
	var params = chartUrls[chartindex];
	var series_arr = [];
	var p_new_st = null;
	var p_new_et = null;
	_.each(charts[chartindex].series,function(s,s_idx){
		if(s.name == 'Navigator' || s.name == 'flags')
			return;
		else
			series_arr.push(s_idx);
	});
	// var p_st = charts[chartindex].xAxis[0].dataMin;
	// var p_et = charts[chartindex].xAxis[0].dataMax;
	var eee = extractParams(chartUrls[chartindex].charthistory[0][1]);
	var p_st = parseInt(eee.st);
	var p_et = parseInt(eee.et);
	var range = (parseInt(p_et) - parseInt(p_st)) / 2;
	_.each(params.seriesurl,function(h,idx){
		var _prm = extractParams(h);
		var new_params = extractParams(h);
		if(x == 1)	// Move Right
		{
			_prm.st = parseInt(p_et) + 0;
			_prm.et = parseInt(p_et) + range;
			new_params.et = _prm.et;
		}
		if(x == 0)	// Move Left
		{
			_prm.et = parseInt(p_st) + 0;
			_prm.st = parseInt(p_st) - range;
			new_params.st = _prm.st;
		}
		if(p_new_st == null || p_new_et == null)
		{
			p_new_st = new_params.st;
			p_new_et = new_params.et
		}
		var operation = params.endpoint[idx].split('/');
		operation = operation[operation.length-1];
		charts[chartindex].showLoading();
		(function(op,index,ep,prm){
		$.get('controllers/proxyman.php',{endpoint:ep,params:retractParams(prm)},
			function(d){
				if(d.errorCode>0){
					Notify('Plotter',d.errorMsg,'normal',true);
					charts[chartindex].hideLoading();
					//throw "Err - Move Module";
					return;
				}
				if(!d.response)
					d.response = {meters:[{data:d.data}]};
				if(!d.response.meters)
					d.response.meters = [{data:d.response.data}];
				var series_index = series_arr[index];
				var units_switch = false;
				var sensortype = prm.sensortype;
				series_axis = charts[chartindex].series[series_index].yAxis.options.index;
						// CHECK FOR UNITS CHANGE
						if(chartUrls[chartindex].units[series_axis]!=units[sensortype]	&&	chartUrls[chartindex].units[series_axis]!='')	//units have been changed
						{
							if(unitsMatch(units[sensortype],chartUrls[chartindex].units[series_axis]))
								units_switch = [units[sensortype],chartUrls[chartindex].units[series_axis]];
						}
						var stuff = d.response.meters[0].data;
						if(units_switch)
						{
							var newdata = convertUnits(units_switch[0],units_switch[1],stuff);
						}
						var	payload	=	[];
						var temp;
						for(var	j=0;	j<stuff.length;	j++)
							{
								temp = {x:stuff[j].ts || null,y:stuff[j].value || null};
								//console.log(stuff[j].value);
								charts[chartindex].series[series_index].addPoint(temp,false);
							}	
				
						charts[chartindex].redraw();
						charts[chartindex].hideLoading();
			},'json');
		})(operation,idx,params.endpoint[0],_prm);
	});
		updateChartMeta(chartindex,p_new_st,p_new_et,null);
		var el = $(charts[chartindex].container).parents('.plotter');
		if(el.length==0)
			el = $(charts[chartindex].container).parents('.stackplotwrap');
		$(el[0]).find('.rng_btn').data('dateRangePicker').setDateRange(new Date(parseInt(p_new_st)),new Date(parseInt(p_new_et)));
		rangeobj.mode = false;
}

function rangeName(x,y){
	var start = new Date(x);
	var end = new Date(y);
	var _start = start.getTime();
	var _end = end.getTime();
	var range = _end - _start; // milliseconds
	var seconds = range/1000;
	var minutes = seconds/60;
	var hour = minutes/60;
	var days = hour/24;
	var months = days/30;
	var year = months/12;

	var maxValue = [9999, 12, 31, 24, 60, 60];
	var dateRange = [year, months, days, hour, minutes, seconds];
	var dateFormat = ['MM YYYY', 'MMMM DD', 'dddd Do HH:MM', 'DD HH:MM:SS', 'HH:MM:SS', 'MM:SS'];

	var specifiedIndex = 0; // default format
	for (var index = 0; index < maxValue.length; index ++)
	{
	    if ((dateRange[index] < maxValue[index]) && (dateRange[index] >= 1))
	    { 
	        specifiedIndex = index;
	        index = maxValue.length;
	    }
	}
	return moment(start).format(dateFormat[specifiedIndex])+' to '+moment(end).format(dateFormat[specifiedIndex]);
}

// NON DESTRUCTIVE REFRESH FOR CHARTS
function autorefresh(chartindex){
	var chart = charts[chartindex];
	var meta = chartUrls[chartindex];
	if(meta.hasOwnProperty('autorefresh') && meta.autorefresh == true)
	{
		meta.autorefresh = false;
		window.clearTimeout(meta.timers.timerid);
		var prnt = $(charts[chartindex].container).parents('.stackplotwrap');
		$(prnt[0]).find('.tag_refresh').toggleClass('active');
	}
	else
	{
		meta.autorefresh = true;
		var thisstack = null;
		$(charts[chartindex].container).parents('.stackedPlotter').each(function(idx,ui){
			thisstack = $(ui).data('id');
		});
		var timeoutid = setTimeout(function(){
			duplicate_nondes(chartindex);	
		},30000);
		meta.timers = {stack:thisstack,timerid:timeoutid};
		var prnt = $(charts[chartindex].container).parents('.stackplotwrap');
		$(prnt[0]).find('.tag_refresh').toggleClass('active');
	}
}
function duplicate_nondes(chartindex,s,op){
	while(charts[chartindex].series.length>0){
		charts[chartindex].series[0].remove();
	}
	// _.each(charts[chartindex].series,function(ser){
	// 	if(ser.name == 'flags' || ser.name == 'Navigator')
	// 		ser.remove();
	// });
	s = s || 0;
	op = op || null;
	if(s === true){
		s = 0;
		op = true;
	}
	if(s==0){
		// reset colors to defult :(
				global_color_arr = [];
				var el = $(charts[chartindex].container).parents('.plotter');
				if(el.length==0)
					el = $(charts[chartindex].container).parents('.stackplotwrap');
				_.each($(el).find('.stats .box'),function(box){
					var attr = $(box).attr('style').split(':');
					attr[1] = attr[1].substring(0, attr[1].length - 1);
					global_color_arr.push(attr[1]);
				});
	}
		var p = chartUrls[chartindex].charthistory[s];
		var prm = extractParams(p[1]);
		var sensor = prm.sensortype;
		var endpoint = p[0].split('data/');
		addplotobj.chartindex = chartindex;
		var cb = null;
		if(chartUrls[chartindex].charthistory.length-1 > s)
			cb = function(){duplicate_nondes(chartindex,s+1,op)}
		else	// reset timer
		{
			if(op != true){
				chartUrls[chartindex].timers.timerid = setTimeout(function(){
					duplicate_nondes(chartindex);	
				},30000);
			}
				_.each(charts[chartindex].series,function(ser){
					if(ser.name == 'flags' || ser.name == 'Navigator')
						return;
					ser.update({color:color_arr[0]});
					color_arr.splice(0,1);
				});
		}
		add2plot(1,sensor,p[1],cb,endpoint[1],true,global_color_arr);
}
function updateChartMeta(chartindex,st,et,res){
	//chartindex = chartindex || null;
	st = st || null;
	et = et || null;
	res = res || null;
	if(charts[chartindex]==undefined)
	{
		alert('Err - Chart Meta Update');
		return;
	}	
		_.each(chartUrls[chartindex].charthistory,function(his,idx){
			var prm = extractParams(his[1]);
			if(st != null && et != null){
				prm.st = st;
				prm.et = et;
			}
			if(res != null)
			{
				prm.intervalmin = res.intervalmin || prm.intervalmin;
				prm.interval = res.interval || prm.interval;
			}
			chartUrls[chartindex].charthistory[idx][1] = retractParams(prm);
		});
		_.each(chartUrls[chartindex].seriesurl,function(ser,idx){
			var prm = extractParams(ser);
			if(st != null && et != null){
				prm.st = st;
				prm.et = et;
			}
			if(res != null){
				prm.intervalmin = res.intervalmin || prm.intervalmin;
				prm.interval = res.interval || prm.interval;	
			}
			chartUrls[chartindex].seriesurl[idx] = retractParams(prm);
		});
}
function checkDerivations(x){
	var ret = {};
	for(var i=0;i<metadata.length;i++){
		if(metadata[i]._id == x){
			console.log('found '+i);
			_.each(metadata[i].sensorList,function(sen){
				if(sen._source.hasOwnProperty('isderived') && sen._source.isderived === true)
					ret[sen._source.sensortype_display] = {'bk':sen._source.sensortype_backend,'_id':metadata[i]._id};
			});
			return ret;
		}
	}
	return null;
}
function findInterval(int_min,int_true){
	var res = "Unknown";
if(int_true==""){
			if(int_min == 60)
				res = 'Hour';
			if(int_min == 1440)
				res = 'Day';
			if(int_min == 720)
				res = '12 Hours';
			if(int_min == 360)
				res = '6 Hours';
			if(int_min == 30)
				res = '30 Mins';
			if(int_min == 15)
				res = '15 Mins';
			if(int_min == 5)
				res = '5 Mins';
			if(int_min == 1)
				res = '1 Min';
			if(int_min == 0.5)
				res = '30 Secs';
			if(int_min == 0.01667)
				res = '1 Sec';
		}
		else
		{
			res = int_true.charAt(0).toUpperCase() + int_true.substring(1);
		}
	return res;
	}

Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
  //  return this; // for testing purposes
};
function highResShift(chartindex,op){
	if(chartindex == null)
	{
		alert('Error HighRes Shift');
		return;
	}
	var params = highresCharts[chartindex].params;
	var shift = params.et - params.st;
	if(op === 'right')
	{
		params.et = params.et + shift;
		params.st = params.st + shift;
	}
	if(op === 'left')
	{
		params.et = params.et - shift;
		params.st = params.st - shift;
	}

		var win = logWindow('highres');
		highplot('highres-'+win,highresCharts[chartindex].includes,params);
		//destroy previous

}
// DataAvailability Module
function availability(stn,sen){
	// generate window
	var uniqid = logWindow('control');
	var ajaxcount = 0;
	var firstoccourance = false;
	// time issues
	var offset = (_tzo.browser - _tzo.offset);
	var offset_millis = offset*60*1000;
	// main loop
	_.each(stn,function(s,idx){
		var sensor = sen[idx];
		++ajaxcount;
		(function(sensortype){
		$.get('controllers/proxyman.php',{endpoint:'data/search/data',params:"st="+st+"&et="+et+"&intervalmin=0.5&includepredicted=false&sensortype="+sensor+"&search=&include="+s+'&exclude=&interval=&turnoffvee=true&includeflag=true'},
			function(d){
				--ajaxcount;
				if(parseInt(d.errorCode)===0)
					{
						if(!firstoccourance){	// generate table
							firstoccourance = true;
							$('#'+uniqid+' .content img').remove();
							var html = '<table id="availabilitytable"><thead><tr><th>Station</th><th>Sensor</th>';
							// get xAxis
							_.each(d.response.meters[0].data,function(data){
								var date = new Date(data.ts+offset_millis).toString();
								html += '<th>'+date+'</th>';
							});
							html += '</tr></thead><tbody></tbody></table>';
							$('#'+uniqid+' .content').append(html);
						}
							var newhtml = '<tr><td>'+d.response.meters[0].meta._source.display_name+'</td><td>'+sensortype+'</td>';
							_.each(d.response.meters[0].data,function(data){
								var color;
								if(data.hasOwnProperty('value')&&data.value!=null)
									color = 'green';
								else{
									color = 'red';
									data.value = 'Bad';
								}
								var txt = 'Good Data';
								if(data.isBad==true)
									txt = data.badReason;
								newhtml += '<td title="'+txt+'" class="'+color+'">'+data.value||'None'+'</td>';
							});
							newhtml += '</tr>';
							$('#'+uniqid+' #availabilitytable tbody').append(newhtml);
							if(ajaxcount==0)	//last
								$('#'+uniqid+' #availabilitytable').tooltip();
					}
			},'json');
		})(sensor);
	});

}
function cleanPlotDataObject(){
	var len1 = charts.length;
	var len2 = chartUrls.length;
	if(len1>len2){
		var diff = len1 - len2;
		for(var i=0;i<diff;i++){
			charts.splice(charts.length-1,1);
		}
	}
	if(len2>len1){
		var diff = len2 - len1;
		for(var i=0;i<diff;i++){
			chartUrls.splice(chartUrls.length-1,1);
		}
	}
	_.each(charts,function(c,idx){
		if(c === undefined){
			charts.splice(idx,1);
			chartsUrls.splice(idx,1);
		}
	});
}
function id2mid(x){ return x;}