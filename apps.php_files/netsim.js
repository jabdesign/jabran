/*
    Network Simulation  - Jab -  Visenti
*/

( function( window, undefined ) {
  "use strict";

  function  Netsim(){
    var privates = {};

    this.shout = function(){console.log(privates);}

    this.button = function(type,id,zone){
      var ret = '';
      if(type != undefined){
        switch(type){
          case 'station':
            ret = '<div class="button tiny expand" onclick="netsim.plotstn()">Pressure (sim)</div>';
          break;
          case 'pipe':
            ret = '<br/><a onclick="netsim.plot(\''+id+'\',\''+zone+'\',\'flow\')">Simulated Flow</a>';
          break;
          case 'junction':
            ret = '<br/><a onclick="netsim.plot(\''+id+'\',\''+zone+'\',\'age\')">WaterAge (S)</a><br/><a onclick="netsim.plot(\''+id+'\',\''+zone+'\',\'pressure\')">Pressure (S)</a>';
          break;
        }
      }
      return ret;
    }

    // stations
    this.plotstn = function(op){
      op = op || null;
      if(unitObj.selectedUnit.length===0)
      {
        Notify('Network Simulations','No Stations selected','normal',true);
        return;
      }
  
      var id = unitObj.selectedUnit[0];
      var sensor = 'pressure';
      if(addseriesobj.mode){
              addplotobj.mode = true;
              addplotobj.chartindex = addseriesobj.chartindex;
              add2plot(9,sensor,'st='+st+'&sensortype='+sensor+'&include='+id,null,'simulationapi/modeldata/station');
              return;
      }
      var cid = logWindow('plot',{});
       $('#'+cid+' img').remove();
      var chart = new Highcharts.Chart({
                chart:{
                  renderTo:cid,
                  type:'line',
                  animation:false
                },
                navigator:{
                  enabled:false
                },
                rangeSelector:{
                  enabled:false
                },
                plotOptions: {
                  series: {
                    marker: {
                        enabled: false
                   }
                 }
                },
                xAxis:  { type:'datetime',minPadding:0.02,maxPadding:0.02},
                yAxis:  [{opposite:false,title:{text:'?'}},{opposite:true,title:{text:''}}],
                title:{
                  text:'Simulated '+sensor+' for '+id
                },
                series:[]
          });
     
      
      $.get('controllers/tlp.php',{endpoint:'simulationapi/modeldata/station',params:'st='+st+'&sensortype='+sensor+'&include='+id},
        function(e){
          console.log(e);
          
          if(e.errorCode>0){
            Notify('Network Simulations',e.errorMsg,'normal',true);
            return;
          }
          var response = e.response.results[0].datalist;
          var payload = [];
          _.each(response,function(r){
            payload.push([r.timestamp,parseFloat(r.value.toFixed(2))])
          });
          chart.addSeries({
                  data:payload,
                  name:id+' (Simulated)'
                });
        },'json');
      $.get('controllers/proxyman.php',{endpoint:'data/search/data',params:'st='+st+'&et='+et+'&sensortype='+sensor+'&include='+id+"&intervalmin="+interval+"&includepredicted="+pred+"&interval="+_interval+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&exclude='},
        function(d){
          if(d.errorCode>0){
            Notify('Network Simulations','d,errorMsg','normal',true);
            return;
          }
          if(!d.response)
              d.response = {meters:[{data:d.data}]};
            if(!d.response.meters)
              d.response.meters = [{data:d.response.data}];
            var payload = [];
            for(var dd=0; dd<d.response.meters.length; dd++){
              var stuff = d.response.meters[dd].data || null;
            d.data = stuff;
            for(var i=0;    i<d.data.length;        i++){
              var temp  = {};
                if(!isNaN(d.data[i].value))
                  temp.y  = parseFloat(d.data[i].value);
                else
                  temp.y = null;
                temp.x  = d.data[i].ts || null;
                payload.push(temp);
                //delete(temp);
            }
          }
          chart.addSeries({
            data:payload,
            name:id+' (Actual)'
          });
        },'json');
    }

    // gis
    this.plot = function(id,zone,op){
      if(addseriesobj.mode){
              addplotobj.mode = true;
              addplotobj.chartindex = addseriesobj.chartindex;
              add2plot(9,op,'st='+st+'&type='+op+'&nodeid='+id+'&zone='+zone,null,'simulationapi/modeldata/gis');
              return;
      }
      var cid = logWindow('plot',{});
      $.get('controllers/tlp.php',{endpoint:'simulationapi/modeldata/gis',params:'st='+st+'&type='+op+'&nodeid='+id+'&zone='+zone},
        function(e){
          console.log(e);
          $('#'+cid+ 'img').remove();   
          if(e.errorCode>0){
            Notify('Network Simulations',e.errorMsg,'normal',true);
            return;
          }
          var response = e.response.results[0].datalist;
          var payload = [];
          _.each(response,function(r){
            payload.push([r.timestamp,parseFloat(r.value.toFixed(2))])
          });
          var chart = new Highcharts.Chart({
                chart:{
                  renderTo:cid,
                  type:'line',
                  animation:false
                },
                navigator:{
                  enabled:false
                },
                rangeSelector:{
                  enabled:false
                },
                plotOptions: {
                  series: {
                    marker: {
                        enabled: false
                   }
                 }
                },
                xAxis:  { type:'datetime',minPadding:0.02,maxPadding:0.02},
                yAxis:  [{opposite:false,title:{text:'?'}},{opposite:true,title:{text:''}}],
                title:{
                  text:e.response.results[0].dataName+' for '+e.response.results[0].nodeName+' ('+e.response.results[0].zone+')'
                },
                series:[
                {
                  data:payload,
                  name:id+' - '+op
                }]
          });
        },'json');
    }

  }

  window.Netsim = Netsim;

})(window);

var netsim = new Netsim();
