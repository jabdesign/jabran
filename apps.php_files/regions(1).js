var color_arr	=	['#FF0000','#CC3300','#009933','#3333CC'];
var	color_index	=	0;
var	polygons	=	[];
function	loadRegions(){
if(!map)
	console.log('Map not available');
//	bksr
	$.getJSON('data/bksr.json',{},function(e){
		processRegion(e);
	});
//	phfc
	$.getJSON('data/phfc.json',{},function(d){
		processRegion(d);
	});
//	qhpz
	$.getJSON('data/qhpz.json',{},function(f){
		processRegion(f);
	});
//	mnsr
	$.getJSON('data/mnsr.json',{},function(g){
		processRegion(g);
	});
}

function	region(x,y)
	{
		switch(x)
			{
				case	'fcph':
					if(y){
					$.getJSON('data/phfc.json',{},function(d){
						processRegion(d,'fcph');
					});
					}
					else
						unloadPoly('fcph');
				break;
				default:
					if(y){
					$.getJSON('data/'+x+'.json',{},function(d){
						processRegion(d,x);
					});
					}
					else
						unloadPoly(x);
				break;
			}					
	}

function	processRegion(r,x)	
	{
		var	coords	=	[];
		for(var	i=0;	i<r.length;	i++)
			{
				coords.push(new google.maps.LatLng(r[i].YCoord,r[i].XCoord));
			}
		poly = new google.maps.Polygon({
    paths: coords,
    strokeColor: color_arr[color_index],
    strokeOpacity: 1,
    strokeWeight: 3,
    fillColor: color_arr[color_index],
    fillOpacity: 0.1
  });

  	poly.setMap(map);
	map.setCenter(poly.my_getBounds().getCenter());
	map.setZoom(13);
	var	temp	=	{};
	temp.zone	=	x;
	temp.poly	=	poly;
	polygons.push(temp);
	++color_index;
	}
	
function	unloadPoly(p){
	for(var	i=0;	i<polygons.length;	i++)
		{
			if(polygons[i].zone	==	p){
				polygons[i].poly.setMap(null);
				polygons.splice(i,1);
				--color_index;
				return;
			}
		}
}
