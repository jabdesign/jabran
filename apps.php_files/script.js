//	memory
var	data;
var includeMarkers = [];
var excludeMarkers = [];
var	markers	=	[];
var zonesarr = [];
var	metadata	=	null;
var globalmeta = null;	// TEMPORARY
var	interval	=	60;
var _interval = '';
var	pred	=	'false';
var	charts	=	[];
var stackedPlots = [];
var highresCharts = [];
var depcharts = [];
var	group	=	[];
var Proxyurl;
var	windows	=	[];
var infowindow;
var resident=false;
var midSearc={};
var actionListObj={};
var unitObj={selectedUnit:[],sensorSelection:{},ids:{},lastselected:{},sensorMeta:{}}
var groupIndexID;
var groupbuildIndexID;
var BuildingSel={};
var infowindowstatus=false;
var infowindow;
var st,et;
var bounds = null;
var metaIDX;
var currentMeter = null;
var MarkerObject={};
var subtrigger=[];
var uniqueList=[];
var sensorsTypes={};
var generiClosure = null;
var defsensor=['pressure','temperature','outflow','consumption'];
var plotrange = [],plotrange_val=[]; 
var eptype = ['search/data','map/aggregate','map/average','map/min','map/max','forecast/forecast_default'];
var selectedep = 'search/data';
var cutoffobj = {chartindex:null,mode:null,cutoff:null,range:null,reset:function(){
	this.chartindex = null;
	this.mode = null;
	this.range = null;
	this.cutoff = null;
}};
var annotateObj = {ts:null,endpoint:'data/search/customer',reset:function(){
	this.ts = null;
}};
var tanksobj = {id:null,chartindex:null,addmode:false,reset:function(){
	this.id = null;
	this.chartindex = null;
	this.addmode = false;
	addseriesobj.reset();
//	showAction(null);
}};
var replotobj = {};
var addplotobj = {mode:false,meters:[],chartindex:null,mutex:false,reset:function(){
	this.mode = false;
	this.meters = [];
	this.chartindex = null;
	this.mutex = false;
	addseriesobj.reset();
//	resetSel();
}};
var aggregateobj = {mode:false,meters:[],addmode:false,chartindex:null,reset:function(){
	this.mode = false;
	this.meters = [];
	this.addmode = false;
	this.chartindex = null;
	addseriesobj.reset();
	$('.aggrBtn').removeClass('btn-selected')
	$('.addmodebtn').remove();
	$('.minWindow').removeClass('addmode');
//	resetSel();
}};
var addseriesobj = {mode:false,meters:[],addmode:false,chartindex:null,reset:function(){
	this.mode = false;
	this.meters = [];
	this.chartindex = null;
	$('.minWindow').removeClass('addmode');
}};
var averageobj = {mode:false,meters:[],addmode:false,chartindex:null,reset:function(){
	this.mode = false;
	this.meters = [];
	this.addmode = false;
	this.chartindex = null;
	addseriesobj.reset();
	$('.aggrBtn').removeClass('btn-selected')
	$('.addmodebtn').remove();
	$('.minWindow').removeClass('addmode');
//	resetSel();
}};
var charttypeobj = {chartindex:null,plotter:null,reset:function(){
	this.chartindex = null;
	this.plotter = null;
	$('#chartdd').remove();
	//resetSel();
}};
var weatherobj = {id:null,addmode:false,chartindex:null,meters:[],reset:function(){
	this.id = null;
	this.addmode = false;
	this.chartindex = null;
	this.meters = [];
	addseriesobj.reset();
//	showAction(null);
//	resetSel();
}};
var chartUrls = [];
var depchartUrls = [];
actionListObj.default=[];
actionListObj.UnitOptions=[{"label":"Readings","id":"reading","dynamic":false,"mode":"single","type":["AMR","WEMS"],"stype":"MTR", 'checked':false},{"label":"Outflow","id":"outflow","dynamic":false,"mode":"single","type":["FLOW"],"stype":"OUTFLOW", 'checked':false},{"label":"Min Night Flow","id":"mnf","dynamic":false,"mode":"single","type":["FLOW"],"stype":"MIN_NIGHT_FLOW", 'checked':false},{"label":"Min Night Consumption","id":"mnc","dynamic":false,"mode":"single","type":["AMR","WEMS"],"stype":"MIN_NIGHT_CONSUMPTION", 'checked':false},{"label":"Consumption","id":"consumption","dynamic":false,"mode":"single","type":["AMR","WEMS"],"stype":"CONSUMPTION", 'checked':true},{"label":"Sales","id":"sales","dynamic":false,"mode":"single","type":["AMR","WEMS"],"stype":"SPSALES", 'checked':false},{"label":"Rain","id":"rain","dynamic":false,"mode":"single","type":["WEATHER"], 'checked':false},{"label":"Temperature","id":"temperature","dynamic":false,"mode":"single","type":["WEATHER"], 'checked':true},{"label":"Humidity","id":"humidity","dynamic":false,"mode":"single","type":["WEATHER"]},{"label":"Pressure","id":"pressure","dynamic":false,"mode":"single","type":["STN"]},{"label":"Battery","id":"battery","dynamic":false,"mode":"single","type":["STN"]},{"label":"PH","id":"ph","dynamic":false,"mode":"single","type":["STN"]}];
actionListObj.triggers=[];
actionListObj.reports=[{"label":"Trend Analysis","id":"overview","dynamic":false,"mode":"default","type":["AMR","WEMS","WEATHER","FLOW",null]},{"label":"Zonal Analysis","id":"zonal","dynamic":false,"mode":"default","type":["AMR","WEMS","WEATHER","FLOW",null]},{"label":"Demand Variation","id":"dva","dynamic":false,"mode":"default","type":["AMR","WEMS","WEATHER","FLOW",null]},{"label":"Meter Quality","id":"MQA","dynamic":false,"mode":"default","type":["AMR","WEMS","WEATHER","FLOW",null]},{"label":"Sector Report","id":"sectreport","dynamic":false,"mode":"default","type":["AMR","WEMS",null]},{"label":"Customer Analysis","id":"custreport","dynamic":false,"mode":"default","type":["AMR","WEMS",null]},{"label":"Daily Report","id":"dailyreport","dynamic":false,"mode":"default","type":["STN",null]}];
actionListObj.subtriggers=[{"label":"Sector Report","id":"sectreport","dynamic":false,"mode":"single","type":["AMR","WEMS",null]}];
var	tags	=	['customername','meterstatus','roomcount','neighbourhood'];
function	initMarkers(){


	
	showAction('singapore','','')
	var jsonobj=defaultjson();
	var response={};
	var pURL='controllers/proxyman.php';
	preLoader('Loading...','modal',true)
	$.ajax({
		type: "POST",
		url: pURL,
			  //contentType: "application/json",
		data: {search:true,"SearchQuery":jsonobj},
		dataType:"json"
	}).done(function( Dataresponse ) {
		response.data=[];

		$.each(Dataresponse.response.hits.hits,function(key,value){
			response.data.push(this._source)
		});
		if(objectSize(Dataresponse.response.hits.hits)<=0)
		{
			//alert('No data available');
			preLoader('Loading...','modal',false)
			Notify('Data Fetch Error','No data available','normal',true)
			return;
		}
		else {
			
			MarkerInfoData=response;
			plotIcons(MarkerInfoData, false);
			if(defaultSettings.watertype=='potable')
			{
				// loadDMAZones()
			}
			preLoader('Loading...','modal',false);
			dma.init();
			//groupMeterbycustomer(MarkerInfoData.data) 
		}
		// $.each(markers,function(key,value){
		// 	value.set('labelContent', value.Markername);
		// 	value.set('labelClass', 'mLabls');
		// } )
	 	



	});
	
		/*$.get('controllers/proxyman.php',{endpoint:"meter/information/all"},
		function(d)
			{
				console.log(d);
				MarkerInfoData=d;
				plotIcons(MarkerInfoData, false);
				
			},'json');*/

}

function fetchZones(){
	return;
	var query = dmajson();
	$.get('controllers/proxyman.php',{search:true,SearchQuery:query},function(e){
		console.log(e);
		if(e.errorCode>0)
		{
			alert(e.errorMsg);
			return;
		}
		var obj = e.response.hits.hits;
		_.each(obj,function(zone){
			zonesarr.push(zone._source);
		});
		processZones();
	},'json');
}
function processZones(){
	_.each(zonesarr,function(zone){
		zonesInfo.push(zone.meta.name);
	});
	//loadZonesettings();
}

function plotIcons(d, animation)
{
	//$('.listview ul').empty();
	bounds = new google.maps.LatLngBounds();
	sensorsTypes.weather=[];
	sensorsTypes.sensorstation=[];
	sensorsTypes.amr=[];
	sensorsTypes.pub_flowmeter=[];
	sensorsTypes.scada=[];
	sensorsTypes.wems=[];
	sensorsTypes.all=[];
	var amrlist=[
		"residential",
		"nonresidential"
	]
	$('.listview .wrapman').empty();
	html = '<table id="listViews"><thead><tr><td>WaterType</td><td>Site Name</td><td>Sector</td><td>Explore</td></tr></thead><tbody>';
	group	=	[];
	var latlngs = [];
	data	=	d.data;
					
					for(var	i=0;	i<data.length;	i++)
					{
						if(data[i].tag_sector == 'dmameter'){
							//continue;
							// console.log('dma found');
							//zonesInfo.push(zone.name);
							//continue;
						}
						if('tag_datatype' in data[i])
						{
							if(data[i].tag_datatype=='amr')
							{
								tag_cat='amr';
							}
							else
							{
								tag_cat=data[i].tag_category;
							}
						}
						else
						{
							tag_cat=data[i].tag_category;
						}
						if(tag_cat == 'dmastation'){
								console.log('Bad DMA found');
								//continue;
							}
						// All amr tag_categories are grouped as AMR
						//
						// if(jQuery.inArray( data[i].tag_category, amrlist )>=0)
						// 	tag_cat='amr';
						// else
						// 	tag_cat=data[i].tag_category;
						// condition to skipp the markers
						if(jQuery.inArray(tag_cat, Parentconfig.markerIcons )>=0)
						{
							if(tag_cat)
							{	
								
								// if(tag_cat=='weather_dud'||tag_cat=='weather')
								// {
								// 	tag_cat='weather';
								// }
								// else if(tag_cat=="wems,nonresidential"||tag_cat=='wems')
								// {
								// 	tag_cat='wems';
								// }
								// else if(tag_cat=="nonresidential")
								// {
								// 	tag_cat='nonresidential';
								// }
								// else if(tag_cat=="residential")
								// {
								// 	tag_cat='nonresidential';
								// }
								
								try{
									sensorsTypes[tag_cat].push(data[i].sensorList)
									TempData=data[i].sensorList;
									$.each(TempData,function(k1,v1){
										TempData[k1]._source.category=tag_cat;
									})
									// TempData[0]._source.category=tag_cat;
									sensorsTypes.all.push(TempData)
								}
								catch(e)
								{
									console.log('failed ----');	
									console.log(data[i]);
								}
								
							}
							else
							{
								console.log(tag_cat)
							}
							var re='(#)(\\d+)(-)(\\d+)';//'.*?(#)(\\d+)';  // Any Single Character 2
							var p = new RegExp(re,["i"]);
							var m = p.exec(data[i].address);
							
							if (m != null)
							{
								var lvel=m[2];
								var unitno=m[4]
							}
							else {
								var lvel='01';
								var unitno='01'+i;
							}
							try{
								if(tag_cat=='sensorstation')
								{
									lat=data[i].latitude;
									lng=data[i].longitude;
								}
								else
								{
									lat=data[i].loc[0].lat;
									lng=data[i].loc[0].lng;
								}
								if(lat === undefined || lng === undefined)
								{
									// console.log(data[i]);
								}
								var	thispos	=	[lat,lng,data[i]._id,i,data[i].bp_name||data[i].customer_name||data[i].customername||data[i].address, data[i].tag_sector,lvel,data[i].tag_category,unitno,data[i].display_name];
							}
							catch(e)
							{
								continue;
							}
							found	=	0;
							for(var j=0;	j<group.length;	j++)
							{
									if(group[j][0][0]	==	thispos[0]	&&	group[j][0][1]	==	thispos[1])
									{
											//console.log('member '+j);
										found	=	1;
										group[j].push(thispos);
										break;
									}
							}
							if(!found)
							{
								var d=[thispos];
								
								group.push(d);
								//console.log(d)
							}
						}
						else
						{
							//console.log('skip >>')
						}
					}
					_unionSensors();
							metadata	=	data;
							//if(globalmeta == null)
								globalmeta = data;

					for(var	k=0;	k<group.length;	k++)
					{
						if(group[k][0][5] == 'dmameter')
						{
							// console.log(group[k]);
							// process zones
							// _.each(group[k],function(g){
							// 	zonesarr.push({meta:metadata[g[3]],idx:g[3]});
							// });
							// processZones();
							continue;
						}
						var whaticon = findGroupIcon(k);
						whaticon=whaticon.toLowerCase();
						//console.log(group[k][0][7]+':'+whaticon);
						var icontype = 'm';
						var wtype_signal;//='_o.png'
	
						switch(whaticon)
						{
							case 'others':
							wtype_signal = '_o.png';
							break;

							case 'mixed':
							wtype_signal = '_m.png';
							break;

							case 'potable':
							wtype_signal = '_p.png';
							break;

							case 'newwater':
							wtype_signal = '_n.png';
							break;
						}
								var thisicon = 'images/mapicons/office'+wtype_signal;
								switch(group[k][0][5])
									{
										case 'weather':
											//console.log('found st');
											thisicon = 'images/mapicons/weatherstation.png';
											icontype = 'w';
										break;
										case 'wems':
											//console.log('found st');
											thisicon = 'images/mapicons/weatherstation.png';
											icontype = 'w';
										break;
										case 'Hospital':
											thisicon = 'images/mapicons/hospital'+wtype_signal;
										break
										case 'Airport':
											thisicon = 'images/mapicons/airport'+wtype_signal;
										break;
										case 'Hotel':
											thisicon = 'images/mapicons/hotel'+wtype_signal;
										break;
										case 'Retail':
											thisicon = 'images/mapicons/retail'+wtype_signal;
										break;
										case 'Manufacturing':
											thisicon = 'images/mapicons/manufacture'+wtype_signal;
										break;
										case 'Sch':
											thisicon = 'images/mapicons/school'+wtype_signal;
										break;
										case 'Prison':
											thisicon = 'images/mapicons/prison'+wtype_signal;
										break;
										case 'residential':
											thisicon = 'images/mapicons/residential'+wtype_signal;
										break;
										case 'scada':
											thisicon = 'images/mapicons/reservoir.png';
											icontype = 'sc';
										break;
										case 'logger':
											thisicon = 'images/mapicons/office_p.png';
										break;

										case 'sensorstation':
											icontype='s';
											if(metadata[group[k][0][3]].tag_owner=='visenti')
											{
												thisicon = 'images/mapicons/station_d.png';//metadata[group[k][0][3]].icon;
											}
											else
											{
												if(group[k][0][5].match('scada'))
												{
													thisicon = 'images/mapicons/reservoir.png';
													icontype = 'sc';
												}
												thisicon = 'images/mapicons/station_gr.png';//metadata[group[k][0][3]].icon;
											
											}


										break;
										case 'dmameter':
											throw 'Error Non Map Object found';
										break;
										default:
											if(group[k][0][5].match('pub_flowmeter')||group[k][0][5].match('flowmeter')){
												thisicon = 'images/mapicons/flow'+wtype_signal;
												icontype = 'f';
											}
										break;
									}
						// check for icon in meta // OVERWRITE LOGIC
								if(metadata[group[k][0][3]].hasOwnProperty('icon'))
									thisicon = metadata[group[k][0][3]].icon;
						try	{
							var customvisiblity;
							if(firstload == true)
							{	

								if(group[k][0][7]=='scada' ) //group[k][0][7]=='pub_flowmeter'|| ||icontype == 'w' 
									customvisiblity = false;
								else
									customvisiblity = true;
							}
							else
								customvisiblity = true;
							//console.log(group[k][0])
							var	latlng	=	new google.maps.LatLng(group[k][0][0],group[k][0][1]);
							latlngs.push(latlng);
							var marker
/*							var	marker	=	new google.maps.Marker({
								position:	latlng,
								map:	map,
								id:	group[k][0][2],
								metaindex:	group[k][0][3],
								groupindex:	k,
								icon:thisicon,
								visible:customvisiblity
							});*/

								if(animation==true)
							{
								// marker	=	new google.maps.Marker({
								marker	=	new MarkerWithLabel({
									position:	latlng,
									map:	map,
									id:	group[k][0][2],
									metaindex:	group[k][0][3],
									groupindex:	k,
									icon:thisicon,
									Markername:group[k][0][9],
									visible:customvisiblity,
									title:'selected',
									flat:true,
									optimized: false,
									labelContent: "",
									labelClass : ""

								});	
							}
							else
							{
								// marker	=	new google.maps.Marker({
								marker	=	new MarkerWithLabel({
									position:	latlng,
									map:	map,
									id:	group[k][0][2],
									metaindex:	group[k][0][3],
									groupindex:	k,
									icon:thisicon,
									Markername:group[k][0][9],
									visible:customvisiblity,
									title:'',
									flat:true,
									optimized: false,
									labelContent: "",
									labelClass : ""
								});
							}
							if(customvisiblity)	//	List View
							{
								var info = group[k][0][4] || group[k][0][2];
								//html += '<li class="outterList Lblock" id="List_'+k+'" data-id="'+k+'" data-smeter="'+group[k][0][2]+'" title="'+info+'">';
								html += '<tr><td>';
								if(group[k][0][5]	==	'weather')
									html += '<span class="_weather circular">WTHR</span>';
								else{
									switch(whaticon){
										case 'others':
										html += '<span class="_others circular">OTHR</span>';
										break;

										case 'mixed':
										html += '<span class="_mixed circular">MXD</span>';
										break;

										case 'potable':
										html += '<span class="_potable circular">PW</span>';
										break;

										case 'newwater':
										html += '<span class="_newwater circular">NW</span>';
										break;
									}
								}
								gidx=group[k][0][3];
								dispname=metadata[gidx].display_name||metadata[gidx].name||group[k][0][2];
								html += '</td><td>'+dispname+'</td><td>'+group[k][0][5]+'</td><td><div class="Lblock" id="List_'+k+'" data-id="'+k+'" data-smeter="'+dispname+'">Open</div></td></tr>';
							/*	if(group[k].length>1){
									html += '<ul>';
									for(var h=0;h<group[k].length;h++)
										html += '<li>'+group[k][h][2]+'</li>';
									html += '</ul>';
									}*/

									//html +='</li>';
									//$('.listview ul').append(html);
									//$('.outterList').tooltip();

								}
								fit2map(latlngs);
								createMarker('test',latlng, marker,icontype);	
								markers.push(marker);	
																
							}								
							catch(e)
								{
									console.log('Error '+k);
									console.log(e);
								}			
						}
		$('.listview .wrapman').append(html);
		$('#listViews').DataTable({
			"paging":false,"info":false
		});						
		map.fitBounds(bounds);
		firstload = true;
}

function LevelFinder()
{
	//console.log(infowindowstatus)
	//console.log(rootSelection)
	//console.log(searchOptions)
	//console.log(buildingSelection)

}
function _unionSensors()
{
	var dupes = {};
	var singles = [];
	var uniqList=[];
	$.each(sensorsTypes.all, function(i, dt) {
		$.each(dt,function(i2,el){


	    if (!dupes[el._source.sensortype_display]) {
	        dupes[el._source.sensortype_display] = true;
	      	el._source.label=el._source.sensortype_display;
			el._source.id=el._source.sensortype_display;
			el._source.dynamic=false;
			el._source.mode='single';
			// delete el._source.type;
			el._source.sentype=[];
			switch(el._source.category)
			{
				case 'sensorstation':
					switch(el._source.sensortype_actual)
					{
						case 'flow':
							type='FLOW';
							break;
						case 'pressure':
							type='STN';
							break;
						case 'btry':
							type='STN';
							break;
						case 'volume':
							type='STN';
							break;
						default:
							type='STN';
							break;		

					}
					break;
				case 'amr':
					type='FLOW';
					break;
				case 'weather':
					type:'WEATHER';
					break;
				case 'wems':
					type:'WEMS';
					break;
				default:
					type='AMR';
					break;	
			}
			el._source.sentype.push(type);
			uniqList.push(el._source);
	    }
	    })
	});
	actionListObj.UnitOptions={};
	actionListObj.UnitOptions=uniqList;
	// JAB EDIT
	var myobj = _.flatten(sensorsTypes.all);
	_.each(myobj,function(u){
		units[u._source.sensortype_backend] = u._source.sensortype_units;
		sensortype_map[u._source.sensortype_backend] = u._source.sensortype_actual;
		if (sensortype_displaymap.hasOwnProperty(u.parent)){
			sensortype_displaymap[u.parent][u._source.sensortype_backend] = u._source.sensortype_display;
		}
		else{
			sensortype_displaymap[u.parent] = {};
			sensortype_displaymap[u.parent][u._source.sensortype_backend] = u._source.sensortype_display;	
		}
	});
}
function getUnionOFsensors(mdata)
{
	var dupes = {};
	var singles = [];
	var uniqList=[];
	$.each(mdata, function(k, v) {
		$.each(v.sensorList,function(l,m){
			 if (!dupes[m._source.sensortype_display]) {
			 	dupes[m._source.sensortype_display] = true;
			 
			 m._source.label=m._source.sensortype_display;
			 m._source.dynamic=false;
			 m._source.mode='single';
			 m._source.sentype=[];
			
			switch(m._source.category)
			{
				case 'sensorstation':
					switch(m._source.sensortype_actual)
					{
						case 'flow':
							type='FLOW';
							break;
						case 'pressure':
							type='STN';
							break;
						case 'btry':
							type='STN';
							break;
						case 'volume':
							type='STN';
							break;
						default:
							type='STN';
							break;		

					}
					break;
				case 'amr':
					type='FLOW';
					break;
				case 'weather':
					type:'WEATHER';
					break;
				case 'wems':
					type:'WEMS';
					break;
			}
			m._source.sentype.push(type);
	        uniqList.push(m._source);
	        }
		})
	});
	return uniqList;
	
	
}
function _unionSensors2()
{
	
	var ul;
	var uniqueList=[];
	$.each(sensorsTypes,function(key,value){
		
		if(value!=undefined)
		{
			var Ulist = _.uniq(value, function(item, key, a) { 
				try{
				 return item[0]._source.sensortype_display.a;
				}
				catch(e)
				{
					
				}
			});
		}
		if(Ulist[0]!=undefined)
		{

			$.each(Ulist[0],function(key1,value1){
					value1._source.label=value1._source.sensortype_display;
					value1._source.id=value1._source.sensortype_display;
					value1._source.dynamic=false;
					value1._source.mode='single';
					delete value1._source.type;
					if('type' in value1._source)
					{
					}
					else
					{
						value1._source.type=[];
					}
					uniqueList.push(value1._source);
					

				
			});
		}
		ul = _.uniq(uniqueList, function(item, key, a) { 
				return item.sensortype_display;
		});
		//defsensor[value].
		if(Ulist[0]!=undefined)
		{
		$.each(Ulist[0],function(key1,value1){
			$.each(ul,function(key2,value2){
						if(value1._source.sensortype_display==value2.sensortype_display)
						{
							if(key=='sensorstation')
							{
								key='STN'
							}
							if(key=='pub_flowmeter')
							{
								key='FLOW'
							}
							
							ul[key2].type.push(key.toUpperCase());
						}
					})
			});
		}
		
	});
	actionListObj.UnitOptions={};
	actionListObj.UnitOptions=ul;
	//console.log(JSON.stringify(actionListObj.UnitOptions))
}
function selectBuilding(GroupId)
{
	if($("#Building_Block .compoundMeter").children().length>0)
	{
		//BuildingSel.push(true)
	}
	buildingSelection=true;
	$('#'+GroupId+' .sltbuilding').hide();
	$('#'+GroupId+' .rmbuilding').show();
	var inClass=$('#'+GroupId).parent().attr('class');
	if(inClass=='innerList')
	{
		//$('#'+GroupId).parent().parent().siblings().addClass('added');
	}
	ParentBuilding=GroupId;
	console.log(ParentBuilding)
	$('#'+GroupId+' .level').each(function(key,value){
		ParentLevel=$(value).attr('id');
		$('#'+ParentLevel+' .label').each(function(key1,value1){
			meterID=$(value1).attr('id');
			console.log(meterID)
			if($('#'+meterID).hasClass('regular'))
			{

			}
			else
			{
				var	thiData={};
				thiData.building=ParentBuilding;
				thiData.levelID=ParentLevel;
				thiData.meterID=meterID;

				if(checkID(meterID)==-1)
				{
					globalSelection.push(thiData);
					selectunit(meterID)
				}

				//$('#'+GroupId+' #'+meterID).removeClass('secondary').addClass('regular');
			}
		})
	})
	aggregateobj.mode = true;
	showAction('aggregate');
	if(aggregateobj.addmode)
		showAction('aggr2plot');
	console.log(unitObj)
}
function selectpanelunit(panelid)
{
	
	$('#c_'+panelid+' .label, #c_'+panelid+' .innerM').each(function(key,value){
		childID=$(this).attr('id');
		//console.log( childID);
		//$('#'+childID).toggleClass('regular').toggleClass('secondary')
		if($('#'+childID).hasClass('regular'))
		{
			$(this).removeClass('regular').addClass('secondary');
			
			removeunitID(childID)
		}
		else
		{
			selectunit(childID)
			
		}
	});
	refreshTray();
	showAction();
	
	console.log(unitObj)
}
function removeunitID(childID)
{
	var i = unitObj.selectedUnit.indexOf(childID);
	if(i != -1) {
		unitObj.selectedUnit.splice(i, 1);
		delete unitObj.sensorSelection[childID];
		delete unitObj.ids[childID];
		/*  -----------dma code change ------------------- */
		delete unitObj.sensorMeta[childID];
		/*  -----------dma code change ------------------- */
		if(unitObj.selectedUnit.length>=1)
		{
			meterID=unitObj.selectedUnit[unitObj.selectedUnit.length-1];
			unitObj.lastselected.meter=meterID;
			unitObj.lastselected.index=unitObj.ids[meterID];
		}
		else
		{
			unitObj.lastselected={};
		}
	}
	if(unitObj.selectedUnit.length==0)
	{
		lastknownType=null;
	}
	refreshTray();
	showBucket();
}
function checkID(childID)
{
	var i = unitObj.selectedUnit.indexOf(childID);
	return i;
}
function removeBuilding(GroupId)
{
	if($("#Building_Block .compoundMeter").children().length>0)
	{
		//BuildingSel.pop(true)
	}
	buildingSelection=false;
	$('#'+GroupId+' .sltbuilding').show();
	$('#'+GroupId+' .rmbuilding').hide();
	ParentBuilding=GroupId;
	$('#'+GroupId+' .level').each(function(key,value){
		ParentLevel=$(value).attr('id');
		$('#'+ParentLevel+' .label').each(function(key1,value1){
			meterID=$(value1).attr('id');
			if($('#'+meterID).hasClass('secondary'))
			{

			}
			else
			{
				if(!allmode)
				{
					removeunitID(meterID)
				}
				else
				{
					excludeMarkers.push(meterID)
					removeunitID(meterID)
				}
				$('#'+meterID).removeClass('regular').addClass('secondary');

			}
		})
	})
	showAction()
	console.log(unitObj)

	checkMeters();
}
// level selection event call
function selectLevel(level)
{
	$('#'+level).siblings('.whold').each(function(){
		MeterID=$(this).children('.label').attr('id');
		if($(this).children('.label').hasClass('regular'))
		{
			$(this).children('.label').removeClass('regular').addClass('secondary');
			
			removeunitID(MeterID)
		}
		else {
			//$(this).children('.label').removeClass('secondary').addClass('regular');
			selectunit(MeterID)
		}
	})
	showAction()
	refreshTray();
	
}
function checkBeforePush(unitid)
{
	var currentknowtype = null;
	unitmeta= meterInfo(unitid)
	switch(unitmeta['tag_category'])
	{
		case 'residential':
			currentknowtype='AMR';
		break;
		case 'nonresidential':
			currentknowtype='AMR';
		break;
		case 'wems':
			currentknowtype='AMR';
		break;
		case 'wems,nonresidential':
			currentknowtype='AMR';
		break;
		case 'pub_flowmeter':
			currentknowtype='FLOW';
		break;
		case 'scada':
			currentknowtype='FLOW';
		break;
		case 'weather':
			currentknowtype='WEATHER';
		break;
		case 'sensorstation':
			if(unitmeta['tag_sector']=='sensorstation')
				currentknowtype='STN';
			else
				currentknowtype='FLOW';
		break;
		default:
			currentknowtype=unitmeta['tag_category'];
		break;

	}
	lastknownType=currentknowtype;
	return currentknowtype;
	// if(lastknownType==null)
	// {
	// 	lastknownType=currentknowtype;
	// 	return true;
	// }
	// else
	// {
	// 	if(currentknowtype==lastknownType)
	// 		return true;
	// 	else
	// 		return false;
	// }
}
// unit selection event call
function selectunit(Hunit)
{
	// var todo=checkBeforePush(Hunit)
	// if(!todo)
	// {
	// 	//alert('cant select the mixed marker types, please clear the tray to start as new')
	// 	Notify('Selection Alert !','Sorry, Mixed marker selection is not possible, please clear the tray to start as new.','normal',true)
	// }
	// else
	{
		if(addplotobj.mode)
		{
			if(addplotobj.meters.indexOf(Hunit) >= 0){
				addplotobj.meters.splice(addplotobj.meters.indexOf(Hunit),1);
				refreshPlotTray(0);
			}
			else{
				addplotobj.meters.push(Hunit);
				refreshPlotTray(1,Hunit);
			}
		}
		if(aggregateobj.mode)
		{
			if(aggregateobj.meters.indexOf(Hunit) >= 0){
				aggregateobj.meters.splice(aggregateobj.meters.indexOf(Hunit),1);
				refreshPlotTray(0);
			}
			else{
				aggregateobj.meters.push(Hunit);
				refreshPlotTray(1,Hunit);
			}
		}

		var unit='#'+Hunit;
		if($(unit).hasClass('regular'))
		{
			
			$(unit).removeClass('regular').addClass('secondary');
			if(allmode)
			{
				console.log($(unit).attr('id'))
				excludeMarkers.push($(unit).attr('id'))

			}
			removeunitID(Hunit)
			showAction('unit')
			
		}
		else {
			$(unit).removeClass('secondary').addClass('regular');
			if(!allmode)
			{
				console.log(unit)
				ParentLevel=$(unit).parent().attr('id');
				ParentBuilding=$(unit).parent().parent().parent().data('grpidx');
				var	thiData={};
				thiData.building=ParentBuilding;
				thiData.levelID=ParentLevel;
				thiData.meterID=$(unit).attr('id');
				globalSelection.push(thiData);
				//showAction('unit','','add');
				addUnitObj()
			}
			
		}
		
		checkMeters();
	}
}
function checkMeters()
{
	if($("#Building_Block ._border .columns").length>0) //$("#Building_Block ._border .columns.compoundMeter").children().length
	{ 
			var build=$('#Building_Block').parent().attr('id')
			$('#Building_Block .innerM').each(function(value){ //innerMeters
				if($(this).hasClass('regular'))
				{
					BuildingSel[build]=true
				}
				else
				{
					BuildingSel[build]=false
				}
			});
	}
}
// array sorting code
function compareSecondColumn(a, b) {
    if (a[6] === b[6]) {
        return 0;
    }
    else {
        return (a[6] < b[6]) ? -1 : 1;
    }
}

// show the action panel on pageload
function showPanel()
{
		//$('#ActionPanel').show();

		var selct= getCookie("timezone");
        if(selct==''||selct==undefined)
        {
          setCookie("timezone",Parentconfig.userzone,365);
        }
		var selct= getCookie("timezone")
        _tzo.offset=moment().tz(selct)._offset;
	 _tzo.browser=moment().zone();
        console.log(_tzo.offset)
		//JSON.parse($('.actdate').text());
		datejson.etime=new Date(new Date().toUTCString()).getTime(); //new Date().getTime()+(((_tzo.offset*-1)+_tzo.browser)*60*1000);
		datejson.stime=new Date(new Date().toUTCString()).getTime()- 7 * 24 * 60 * 60 * 1000; //datejson.etime - 7 * 24 * 60 * 60 * 1000;
		datejson.et=new Date(new Date(new Date().toUTCString()).getTime()+(((_tzo.offset*-1)+_tzo.browser)*60*1000)); //new Date(new Date().getTime()+(((_tzo.offset*-1)+_tzo.browser)*60*1000));
		datejson.st=new Date(datejson.et.getTime() - 7 * 24 * 60 * 60 * 1000); //new Date(datejson.et.getTime() - 7 * 24 * 60 * 60 * 1000);
		console.log(datejson)
		st=datejson.stime//+(_tzo.offset*60*1000);
		et=datejson.etime//+(_tzo.offset*60*1000);
		var effect = 'slide';
		
	
	   // Set the options for the effect type chosen
	   var options = { direction: 'right' };
	
	   // Set the duration (default: 400 milliseconds)
	   var duration = 500;
	
	   $('#ActionPanel').show(effect, options, duration);//.toggle(effect, options, duration);
	   
	   $('#date-range1').dateRangePicker(
	   {
	   	startOfWeek: 'monday',
	   	separator : ' - ',
	   	format: 'DD/MM/YYYY HH:mm',
	   	autoClose: false,
	   	time: {
	   		enabled: true
	   	},
	   	shortcuts : 
	   		{
	   			'prev-days': [1,3,5,7],
	   			'prev': ['week','month','year'],
	   			'next-days':null,
	   			'next':null
	   		}
	   }).bind('datepicker-apply',function(event,obj)
	   {
	   		
	   		var procdate=[];
	   		var tempDates=obj.value.split(' - ');
	   		$.each(tempDates, function(key, value){
	   		 		dateSplit=value.split(' ');
	   		 		var	ts	=	dateSplit[0].split('/');
	   		 		var	ts2	=	dateSplit[1].split(':');
				var temp = new Date(ts[2],ts[1]-1,ts[0],ts2[0],ts2[1],0).getTime()-(((_tzo.offset*-1)+_tzo.browser)*60*1000)//(_tzo.offset*60*1000);

	   		 		
	   		 		procdate.push(temp);	
	   		 })
	   		 st=procdate[0];
	   		 et=procdate[1];
	   		 
	   }).bind('datepicker-close',function(obj)
	   {
		   //console.log(obj);
		   
	   });
	   $('#date-range1').data('dateRangePicker').setDateRange(datejson.st,datejson.et);

	  
}
function addUnitObj()
{
	var unitTemp=[];
		$('#Building_Block .windows, #Building_Block .innerMeters, .innerM').each(function(){
			if($(this).hasClass('regular'))
			{
					var id = $(this).attr('id');
					//unitTemp.push($(this).attr('id'));
					if(unitObj.selectedUnit.indexOf(id) < 0)
					{
						unitObj.selectedUnit.push(id);
						console.log($(this))
					}
			}
		});
		refreshTray();
		showAction('unit')
}
function removesubtrigger(childID)
{
	var i = unitObj.selectedUnit.indexOf(childID);
		if(i != -1) {
			unitObj.selectedUnit.splice(i, 1);
		}
	if(unitObj.selectedUnit.length==0)
	{
		lastknownType=null;
	}
	refreshTray();
}
//show the custom actions based on selection
function showAction(actionLevel,data,todo)
{
		subtrigger.splice(0,subtrigger.length)
		LevelFinder()
		$('#actionsList').empty();
		//$('#reportArea').empty();
		$('#actionsList').append('<div class="row borBtm"><div class="large-12 large-centered columns noPadding " id="default">');
		zonesize=settings.subzone.length
		html='<a href="#" class="button tiny radius split demanddrop" data-id="demanddrop" style="display:block; width: 100%;"><div class="" id="demandClick" style="width:100%; height:100%;">Demand</div> <span data-dropdown="drop"></span></a><br>\
				<ul id="demanddrop" class="j-dropdown" style="display:none;">\
				</ul>';
		$('#default').append(html)
		$.each(actionListObj.default,function(key, value){
			if(this.level=='default')
			{
				$('.demanddrop').show();
				if(this.type == 'button')

					$('#demanddrop').append('<div class="large-12 columns"><input type="radio" name="dailydemand" value="'+this.label+'" id="'+this.id+'" style="margin-bottom:.5rem;"><label for="'+this.id+'" style="width:auto; margin:0px;  margin-left: 5px;">'+this.label+'</label></div>');
				else
					$('#default').append('<label for="act_'+this.id+'"><input id="act_'+this.id+'" type="radio" name="opt" value="'+this.id+'" class="optionRadios"> <div class="labelspan">'+this.label+'</div></label>');

			}
		});
		$('.demanddrop span').click(function(e){
					e.stopPropagation()
					$('#demanddrop').toggle();

				})
		//zone stuff JAB-EDIT
		if(zonesize>0){
			//var sensors = [];
			_.each(zonesarr,function(zone){
				if(settings.subzone.indexOf(zone.meta.name)>=0){
					var check = '';
					if(unitObj.selectedUnit.indexOf(zone.meta._id)>=0)
						check = 'checked';
					$('#default').append('<label><input type="checkbox" class="opt_dma optionRadios" '+check+'/><div class="labelspan" data-index="'+zone.idx+'" data-id="'+zone.meta._id+'">'+zone.meta.name+'</div></label>');
					//found
					// _.each(zone.meta.sensorList,function(sen){
					// 	sensors.push(sen._source.sensortype_display);
					// });
					//selectweather(zone.meta._id,zone.idx);
				}
			});
			// sensors = _.uniq(sensors);
			// _.each(sensors,function(sen){
			// 	$('#default').append('<label><input type="checkbox" name="opt_dma" class="optionRadios opt_dma"/><div class="labelspan">'+sen+'</div></label>');
			// });
		}
		
		$('#actionsList').append('</div></div>');
		//console.log(rootSelection)
		unitLength=unitObj.selectedUnit.length;
		//console.log(unitLength)
		switch(unitLength)
		{
				case 0:
					if(!allmode)
					{
						mode=["default"]
					}
					else
					{
						mode=["default","multiple"];
					}
				break;
				case 1:
					mode=["default"]
				break;
				default:
					mode=["default","multiple"];
				break;

		}

		if($('#flowMeters_options .label.regular').size()>0)
		{
		 	rootSelection='flow';
		}
		else
		{
			if(lastSelection)
			{
				rootSelection=lastSelection; 
			}
			else
			{
				rootSelection='watersource'; 
			}
			

		}
		if($('#BuildingType #nonresidential').hasClass('regular')>0)
		{
		 	subtrigger.push('nonresidential');
		}
		// Jab Edit - Decouple Charts/Stacked Charts Option
		var dec = '';
		var stk = '';
/*		if(_decouple)
		dec = 'checked';
		if(_stacked)
		stk = 'checked';
		$('#actionsList').append('<div class="plotsettings"><label for="coupling"><input type="checkbox" id="coupling" style="margin:0 10px; display:inline-block;" '+dec+'/><div class="inline">Decouple</div></label><label for="stacking"><input type="checkbox" id="stacking" style="margin:0 10px; display:inline-block;" '+stk+'/><div class="inline">Stack</div></label></div><br/>');*/
		// Jab Edit ends
		if(!allmode&&!lossoMode&&!AMRState)
		{			
			if(unitObj.selectedUnit.length!=0)
			{
				_idx=unitObj.lastselected.index;
				/*  -----------dma code change ------------------- */
				MeterID=unitObj.lastselected.meter;
				metainfo=unitObj.sensorMeta[MeterID];//metadata[_idx];
				switch(metainfo.tag_sector)
				{
					case 'dmameter':
						$('#actionsList').append('<div class="row"><div class="large-12 large-centered columns noPadding borBtm" id="basic" style="max-height: 200px;overflow-y: auto;overflow-x: hidden;">');
						$.each(metainfo.sensorList,function(key,value){
							console.log(value)
							check='';
							_type = 'checkbox'; 
							$('#basic').append('<label for="act_'+value._id+'"><input id="act_'+value._id+'" type="'+_type+'" name="opt" value="'+value._source.sensortype_display+'" class="optionRadios" '+check+'/><div class="labelspan">'+value._source.sensortype_display+'</div></label>');
						});
					break;
					default:
						tm={}
                         tm[metainfo._id]=metainfo;
                          uniqsensorList=getUnionOFsensors(tm);
                       var sensorGroupedObj=groupByActual(uniqsensorList)
						console.log(sensorGroupedObj)
						$('#actionsList').append('<div class="row"><div class="large-12 large-centered columns noPadding borBtm" id="basic" style="max-height: 200px;overflow-y: auto;overflow-x: hidden;">');
						// $.each(actionListObj.UnitOptions,function(key, value){
						$.each(sensorGroupedObj,function(key, value){	
								 	
							

							$('#basic').append('<div class="Hdholder" id="H_'+key+'"><img src="images/expand.png" width="15px"/><label id="HeaderLabel" for="grp_'+key+'"><input id="grp_'+key+'" type="checkbox" name="groupHeader" value="'+key+'" class="optionRadios" > &nbsp;&nbsp;'+key+'</label><div class="snrGrp '+key+'" id="'+key+'"></div></div>');
								$.each(value,function(ky, vl){
									if(this.checked==true)
									{
										check='checked';
									}
									else
									{
										check='';
									}
									
									var _type = 'radio'; 
									if(this.sentype.indexOf('WEATHER')>=0 || this.sentype.indexOf('STN'>=0)) 	 	
										_type = 'checkbox'; 
									if(!vl.isderived)
											$('#basic #'+key).append('<label for="act_'+vl.sensortype_display+'"><input id="act_'+vl.sensortype_display+'" type="'+_type+'" name="opt" value="'+vl.sensortype_display+'" class="optionRadios" > &nbsp;&nbsp;'+vl.label+'</label>');
							
								});
							// $.each(metainfo.sensorList,function(key1,value1){
							// 	if(value.sensortype_display==value1._source.sensortype_display &&!value.isderived)
							// 		$('#basic').append('<label for="act_'+value.id+'"><input id="act_'+value.id+'" type="'+_type+'" name="opt" value="'+value.id+'" class="optionRadios" '+check+'/><div class="labelspan">'+value.label+'</div></label>');
							// });
							
						})
				}
				/*  -----------dma code change ------------------- */
				groupbuildIndexID=$('#'+unitObj.selectedUnit[0]).data('ind');
			}
			$('#actionsList').append('</div></div>');

			$('#actionsList').append('<div class="row"><div class="large-12 large-centered columns noPadding borBtm" id="triggerArea">');

				$.each(actionListObj.triggers,function(key, value){
					
					if(this.type.indexOf(lastknownType) > -1&&mode.indexOf(this.mode) > -1){
						//special case PRESSURE
						if(this.label == 'Pascal View' || this.label == 'netsim'){
							// check if bucket has pressure sensors
							//if(unitObj.selectedUnit.length  1){
							var killme = true;
							_.each(unitObj.selectedUnit,function(val,idx){
								var meta = unitObj.sensorMeta[unitObj.selectedUnit[idx]]; 
									for(var i=0;i<meta.sensorList.length;i++){
										if(meta.sensorList[i]._source.sensortype_actual == 'pressure')
											killme = false;
									}
							});
							if(!killme)
								$('#actionsList').append(actionDiv(this.id,this.label));
						}
						else
						$('#actionsList').append(actionDiv(this.id,this.label));
				}
			});
				if(window.hasOwnProperty('coveragearr') && window.coveragearr.pipes.length>0)
					$('#actionsList').append(actionDiv('coverage','Get HighRes'));
	
				if(window.hasOwnProperty('map_context') && map_context!=null)
					$('#actionsList').append(actionDiv('mapcontext','Clear Map'));
			$('#actionsList').append('</div></div>');
		}
		else
		{
			$('#basic').empty()
			if(unitObj.selectedUnit.length!=0)
			{
			$('#actionsList').append('<div class="row"><div class="large-12 large-centered columns noPadding borBtm" id="basic" style="max-height: 200px;overflow-y: auto;overflow-x: hidden;">');
			if(settings.subzone.length>0&&settings.subzone.length<=1)
			{
				// firstKey=Object.keys(unitObj.sensorMeta)[0];
				var firstKey
				dmaMeta=dma.requestmeta();
				$.each(dmaMeta,function(kt,vt){
					if(vt.tag_sector=='dmameter')
					{
						firstKey=kt;
						return false;
					}
				})
				metainfo=dmaMeta[firstKey];

				$.each(metainfo.sensorList,function(key,value){
								console.log(value)
								check='';
								_type = 'checkbox'; 
								$('#basic').append('<label for="act_'+value._id+'"><input id="act_'+value._id+'" type="'+_type+'" name="opt" value="'+value._source.sensortype_display+'" class="optionRadios" '+check+'/><div class="labelspan">'+value._source.sensortype_display+'</div></label>');
							});
				$('#basic').append('<hr style="margin-top:0.2rem !important; margin-bottom: 1rem !important;">');
			}
			else
			{
				
			}
			uniqsensorList=getUnionOFsensors(unitObj.sensorMeta);
			var sensorGroupedObj=groupByActual(uniqsensorList)
			$.each(sensorGroupedObj,function(key,value){
				$('#basic').append('<div class="Hdholder" id="H_'+key+'"><img src="images/expand.png" width="15px"/><label id="HeaderLabel" for="grp_'+key+'"><input id="grp_'+key+'" type="checkbox" name="groupHeader" value="'+key+'" class="optionRadios" > &nbsp;&nbsp;'+key+'</label><div class="snrGrp '+key+'" id="'+key+'"></div></div>');
				$.each(value,function(ky, vl){
					_type = 'checkbox';
					if(!vl.isderived)
							$('#basic #'+key).append('<label for="act_'+vl.sensortype_display+'"><input id="act_'+vl.sensortype_display+'" type="'+_type+'" name="opt" value="'+vl.sensortype_display+'" class="optionRadios" > &nbsp;&nbsp;'+vl.label+'</label>');
			
				});
			})
			// $.each(uniqsensorList,function(key, value){
			// 		//console.log(this.type.indexOf(rootSelection))
			// 		// if(this.checked==true)
			// 		// {
			// 		// 	check='checked';
			// 		// }
			// 		// else
			// 		// {
			// 		// 	check='';
			// 		// }
			// 		// var _type = 'radio'; 	 	
			// 		// if(this.type.indexOf('WEATHER')>=0 || this.type.indexOf('STN'>=0)) 	 	
			// 			_type = 'checkbox';

			// 		// if(this.type.indexOf(lastknownType) > -1)//
			// 			//$('#actionsList').append('<a href="#" class="button small expand cbtn" id="'+this.id+'">'+this.label+'</a>');
			// 			if(!value.isderived)
			// 				$('#basic').append('<label for="act_'+value.sensortype_display+'"><input id="act_'+value.sensortype_display+'" type="'+_type+'" name="opt" value="'+value.sensortype_display+'" class="optionRadios" > &nbsp;&nbsp;'+value.label+'</label>');
			// 	})
				groupbuildIndexID=$('#'+unitObj.selectedUnit[0]).data('ind');
			
			$('#actionsList').append('</div></div>');
		}
			$('#actionsList').append('<div class="row"><div class="large-12 large-centered columns noPadding borBtm" id="triggerArea">');

				$.each(actionListObj.triggers,function(key, value){
					
					if(this.type.indexOf(lastknownType) > -1&&mode.indexOf(this.mode) > -1){
						//special case PRESSURE
						if(this.label == 'Pascal View' || this.label == 'netsim'){
							// check if bucket has pressure sensors
							//if(unitObj.selectedUnit.length  1){
							var killme = true;
							_.each(unitObj.selectedUnit,function(val,idx){
								var meta = unitObj.sensorMeta[unitObj.selectedUnit[idx]]; 
									for(var i=0;i<meta.sensorList.length;i++){
										if(meta.sensorList[i]._source.sensortype_actual == 'pressure')
											killme = false;
									}
							});
							if(!killme)
								$('#actionsList').append(actionDiv(this.id,this.label));
						}
						else
						$('#actionsList').append(actionDiv(this.id,this.label));
				}
			});
			
			$('#actionsList').append('</div></div>');


		}
		var setmeup=[];
		lent=$('#ActionPanel #basic input[name=opt]').size();
		if(lent==1)
		{
			$('#ActionPanel #basic input[name=opt]').each(function(){
				
				sen=$(this).val();
				setmeup.push(sen)
			});	
		}	
		else
		{
			$('#ActionPanel #basic input[name=opt]').each(function(){
				// if($(this).attr('type')!=thistype)
				// 	$(this).prop('checked',false);
				sen=$(this).val();
				if(jQuery.inArray(sen,defsensor)>-1&&lent>1)
				{
					setmeup.push(sen)
					return;
				}
			});	
		}
		$.each(setmeup,function(key,value){
			
			// $('#ActionPanel #'+value).prop('checked',true);
			ID=unitObj.selectedUnit[unitObj.selectedUnit.length-1];
			if(ID in unitObj.sensorSelection)
			{
				// if(jQuery.inArray(value,unitObj.sensorSelection[ID])==-1)
				// 	unitObj.sensorSelection[ID].push(value);
			}
			else
			{
				unitObj.sensorSelection[ID]=[];
				// unitObj.sensorSelection[ID].push(value);
			}
			showBucket()
		});
		//reportPermission
		// $('#reportArea').append('<div class="row"><div><a href="#" >Report</a></div><div class="large-12 large-centered columns noPadding borBtm" id="triggerArea">');
  //        console.log(reportPermission)                 
  //       $.each(actionListObj.reports,function(key, value){
                              
  //       if(this.type.indexOf(lastknownType) > -1&&mode.indexOf(this.mode) > -1)
  //             $('#reportArea').append('<a href="#" class="button small expand cbtn" id="'+this.id+'">'+this.label+'</a>');
  //       })

  //       $('#reportArea').append('</div></div>');
		// _loadgraphscript();
		
		if(reportPermission)
		{
			$('#reportArea1 a.cbtn').hide();
			$.each(reportPermission,function(key, value){
	                              
	        	if(value.type.indexOf(lastknownType) > -1&&mode.indexOf(this.mode) > -1)
	             	$('#'+value.alias).show();
	        })
		}
		if(lastknownType=='AMR')
		{
			
		}



	}

//array reverse code
function _reverse(GroupData) {
	var ar=[];
	var k=0;
	for(i=(GroupData.length-1);i>=0;i--)
	{
		//console.log(GroupData[i])
		ar[k]=GroupData[i];
		k++; 
	}
	return ar;
}

var sortObjectByKey = function(obj){
    var keys = [];
    var objdata=[];

    var sorted_obj = {};

    for(var key in obj){
        if(obj.hasOwnProperty(key)){
            keys.push(key);
        }
    }

    // sort keys
    keys.sort();

    // create new array based on Sorted Keys
    jQuery.each(keys, function(i, key){
        sorted_obj[key] = obj[key];
        var temp={};
        temp[key]=obj[key];
        objdata.push(temp)
    });
    return objdata;
};
function selectweather(MeterID,idx)
{
	removeEmpty();
	var todo=checkBeforePush(MeterID)
	// if(!todo)
	// {
	// 	//alert('cant select the mixed marker types, please clear the tray to start as new')
	// 	Notify('Selection Alert !','Sorry, Mixed marker selection is not possible, please clear the tray to start as new.','normal',true)
	// }
	// else
	{
		var unit='#'+MeterID;
		if($(unit).hasClass('regular') || unitObj.selectedUnit.indexOf(MeterID)>=0)
		{
			
			$(unit).removeClass('regular').addClass('secondary');
			removeunitID(MeterID)
			showAction('unit');
			refreshTray();
		}
		else
		{
			//weatherobj.meters.push(MeterID);
			unitObj.selectedUnit.push(MeterID);
			unitObj.sensorSelection[MeterID]=[];
			unitObj.sensorMeta[MeterID]=metadata[idx]
			//unitObj.ids[MeterID]=idx;
			unitObj.lastselected.meter=MeterID;
			unitObj.lastselected.index=idx;
			showAction('unit');
			$(unit).removeClass('secondary').addClass('regular');
			refreshTray();
		}
	}
}
function selectAMR(MeterID,idx)
{
       
        var todo=checkBeforePush(MeterID)
        var unit='#'+MeterID;
        if($(unit).hasClass('regular') || unitObj.selectedUnit.indexOf(MeterID)>=0)
        {

                $(unit).removeClass('regular').addClass('secondary');
                removeunitID(MeterID)
                showAction('unit');
                refreshTray();
        }
        else
        {
                unitObj.selectedUnit.push(MeterID);
                unitObj.sensorSelection[MeterID]=[];
                       
                unitObj.sensorMeta[MeterID]=metadata[idx];
                unitObj.lastselected.meter=MeterID;
                unitObj.lastselected.index=idx;
                showAction('unit');
                $(unit).removeClass('secondary').addClass('regular');
                refreshTray();
        }
}
function selectStation(MeterID)
{
	removeEmpty();
	// var todo=checkBeforePush(MeterID)
	// if(!todo)
	// {
	// 	Notify('Selection Alert !','Sorry, Mixed marker selection is not possible, please clear the tray to start as new.','normal',true)
	// }
	// else
	{
		var unit='#'+MeterID;
		if($(unit).hasClass('regular'))
		{
			
			$(unit).removeClass('regular').addClass('secondary');
			removeunitID(MeterID)
			showAction('unit');
			refreshTray();
		}
		else
		{
			unitObj.selectedUnit = weatherobj.meters;
			unitObj.ids={}
			unitObj.ids.MeterID=$('#'+MeterID).data('ind');
			showAction('unit');
			$(unit).removeClass('secondary').addClass('regular');
			refreshTray();
		}
	}
}

function createMarker(name, latlng,marker,icontype) {
	var contentString;
	google.maps.event.addListener(marker, 'mouseover', function() {
		  MetaInfo=metadata[this.metaindex]
		  senText=this.Markername;
		  i=1;
		  senText+='<br /><div class="senlist" style="margin-top:4px;"><b>Sensor List</b> <hr style="padding:2px; margin:2px;">';
		if(!('sensorList' in MetaInfo))
		{
			console.log(MetaInfo)
		}	
		else
		{
		  $.each(MetaInfo.sensorList,function(key,value){
		  		if(!value._source.isderived)
		  		{
		  			senText+='<br />'+i+': '+value._source.sensortype_display;
		  			i++;
		  		}
		  })
		} 
		 senText+='</div>';
		  this.set('labelContent', senText);
		  this.set('labelClass', 'mLabls');
	});

	google.maps.event.addListener(marker, 'mouseout', function() {

		// console.log(this)
		this.set('labelContent', '');
		this.set('labelClass', '');
	});
	 google.maps.event.addListener(marker, "click", function() {
	/* 	for(var i=0; i<markers.length; i++)
	 		markers[i].setTitle('none');	*/
	 		console.log(this.groupindex)
	 if(allmode)
	 	stopAnimation(this);
	 //toggleBounce(this);
	 infowindowstatus=true;
	if(icontype == 'm')
	{
		AMRState=true;
		gidx=this.groupindex;
		grList=group[gidx];
		removeAllEmpty();
		$.each(grList,function(key,value){
			metaIdx=value[3];
			MID=metadata[metaIdx]._id;
			if(unitObj.selectedUnit.indexOf(MID)==-1)
                   	     selectAMR(MID,metaIdx);
		});
		//MID=metadata[marker.metaindex]._id;
		//console.log(metadata[marker.metaindex])
	//	if(!allmode)
	//	 	showAction('block');
		 contentString=generateHTML(this.groupindex);
	//	if(unitObj.selectedUnit.indexOf(MID)==-1)
	//		selectweather(MID,marker.metaindex);
  	}
  	if(icontype == 'w')
  	{
		AMRState=false;
  			MID=metadata[marker.metaindex]._id;
			console.log(metadata[marker.metaindex])  			
  			if(unitObj.selectedUnit.indexOf(MID) > -1)
  			{
  				css='regular'
  			}
  			else
  			{
  				css='secondary'
  			}
  			contentString = '<div style="width:120px;height:150px;padding:5px;"><h5>ID '+metadata[marker.metaindex]._id+'</h5><br/><b>Location: </b>'+metadata[marker.metaindex].location+'<br /><br /><div class="info"><img src="images/info_b.png" width="15px" onclick="showMetaUser(\''+MID+'\')"></div>'; 
  			// contentString = '<div style="width:120px;height:150px;padding:5px;"><h5>ID '+metadata[marker.metaindex]._id+'</h5><br/><b>Location: </b>'+metadata[marker.metaindex].location+'<br /><div class="addweather label '+css+'" id="'+MID+'" onclick="selectweather(\''+MID+'\')" data-ind='+this.groupindex+'>Add Weather</div> <br /><div class="info"><img src="images/info_b.png" width="15px" onclick="showMetaUser(\''+MID+'\')"></div>';
  			if(unitObj.selectedUnit.indexOf(MID)==-1)
  				selectweather(MID,marker.metaindex);

  	}
  	if(icontype == 's'||icontype=='sc')
  	{
		AMRState=false;
  			MID=metadata[marker.metaindex]._id;
  			if(unitObj.selectedUnit.indexOf(MID) > -1)
  			{
  				css='regular'
  			}
  			else
  			{
  				css='secondary'
  			}
  			var contentString = '<div style="width:250px; max-height:300px; height:auto;" class="Buildblock" id="Build_'+metadata[marker.metaindex]._id+'" data-grpIdx='+metadata[marker.metaindex]._id+'><div id="siteNotice"></div><div id="Building_Block" class="stationBlock" style="min-height:auto; width: 100%;">';
			
	lat=metadata[marker.metaindex].loc.lat||metadata[marker.metaindex].loc[0].lat;
	lng=metadata[marker.metaindex].loc.lng||metadata[marker.metaindex].loc[0].lng;
	elev=metadata[marker.metaindex].elevation||'NA';
	name=metadata[marker.metaindex].display_name||metadata[marker.metaindex].name||metadata[marker.metaindex]._id;
  			contentString += '<div class="stnTitle">Station<h4 style="margin-top: -7px; font-size:18px;">'+name+'</h4></div><div class="LocationBox"><div class="loclabel">Location:</div><div class="locContent"> Lat: '+lat+'<br />Lng: '+lng+'</div></div><div class="ElevationBox"><div class="loclabel">Elevation:</div><div class="locContent"> '+elev+'</div></div><div class="actArea" onclick="showMetaUser(\''+MID+'\')"><div class="addstation Lblbtn " id="'+MID+'" >Show Information</div><div class="info" ><img src="images/info_b.png" width="15px" ></div></div></div>'; 
  			// contentString += '<div class="stnTitle">Station<h4 style="margin-top: -10px;">'+metadata[marker.metaindex]._id+'</h4></div><div class="LocationBox"><div class="loclabel">Location:</div><div class="locContent"> Lat: '+lat+'<br />Lng: '+lng+'</div></div><div class="actArea"><div class="addstation Lblbtn '+css+'" id="'+MID+'" onclick="selectweather(\''+MID+'\')" data-ind='+this.groupindex+'>Add Station</div><div class="info" onclick="showMetaUser(\''+MID+'\')"><img src="images/info_b.png" width="15px" ></div></div></div>';
  			if(unitObj.selectedUnit.indexOf(MID)==-1)
				selectweather(MID,marker.metaindex);
  			//showAction();

  	}
  	if(icontype == 'f')
  	{
		AMRState=false;
  			MID=metadata[marker.metaindex]._id;
  			if(unitObj.selectedUnit.indexOf(MID) > -1)
  			{
  				css='regular'
  			}
  			else
  			{
  				css='secondary'
  			}
  			var contentString = '<div style="width:250px; max-height:300px; height:auto;" class="Buildblock" id="Build_'+metadata[marker.metaindex]._id+'" data-grpIdx='+metadata[marker.metaindex]._id+'><div id="siteNotice"></div><div id="Building_Block" class="stationBlock" style="min-height:auto; width: 100%;">';
			
	lat=metadata[marker.metaindex].loc.lat||metadata[marker.metaindex].loc[0].lat;
	lng=metadata[marker.metaindex].loc.lng||metadata[marker.metaindex].loc[0].lng;
	elev=metadata[marker.metaindex].Elevation||'NA';
	stnName=metadata[marker.metaindex].display_name||metadata[marker.metaindex].display_id||metadata[marker.metaindex].display_name;
  			contentString += '<div class="stnTitle">Station<h4 style="margin-top: -7px; font-size:18px;">'+stnName+'</h4></div><div class="LocationBox"><div class="loclabel">Location:</div><div class="locContent"> Lat: '+lat+'<br />Lng: '+lng+'</div></div><div class="ElevationBox"><div class="loclabel">Elevation:</div><div class="locContent"> '+elev+'</div></div><div class="actArea" onclick="showMetaUser(\''+MID+'\')"><div class="addstation Lblbtn " id="'+MID+'">Show Information</div><div class="info" ><img src="images/info_b.png" width="15px" ></div></div></div>'; 
  			// contentString += '<div class="stnTitle">Station<h4 style="margin-top: -8px; font-size:18px;">'+stnName+'</h4></div><div class="LocationBox"><div class="loclabel">Location:</div><div class="locContent"> Lat: '+lat+'<br />Lng: '+lng+'</div></div><div class="actArea"><div class="addstation Lblbtn '+css+'" id="'+MID+'" onclick="selectweather(\''+MID+'\')" data-ind='+this.groupindex+'>Add Station</div><div class="info" onclick="showMetaUser(\''+MID+'\')"><img src="images/info_b.png" width="15px" ></div></div></div>';
			if(unitObj.selectedUnit.indexOf(MID)==-1)
				selectweather(MID,marker.metaindex);
  			//showAction();

  	}

  	console.log(icontype)
      if (infowindow) 
      {
      	infowindow.close();
      	console.log('closed')
      }
      infowindow = new google.maps.InfoWindow({content: contentString, maxWidth: 500});
      infowindow.open(map, marker);
      
    });
    
    return marker;
}

function generateHTML2(type,indexID){
	var gdata = group[indexID];
	var thismeta = metadata[gdata[0][3]];
	var site = thismeta.site || '';
	var address = thismeta.address || '';
	var contentString = '<div style="width:500px; max-height:200px; height:auto;" class="Buildblock" id="Build_'+indexID+'" data-grpIdx='+indexID+'><div id="siteNotice"></div><div id="Building_Block" class="buildingBlock" style="min-height:auto; width: 100%;">';
	contentString+= '<div class="button tiny" onclick="newSelection(0)">Premise</div><div class="button tiny" onClick="newSelection(1)">Bulks</div><div class="button tiny" onClick="newSelection(2)">Privates</div>';
	var solved = []; // keep track of solved bulks
	var sites=[]; // Premise
	var bulks = [];	// Bulks
	var bulks_backend = [];
	var meters = [];
	var privates = [] // Private Meters
	var privates_backend = [];
	var b_wtype = []; // WaterType
	var b_accnt = []; // Accounts
	var b_trueid = []; // MI IDs
	var b_trueid_backend = [];
	var premise = []; // premise address
	var b_index = [];
	var p_index = [];
	var p_loc = [];
	var p_wtype = [];
	var c_type = [];
	_.each(gdata,function(el){
		var mt = metadata[el[3]];
		meters.push({"id":mt.meterid,"dev":mt.device,'ind':el[3],'bid':mt._id});
		//c_type.push(mt.component_type||null);
		if(solved.indexOf(mt._id)>=0 || solved.indexOf(mt.meterid)>=0 || solved.indexOf(mt.device)>=0)
			return;
		sites.push(mt.site||mt.bp_name||mt.customer_name||'');
		premise.push(mt.premise_address||'');
		var isBulk = false;
		if(mt.hasOwnProperty('component_id_new') && mt.component_id_new != null && mt.component_id_new.split('_')[1]!='null')
		{
			var blk = mt.component_id_new.split('_');
			solved.push(blk[0]); solved.push(blk[1]);
			var thistype = [null,null];
			var thisid = [null,null];
			var thisindex = [null,null];
			if(blk[0]==mt._id){
				thistype[0] = mt.component_type;
			}
			else {

				if(blk[1]==mt._id){
				thistype[1] = mt.component_type;
			}
				else
					//console.log('ERR - True IDs');
					throw new Error("- True IDs");
			}
			b_trueid.push(thisid);
			b_trueid_backend.push(thisid);
			c_type.push(thistype);
			bulks.push(blk);
			bulks_backend.push(blk);
			b_wtype.push(mt.tag_watertype);
			b_accnt.push(mt.tru_ca);
			b_index.push(thisindex);
		}
		else
		{
			privates.push(mt.meterid);
			privates_backend.push(mt._id);
			p_wtype.push(mt.tag_watertype);
			p_index.push(el[3]);
			if(mt.hasOwnProperty('tag_category') && mt.tag_category=='pub_flowmeter')
			{
				sites[sites.length-1]='Flow Meter';
				p_loc.push(mt.meterid);
			}
			else
				p_loc.push(mt.customer_name||'P');

		}
	});
	// solve bulk types
	_.each(c_type,function(e){
		var x=null,y=null;
		if(e[0] == null)
			x = e[1];
		else
			x = e[0];
		if(x == 'master')
			y = 'sub';
		else
			y = 'master';
		if(e[0] == null)
			e[0] = y;
		else
			e[1] = y;
	});
	// find true ids
	_.each(bulks,function(e,r){
		var temp = [null,null];
		_.each(e,function(f,g){
			_.each(meters,function(m){
				if(m.bid == f)
				{
					b_trueid[r][g] = m.id;
					b_index[r][g] = m.ind;
					b_trueid_backend[r][g] = m.bid;
					bulks_backend[r][g] = m.bid;
					return;
				}
			})
		});
	});
	// check for legit 
	sites = _.compact(_.uniq(sites));
	premise = _.compact(_.uniq(premise));
	var premiseadd = premise[0]||'';
	var totalaccounts = _.uniq(b_accnt);
	if(site == '')
		site = sites[0] || '';
	if(sites.length>1)
		site += ' (*)';	// Make sure site is unique, otherwise state.
	contentString += '<div class="right newtitle">'+site;
	if(premiseadd.length>1)
		premiseadd += '(*)';
	contentString += '<br/><small>'+premiseadd+'</small></div>';
	var html = '';
	var wtest = _.uniq(b_wtype.concat(p_wtype));
	for(var u=0;u<wtest.length;u++)
	{
		var len = parseInt(12 / (wtest.length));
		var wclass,wterm;
		if(wtest[u] != 'newwater' && wtest[u] != 'potable'){
			wclass = '_others';
			wterm = 'OTHR';
		}
		else{
			wterm = wtest[u].charAt(0).toUpperCase()+'W';
			wclass = '_'+wtest[u].toLowerCase();
		}

		html += '<div class="medium-'+len+' columns wclm text-center"><div class="circular left '+wclass+'" onClick="newSelection(3,\''+wclass+'\')" style="cursor:pointer">'+wterm+'</div><br/>';
		for(var t=0;t<totalaccounts.length;t++){
			if(typeof(totalaccounts[t]) == 'undefined')
				continue;
			// first occurence
			var first = b_accnt.indexOf(totalaccounts[t]);
			_.each(b_accnt,function(ac,ic){
				if(ac == totalaccounts[t] && b_wtype[ic] == wtest[u]){
					if(ic == first)
					html +='<div class="accnttype">'+totalaccounts[t]+'</div><div class="_connector"></div>';
				else
					html +='<div class="_connector"></div>';
				html += '<div class="level row _border text-center" id="c_'+bulks[ic].toString().replace(',','_')+'"><div class="right smallbtn" onClick="selectpanelunit(\''+bulks[ic].toString().replace(',','_')+'\')"></div>';
				_.each(bulks[ic],function(r,rr){
					var themtypes = c_type[ic];
					var txt = themtypes[rr].charAt(0).toUpperCase();
					if(txt=='S')
						txt='B';	// nomenclature to ByPass
					var glow = 'secondary';
					if(unitObj.selectedUnit.indexOf(b_trueid[ic][rr])>=0)
						glow = 'regular';
					html +='<div class="medium-12 columns"><div id="'+b_trueid_backend[ic][rr]+'" class="innerM '+glow+' medium-8 columns" id="'+bulks_backend[ic].toString().replace(',','_')+'" onclick="selectAMR(\''+b_trueid_backend[ic][rr]+'\' ,'+b_index[ic][rr]+')" data-ind="'+b_index[ic][rr]+'">'+txt+'</div><img class="right" style="margin-top:10px;" src="images/info_b.png" width="15px" onclick="showMetaUser(\''+b_trueid_backend[ic][rr]+'\' )"></div>';
				});
				html += '</div>';
				}
			});	
		}
		if(bulks.length>0)
			html += '<br/>';
		html += '<strong>Private Meters</strong><br/>';
			_.each(privates,function(p,pp){
				if(p_wtype[pp] == wtest[u]){
					var glow = 'secondary';
					if(unitObj.selectedUnit.indexOf(p)>=0)
						glow = 'regular';
					var loc = p_loc[pp].toString().split(',');
						html +='<div class="medium-12 columns level_p"><div id="'+privates_backend[pp]+'" class="innerM '+glow+' medium-8 columns truncate" onclick="selectAMR(\''+privates_backend[pp]+'\', '+p_index[pp]+')" data-ind="'+p_index[pp]+'" title="'+loc+'">'+loc+'</div><img class="right" style="margin-top:10px;" src="images/info_b.png" width="15px" onclick="showMetaUser(\''+privates_backend[pp]+'\' )"></div>';
					}
			});
		html += '</div>';
	}
	return contentString+html+'</div></div>';
}


function generateHTML3(type,indexID){
	var gdata = group[indexID];
	var thismeta = metadata[gdata[0][3]];
	var site = thismeta.bp_name || thismeta.customer_name || thismeta.customername || '';
	var address = thismeta.address || 'N.A';
	var contentString = '<div style="width:500px; max-height:200px; height:auto;" class="Buildblock" id="Build_'+indexID+'" data-grpIdx='+indexID+'><div id="siteNotice"></div><div id="Building_Block" class="buildingBlock" style="min-height:auto; width: 100%;">';
	contentString+= '<div class="button tiny" onclick="newSelection(0)">Premise</div><div class="button tiny" onClick="newSelection(1)">Bulks</div><div class="button tiny" onClick="newSelection(2)">Privates</div>';
	var solved = []; // keep track of solved bulks
	var sites=[]; // Premise
	var bulks = [];	// Bulks
	var meters = [];
	var privates = [] // Private Meters
	var b_wtype = []; // WaterType
	var b_accnt = []; // Accounts
	var b_trueid = []; // MI IDs
	var premise = [];
	var b_index = [];
	var p_loc = [];
	var p_index = [];
	var p_wtype = [];
	var c_type = [];
	var handy = [];
	_.each(gdata,function(g){ // keep a list of meters handy
		handy.push(g[2]);
	});
	_.each(gdata,function(el){
		var mt = metadata[el[3]];
		meters.push({"id":mt.meterid,"dev":mt.device,'ind':el[3]});
		//c_type.push(mt.component_type||null);
		if(solved.indexOf(mt.meterid)>=0 || solved.indexOf(mt.device)>=0)
			return;
		sites.push(thismeta.bp_name || thismeta.customer_name || thismeta.customername || '');
		premise.push(thismeta.premise_address||'');
		var isBulk = false;
		if(mt.hasOwnProperty('component_id_new') && handy.indexOf(mt.meterid)>=0)
		{
			var blk = mt.component_id_new.split('_');
			solved.push(blk[0]); solved.push(blk[1]);
			var thistype = [null,null];
			var thisid = [null,null];
			var thisindex = [null,null];
			if(blk[0]==mt.meterid){
				thistype[0] = mt.component_type;
			}
			else {

				if(blk[1]==mt.meterid){
				thistype[1] = mt.component_type;
			}
				else
					//console.log('ERR - True IDs');
					throw new Error("- True IDs");
			}
			b_trueid.push(thisid);
			c_type.push(thistype);
			bulks.push(blk);
			b_wtype.push(mt.tag_watertype);
			b_accnt.push(mt.tru_ca);
			b_index.push(thisindex);
		}
		else
		{
			privates.push(mt.meterid);
			p_wtype.push(mt.tag_watertype);
			p_index.push(el[3]);
			if(mt.hasOwnProperty('tag_category') && mt.tag_category=='pub_flowmeter')
			{
				sites[sites.length-1]='Flow Meter';
				premise[premise.length-1]=mt.location||'';
				site='Flow Meter';
				p_loc.push(mt.meterid);
			}
			else
				p_loc.push('P');
		}
	});
	// solve bulk types
	_.each(c_type,function(e){
		var x=null,y=null;
		if(e[0] == null)
			x = e[1];
		else
			x = e[0];
		if(x == 'master')
			y = 'sub';
		else
			y = 'master';
		if(e[0] == null)
			e[0] = y;
		else
			e[1] = y;
	});
	// find true ids
	_.each(bulks,function(e,r){
		_.each(e,function(f,g){
			_.each(meters,function(m){
				if(m.id == f)
				{
					b_trueid[r][g] = m.id;
					b_index[r][g] = m.ind;
					return;
				}
			})
		});
	});
	// check for legit 
	sites = _.compact(_.uniq(sites));
	premise = _.compact(_.uniq(premise));
	var premiseadd = premise[0]||'';
	var totalaccounts = _.uniq(b_accnt);
	if(site == '')
		site = sites[0] || '';
	if(sites.length>1)
		site += ' (*)';	// Make sure site is unique, otherwise state.
	contentString += '<div class="right newtitle">'+site;
	if(premise.length>1)
		premiseadd += '(*)';
	contentString += '<br/><small>'+premiseadd+'</small></div>';
	var html = '';
	var wtest = _.uniq(b_wtype.concat(p_wtype));
	for(var u=0;u<wtest.length;u++)
	{
		var len = parseInt(12 / (wtest.length));
		var wclass,wterm;
		if(wtest[u] != 'newwater' && wtest[u] != 'potable'){
			wclass = '_others';
			wterm = 'OTHR';
		}
		else{
			wterm = wtest[u].charAt(0).toUpperCase()+'W';
			wclass = '_'+wtest[u].toLowerCase();
		}

		html += '<div class="medium-'+len+' columns wclm text-center"><div class="circular left '+wclass+'" onClick="newSelection(3,\''+wclass+'\')" style="cursor:pointer">'+wterm+'</div><br/>';
		for(var t=0;t<totalaccounts.length;t++){
			if(typeof(totalaccounts[t]) == 'undefined')
				continue;
			// first occurence
			var first = b_accnt.indexOf(totalaccounts[t]);
			_.each(b_accnt,function(ac,ic){
				if(ac == totalaccounts[t] && b_wtype[ic] == wtest[u]){
					if(ic == first)
					html +='<div class="accnttype">'+totalaccounts[t]+'</div><div class="_connector"></div>';
				else
					html +='<div class="_connector"></div>';
				html += '<div class="level row _border text-center" id="c_'+bulks[ic].toString().replace(',','_')+'"><div class="right smallbtn" onClick="selectpanelunit(\''+bulks[ic].toString().replace(',','_')+'\')"></div>';
				_.each(bulks[ic],function(r,rr){
					var themtypes = c_type[ic];
					var txt = themtypes[rr].charAt(0).toUpperCase();
					if(txt=='S')
						txt='B';	// nomenclature to ByPass
					var glow = 'secondary';
					if(unitObj.selectedUnit.indexOf(b_trueid[ic][rr])>=0)
						glow = 'regular';
					html +='<div class="medium-12 columns"><div id="'+b_trueid[ic][rr]+'" class="innerM '+glow+' medium-8 columns" id="'+bulks[ic].toString().replace(',','_')+'" onclick="selectAMR(\''+b_trueid[ic][rr]+'\','+b_index[ic][rr]+' )" data-ind="'+b_index[ic][rr]+'">'+txt+'</div><img class="right" style="margin-top:10px;" src="images/info_b.png" width="15px" onclick="showMetaUser(\''+b_trueid[ic][rr]+'\' )"></div>';
				});
				html += '</div>';
				}
			});	
		}
		if(bulks.length>0)
			html += '<br/>';
		html += '<strong>Single Meters</strong><br/>';
			_.each(privates,function(p,pp){
				if(p_wtype[pp] == wtest[u]){
					var glow = 'secondary';
					if(unitObj.selectedUnit.indexOf(p)>=0)
						glow = 'regular';
						html +='<div class="medium-12 columns level_p"><div id="'+p+'" class="innerM '+glow+' medium-8 columns" onclick="selectAMR(\''+p+'\','+p_index[pp]+' )" data-ind="'+p_index[pp]+'">'+p_loc[pp]+'</div><img class="right" style="margin-top:10px;" src="images/info_b.png" width="15px" onclick="showMetaUser(\''+p+'\' )"></div>';
					}
			});
		html += '</div>';
	}
	return contentString+html+'</div></div>';
}
function newSelection(x,y){
	y = y || null;
	switch(x){

		case 0: //ALL
			$('.innerM').each(function(key,value){
				childID=$(this).attr('id');
				if($('#'+childID).hasClass('regular'))
				{
					$(this).removeClass('regular').addClass('secondary');
					removeunitID(childID)
				}
				else
					// selectunit(childID);
					selectAMR(childID,idx)
			});
		break;

		case 1: //BULKS
		$('.level .innerM').each(function(key,value){
				childID=$(this).attr('id');
				idx=$(this).data('ind');
				if($('#'+childID).hasClass('regular'))
				{
					$(this).removeClass('regular').addClass('secondary');
					removeunitID(childID)
				}
				else
				{
					// selectunit(childID);
					selectAMR(childID,idx)
				}
			});
		break;

		case 2: //PRIVATES
		$('.level_p .innerM').each(function(key,value){
				childID=$(this).attr('id');
				idx=$(this).data('ind');
				if($('#'+childID).hasClass('regular'))
				{
					$(this).removeClass('regular').addClass('secondary');
					removeunitID(childID)
				}
				else
				{
					// selectunit(childID)
					selectAMR(childID,idx)
				}
			});
		break;
		case 3: // WTYPE
		var el = $('.wclm .'+y).parent('.wclm');
		$(el).find('.innerM').each(function(key,value){
				childID=$(this).attr('id');
				idx=$(this).data('ind');
				if($('#'+childID).hasClass('regular'))
				{
					$(this).removeClass('regular').addClass('secondary');
					removeunitID(childID)
				}
				else
				{
					// selectunit(childID,idx)
					selectAMR(childID,idx)
				}
			});
		break;
	}
}

function generateHTML(indexID)
{
	 var floorwisedata=[];
	 var temp={};
	 var templan={};
	 var tempblock;	
 	 var Typeis='';
	 var GroupData=group[indexID].slice();
	 groupIndexID=this.groupindex;
	 GroupData.sort(compareSecondColumn);
	 
     $.each(GroupData, function(key,value){

      		if(value[7]=='wems' || value[7]=='wems,nonresidential' || value[7]=='nonresidential')
      		{
      			Typeis='wems';
      		}
      		else if(value[7]=='pub_flowmeter')
      		{
      			Typeis='flowmeter';
      		}
      		else {
      		
	      		if(value[5].indexOf('residential')==0)
	      		{
	      			Typeis='residential';
	      		}
	      		
	      		else {
	      			Typeis='OTHERS';
	      		}
      		}
	 });
	 // //console.log(resident)
	 // if(!allmode)
	 // {
	 // 	// addthis='<div class="SelectBuilding cname1 sltbuilding" onclick="selectBuilding(\'Build_'+indexID+'\')"> + </div>';
	 // 	addthis='<div class=""><div class="SelectAreaBlock"><div class="SelectBuilding cname1 rmbuilding" style="display:none;" onclick="removeBuilding(\'Build_'+indexID+'\')"> - </div></div></div>';
	 
	 // }
	 // else
	 {
	 	addthis='<div class="TopBlkArea" id="TopBlkArea"><div class="SelectAreaBlock" style="  width: 15%; float: left;"><div class="SelectBuilding cname1" onclick="removeBuilding(\'Build_'+indexID+'\')"> REMOVE </div></div><div class="adddrInfoArea" id="adddrInfoArea">';
	 }
	 contentString = '<div style="width:500px; max-height:200px; height:auto;" class="Buildblock" id="Build_'+indexID+'" data-grpIdx='+indexID+'><div id="siteNotice"></div><div id="Building_Block" class="buildingBlock" style="min-height:auto; width: 100%;">'+addthis;

	 
	 var i=0;
	 var j=0;
	 var temp={};
     if(Typeis=='residential')
     {
     	var floorD={};
     	var current='#'+GroupData[0][6];
     	var currentFirst='#'+GroupData[0][6];
     	Glength=GroupData.length;
     	var li=1;
     	var tempate=data[GroupData[0][3]];
		var currentFirst='';
		$.each(GroupData,function(key,value){
     		
     		var tempdata=value[6];//'#'+
     		var dpos=value[3];
     		var currentData=data[dpos];
     		if(currentFirst=='')
     		{
     			currentFirst=tempdata;
     			//console.log(currentFirst)
     			currentData.StoreIndex=value[3];
     			currentData.unitno=value[8];
	    		floorD[currentFirst]=[];
	    		floorD[currentFirst].push(currentData);

     		}
     		else
     		{
     			
     			currentData.StoreIndex=value[3];
     			currentData.unitno=value[8];
     			if(currentFirst!=tempdata)
     			{
     				currentFirst=tempdata;
     				if (currentFirst in floorwisedata)
	    			{
	    					console.log('yes')
	    			}
	    			else
	    			{

	    					floorD[currentFirst]=[];

	    			}
	    			floorD[currentFirst].push(currentData);

     			}
     			else
     			{
     				
     				floorD[currentFirst].push(currentData);
     			}
     		}
     		
     		
     	});
     	
     	var sector=tempate.tag_sector;
     	var matchme=tempate.address;
     	var re='(#)(\\d+)(-)(\\d+)';
     	var p = new RegExp(re,["i"]);
     	var m = p.exec(matchme);
     	if (m != null)
     	{
     	
     		var unit=m[1]+m[2]+m[3]+m[4];
     		var parsedAddress=matchme.replace(unit, "");
     		
     	}
     	
		    var subcontent='';
		    if(parsedAddress)
		    {
		    	subcontent+='<div class="cname">'+parsedAddress+'</div>';
		    }
		   	if(sector)
		    {
		    	subcontent+='<div class="address">'+sector+'</div>';
		    }
		    subcontent+='</div></div>'
		    var floordata=sortObjectByKey(floorD);
		      $.each(floordata,function(key,level){
		      	$.each(level,function(key1,level1){
					flvl='L'+key1;
					level1.sort( function(a,b) { return a.unitno > b.unitno; });
			      	subcontent+='<div id="'+key1+'" class="level"> <div class="levelDetail" id="'+flvl+'" onclick="selectLevel(\''+flvl+'\')" >'+key1+'</div>';
			      		$.each(level1,function(key2,level2){
			      			switch(level2.meter_scheme)
			      			{
			      				case 'MASTER':
			      					Cls='Mas'
			      				break;
			      				case 'SUB':
			      					Cls='Sub'
			      				break;
			      				default:
			      					Cls='Def'
			      				break;
			      			}
			      			if(!allmode&&!lossoMode&&!AMRState)
			      			{
								if(unitObj.selectedUnit.indexOf(level2._id) > -1)
					  			{
					  				css='regular';
					  			}
					  			else
					  			{
					  				css='secondary'

					  			}
					  			address=level2.address.replace()
								subcontent+='<div class="whold '+Cls+' resi"><div class="windows resident label '+css+' '+Cls+'" id="'+level2._id+'" onclick="selectAMR(\''+level2._id+'\','+level2.StoreIndex+')"  title="'+level2.address+'" data-ind='+level2.StoreIndex+' style="float:left">'+level2.meterid+'</div><div class="info"><img src="images/info_b.png" width="15px" onclick="showMetaUser(\''+level2._id+'\')"></div></div>'; //
								//<div class="info"><img src="images/info_b.png" width="15px" onclick="showMetaUser(\''+level2._id+'\')"></div>
			      			}
			      			else
			      			{
								if(excludeMarkers.indexOf(level2._id) > -1)
					  			{
					  				css='secondary';
					  			}
					  			else
					  			{
					  				css='regular'
					  			}
								subcontent+='<div class="whold '+Cls+' resi"><div class="windows resident label '+css+' '+Cls+'"   id="'+level2._id+'" onclick="selectAMR(\''+level2._id+'\','+level2.StoreIndex+')" title="'+level2.address+'" data-ind='+level2.StoreIndex+' style="float:left">'+level2.meterid+'</div><div class="info"><img src="images/info_b.png" width="15px" onclick="showMetaUser(\''+level2._id+'\')"></div></div>';		//
								//<div class="info"><img src="images/info_b.png" width="15px" onclick="showMetaUser(\''+level2._id+'\')"></div>      				
			      			}
			      		});
			      	subcontent+='</div>';
		      	})
		      })	
		      contentString+=subcontent;
      
	    }
	    else if(Typeis=='flowmeter')
	    {
	    	return generateHTML2(null,indexID);
	    }
	  	else {	//	NON-RESIDENTIAL & WEMS
			  	try{
			  		if(Typeis == 'wems')
			  			return generateHTML2(null,indexID);
			  		else
			  			return generateHTML3(null,indexID);
				}
			  	catch(e){
			  		console.log('ERR - PopUp '+e);
			  	}
	  		
	    	
	    	Panelcurrent=''
	    	var subcontent='';
	    	var Temp=[];
	    	var floorD={};
	    	var initialsize;
	    	var tempate=data[GroupData[0][3]];
			$.each(GroupData, function(key,value){
	    		var dpos=value[3];
	    		var currentData=data[dpos];

	    		currentData.StoreIndex=value[3];
	    		if(currentData['component_id'])
	    			var panelID=currentData['component_id'];
	    		else
	    			var panelID='cmp'+Math.floor((Math.random()*9999999)+9999);
	    		if(currentData['_id']=='wems_39'||currentData['_id']=='wems_391') //_id
	    		{
	    		//console.log(panelID)	
	    		}
				console.log(panelID)
	    		var meter_type=currentData['component_type'];
	    		var Temp=[];
	    		if(Panelcurrent=='')
	    		{
	    			
	    			Panelcurrent=panelID;
	    			value.push(meter_type);
	    			Temp.push(currentData);
	    			floorD[Panelcurrent]=[];
	    			if(meter_type=='master')
	    			{
	    				floorD[Panelcurrent].push({'master':Temp});
	    			}
	    			else
	    			{
	    				floorD[Panelcurrent].push({'sub':Temp});
	    			}
	    			
	    		}
	    		else
	    		{
	    			value.push(meter_type);
	    			Temp.push(currentData);
	    			if(Panelcurrent!=panelID)
	    			{
	    				Panelcurrent=panelID;
	    				if (panelID in floorD)
	    				{
	    					console.log('yes')
	    				}
	    				else
	    				{
	    					floorD[Panelcurrent]=[];
	    				}
	    				
	    				
	    				if(meter_type=='master')
		    			{
		    				floorD[Panelcurrent].push({'master':Temp});
		    			}
		    			else
		    			{
		    				floorD[Panelcurrent].push({'sub':Temp});
		    			}
	    			}
	    			else
	    			{
	    				console.log('process')
	    				
	    				if(meter_type=='master')
		    			{
		    				floorD[Panelcurrent].push({'master':Temp});
		    			}
		    			else
		    			{
		    				floorD[Panelcurrent].push({'sub':Temp});
		    			}
	    			}
	    		}
	    		
	    	});
	    	
	    	var floorData=[]
	    	$.each(floorD, function(key,value){
	    		var DData={}
	    		var temp={}
	    		//console.log(value[0]['customer_name']);
	    		$.each(value, function(key1,value1){
	    			$.each(value1, function(key2,value2){
		    			if(key2=='sub')
			    		{
			    			temp.sub=value2[0];
			    		}
			    		else {
			    			temp.master=value2[0];
			    		}
	    			})
	    			
	    		});
	    		DData[key]=temp;
	    		
	    		floorData.push(DData);	
	    	});
	    	console.log(floorD)
	    	try {
		    		
		    		console.log(tempate.bp_name)
		    		console.log(tempate)
		    		if(Typeis!='wems')
		    		{
		    			
		    			floorData.name=tempate.bp_name||tempate.display_customer_name;
			    		floorData.address=tempate.mailing_address||tempate.mailing_address||tempate.premise_address;
			    		floorData.sector=tempate.tag_sector;
			    		floorData.description=tempate.description;
						if(floorData.name)
			    			subcontent+='<div class="cname">'+floorData.name+'</div>';
			    		if(floorData.description)
			    			subcontent+='<div class="address">'+floorData.description+'</div>';
			    		if(floorData.address)
			    			subcontent+='<div class="address">'+floorData.address+'</div>';
			    		if(floorData.sector)
			    			subcontent+='<div class="sector">'+floorData.sector+'</div>';
		    		}
		    		else
		    		{
		    			floorData.name=tempate.bp_name||tempate.site;
				    	floorData.desc=tempate.meter_description;
				    	floorData.address=tempate.mailing_address||tempate.mailing_address||tempate.premise_address;
				    	floorData.sec=tempate.tag_sector;
				    	if(floorData.name)
			    			subcontent+='<div class="cname">'+floorData.name+'</div>';
			    		if(floorData.description)
			    			subcontent+='<div class="address">'+floorData.description+'</div>';
			    		if(floorData.address)
			    			subcontent+='<div class="address">'+floorData.address+'</div>';
		    		}

		    }
		    catch(err)
		    {

		    }
	    	subcontent+='</div></div>'
	    	$.each(floorData, function(Lkey,Lvalue){
		    	
	    		
		    	$.each(Lvalue, function(key,value){
		    		if(key!='name'&&key!='address'&&key!='sector')
		    		{
		    			if(Typeis=='wems')
		    			{
			    			if('sub' in value)
			    			{
			    				var descp=value.sub.meter_description;
			    				var meter_type=value.sub.meter_type;
			    			}
			    			else
			    			{
			    				var descp=value.master.meter_description;
			    				var meter_type=value.master.meter_type;
			    			}
			    			subcontent+='<div class="level" id="L'+key+'"><div class="titleDesc">'+descp+'<br />'+meter_type+'</div>';
		    			}
			    		subcontent+='<div class="compoundMeter level" id="c_'+key+'" >';
			    		// subcontent+='<div  class="selectCompund" onclick="selectpanelunit(\''+key+'\')"> select</div>'
			    		if(objectSize(value)>=2)
			    		{
			    			//subcontent+='<h6 style="color:#000 !important;">Compound Meter</h6>';
			    		}
		    			$.each(value,function(key1,value1) {
		    				
		    				if(value1.size!=undefined){
		    					var size=value1.size;
		    				}
		    				else
		    				{
		    					var size='';
		    				}
			    			if(!allmode)
			      			{	
			      				if(unitObj.selectedUnit.indexOf(value1._id) > -1)
					  			{
					  				css='regular'
					  			}
					  			else
					  			{
					  				css='secondary'
					  			}
			    				subcontent+='<div class="MARBLK"><div class="MARTEXT">'+key1.toUpperCase()+'</div><div class="whold bigger l'+key1+'"><div class="innerMeters '+key1+' label '+css+'" id="'+value1._id+'" onclick="selectunit(\''+value1._id+'\')" data-ind='+value1.StoreIndex+'>'+size+'</div><div class="info"><img src="images/info_b.png" width="15px" onclick="showMetaUser(\''+value1._id+'\')"></div></div></div>';
			    			}
			    			else{
			    				if(excludeMarkers.indexOf(value1._id) > -1)
						  		{
						  				css='secondary'
						  		}
						  		else
						  		{
						  				css='regular'
						  		}
			    				//subcontent+='<div class="innerMeters '+key1+' label '+css+'" id="'+value1._id+'" onclick="selectunit(\''+value1._id+'\')" data-ind='+value1.StoreIndex+'>'+size+'</div>';
			    				subcontent+='<div class="MARBLK"><div class="MARTEXT">'+key1.toUpperCase()+'</div><div class="whold bigger l'+key1+'"><div class="innerMeters '+key1+' label '+css+'" id="'+value1._id+'" onclick="selectunit(\''+value1._id+'\')" data-ind='+value1.StoreIndex+'>'+size+'</div><div class="info"><img src="images/info_b.png" width="15px" onclick="showMetaUser(\''+value1._id+'\')"></div></div></div>';
			    			}	
		    			});
		    			subcontent+='</div>';
		    			if(Typeis=='wems')
		    			{
		    				subcontent+='</div>';
		    			}
		    		}
		    	});
	    	});
	    	contentString+=subcontent;
	    	//MarkerObject={};
	    	//MarkerObject.index=
	    	//showAction('Marker','');
	    }  
      contentString+='</div></div>'; 

 /*  	if(icontype == 'w')
  		{
  			//	Weather Node Clicked.
  			contentString = '<div style="width:120px;height:150px;padding:5px;"><h5>ID '+metadata[marker.metaindex]._id+'</h5><br/><b>Location: </b>'+metadata[marker.metaindex].location+'</div>';
  			weatherobj.id = marker.id;
			showAction('weather');
  		}
      if (infowindow) infowindow.close();
      infowindow = new google.maps.InfoWindow({content: contentString, maxWidth: 250});
      infowindow.open(map, marker);
   
   return marker;
=======	*/
      return contentString;

}
// bookmark script
function _bookmark(Proxyurl, data)
{
	var WidgetArray={};
	$('#bookmark').click(function(){
		//console.log(Proxyurl,data)
		var WgetArray=localStorage.WidgetArray;
		//console.log('test',WgetArray)
		$.each(WgetArray,function(key,value){
			console.log(value)
		});
		//WidgetArray.hash=$.md5(data);
		//WidgetArray.data=data;
		//localStorage.setItem("WidgetArray", WidgetArray);
	});
}





// get the search selection
function fetchselection(stobj)
{
  //console.log(stobj)
  searchTstring='';
  searchOptions = {};
  
  if(stobj!=undefined)
  {
    searchOptions.text=stobj//$('#search').val();
    $.each(stobj,function(key,value){
      $.each(value,function(key1,value2){
        searchTstring+=$.trim(value2)+',';
      })
    })
  }
  else
  {
    searchOptions.text=[]
  }
  //console.log(rootSelection)
  searchOptions.filterType=[];


  $('.root_options .label').each(function(){
          if($(this).hasClass('regular'))
          {
            searchOptions.filterType.push($(this).attr('id'));
        	searchTstring+=$(this).attr('id')+',';
          }
        
  })

  
  $('.parent_options').each(function(){
    sectionID=$(this).attr('id');
    
    
    midSearc={};
    $('#'+sectionID+' .row .columns').each(function(index,element){
    midID=$(element).attr('id');
    //console.log(midID)
    var temp=[];
      $('#'+midID+' .label').each(function( index, element ){
      
        if($(element).hasClass('regular'))
        {
          temp.push($(element).attr('id'));
          searchTstring+=$(element).attr('id')+',';
          //searchOptions.waterType=$(this).attr('id');
        }
        
        midSearc[midID]=temp;
      })
    });
    
    
    searchOptions[sectionID]=midSearc;
  })
  //console.log($('#clusters_options .row .columns'))
  $('.sub_parent_options').each(function(){
    sectionID=$(this).attr('id');
    
    
    midSearc={};
    $('#'+sectionID+' .row .columns').each(function(index,element){
    midID=$(element).attr('id');
    //console.log(midID)
    var temp=[];
      $('#'+midID+' .label').each(function( index, element ){
      
        if($(element).hasClass('regular'))
        {
          temp.push($(element).attr('id'));
          searchTstring+=$(element).attr('id')+',';
          //searchOptions.waterType=$(this).attr('id');
        }
        
        midSearc[midID]=temp;
      })
    });
    
    
    searchOptions[sectionID]=midSearc;
  })
  //console.log($('#clusters_options .row .columns'))
  searchOptions.settings={};
  var sidebutton={}
  sidebutton.watertype={"watertype":[]};
  sidebutton.zoneSelection={"zoneSelection":[]};
  sidebutton.ZoneCluster={"ZoneCluster":[]};
  $('.typewater li.sel').each(function(){
  		sidebutton.watertype.watertype.push($(this).data('id'))
  })
  $('.typezone >ul li.sel').each(function(){
  		sidebutton.zoneSelection.zoneSelection.push($(this).data('id'))
  })
  $('.typezone .sel #ZoneCluster ul li.sel').each(function(){
  		sidebutton.ZoneCluster.ZoneCluster.push($(this).data('id'))
  })
  searchOptions.settings=sidebutton;
  // $('#zonesettingsArea .optItems').each(function(){

  //   sectionID=$(this).attr('id');
    
    
  //   midSearc={};
  //   $('#'+sectionID+'.columns').each(function(index,element){
  //   midID=$(element).attr('id');
  //   //console.log(midID)
  //   var temp=[];
  //     $('#'+midID+' .label').each(function( index, element ){
      
  //       if($(element).hasClass('regular'))
  //       {
  //         temp.push($(element).attr('id'));
  //         searchTstring+=$(element).attr('id')+',';
  //         //searchOptions.waterType=$(this).attr('id');
  //       }
        
  //       midSearc[midID]=temp;
  //     })
  //   });
    
    
  //   searchOptions.settings[sectionID]=midSearc;
  // })


  searchTstring=searchTstring.replace(/,(?=[^,]*$)/, '');
  //showAction('search',searchOptions);
}
function	logWindow(method,id,opt)
{
	opt = opt || null;
	retval = null;
	if(typeof(id)	==	'object'){
		id = getRandomInt(0,100);
		retval = 'container-'+id;	
	}
	var themetype = '';
	if(!Highcharts.themetype)
        themetype = 'light';
	switch(method)
		{
			case	'plot':
					var uniqid	=	id+','+windows.length;
					$('.bottom-panel').append('<div data-id="'+uniqid+'" class="minWindow"><span class="winTitle">Plot-'+id+'</span><span class="cross">x</span>');
					//	setup window
					windows.push(uniqid);
					var	newwindow	=	    '<div class="plotter '+themetype+'" id="'+id+'" data-id="'+uniqid+'"><div class="medium-12"><div class="topheader"></div><span class="right closeme"><img src="images/cross.png" width="15"/></span><span class="right minme" id="bookmark"><img src="images/minus.png" width="15"/></span><div class="clear"></div></div>';
					newwindow	+=	'<div class="replot"><a class="button tiny">Plot Again</a></div><div class="sidebar"></div><div class="sidepanel hide"></div>';
					newwindow += '<div class="dayselect hide"><div class="crossme right">x</div><input type="checkbox" class="daycheck" id="daycheck" class="left"/><label for="daycheck"></label><ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thr</li><li>Fri</li><li>Sat</li><li>Pub</li></ul></div>';
	       			newwindow	+=	'<div id="container-'+id+'" class="plotcontainer columns medium-10"><img src="images/ajax-loader-black.gif"></div><div class="stats hide columns medium-12"></div>';	       			
	       			newwindow += controlmarkup(1)+'</div>';
//newwindow	+=	'<div id="container'+id+'" class="plotcontainer"></div></div>';
					var	el	=	$('body').append(newwindow); 	
					$('.plotter:last-child').draggable({start: function(event,ui) {
            			//$( event.toElement ).one('click', function(e){ e.stopImmediatePropagation(); } );
            			if(event.toElement.localName == "rect" || event.toElement.localName == "path"){
            				event.stopImmediatePropagation();
            				event.preventDefault();
            			}
            		}
            		});
					$('.plotter:last-child').resizable({ghost: false,animate: false,
					stop: function( event, ui ) {
						var el = ui.element[0];
						var chartid = $(el).children('.plotcontainer').attr('id');
						var chartindex = findChart(chartid);
						var w = $(el).width();
						var r = 0;
						if(w<805){
							r = 5;
						if(w<620)
							r = 2
							$(el).find('.topheader li:gt('+r+')').each(function(u,e){
								$(this).css('display','none');
								console.log(u);
							});
						}
						else
							$(el).find('.topheader li').each(function(u,e){
								$(e).removeAttr('style');
							});
						charts[chartindex].setSize(w-20,$(el).height()-25);
					}});	
					return retval;
			break;
			case	'wide':
					var uniqid	=	id+','+windows.length;
					$('.bottom-panel').append('<div data-id="'+uniqid+'" class="minWindow"><span class="winTitle">Plot-'+id+'</span><span class="cross">x</span>');
					//	setup window
					windows.push(uniqid);
					var	newwindow	=	    '<div class="plotter big" id="'+id+'" data-id="'+uniqid+'"><div class="medium-12"><div class="topheader"></div><span class="right closeme"><img src="images/cross.png" width="15"/></span><span class="right minme" id="bookmark"><img src="images/minus.png" width="15"/></span><div class="clear"></div></div>';
					newwindow	+=	'<div class="replot"><a class="button tiny">Plot Again</a></div><div class="sidebar"></div><div class="sidepanel hide"></div>';
					newwindow += '<div class="dayselect hide"><div class="crossme right">x</div><input type="checkbox" class="daycheck" id="daycheck" class="left"/><label for="daycheck"></label><ul><li>Sun</li><li>Mon</li><li>Tue</li><li>Wed</li><li>Thr</li><li>Fri</li><li>Sat</li><li>Pub</li></ul></div>';
	       			newwindow	+=	'<div id="container-'+id+'" class="plotcontainer columns medium-10"><img src="images/ajax-loader.gif"></div><div class="extraregion medium-12 columns"></div><div class="stats hide columns medium-12"></div>';	       			
	       			newwindow += controlmarkup(1)+'</div>';
					var	el	=	$('body').append(newwindow); 	
					$('#'+id).draggable({start: function(event,ui) {
            			//$( event.toElement ).one('click', function(e){ e.stopImmediatePropagation(); } );
            			if(event.toElement.localName == "rect" || event.toElement.localName == "path"){
            				event.stopImmediatePropagation();
            				event.preventDefault();
            			}
            		}
            		});
					$('#'+id).resizable({ghost: false,animate: false,
					stop: function( event, ui ) {
						var el = ui.element[0];
						var chartid = $(el).children('.plotcontainer').attr('id');
						var chartindex = findChart(chartid);
						var w = $(el).width();
						var r = 0;
						if(w<805){
							r = 5;
						if(w<620)
							r = 2
							$(el).find('.topheader li:gt('+r+')').each(function(u,e){
								$(this).css('display','none');
								console.log(u);
							});
						}
						else
							$(el).find('.topheader li').each(function(u,e){
								$(e).removeAttr('style');
							});
						charts[chartindex].setSize(w-20,$(el).height()-25);
					}});	
					return retval;
			break;
				case 'dependant':
			var uniqid	=	id+','+windows.length;
					//$('.bottom-panel').append('<div data-id="'+uniqid+'" class="minWindow"><span class="winTitle">Plot-'+id+'</span><span class="cross">x</span>');
					//	setup window
					windows.push(uniqid);
					var	newwindow	=	    '<div class="plotter" id="'+id+'" data-id="'+uniqid+'"><div class="deptitle"></div><div class="medium-12"><div class="topheader"></div><span class="right closeme"><img src="images/cross.png" width="15"/></span><div class="clear"></div></div>';
					newwindow	+=	'<div id="container'+id+'" class="plotdependant columns medium-10"><img src="images/ajax-loader.gif"></div><div class="stats hide columns medium-12"></div></div>';	       			
	       			var	el	=	$('body').append(newwindow); 	
					$('.plotter:last-child').draggable({start: function(event,ui) {
            			if(event.toElement.localName == "rect" || event.toElement.localName == "path"){
            				event.stopImmediatePropagation();
            				event.preventDefault();
            			}
            		},
            		stop:function(event,ui){
            			//jsPlumb.repaintEverything();
            		},
            		drag:function(e,u){
			                jsPlumb.repaintEverything();
	     			}});
					$('.plotter:last-child').resizable({ghost: false,animate: false});	
					return retval;
			break;

			case 	'sr':
					var	uniqid	=	id+','+windows.length;
					$('.bottom-panel').append('<div data-id="'+uniqid+'" class="minWindow"><span class="winTitle">Plot-'+id+'</span><span class="cross">x</span>');
					windows.push(uniqid);
					var	newwindow	=	'<div class="plotter" data-id="'+uniqid+'" id="'+id+'"><div class="topheader"><span class="right closeme"><img src="images/cross.png" width="15"/></span><span class="right minme" id="bookmark"><img src="images/minus.png" width="15"/></span><span class="right widget" id="widget"><img src="images/progress-0.png" width="15"/></span><div class="clear"></div></div></div>';
					newwindow	+=	'<div class="replot"><a class="button tiny">Plot Again</a></div><div class="sidebar"></div><div class="sidepanel hide"></div>';
					var	el = $('body').append(newwindow);
					$('#'+id).draggable({start: function(event,ui) {
            			//$( event.toElement ).one('click', function(e){ e.stopImmediatePropagation(); } );
            			if(event.toElement.localName == "rect" || event.toElement.localName == "path"){
            				event.stopImmediatePropagation();
            				event.preventDefault();
            			}
            		}
            		});
					//$('#'+id).resizable({ghost: false,animate: false, alsoResize: ".plotcontainer"});
			break;
			case 'vertical':
					var uniqid = 'info'+getRandomInt(0,100);
					$('.bottom-panel').append('<div data-id="'+uniqid+'" class="minWindow"><span class="winTitle">Plot-'+uniqid+'</span><span class="cross">x</span>');
					windows.push(uniqid);
					var	newwindow	=	'<div class="plotter vert" data-id="'+uniqid+'" id="'+uniqid+'"><div class="topheader"><span class="right closeme"><img src="images/cross.png" width="15"/></span><div class="clear"></div></div></div>';
					var	el = $('body').append(newwindow);
					$('#'+uniqid).draggable();
					$('#'+uniqid).resizable({ghost: false,animate: true, alsoResize: ".container"});
					return uniqid;
			break;

			case 'report':
				var uniqidentifier = getRandomInt(0,99);
				var uniqid = 'report'+uniqidentifier;
				$('.bottom-panel').append('<div data-id="'+uniqid+'" class="minWindow"><span class="winTitle">Report-'+uniqidentifier+'</span><span class="cross">x</span>');
				var newwindow = document.createElement('div');
				newwindow.className = 'reporter animated zoomIn';
				newwindow.id = uniqid;
				newwindow.setAttribute("data-id", uniqid);
				newwindow.setAttribute("data-type",id);
				//newwindow.innerHTML = '<h1>Sector Report</h1>';
				//$('#overlay').removeClass('hide');
				$('body').append(newwindow);
				$('.reporter').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$(this).removeClass('zoomIn');
				});
				reporterSize = $(window).height()-75;
				$('.reporter').css('height',reporterSize);
				return uniqid;
			break;

			case 'control':
					var uniqid = 'control'+getRandomInt(0,100);
					var theme = '';
					if(opt!=null && opt.hasOwnProperty('theme'))
						theme = opt.theme;
					var name = (opt!=null && typeof(opt)==='object' && opt.hasOwnProperty('display')) ? opt.display : '';
					$('.bottom-panel').append('<div data-id="'+uniqid+'" class="minWindow"><span class="winTitle">Plot-'+uniqid+'</span><span class="cross">x</span>');
					windows.push(uniqid);
					var	newwindow	=	'<div class="stackedPlotter control '+theme+'" data-id="'+uniqid+'" id="'+uniqid+'"><div class="topheader nopad"><span class="txt noact">'+name+'</span><span class="right closeme"><img src="images/cross.png" width="15"/></span><span class="right minme" id="bookmark"><img src="images/minus.png" width="15"/></span><div class="clear"></div></div></div>';
					var	el = $('body').append(newwindow);
					if(opt.hasOwnProperty('drag') && opt.drag === true){
						$('#'+uniqid).draggable();
						$('#'+uniqid).resizable({ghost: false,animate: true, alsoResize: ".container"});
					}
					return uniqid;
			break;
			case 'stacked':
				var uniqidentifier = getRandomInt(0,500);
				var bodyHeight = $('body').height();
				var goodHeight = bodyHeight - (20/100 * bodyHeight);
				var html = '<div class="stackedPlotter '+themetype+'" id="stckd'+uniqidentifier+'" data-id="'+uniqidentifier+'">';
				html += '<div class="stackheader"><span class="txt">+ Add another chart to this stack</span><span class="right closeme"><img src="images/cross.png" width="15"/></span><span class="right minme"><img src="images/minus.png" width="15"/></span><div style="clear:both"></div></div><div class="thiswrap" style="max-height:'+goodHeight+'px"></div></div>';
				$('body').append(html);
				$('.bottom-panel').append('<div data-id="'+uniqidentifier+'" class="minWindow"><span class="winTitle">Stacked-'+uniqidentifier+'</span><span class="cross">x</span>');
				stackedPlots.push(uniqidentifier);
				windows.push(uniqidentifier);
				return 'container-stckd'+uniqidentifier;
			break;
			case 'generic':
				var thisname = '';
				if(opt!=null && typeof(opt) == 'object')
					thisname = opt.name;
				var uniqidentifier = getRandomInt(0,500);
				var bodyHeight = $('body').height();
				var goodHeight = bodyHeight - (20/100 * bodyHeight);
				var html = '<div class="stackedPlotter genericstyle" id="stckd'+uniqidentifier+'" data-id="'+uniqidentifier+'">';
				html += '<div class="genheader"><span class="txt">Simulator</span><span class="right closeme"><img src="images/cross.png" width="15"/></span><span class="right minme"><img src="images/minus.png" width="15"/></span><div style="clear:both"></div></div><div class="thiswrap" style="max-height:'+goodHeight+'px"></div></div>';
				$('body').append(html);
				$('.bottom-panel').append('<div data-id="'+uniqidentifier+'" class="minWindow"><span class="winTitle">'+thisname+'-'+uniqidentifier+'</span><span class="cross">x</span>');
				stackedPlots.push(uniqidentifier);
				windows.push(uniqidentifier);
				return 'stckd'+uniqidentifier;
			break;


			case 'highres':
				var uniqidentifier = getRandomInt(0,500);
				var html = '<div class="plotter highresstyle" id="'+uniqidentifier+'" data-id="'+uniqidentifier+'"><div class="medium-12"><div class="topheader" style="padding:2px;background:#eee;"><div class="txt">Highest Resolution</div><div class="plotstatus"><span class="statustext">No Connection</span><div class="statussignal"></div></div><span class="right closeme"><img src="images/cross.png" width="15"/></span><span class="right minme"><img src="images/minus.png" width="15"/></span><span class="right widget" id="widget"><div class="clear"></div></div>';
				html += '<div class="arr_left shiftbtn"><img class="smallimg" src="images/left_arr.png"></div><div class="plotcontainer columns small-10" style="width: 95%;margin-left: -10px;" id="highres-'+uniqidentifier+'"></div><div class="arr_right shiftbtn"><img class="smallimg" src="images/right_arr.png"></div><div class="stats columns medium-12"><div class="button tiny right hide closedvalvesbtn">Closed Valves Evaluation</div></div></div>';
				$('body').append(html);
				$('.bottom-panel').append('<div data-id="'+uniqidentifier+'" class="minWindow"><span class="winTitle">HighRes-'+uniqidentifier+'</span><span class="cross">x</span>');
				$('#'+uniqidentifier).draggable({start: function(event,ui) {
            			//$( event.toElement ).one('click', function(e){ e.stopImmediatePropagation(); } );
            			if(event.toElement.localName == "canvas"){
            				event.stopImmediatePropagation();
            				event.preventDefault();
            			}
            		}
            	});
				return uniqidentifier;
			break;
			case 	'mindash':
				var uniqidentifier = getRandomInt(0,99);
				var uniqid = 'Action-'+uniqidentifier;
				$('.bottom-panel').append('<div data-id="'+uniqid+'" class="minWindow"><span class="winTitle">Action-'+uniqidentifier+'</span><span class="cross">x</span>');
				var newwindow = document.createElement('div');
				newwindow.className = 'mindash animated zoomIn';
				newwindow.id = uniqid;
				newwindow.setAttribute("data-id", uniqid);
				newwindow.setAttribute("data-type",id);
				$('body').append(newwindow);
				$('.mindash').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$(this).removeClass('zoomIn');
				});
				reporterSize = $(window).height()/1.5;
				$('#'+uniqid).css('height',reporterSize);
				reporterSize = $(window).width()/1.5;
				$('#'+uniqid).css('width',reporterSize);
				$('#'+uniqid).css('margin', '9% 10% 0px');
					// $('#'+uniqid).draggable({start: function(event,ui) {
	    //         			//$( event.toElement ).one('click', function(e){ e.stopImmediatePropagation(); } );
	    //         			if(event.toElement.localName == "canvas"){
	    //         				event.stopImmediatePropagation();
	    //         				event.preventDefault();
	    //         			}
	    //         		}
	    //         	});
					return uniqid;
			break;

			default:
			throw "Window method unknown - LogWindow";

		}
}
function	meterType(x,options){
	x	=	x	||	null;
	options = options || null;
	if(!x)
		return;
	clearMarkers();
	var	str = new RegExp(x,"i");
	for(var i=0; i<markers.length; i++){
		if(metadata[markers[i].metaindex].metertype.match(str))
			{
				if(x.match(/itron/i))
					markers[i].setIcon("images/itron_small.gif");
				else if(x.match(/maddalena/i))
					markers[i].setIcon("images/maddalena_small.gif");
				else if(x.match(/sappel/i))
					markers[i].setIcon("images/sappel_small.jpg");
				else if(x.match(/kent/i))
					markers[i].setIcon("images/kent_small.jpg");
				else if(x.match(/sensus/i))
					markers[i].setIcon("images/itron_small.gif");
				else if(x.match(/arad/i))
					markers[i].setIcon("images/arad_small.jpg");
				else if(x.match(/elster/i))
					markers[i].setIcon("images/elster_small.png");
				else
					console.log('no match');
			}
		else
			{
				if(options != null && options.showall == false)
					{
						markers[i].setMap(null);
					}
			}
	}
}

function	blockType(x,options){
	x	=	x	||	null;
	options = options || null;
	if(!x)
			return;
	clearMarkers();
	var	str = new RegExp(x,'i');
	for(var i=0; i<markers.length; i++){
		if(metadata[markers[i].metaindex].installationtype.match(str))
		{
			//console.log('match found');
			switch(x){
				case 'RESIDENTIAL':
					markers[i].setIcon("images/res_small.png");
				break;
				default:
					markers[i].setIcon("images/com_small.png");
				break;
			}
		}
		else
		{
			if(options != null && options.showall == false)
				markers[i].setMap(null);
		}
	}
}
function	clearMarkers(bit){
bit = bit || null;
	for(var	i=0;	i<markers.length;	i++){
		if(bit)
		markers[i].setMap(null);
		else
		markers[i].setMap(map);
		markers[i].setIcon(null);
	}
	if(bit)
	markers.splice(0,markers.length);
}

//	Event Handlers
$(document).on('click','.plotagain',function(){
	$(this).parent().find('a').removeClass('boldme');
	$(this).addClass('boldme');
	var plotid = $(this).parents('.plotter').data('id');
	var temp = plotid.split(',');
	replotobj.meter = temp[0];
	var functionid = $(this).data('id').split(',');
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	replotobj.id = chartid;
	switch(functionid[0]){
		case 'res':
			var	resolution = functionid[1];
			replotobj.res = resolution;
			showReplot(chartid);
		break;
		case 'range':
			showRange(this,chartid);
		break;
	}
});
$(document).on('click','.replot',function(){
	var id = findChart($(this).parents('.plotter').children('.plotcontainer').attr('id'));
	replot(id);
	$(this).css('display','none');
});
$(document).on('click','.addplot',function(){
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	var plotid = $(this).parents('.plotter').data('id');
	//	Add plot mode
	$('.bottom-panel .minWindow').each(function(ind,el){
		if(plotid == $(el).data('id')){
			addPlotMode(el,chartindex);
			return;
		}

	});

});
$(document).on('click','.killme',function(){
	/*if(addplotobj.mode)
		addplotobj.reset();
	if(aggregateobj.mode)
		aggregateobj.reset();
	if(tanksobj.mode)
		tanksobj.reset();
	if(averageobj.mode)
		averageobj.reset();
/*	$('.addTray').remove();
	$('.addmodebtn').remove();	*/
	resetSel();
	showAction(null);
});
$(document).on('click','.aggrBtn',function(){
	$(this).toggleClass('btn-selected');
	if(averageobj.mode){
		averageobj.reset();
	}
	else{
	if(rootSelection == 'weather'){
		averageobj.mode = !averageobj.mode;
	var tray = document.createElement('div');
	tray.className = 'addTray upper';
	tray.innerHTML = '<ul></ul><div class="connection"></div><div class="killme">x</div>';
	$('body').append(tray);	
	}
	else
	{
 		if(aggregateobj.mode){
		aggregateobj.reset();
	}
	else{
	aggregateobj.mode = !aggregateobj.mode;
	var tray = document.createElement('div');
	tray.className = 'addTray upper';
	tray.innerHTML = '<ul></ul><div class="connection"></div><div class="killme">x</div>';
	$('body').append(tray);
	}

	}
}
});
$(document).on('click','.aggr2plot',function(){
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	var plotid = $(this).parents('.plotter').data('id');
	//	Add plot mode
	$('.bottom-panel .minWindow').each(function(ind,el){
		if(plotid == $(el).data('id')){
			aggr2plot(el,chartindex);
			return;
		}
	});
});
$(document).on('click','.regressionBtn',function(){
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	applyReg(charts[chartindex],chartindex);
	$(this).parent().addClass('hide');
});
$(document).on('click','.mgd2m3',function(){
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	if($(this).parent().hasClass('strong'))
		mgdconvert(charts[chartindex],chartindex,'M^3','CONSUMPTION','MGD',true);
	else
		mgdconvert(charts[chartindex],chartindex,'MGD','CONSUMPTION','M^3',false);
	$(this).parent().toggleClass('strong');
});
$(document).on('click','.avg2plot',function(){
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	var plotid = $(this).parents('.plotter').data('id');
	//	Add plot mode
	$('.bottom-panel .minWindow').each(function(ind,el){
		if(plotid == $(el).data('id')){
			averageobj.mode = true;
			averageobj.addmode = true;
			averageobj.chartindex = ind;
			$('.plotter .minme').trigger('click');
			$(el).addClass('addmode');
			var pos = $(el).position();
		/*	var tray = document.createElement('div');
			tray.className = 'addTray';
			tray.innerHTML = '<ul></ul><div class="connection"></div><div class="killme">x</div>';
			$('body').append(tray);*/
			showAction();
			$('.addTray').animate({left:pos.left},'slow');
			return;
		}
	});
});

$(document).on('click','.flow2plot',function(){
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	var plotid = $(this).parents('.plotter').data('id');
	tanksobj.chartindex = chartindex;
	tanksobj.addmode = true;
	$('.plotter .minme').trigger('click');
	$('.bottom-panel .minWindow').each(function(ind,el){
		if(plotid == $(el).data('id')){
			$(el).addClass('addmode');
			var pos = $(el).position();
		/*	var tray = document.createElement('div');
			tray.className = 'addTray';
			tray.innerHTML = '<ul></ul><div class="connection"></div><div class="killme">x</div>';
			$('body').append(tray);*/
			showAction();
			$('.addTray').animate({left:pos.left},'slow');
			return;
		}
	});
});
$(document).on('click','#tankFlow',function(){
	plotFlow();
});
$(document).on('click','#showinfo',function(){
	var idx = $('#'+unitObj.selectedUnit[unitObj.selectedUnit.length - 1]).data('ind');
	var mywin = logWindow('vertical');
	html = '<ul>';
	for (var k in metadata[idx]){
		html += '<li><b>'+k+'</b> : '+metadata[idx][k]+'</li>';
	}
	html += '</ul>';
	$('#'+mywin).append(html);
});
$(document).on('click','.chartopt',function(){
	$('#chartdd').remove();
	var el = null;
	if($(this).parents('.plotter').length>0)
		el = $(this).parents('.plotter');
	else
		el = $(this).parents('.stackplotwrap');
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id') || $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	var type;
	if($(this).hasClass('lineopt'))
		type = 'chart2line';
	else if($(this).hasClass('baropt'))
		type = 'chart2column';
	charttypeobj.chartindex = chartindex;
	charttypeobj.plotter = $(this).parents('.plotter');
	var html = '<ul id="chartdd">';
	for(var i=0; i<charts[chartindex].series.length; i++){
		if(charts[chartindex].series[i].name == 'flags' || charts[chartindex].series[i].name == 'Navigator')
			continue;
		html += '<li onClick="'+type+'('+i+');">'+charts[chartindex].series[i].name+'</li>';
	}
	html += '</ul>';
	$(this).parent().append(html);
	//var pos = $(this).position();
	//$('#chartdd').css('left',(pos.left - 50)+'px');
});
$(document).on('click','.swapaxis',function(){
	$('#chartdd').remove();
	var chartid = null;
	var el = $(this).parents('.plotter');
	if($(this).parents('.plotter').length>0)
		chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	else{
		chartid = $(this).parents('.stackplotwrap').children('.plotcontainer').attr('id');
		el = $(this).parents('.stackplotwrap');
	}
	var	chartindex = findChart(chartid);
	var html = '<ul id="chartdd">';
	for(var i=0; i<charts[chartindex].series.length; i++){
		if(charts[chartindex].series[i].name == 'flags' || charts[chartindex].series[i].name == 'Navigator')
			continue;
		var ls='',rs='';
		if(charts[chartindex].series[i].yAxis.options.index==0)
			ls='checked';
		else if(charts[chartindex].series[i].yAxis.options.index==1)
			rs='checked';
		html += '<li>'+charts[chartindex].series[i].name+'<br/><img src="images/left_arr.png" width="16px"><input type="radio" '+ls+' name="seraxis'+i+'" onClick="shiftAxis('+chartindex+','+i+',0)">&nbsp;<input type="radio" '+rs+' name="seraxis'+i+'" onClick="shiftAxis('+chartindex+','+i+',1)"><img src="images/right_arr.png" width="16px"></li>';
	}
	html += '</ul>';
	$(this).parent().append(html);
	var pos = $(this).position();
	var ht = $('#chartdd').height();
	$('#chartdd').css('left',(pos.left - 20)+'px');
	$('#chartdd').css('margin-top','-'+ht+'px')
});
$(document).on('click','.dayonday',function(){
	$(this).toggleClass('strong');
	var pos = $(this).position();
	if($(this).parents('.plotter').length>0)
		$(this).parents('.plotter').find('.dayselect').toggleClass('hide').css('left',(pos.left - 20)+'px');
	else
		$(this).parents('.stackplotwrap').find('.dayselect').toggleClass('hide').css('left',(pos.left - 20)+'px');
});

$(document).on('click','.weather2plot',function(){
	var chartid = $(this).parents('.plotter').children('.plotcontainer').attr('id');
	var	chartindex = findChart(chartid);
	var plotid = $(this).parents('.plotter').data('id');
	weatherobj.chartindex = chartindex;
	weatherobj.addmode = true;
	$('.plotter .minme').trigger('click');
		$('.bottom-panel .minWindow').each(function(ind,el){
		if(plotid == $(el).data('id')){
			$(el).addClass('addmode');
		/*	var pos = $(el).position();
			var tray = document.createElement('div');
			tray.className = 'addTray';
			tray.innerHTML = '<ul></ul><div class="connection"></div><div class="killme">x</div>';
			$('body').append(tray);
			$('.addTray').css('left',pos.left);*/
			return;
		}
	});
});

$(document).on('mouseleave','#chartdd',function(){
	charttypeobj.reset();
});
function findChart(x){
	for(var i=0; i<charts.length; i++){
		if(!charts[i].container || !charts[i].container.offsetParent)
				continue;
		if(charts[i].container.offsetParent.id == x)
			return i;
	}
	return null;
}
function findHChart(x){
	for(var i=0;i<highresCharts.length;i++)
		if(highresCharts[i].id == x)
			return	i;
	return null;
}

function showRange(me,x){
$(me).dateRangePicker(
 {
  format:'DD/MM/YYYY HH:mm',
  separator:' - ',
  getValue: function()
  {
   return this.innerHTML;
  },
  setValue: function(s)
  {
   $(this).parent().find('._timerange').empty().append(' '+s);
  }
 }).bind('datepicker-apply',function(event,obj)
    {
      var procdate=[];
      var tempDates=obj.value.split(' - ');
      $.each(tempDates, function(key, value){
         dateSplit=value.split(' ');
         var ts = dateSplit[0].split('/');
         var ts2 = dateSplit[1].split(':');
         procdate.push(new Date(ts[2],ts[1]-1,ts[0],ts2[0],ts2[1],0).getTime()); 
         showReplot(x);
       })
       replotobj.st=procdate[0];
       replotobj.et=procdate[1];
       $(me).parent().find('a').removeClass('boldme');
		$(me).addClass('boldme');
		var plotid = $(me).parents('.plotter').data('id');
		var temp = plotid.split(',');
		//console.log(temp[0]);
		replotobj.meter = temp[0];
		var functionid = $(me).data('id').split(',');
		var chartid = $(me).parents('.plotter').children('.plotcontainer').attr('id');
		var	chartindex = findChart(chartid);
		//console.log(plotid); console.log(functionid);	console.log(chartid);console.log(chartindex);
		replotobj.id = chartid;
	});
}

/*function replot(idx){
	charts[idx].destroy();
	replotobj.index = idx;
	var mid = chartUrls[idx].meter;
	var cid = replotobj.id;
	var met = replotobj.et || et;
	var mst = replotobj.st || st;
	var mint = replotobj.res || interval;
	var mpred = replotobj.pred || pred;
	$.get('controllers/proxyman.php',{params:"data/"+mid+"/meter?st="+mst+"&et="+met+"&intervalmin="+mint+"&includepredicted="+mpred},
		function(d)
			{
				//console.log(d);
				var	payload	=	[];
				for(var	i=0;	i<d.data.length;	i++)
					{
						var	temp	=	{};
						temp.y	=	d.data[i].value;
						temp.x	=	d.data[i].ts;
						payload.push(temp);
						delete(temp);
					}
				//$('.plotter').css('display','block');
				var	chart = new Highcharts.Chart({
					chart: {
					renderTo: cid,
					type:'line',
				},
				plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
		            }
		        },
					xAxis:	{	type:'datetime'},
					yAxis:	{
						title	:	{	text:"Consumption"
							}
					},
					title: {
						text:'Meter '+mid
					},
					series:	[{data:payload,turboThreshold:5000,name:mid}]
				});
					charts[replotobj.index]	=	chart;
					replotobj = {};
					chartUrls[idx]	=	{"meter":mid,"st":mst,"et":met,"intervalmin":interval,"includepredicted":pred};
				//_bookmark(proxyUrl,Proxydata);
			},'json');
}*/

function showReplot(id){
	//console.log($('#'+id).parents('.plotter').find('.replot'));
	$('#'+id).parents('.plotter').find('.replot').css('display','block');
}
function addPlotMode(el,ind){
	addplotobj.mode = true;
	addplotobj.chartindex = ind;
	$('.plotter .minme').trigger('click');
	$(el).addClass('addmode');
	var pos = $(el).position();
/*	tray.className = 'addTray';
	tray.innerHTML = '<ul></ul><div class="connection"></div><div class="killme">x</div>';
	$('body').append(tray);	*/
	showAction();
	$('.addTray').animate({left:pos.left},'slow');
}
function refreshPlotTray(e,title){
	title = title || '';
	if(e)
		$('.addTray ul.trayul').append('<li title="'+title+'"><img src="images/watermeter.png"></li>');
	else
		$('.addTray ul.trayurl li:last-child').remove();
}
function refreshTray(){
	//var el = $('.addTray #tray'+id);
	//if(el.length==0)
	//if(!allmode)

	{
		$('.addTray ul.trayul').empty();
		if(unitObj.selectedUnit.length==0)
		{
		//	showAction();
			return;
		}
		for(var i=0; i<unitObj.selectedUnit.length; i++)
		{
			//var idx = $('#'+unitObj.selectedUnit[i]).data('ind');
			/*  -----------dma code change ------------------- */
			selectData=unitObj.sensorMeta[unitObj.selectedUnit[i]]
			if('tag_datatype' in selectData)
			{
				if(selectData.tag_datatype=='amr')
				{
					showname=selectData.display_name||selectData.name||selectData._id;
					etcadd=selectData.meter_id||selectData.meterid;
					showname+=' ('+etcadd+')';
				}
				else
				{
					showname=selectData.display_name||selectData.name||selectData._id;
				}
				
			}
			else	
			{
				showname=selectData.display_name||selectData.name||selectData._id;
			}
			/*  -----------dma code change ------------------- */
			// idx=unitObj.ids[unitObj.selectedUnit[i]];
			// showname=metadata[idx].display_name||metadata[idx].name||metadata[idx]._id||zonesarr[idx]._source.name;
			$('.addTray ul.trayul').append('<li class="radius" title="'+unitObj.selectedUnit[i]+'" id="T_'+unitObj.selectedUnit[i]+'" >'+showname+'</li>'); //data-id="'+idx+'"
		}
	}
	/*else
	{
		$('.addTray ul').empty();
		$('.addTray ul').append('<li class="radius" title="All Markers">All Markers</li>');
		$('.addTray ul').append('<span style="font-size:10px;">- Excluded Markers</span>');
		$.each(excludeMarkers, function(key, value){
			$('.addTray ul').append('<li class="radius colorRed" title="'+value+'">'+value+'</li>');
		})
	}*/
}
function add2plot(type,m,src,callback,optional_ep,nometa,ser_clr)
{
	var this_chart=null;
	src = src || null;
	nometa = nometa || null;
	callback = callback || null;
	ser_clr = ser_clr || null;
	$('.minWindow.addmode .winTitle').trigger('click');
	if(!type)
		{
			//alert('Incomplete Parameters');
			Notify('Incomplete!','Incomplete Parameters','normal',true);
			addseriesobj.reset();
			return;
		}
	var counter = 0;
	if(type == 1){	// REGULAR PLOT
	var myplotobj = JSON.parse(JSON.stringify(addplotobj));
	addplotobj.reset();
	var sensorT = m
	var axis = null;
	if(interval === 0)	//HIGHRES
	{
		var stuff = getActionParams();
		updateme(myplotobj.chartindex,stuff.include);
		return;
	}
	// finding axis
/*	if(chartUrls[addplotobj.chartindex].type != sensorT)
		axis = 1;
	else
		axis = 0;	*/
	if(units[chartUrls[myplotobj.chartindex].type] != units[sensorT])
		axis = 1;
	else
		axis = 0;
	// find eligibality
	if(chartUrls[myplotobj.chartindex].type2 != null){
		//if(m != chartUrls[myplotobj.chartindex].type && m != chartUrls[myplotobj.chartindex].type2 )
		if(units[m] != units[chartUrls[myplotobj.chartindex].type] && units[m] != units[chartUrls[myplotobj.chartindex].type2] )
		{
			//alert('Cannot add to this chart. Axis full');
			Notify('Chart Alert!','Cannot add to this chart. Axis full','normal',true)
			$('.addmodebtn').remove();
			$('.minWindow').removeClass('addmode');
			addseriesobj.reset();
		//	return;
			plotcomplete(1);
		}
	}
	// smart units selection
	var units_switch = false;
	if(chartUrls[myplotobj.chartindex].units[axis]!=units[m]	&&	chartUrls[myplotobj.chartindex].units[axis]!='')	//units have been changed
	{
		if(unitsMatch(units[m],chartUrls[myplotobj.chartindex].units[axis]))
			units_switch = [units[m],chartUrls[myplotobj.chartindex].units[axis]];
	}
	charts[myplotobj.chartindex].showLoading();
	var ep,searchjson,inc,exc;
	var pp = getActionParams();
	if(!src){
//		if(allmode){
//			searchjson = pp.Searchjson;
//			inc = '';
///			exc = pp.exclude;
//		}
//		else
//		{
			searchjson = '';
			//inc = pp.include;
			inc = jobpool[0];
			exc = '';
//		}
	}
	var _prm = src || "st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+inc+"&exclude="+exc+"&interval="+_interval+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&turnoffglobalexclusion='+!globalex;
	if(selectedep != 'search/data')
		_prm += '&mapjson='+JSON.stringify(mapjson);
	var extract = extractParams(_prm);
	if(src){
		inc = extract.include;
		exc = extract.exclude;
		if(inc!='')
			inc = inc.split(',');
		if(exc!='')
			exc = exc.split(',');
	}
	// FORCE CHART PERSISTANCE
	for(var key in persist){
		extract[key] = persist[key]||extract[key];
	}
	_prm = retractParams(extract);
	
	if(!src)
	myplotobj.meters = unitObj.selectedUnit;
	else
	myplotobj.meters = extract.include.split(',');
	ep = 'data/'+ (optional_ep || selectedep);
	var derived_insert = {};
	var cmeters = [];
				$.get('controllers/proxyman.php',{endpoint:ep,params:_prm},
				function(d,e,f)
					{
						if(d.errorCode>0)
						{
							Notify('Warning',d.errorMsg,'normal',true);
							charts[myplotobj.chartindex].hideLoading();
							plotcomplete(1);
							return;
						}
						if(!d.response)
							d.response = {meters:[{data:d.data}]};
						if(!d.response.meters)
							d.response.meters = [{data:d.response.data}];
						var uniquel = charts[myplotobj.chartindex].series.length;
						var operation = ep.split('/');
						operation = operation[operation.length-1];
						var erroneous = [];
						for(var dd=0; dd<d.response.meters.length; dd++){
						chartUrls[myplotobj.chartindex].endpoint.push(ep);
						var thisguy = null;
						if(d.response.meters[dd].hasOwnProperty('meta')){
							thisguy = d.response.meters[dd].meta._id;
						derived_insert = merge_options(derived_insert,checkDerivations(thisguy));
						}
						else
						{
							var meters_list = extract.include.split(',');
							_.each(meters_list,function(ml){
								derived_insert = merge_options(derived_insert,checkDerivations(thisguy));
							});
						}
						//chartUrls[myplotobj.chartindex].chartmeters.push(d.response.meters[dd].mtrid);
						cmeters.push(d.response.meters[dd].mtrid);
						chartUrls[myplotobj.chartindex].seriesurl.push("st="+extract.st+"&et="+extract.et+"&intervalmin="+extract.intervalmin+"&includepredicted="+extract.includepredicted+"&sensortype="+m+"&search="+extract.search+"&exclude="+extract.exclude+"&include="+d.response.meters[dd].mtrid+"&interval="+extract.interval+"&turnoffvee="+extract.turnoffvee);
						var stuff = d.response.meters[dd].data || null;
						var stats = null;
					//	if(extract.hasOwnProperty('turnoffvee')&&extract.turnoffvee=="true"){
							var badpoints = detectBad(d.response.meters[dd]);
							_.each(badpoints,function(bad){
								erroneous.push(bad);
							});
					//	}
						try{
						stats = [(d.response.meters[dd]||{}).statistics];
						stats[0].quality = ((d.response.meters[dd]||{}).quality||{});
						}
						catch(e){
							console.log('Err(Statistics) '+e);
							stats = [];
						}
						d.data = stuff;
						if(units_switch)
						{
							var newdata = convertUnits(units_switch[0],units_switch[1],d.data);
						}
						var	payload	=	[];
  					var isDigital = false;
            if(sensortype_map[m] == 'digital')
                    isDigital = true;
            for(var i=0;    i<d.data.length;        i++)
                    {
            if(isDigital && i != 0){
                    var     temp    =       {};
                    if(!isNaN(d.data[i].value))
                            temp.y  =       parseFloat(d.data[i].value);
                    else
                            temp.y = payload[i-1].y;
            //              continue;
                    temp.x  =       d.data[i].ts||null;
                    payload.push(temp);
                    delete(temp);
						}	
						else
						{
								var	temp	=	{};
								if(!isNaN(d.data[i].value))
									temp.y	=	parseFloat(d.data[i].value);
								else
									temp.y = null;
								temp.x	=	d.data[i].ts || null;
								payload.push(temp);
								delete(temp);
							}
						}
						var rawannt = loadAnn("st="+extract.st+"&et="+extract.et+"&intervalmin="+extract.intervalmin+"&includepredicted="+extract.includepredicted+"&sensortype="+m+"&search="+extract.search+"&include="+extract.include+"&exclude="+extract.exclude,true);						
						var ret = [];
						for(var g=0;g<rawannt.sensors.length;g++){
						if(rawannt.sensors[g].id == thisname){
							for(var h=0; h<rawannt.sensors[g].data.length; h++)
								ret.push({x:rawannt.sensors[g].data[h].ts,title:'A', text:rawannt.sensors[g].id+'<br/> (MSG): '+rawannt.sensors[g].data[h].annotation.replace(/(?:\r\n|\r|\n)/g, '<br />')})
						}
					}
						var flagobj = {data:flags(ret),width:18,showInLegend:false,type:'flags',shape : 'squarepin',name:'flags',onSeries:'ID_'+uniquel+dd,useHTML:true};
						var thisname = 'Unknown';
						var meta = null;
						if(d.response.meters[dd].hasOwnProperty('mtrid')){	// REGULAR PLOT
							if(d.response.meters[dd]!=undefined && d.response.meters[dd].hasOwnProperty('meta'))
		                        meta = d.response.meters[dd].meta._source
		                    else
								meta = meterInfo(d.response.meters[dd].mtrid);
							if(meta){
								if(sensortype_displaymap[meta._id] == undefined){
									Notify('Plotter','Station '+meta._id+' is not available on map','normal',true);
									continue;
								}
								if(sensortype_displaymap[meta._id][m]==undefined){
									Notify('Plotter','Station '+meta._id+' is not available on map for '+m,'normal',true);
									continue;
								}
								var __txt = sensortype_displaymap[meta._id][m].substr(0,sensortype_displaymap[meta._id][m].length-1)+'-'+meta.supply_zone+')';
								if(meta.tag_category != undefined && meta.tag_category == 'residential')
									thisname = __txt+'('+units[m]+')';
								else
									thisname = __txt+'('+units[m]+')';
								}
							else
								thisname = thisname = m+':'+units[m];

						}
						else
							thisname = operation;
						if(operation != 'data')
							thisname = thisname + ' :: '+operation;
						var isstep = false;
						if(sensortype_map[m]=='digital')
							isstep = true;
						var obj = {
							name:thisname,
							data:payload,
							id:'ID_'+uniquel+dd,
							yAxis:axis,
							stack:axis,
							step:isstep,
							dataGrouping :  {
			                enabled: _datagroupingoption
			                }
						}
						if(ser_clr)
							obj.color = ser_clr[0];
						
						charts[myplotobj.chartindex].addSeries(obj);
						charts[myplotobj.chartindex].addSeries(flagobj);
					
						if(!units_switch){
						charts[myplotobj.chartindex].yAxis[axis].update({
		                title:{               
        					text: units[m]	}
    					});
					}
						if(ser_clr)
							ser_clr.splice(0,1);
						
        					//if(axis){
							if(!chartUrls[myplotobj.chartindex].type2 && units[chartUrls[myplotobj.chartindex].type] != units[sensorT])
								chartUrls[myplotobj.chartindex].type2 = m;
						//	}
						var clr = charts[myplotobj.chartindex].series[charts[myplotobj.chartindex].series.length-2].color;
					}
					if(nometa == null){
						if(operation == 'data')
							modLegend(myplotobj.chartindex,[clr],inc,1,stats,myplotobj);
						else
							modLegend(myplotobj.chartindex,[clr],inc,2,stats,myplotobj);
					}
						if(erroneous.length>0)
						{
							var err_load = {data:erroneous,width:16,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags',useHTML:true};
							charts[myplotobj.chartindex].addSeries(err_load);
						}
					if(nometa == null){
						chartUrls[myplotobj.chartindex].chartmeters.push(cmeters.toString());
						chartUrls[myplotobj.chartindex].names.push(m +' '+ (inc[dd] || myplotobj.meters[dd]));
						chartUrls[myplotobj.chartindex].sensorType.push(m);
						chartUrls[myplotobj.chartindex].charthistory.push([ep,_prm]);
						var derived_list = merge_options(chartUrls[myplotobj.chartindex].derivations,derived_insert);
						chartUrls[myplotobj.chartindex].derivations = derived_list;
						//delete myplotobj;
						$('.addmodebtn').remove();
						$('.minWindow').removeClass('addmode');
					}
						charts[myplotobj.chartindex].hideLoading();
						checkReg(charts[myplotobj.chartindex]);
						plotcomplete(1);
			},'json');
		addplotobj.reset();
	}
	if(type == 2){	//AGGREGATE
	m = 'CONSUMPTION';
	var sensorT = m
	var myplotobj = JSON.parse(JSON.stringify(aggregateobj));
	// finding axis
	if(units[chartUrls[myplotobj.chartindex].type] != units[sensorT])
		axis = 1;
	else
		axis = 0;
	// find eligibality
	if(chartUrls[myplotobj.chartindex].type2 != null){
		if(units[m] != units[chartUrls[myplotobj.chartindex].type] && units[m] != units[chartUrls[myplotobj.chartindex].type2] )
		{	
			//alert('Cannot add to this chart. Axis full');
			Notify('Chart Alert!','Cannot add to this chart. Axis full','normal',true)
			$('.addmodebtn').remove();
			$('.minWindow').removeClass('addmode');
			addseriesobj.reset();
			return;
		}
	}
	// smart units selection
	var units_switch = false;
	if(chartUrls[myplotobj.chartindex].units[axis]!=units[m]	&&	chartUrls[myplotobj.chartindex].units[axis]!='')	//units have been changed
	{
		if(unitsMatch(units[m],chartUrls[myplotobj.chartindex].units[axis]))
			units_switch = [units[m],chartUrls[myplotobj.chartindex].units[axis]];
	}
	var pp = getActionParams();
	var searchjson;
	if(allmode){
		searchjson = pp.Searchjson;
		pp.include = '';
	}
	else
		searchjson = '';
	var _prm = src || "st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()+"&interval="+_interval+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&turnoffglobalexclusion='+!globalex;
	var extract = extractParams(_prm);
	$.get('controllers/proxyman.php',{endpoint:"data/search/aggregate",params:_prm},
			function(d,e,f){
				var title;
				if(pp.searchtxt == undefined || src!=null)
					title = 'Total'
				else{
					var titexpl = pp.searchtxt.split(',');
					title = titexpl[titexpl.length-1];
					}
				// Title Selection
				var meterlist = [];
				if(extract.include.length>0 && allmode == false){
				_.each(extract.include.split(','),function(mm){
					var info = meterInfo(mm);
					var name = info.bp_name || info.customer_name || info.customername || null;
					meterlist.push(name);
				});
				var groupmeters = _.uniq(meterlist);
				if(groupmeters.length == 1)
					title = groupmeters[0];
			}
				var stuff = d.response.meters[0].data || null;
				var stats = [d.response.meters[0].statistics];
				stats[0].quality = d.response.meters[0].quality;
				d.data = stuff;
				if(units_switch)
				{
					var newdata = convertUnits(units_switch[0],units_switch[1],d.data);
					console.log(newdata);
				}
				var uniquel = charts[myplotobj.chartindex].series.length;
						var	payload	=	[];
						for(var	i=0;	i<d.data.length;	i++)
							{
								var	temp	=	{};
								temp.y	=	d.data[i].value || null;
								temp.x	=	d.data[i].ts || null;
								payload.push(temp);
								delete(temp);
							}
						charts[myplotobj.chartindex].addSeries({
							name:title,
							data:payload,
							yAxis:axis,
							id:'aggr'+uniquel,
							stack:axis,
							dataGrouping :  {
			                    enabled: _datagroupingoption
			                }
						});
						var annotations = loadAnn("st="+extract.st+"&et="+extract.et+"&intervalmin="+extract.intervalmin+"&includepredicted="+extract.includepredicted+"&sensortype="+extract.sensortype+"&search="+extract.search+"&include="+extract.include+"&exclude="+extract.exclude);
						var flagobj = {data:flags(annotations),width:16,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags',onSeries:'aggr'+uniquel,useHTML:true};
						charts[myplotobj.chartindex].addSeries(flagobj);
						if(!units_switch){
						charts[myplotobj.chartindex].yAxis[axis].update({
		                title:{               
        					text: units[m]	}
    					});
					}
    					var cmeters=extract.include;
    					if(extract.include.length==0)
    						cmeters = searchjson;
						chartUrls[myplotobj.chartindex].names.push('Aggr');
						chartUrls[myplotobj.chartindex].chartmeters.push(cmeters);
						chartUrls[myplotobj.chartindex].endpoint.push("data/search/aggregate");
						chartUrls[myplotobj.chartindex].charthistory.push(["data/search/aggregate",_prm]);
						checkReg(charts[myplotobj.chartindex]);
						chartUrls[myplotobj.chartindex].seriesurl.push("st="+extract.st+"&et="+extract.et+"&intervalmin="+extract.interval+"&includepredicted="+extract.includepredicted+"&sensortype="+extract.sensortype+"&search="+extract.search+"&include="+extract.include+"&exclude="+extract.exclude+"&interval="+extract.interval);
						if(!chartUrls[myplotobj.chartindex].type2 && chartUrls[myplotobj.chartindex].type != m)
							chartUrls[myplotobj.chartindex].type2 = m;
						var clr = charts[myplotobj.chartindex].series[charts[myplotobj.chartindex].series.length-2].color;
						modLegend(myplotobj.chartindex,[clr],extract.include.split(','),2,stats);
						delete myplotobj;
						$('.addmodebtn').remove();
						$('.minWindow').removeClass('addmode');
			},'json');
		aggregateobj.reset();
	}
	if(type == 3){	//FLOW TANKS (DEPRECATED)
			var url = "https://waterwise.visenti.com/waterwise2/fcph/support/getdata_support.php?op=eventmon&ev=0&ad=0&ut=stdout&starttime="+st+"&endtime="+et+"&avg=1h&nodeid="+tanksobj.id+"&sensorname=OUTFLOW";
			var sensorT = 'tankflow';
			var un = units[sensorT];
			// finding axis
			if(chartUrls[tanksobj.chartindex].type != sensorT)
				axis = 1;
			else
				axis = 0;
			// find eligibality
			if(chartUrls[tanksobj.chartindex].type2 != null){
				if(sensorT != chartUrls[tanksobj.chartindex].type && sensorT != chartUrls[tanksobj.chartindex].type2 )
				{	
					//alert('Cannot add to this chart. Axis full');
					Notify('Chart Alert!','Cannot add to this chart. Axis full','normal',true)
					return;
				}
			}
			$.get('controllers/proxyman.php',{params:url,ww:'true'},function(d)
				{
					if(!d)	{
						//alert('Error - Bad Data Received');
						Notify('Data Alert!','Error - Bad Data Received','normal',true)
						return;
					}
					var payload = [];
					var stuff = d.split('|');
					for(var i=0; i<stuff.length; i++)
					{
						var extracted = stuff[i].substring(1, stuff[i].length-1);
						var tankdata = extracted.split(',');
						if(isNaN(parseInt(tankdata[0])) || isNaN(parseFloat(tankdata[1])))
							continue;
						var temp = {x:parseInt(tankdata[0]),y:parseFloat(tankdata[1])};
						payload.push(temp);
					}
					charts[tanksobj.chartindex].addSeries({
							name:'Flow '+tanksobj.id,
							data:payload,
							yAxis:axis
						});
					charts[tanksobj.chartindex].yAxis[axis].update({
		                title:{               
        					text: un }
    					});
					chartUrls[tanksobj.chartindex].names.push('Flow '+tanksobj.id);
					if(!chartUrls[tanksobj.chartindex].type2 && chartUrls[tanksobj.chartindex].type != sensorT)
						chartUrls[tanksobj.chartindex].type2 = sensorT;
					var clr = charts[tanksobj.chartindex].series[charts[tanksobj.chartindex].series.length-1].color;
					modLegend(tanksobj.chartindex,[clr],tanksobj.id,1);
					tanksobj.reset();
					$('.minWindow').removeClass('addmode');
	});
	}
	if(type == 4)	// WEATHER STATIONS (DEPRECATED)
	{
		var un = units[m];
			// finding axis
			if(chartUrls[weatherobj.chartindex].type != m)
				axis = 1;
			else
				axis = 0;
			// find eligibality
			if(chartUrls[weatherobj.chartindex].type2 != null){
				if(sensorT != chartUrls[weatherobj.chartindex].type && sensorT != chartUrls[weatherobj.chartindex].type2 )
				{	
					//alert('Cannot add to this chart. Axis full');
					Notify('Chart Alert!','Cannot add to this chart. Axis full','normal',true)
					return;
				}
			}
		var ep;
		if(m == 'RAIN')
			ep = "rainfile";
		if(m == "TEMP")
			ep = "tempfile";
		$.get('controllers/proxyman.php',{endpoint:'data/'+ep+'?st='+st+'&et='+et+'&sample='+weatherobj.id},
			function(d,e,f){
					var	payload	=	[];
					for(var	i=0;	i<d.data.length;	i++)
						{
							var	temp	=	{};
							if('temp' in d.data[i]){
								if(d.data[i].temp==0)
									temp.y=null;
								else
									temp.y	=	d.data[i].temp;
							}
							else
							{
								temp.y	=	d.data[i].rainfall;
							}
							//temp.y	=	d.data[i].temp;
							temp.x	=	d.data[i].ts;
							payload.push(temp);
							delete(temp);
						}
					charts[weatherobj.chartindex].addSeries({
						name:m+' '+weatherobj.id,
						data:payload,
						yAxis:axis
				});
					charts[weatherobj.chartindex].yAxis[axis].update({
		                title:{               
        					text: un }
    					});
					chartUrls[weatherobj.chartindex].names.push(m+' '+weatherobj.id);
					if(!chartUrls[weatherobj.chartindex].type2 && chartUrls[weatherobj.chartindex].type != m)
							chartUrls[weatherobj.chartindex].type2 = m;
					var clr = charts[weatherobj.chartindex].series[charts[weatherobj.chartindex].series.length-1].color;
					modLegend(weatherobj.chartindex,[clr],weatherobj.id,1);
					weatherobj.reset();
					$('.addmodebtn').remove();
					$('.minWindow').removeClass('addmode');
			},'json');
	}

	if(type == 5){	//AVERAGE
	// finding axis
	if(units[chartUrls[averageobj.chartindex].type] != units[m])
		axis = 1;
	else
		axis = 0;
	// find eligibality
	if(chartUrls[averageobj.chartindex].type2 != null){
		if(units[m] != units[chartUrls[averageobj.chartindex].type] && units[m] != units[chartUrls[averageobj.chartindex].type2] )
		{	
			//alert('Cannot add to this chart. Axis full');
			Notify('Chart Alert!','Cannot add to this chart. Axis full','normal',true)
			$('.addmodebtn').remove();
			$('.minWindow').removeClass('addmode');
			addseriesobj.reset();
			return;
		}
	}
	var pp = getActionParams();
	var searchjson;
	if(allmode){
		searchjson = pp.Searchjson;
		pp.include = '';
	}
	else
		searchjson = '';
		$.get('controllers/proxyman.php',{endpoint:"data/search/average",params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include="+pp.include.toString()+"&exclude="+pp.exclude.toString()},
			function(d,e,f){
				var title;
				if(pp.searchtxt == undefined)
					title = 'Avg'
				else{
					var titexpl = pp.searchtxt.split(',');
					title = titexpl[titexpl.length-1];
				}
				var stuff = d.response.meters[0].data || null;
				var stats = [d.response.meters[0].statistics];
				stats[0].quality = d.response.meters[0].quality;
				d.data = stuff;
						var	payload	=	[];
						for(var	i=0;	i<d.data.length;	i++)
							{
								var	temp	=	{};
								if(m=='TEMP')
								{
									if(d.data[i].value==0)
										d.data[i]=null;
									temp.y	=	d.data[i].value || null;
								}
								else
								{
									temp.y	=	d.data[i].value || null;
								}
								//temp.y	=	d.data[i].value;
								temp.x	=	d.data[i].ts || null;
								payload.push(temp);
								delete(temp);
							}
						charts[averageobj.chartindex].addSeries({
							name:m+' '+title,
							data:payload,
							yAxis:axis,
							stack:axis,
							dataGrouping :  {
			                    enabled: _datagroupingoption
			                }
						});
						charts[averageobj.chartindex].yAxis[axis].update({
		                title:{               
        					text: units[m]	}
    					});
						chartUrls[averageobj.chartindex].names.push('AVG '+m);
						checkReg(charts[averageobj.chartindex]);
						if(!chartUrls[averageobj.chartindex].type2 && chartUrls[averageobj.chartindex].type != m)
							chartUrls[averageobj.chartindex].type2 = m;
						averageobj.reset();
						$('.addmodebtn').remove();
						$('.minWindow').removeClass('addmode');
			},'json');
	}
	if(type == 6)	// DailyDemand
	{
		//THIS CASE ONLY
		var w = m;
		if(defaultSettings.demandForecast)
		{
			var m = inc=defaultSettings.dmaItems[0].sensor
			var ep = 'data/forecast/default';
			inc=defaultSettings.dmaItems[0].id
			var watertype='&search=&include='+inc+'&exclude=&interval=';
		}
		else
		{
			var m = 'SGDEMAND';
			var ep = 'singaporedata/demand';
			var watertype = '&watertype='+w;
		}
		var cluster = '';
		// if($('#DemandCluster .regular').length>0)
		// {
		// 	cluster = '&include=';
		// 	var txt=[];
		// 	$('#DemandCluster .regular').each(function(i,e){
		// 		txt.push($(this).text().toLowerCase().replace(/\s+/g, ''));
		// 	});
		// 	cluster = cluster+txt.toString();
		// }
		
		if(isZone(w)){
			m = 'ACTUAL';
			ep = ep+'/'+wwMap(w).toLowerCase()+'/0';
			watertype = '&search=&include=&exclude=&i=&interval=';
		}
		if(units[chartUrls[addseriesobj.chartindex].type] != units[m])
			axis = 1;
			else
			axis = 0;
		// find eligibality
		if(chartUrls[addseriesobj.chartindex].type2 != null){
			if(units[m] != units[chartUrls[addseriesobj.chartindex].type] && units[m] != units[chartUrls[addseriesobj.chartindex].type2] )
			{	
				//alert('Cannot add to this chart. Axis full');
				Notify('Chart Alert!','Cannot add to this chart. Axis full','normal',true);
				$('.addmodebtn').remove();
				$('.minWindow').removeClass('addmode');
				addseriesobj.reset();
				return;
			}
	}
		var units_switch = false;
		if(chartUrls[addseriesobj.chartindex].units[axis]!=units[m]	&&	chartUrls[addseriesobj.chartindex].units[axis]!='')	//units have been changed
		{
			if(unitsMatch(units[m],chartUrls[addseriesobj.chartindex].units[axis]))
				units_switch = [units[m],chartUrls[addseriesobj.chartindex].units[axis]];
		}
		$.get('controllers/proxyman.php',{endpoint:ep,params:'st='+st+'&et='+et+'&includepredicted='+pred+'&intervalmin='+interval+'&sensortype='+m+watertype+"&turnoffvee="+veecheck+'&includeflag='+flagsbit+'&turnoffglobalexclusion='+!globalex},function(d){
			d.data = d.data || null;
			if(d.errorCode>0){
				//alert('No Data Available');
				Notify(d.errorMsg,'Server Errors','normal',true)
				return;
			}
			if(!d.response)
			{
				//alert('No Data Available');
				//Notify('Data Fetch Error','No data available','normal',true)
			//	return;	
			}
			//d.data = d.response.data;
			if(!d.data)
				d.data = d.response.data || d.response.meters[0].data || [];
			var payload = [];
			if(units_switch)
			{
				var newdata = convertUnits(units_switch[0],units_switch[1],d.data);
			}
			for(var i=0; i<d.data.length; i++)
			{
				var temp = {};
				temp.y = d.data[i].value || null;
				temp.x = d.data[i].ts || null;
				payload.push(temp);
				delete payload;
			}
			charts[addseriesobj.chartindex].addSeries({
							name:'Daily Demand '+w+' (SG)',
							data:payload,
							yAxis:axis,
							stack:axis
						});
					if(!units_switch){
						charts[addseriesobj.chartindex].yAxis[axis].update({
		                title:{               
        					text: units[m]	}
    					});
					}
						chartUrls[addseriesobj.chartindex].names.push('Daily Demand '+w+' (SG)');
						if(!chartUrls[addseriesobj.chartindex].type2 && chartUrls[addseriesobj.chartindex].type != m)
							chartUrls[addseriesobj.chartindex].type2 = m;
						checkReg(charts[addseriesobj.chartindex]);
						addseriesobj.reset();
						$('.minWindow').removeClass('addmode');
		},'json');
	}
	if(type == 7)	// Zone Demand
	{	//DECAPRECATED
		//THIS CASE ONLY
		var zone = m;
		m = 'OUTFLOW';
		if(chartUrls[addseriesobj.chartindex].type != m)
			axis = 1;
			else
			axis = 0;
		// find eligibality
		if(chartUrls[addseriesobj.chartindex].type2 != null){
			if(m != chartUrls[addseriesobj.chartindex].type && m != chartUrls[addseriesobj.chartindex].type2 )
			{	
				//alert('Cannot add to this chart. Axis full');
				Notify('Chart Alert!','Cannot add to this chart. Axis full','normal',true);
				addseriesobj.reset();
				return;
			}
	}
	//url_f = "https://waterwise.visenti.com/waterwise2/"+zone+"/support/getDemandPredictionData_support.php?op=forecast&z=0&st="+st+"&et="+et;
	url_a = "https://waterwise.visenti.com/waterwise2/"+wwMap(zone).toLowerCase()+"/support/getDemandPredictionData_support.php?op=dailyactual&z=0&st="+st+"&et="+et;
		/*	$.get('controllers/proxyman.php',{params:url_f,ww:'true'},
		function(d){
			console.log(d);
			var stuff = JSON.parse(d.data);
			charts[addseriesobj.chartindex].addSeries({
				name:'Forecast '+zone.toUpperCase(),
				data:stuff,
				yAxis:axis
			});*/
			$.get('controllers/proxyman.php',{params:url_a,ww:'true'},
		function(d){
			console.log(d);
			var stuff = JSON.parse(d.data);
			console.log(stuff);
			charts[addseriesobj.chartindex].addSeries({
				name:'Daily Actual '+zone.toUpperCase(),
				data:stuff,
				yAxis:axis
			});
		//	Non Sync
		charts[addseriesobj.chartindex].yAxis[axis].update({
	                title:{               
    				text: units[m]	}
    				});
		if(!chartUrls[addseriesobj.chartindex].type2 && chartUrls[addseriesobj.chartindex].type != m)
		chartUrls[addseriesobj.chartindex].type2 = m;
		chartUrls[addseriesobj.chartindex].names.push(zone.toUpperCase()+' Demand');
		addseriesobj.reset();
		$('.minWindow').removeClass('addmode');
		},'json');
		//},'json');
	}
	if(type == 8)	// IN PROGRESS
	{
		alert('This feature is not supported yet');
		addseriesobj.reset();
		return;
	}

	// NETWORK SIMULATIONS
	if(type == 9)
	{
		if(!src){
			Notify('Warning - Plotter','Source not found','normal',true);
			addseriesobj.reset();
			return;
		}
		var par = extractParams(src);
		var _id = par.nodeid||par.include;
		var _zone = par.zone||'';
		$.get('controllers/tlp.php',{endpoint:optional_ep,params:src},function(e){
			var payload = [];
			if(e.errorCode>0){
            Notify('Network Simulations',e.errorMsg,'normal',true);
            addseriesobj.reset();
            return;
          }
          var response = e.response.results[0].datalist;
          var payload = [];
          _.each(response,function(r){
            payload.push([r.timestamp,parseFloat(r.value.toFixed(2))])
          });
          charts[addseriesobj.chartindex].addSeries({
          	data:payload,
          	name:_id+' - Simulated - '+_zone
          });
          addseriesobj.reset();
		},'json');
		$('.minWindow').removeClass('addmode');
	}
	//callback
	if(typeof(callback)=='function')
		callback();
	/*joblist.splice(0,1);
	batch.splice(0,1);
	if(joblist.length>0)
	{
		Radiochecked = joblist[0]||null;
		if(addseriesobj.mode)
			addseriesobj.reset();
		if(!_decouple){
		addseriesobj.mode=true;
		addseriesobj.chartindex = charts.length-1;	//Latest Chart created
		}
		plotHQ(type);
	}*/
}
function aggr2plot(el,ind){
	aggregateobj.mode = true;
	aggregateobj.addmode = true;
	aggregateobj.chartindex = ind;
	$('.plotter .minme').trigger('click');
	$(el).addClass('addmode');
	var pos = $(el).position();
	/*var tray = document.createElement('div');
	tray.className = 'addTray';
	tray.innerHTML = '<ul></ul><div class="connection"></div><div class="killme">x</div>';
	$('body').append(tray);*/
	showAction();
	$('.addTray').animate({left:pos.left},'slow');
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function checkBounds()
{
	var res = [];
	for(var i=0; i<metadata.length; i++){
		console.log(metadata[i].loc[0].lat)
		var coordinate = new google.maps.LatLng(metadata[i].loc[0].lat, metadata[i].loc[0].lng);
		var isWithinPolygon = polygons[0].poly.containsLatLng(coordinate);
		if(isWithinPolygon)
			res.push(metadata[i]._id)
	}
	console.log(JSON.stringify(res));
}

function showweatherInfo()
{
	
	var Ww={};
	Ww.weather='weather';
	var parseSearch=parseQueryJson(Ww);
	//console.log(parseSearch)
	var response={};
	var pURL='controllers/proxyman.php';
	$.ajax({
		type: "POST",
		url: pURL,
			  //contentType: "application/json",
		data: {search:true,"SearchQuery":parseSearch},
		dataType:"json"
	}).done(function( Dataresponse ) {
		response.data=[];
		$.each(Dataresponse.response.hits.hits,function(key,value){
			response.data.push(this._source)
		});
		
		if(objectSize(Dataresponse.response.hits.hits)<=0)
		{
			//alert('No data available');
			Notify('Data Fetch Error','No data available','normal',true)
			return;
		}
		else {
			console.log(response)
			clearMarkers(true);
			MarkerInfoData=response;
			plotIcons(MarkerInfoData, false);
		}
	});
}
function showwebmsInfo()
{
	
	var Ww={};
	Ww.wems='wems';
	var parseSearch=parseQueryJson(Ww);
	console.log(parseSearch)
	var response={};
	var pURL='controllers/proxyman.php';
	$.ajax({
		type: "POST",
		url: pURL,
			  //contentType: "application/json",
		data: {search:true,"SearchQuery":parseSearch},
		dataType:"json"
	}).done(function( Dataresponse ) {
		response.data=[];
		$.each(Dataresponse.response.hits.hits,function(key,value){
			response.data.push(this._source)
		});
		
		if(objectSize(Dataresponse.response.hits.hits)<=0)
		{
			//alert('No data available');
			Notify('Data Fetch Error','No data available','normal',true)
			return;
		}
		else {
			console.log(response)
			clearMarkers(true);
			MarkerInfoData=response;
			plotIcons(MarkerInfoData,false);
		}
	});
	
}

function findGroupIcon(x){
	var arr = group[x];
	var type = null;
	for(var i=0; i<group[x].length; i++)
	{
		var sector = metadata[group[x][i][3]].tag_watertype	||	null;
		if(!sector)
			return 'others';
		else
		{
			if(!type)
				type = sector;
			else
			{
				if(type != sector)
					return 'mixed';
			}
		}
	}
	return type;
}
function resetMap(){
	for(var i=0; i<markers.length; i++){
		markers[i].setMap(null);
	}
	markers.splice(0,markers.length);
	$('.plotter .closeme').trigger('click');
	$('.close_btn').trigger('click');
	$('.sub_close').trigger('click');
	$('.sub_close_btn').trigger('click');
	$('.clearlink').trigger('click')
	$('.control_area .label').each(function(){
		$(this).removeClass('regular');
		$(this).addClass('secondary');
	});
	lastSearchList=[];
	resetSel();
	showAction(null);
	reloadmarkers()

}

function reloadmarkers()
{
	firstload = true;
	var jsonobj=defaultjson();
	var response={};
	var pURL='controllers/proxyman.php';
	$.ajax({
		type: "POST",
		url: pURL,
			  //contentType: "application/json",
		data: {search:true,"SearchQuery":jsonobj},
		dataType:"json"
	}).done(function( Dataresponse ) {
		response.data=[];
		$.each(Dataresponse.response.hits.hits,function(key,value){
			response.data.push(this._source)
		});
		
		if(objectSize(Dataresponse.response.hits.hits)<=0)
		{
			//alert('No data available');
			Notify('Data Fetch Error','No data available','normal',true)
			return;
		}
		else {
			
			MarkerInfoData=response;
			plotIcons(MarkerInfoData, false);
			//loadDMAZones()
		}
	});

}
function toggleBounce(marker) {

  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
function showList(){
	$('.listview').removeClass('hide').addClass('slideInDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
    console.log('Animation Ended');
    $(this).removeClass('slideInDown');
	});
}
function hideList(){
	$('.listview').addClass('slideOutUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
    console.log('Animation Ended');
    $(this).removeClass('slideOutUp').addClass('hide');
	});
}
function selectAll(){
/*	alert('Error');
	return;*/
	if(allmode)
	{
		// unitObj.selectedUnit.splice(0,unitObj.selectedUnit.length);
		removeAllEmpty()
		$.each(markers,function(key,value){
			if(value.visible==true)
			{
				$.each(group[value.groupindex], function (key1,value1){
					
					//if(checkBeforePush(value1[2]))
					{
						unitObj.selectedUnit.push(value1[2]);
						_id=value1[2];
						unitObj.ids[_id]=value.metaindex;
						unitObj.sensorSelection[_id]=[];
						unitObj.sensorMeta[_id]=metadata[value.metaindex]
						// $.each(actionListObj.UnitOptions,function(key2,value2){
						// 	if(value2.type.indexOf(lastknownType)>-1)
						// 	{
						// 		unitObj.sensorSelection[_id].push(value2.sensortype_actual)
						// 	}
						// })
						unitObj.lastselected.meter=unitObj.selectedUnit[unitObj.selectedUnit.length-1];
						unitObj.lastselected.index=unitObj.ids[unitObj.lastselected.meter]
					}
					// else
					// {
					// 	Notify('Selection Alert !','Sorry, Mixed marker selection is not possible, please clear the tray to start as new.','normal',true)
					// }
				})
			}

		})		
	}
	else
	{
		unitObj.selectedUnit.splice(0,unitObj.selectedUnit.length);
		lastknownType=null;
	}
	showAction('unit');
	refreshTray();
}

function stopAnimation(marker)
{
	if(marker.getTitle()=='none')
	{
		marker.setTitle('selected');
	}
	else
	{
		marker.setTitle('none')
	}
}
function resetObj()
{
	unitObj.selectedUnit.splice(0,unitObj.selectedUnit.length);
    unitObj.sensorSelection={};
    unitObj.sensorMeta={};
    unitObj.lastselected={};
    unitObj.ids={};
 //    $.each(settings.subzone,function(key,value){
	// 	$('#zoneSelection #'+value).trigger('click');
	// })
	
	$('#zoneSelection li.sel').each(function(key,value){
		$(this).trigger('click');
	})
	$('#taglist .tags .closeTag').each(function(e){
   			$(this).trigger('click');
   	})
}
function resetTrayObj()
{
	unitObj.selectedUnit.splice(0,unitObj.selectedUnit.length);
    unitObj.sensorSelection={};
    unitObj.sensorMeta={};
    unitObj.lastselected={};
    unitObj.ids={};
}
function resetSel(){
	resetObj();
	includeMarkers.splice(0,includeMarkers.length);
	excludeMarkers.splice(0,excludeMarkers.length);

	lastknownType=null;
	$('.windows').removeClass('regular').addClass('secondary');
	$('.innerMeters').removeClass('regular').addClass('secondary');
	$('.addweather').removeClass('regular').addClass('secondary');
	if(allmode)
	{
		$('#selectAll').trigger("click");
		$.each(settings.subzone,function(key,value){
			$('#zoneSelection #'+value).trigger('click');
		})
		
	}
	refreshTray();
	showBucket();
	showAction();
	if(infowindow)
		infowindow.close();
}
function tglLegend(){
	$('.legend ul').toggleClass('hide');
	if($('.legend ul').hasClass('hide')){
		$('.legend a').empty().append('Show Legend');
	}
	else
		$('.legend a').empty().append('Hide Legend');	

}
function getDays(chart,x){
	var serieslength = chart.series[x].data.length;
	for(var i=0; i<serieslength; i++){
		var temp = new Date(chart.series[x].data[i].x).getDay();
		if(temp == 0 || temp == 6)
			annotate(chart,x,temp,i);
		delete temp;
	}
}
function annotate(chart,x,day,idx){
	console.log(day);
	if(day == 0)
		day = 'Sun';
	if(day == 6)
		day = 'Sat';
	var point = chart.series[x].data[idx];
	var text = chart.renderer.text(day, point.plotX + chart.plotLeft - 10,
				point.plotY + chart.plotTop - 10)
				.attr({ zIndex:5 }).add();
	var box = text.getBBox();
        chart.renderer.rect(box.x - 5, box.y - 5, box.width + 10, box.height + 10, 5)
            .attr({
                fill: '#FFFFEF',
                stroke: 'gray',
                'stroke-width': 1,
                zIndex: 4
            })
            .add();
}
function fit2map(x){
	for(var i=0; i<x.length; i++)
		bounds.extend(x[i]);
}
// jQUERY CUSTOM PLUGINS
jQuery.fn.justtext = function() { 
    return $(this)  .clone()
            .children()
            .remove()
            .end()
            .text();
};

function applyReg(this_chart,cindex){
		// REGRESSION ROUTINE
	if(!this_chart)
	{
		alert('Err - Regression');
		return;
	}
	var count=0;
	var ser_idx=[];
	var title = [];
	for(var xx=0; xx<this_chart.series.length; xx++){
		if(this_chart.series[xx].name=='Navigator' || this_chart.series[xx].name=='flags')
			continue;
		else{
			++count;
			title.push(this_chart.series[xx].yAxis.options.title.text || null);
			ser_idx.push(xx);
		}
	}
	title = _.uniq(title);
	if(count==2 && title.length == 1){	//create regression line
		var ser=[this_chart.series[ser_idx[0]],this_chart.series[ser_idx[1]]];
		var regdata = [];
		var xdata = [];
		for(var i=0;i<ser.length;i++){
			for(var j=0;j<ser[i].xData.length;j++)
				xdata.push(ser[i].xData[j]);
		}
		xdata = _.uniq(xdata);
		for(var cnt=0;cnt<xdata.length;cnt++){
			var s1 = ser[0].yData[cnt];
			var s2 = ser[1].yData[cnt];
			var newval = (s1 - s2) || null;
			regdata.push([xdata[cnt],newval]);
		}
		this_chart.addSeries({
			name:'Difference',
			data:regdata,
			type:'line',
			yAxis:1
		});
		if(this_chart.type2==null){
			this_chart.yAxis[1].update({
			     title:{ 	
			    	text: title.toString()	}
	   		});
   		chartUrls[cindex].type2 = 'trend';
   		}
	}
}
function checkReg(this_chart){
		var count=0;
		var title=[];
		for(var xx=0; xx<this_chart.series.length; xx++){
		if(this_chart.series[xx].name=='Navigator' || this_chart.series[xx].name=='flags')
			continue;
		else{
			++count;
			title.push(this_chart.series[xx].yAxis.options.title.text || null);
		}
	}
	title = _.uniq(title);
	var el = $(this_chart.container).parents('.plotter');
	if(el.length == 0)
		el = $(this_chart.container).parents('.stackplotwrap');

	if(count>1){	//TURNING OFF AUTO RESOLUTION IF MULTIPLE SERIES FOUND
		$(el).find('.autoplot_switch').parent('.controls').addClass('hide');	
		if(chartUrls[this_chart.getChartIndex()].hasOwnProperty('autoload'))
			chartUrls[this_chart.getChartIndex()].autoload = false;
	}
	// if(title.length==1)
	// 	$(el).find('.swapaxis').parent('.controls').removeClass('hide');	
// per resolution for all yaxis labels
	// var this_resolution = $(el).find('.res_btn').text();
	// _.each(this_chart.yAxis,function(d){ 
	// 	if(d.options.title.text == null || d.options.title.text == '')
	// 		return;
	// 	var type = "type";
	// 	if(d.options.index>0)
	// 		type = "type2";
	// 	d.update({ title:{text:units[chartUrls[this_chart.getChartIndex()][type]]+' per '+this_resolution} });

	//});
}
 function AjaxRequest(url)
{ 
	var xmlhttp = null;
	if(xmlhttp != null)
	{ 
		if(xmlhttp.abort)
			xmlhttp.abort();
		xmlhttp = null; 
	};
	 
	if(window.XMLHttpRequest) // good browsers 
		xmlhttp=new XMLHttpRequest(); 
	else if(window.ActiveXObject) // IE 
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 

	if(xmlhttp == null)
		return null; 

	xmlhttp.open("GET",url,false); 
	xmlhttp.send(null); 

	if(xmlhttp.status >= 200 && xmlhttp.status < 300)// 2xx is good enough 
		return xmlhttp.responseText.split("|"); 
	else 
		return null; 
}
function actionDiv(method,label){
	switch(method){
		case 'plotgraph':
		var dec='';
		var stk='';
		var stn='';
		if(_splitsensors)
		dec = 'checked';
		if(_splitstns)
		stn = 'checked'
		if(_stacked)
		stk = 'checked';
		var ret = '';
			ret = '<div class="newaction"><div id="plotgraph" class="button expand tiny radius split" data-id="plotdrop"><a style="color:#fff;" id="'+method+'">'+label+'</a><span></span></div>\
					<ul id="plotdrop" class="j-dropdown hide">\
					  <li><label for="stacking"><input id="stacking" for="stacking" type="checkbox" '+stk+'>Stack</label></li>\
					  <li><label for="coupling"><input id="coupling" type="checkbox" '+dec+'>Split By Sensors</label></li>\
					  <li><label><input id="stncoupling" type="checkbox" '+stn+'>Split By Stations</label></li>\
			      <li class="dividerli"><label for="lind"><input type="radio" name="eptype" value="0" id="lind" checked>Regular</label></li>';
					_.each(eptype,function(e,idx){
						if(idx === 0)
							return;
						var checked = '';
						if(selectedep === e)
							checked = 'checked';
						var explode = e.split('/');
						var _name = explode[1]+'_l';
						explode[1].charAt(0)
						ret += '<li><label for="'+_name+'"><input type="radio" name="eptype" id="'+_name+'" value="'+(idx)+'" '+checked+'>'+explode[1]+'</label></li>';
					});						    
					ret += '</ul>';
					// <li><label for="laggr"><input type="radio" name="eptype" id="laggr" value="1">Aggregate</label></li>\
					//     <li><label for="lavg"><input type="radio" name="eptype" id="lavg" value="2">Average</label></li>\
					//     <li><label for="lmin"><input type="radio" name="eptype" id="lmin" value="3">Min</label></li>\
					//     <li><label for="lmax"><input type="radio" name="eptype" id="lmax" value="4">Max</label></li>\
					//     <li><label for="fcast"><input type="radio" name="eptype" id="fcast" value="5">Forecast</label></li>\
					return ret;
		break;
		case 'pascalytics':
			return '<div class="newaction"><div id="pascalytics" class="button expand tiny radius split" data-id="pascaldrop"><a style="color:#fff;" id="'+method+'">'+label+'</a><span></span></div>\
					<ul id="pascaldrop" class="j-dropdown hide">\
					  <li><label for="filtering"><input id="filtering" for="filtering" type="checkbox">Critical Transients</label></li></ul>';

		break;

		case 'netsim':
			if(window.hasOwnProperty('netsim'))
				return netsim.button('station');
		break;

		case 'mapcontext':
			return '<a href="#" class="button small expand cbtn" onClick="resetMapContext()">'+label+'</a>'
		break;
		default:
			return '<a href="#" class="button small expand cbtn" id="'+method+'">'+label+'</a>';
		break;
	}
}
     function closest (num, arr) {
                var curr = arr[0];
                var diff = Math.abs (num - curr);
                for (var val = 0; val < arr.length; val++) {
                    var newdiff = Math.abs (num - arr[val]);
                    if (newdiff < diff) {
                        diff = newdiff;
                        curr = arr[val];
                    }
                }
                return curr;
            }
function merge_options(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}
function clearFromLegend(e,c){
	e.options.showInLegend = false;
	e.legendItem = null;
	c.legend.destroyItem(e);
	c.legend.render();
}

function resetObject(x,ignores){
	ignores = ignores || null;
	for(var key in x){
							if(ignores!=null && ignores.indexOf(key)>=0)
								continue;
							if(x[key] === null || x[key] == undefined)
								continue;
              if(typeof(x[key]) === 'function')
                continue;
              else if(typeof(x[key]) === 'object' && x[key].hasOwnProperty('length'))
           	    x[key].splice(0,x[key].length)
              else
                x[key] = null;
          }
}
