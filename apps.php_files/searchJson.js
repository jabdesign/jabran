var jsonQuery={};
var allFilter;
var vtdata={};
var parentChild=[]
var itemRoot=[]
var typesList={"amr":["tag_category","tag_watertype"],"sensor":["sensortype_actual","tag_watertype"]}
var baseSetup={}
var processedJson={}
function elasticQuery(searchOptions) //
{
	allFilter={};
	itemRoot=[];
	var Beforeand={"or":[]};
	jsonQuery={"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"or":[]}]}}}},"size":100000}

	baseSetup.station={};
	baseSetup.sensor={};
	$.each(searchOptions,function(key, value){
		if(key=='text')
		{
			generateBaseList(key,value)
		}
		else if(key=='settings')
		{
			generateBaseList(key,value)
		}
		else
		{
			generateBaseList('dropdown',value)
		}
	});
	ProcessSearchObject(baseSetup)
	generateJson()
	return JSON.stringify(jsonQuery)
}
function generateJson()
{
	
	$.each(processedJson,function(sk,sv){
		
		temp=[]
		baseJson={}
		bool='and'
		baseJson[bool]=[];
		$.each(sv,function(key,value){
			$.each(value.or,function(k,v){
				if(k!='sensortype_actual'&&k!='sensortype_display')
				{
					if(v.length>1)
					{
						var mainTemp={"or":[]};
						$.each(v,function(l,m){

								if(typeof(m)=='object')
								{
									var main={}
									main.and=[];
									$.each(m,function(o,p){
										an={}
										an.term={};
										an.term[k]=p;
										main.and.push(an)
									})
									mainTemp.or.push(main)
								}
								else
								{
									var temp={}
									temp.term={};
									temp.term[k]=m;
									mainTemp.or.push(temp)
								}
						});
						baseJson[bool].push(mainTemp)
					}
					else
					{
						$.each(v,function(l,m){

								if(typeof(m)=='object')
								{
									var main={}
									main.and=[];
									$.each(m,function(o,p){
										an={}
										an.term={};
										an.term[k]=p;
										main.and.push(an)
									})
									baseJson[bool].push(main)
								}
								else
								{
									var temp={}
									temp.term={};
									temp.term[k]=m;
									baseJson[bool].push(temp)
								}
						});
					}
				}
			});
		})
		if(Object.keys(baseJson[bool]).length>0)
		{
			temp.push(baseJson)
			var haschildbool=false;
			if(Object.keys(processedJson[sk].sensor.or).length>0)
			{
				child=generateChild(processedJson[sk].sensor)
				// console.log(baseJson)
				baseJson.and.push(child)
				// temp.push(child)
				haschildbool=true;
			}
			var parent=generateParent(baseJson,haschildbool)
			temp.push(parent)
			myjson={};
			myjson.or=temp
			// $.each(temp,function(y,z){
			// 	jsonQuery.query.filtered.filter.bool.must[0].or.push(z)
			// })
		jsonQuery.query.filtered.filter.bool.must[0].or.push(myjson)
		}
	});
	
	// console.log(myjson)
	
}
function generateChild(forChild)
{
	var baseJson={}
	baseJson.or=[];
	var temp={
	  "has_child": {
	    "type": Parentconfig.sensorIndex,
	    "query": {
	      "filtered": {
	        "query": {
	          "match_all": {}
	        },
	        "filter": {}
	      }
	    }
	  }
	}
	$.each(processedJson.sensors.sensor.or,function(k,v){
				var temp={}
				$.each(v,function(l,m){
					if(typeof(m)=='object')
					{
						var main={}
						main.and=[];
						$.each(m,function(o,p){
							an={}
							an.term={};
							an.term[k]=p;
							main.and.push(an)
						})
						baseJson.or.push(main)
					}
					else
					{
						temp.term={};
						temp.term[k]=m;
						baseJson.or.push(temp)
					}
				});
				
	});
	temp.has_child.query.filtered.filter.and=baseJson;
	return temp
}
function generateParent(forParent,childProp)
{
	var temp={
	  "has_parent": {
	    "type": Parentconfig.stationIndex,
	    "query": {
	      "filtered": {
	        "query": {
	          "match_all": {}
	        }
	        //,
	       // "filter": {}
	      }
	    }
	  }
	}
	if(!childProp)
	{
		temp.has_parent.query.filtered.filter={}
		if('or' in forParent)
			temp.has_parent.query.filtered.filter.or=forParent.or;
		else
			temp.has_parent.query.filtered.filter.or=forParent.and;
	}
	return temp
}
function ProcessSearchObject(baseSetup)
{
	if('tag_datatype' in baseSetup.station&&baseSetup.station.tag_datatype.length==1&&baseSetup.station.tag_datatype[0]=='sensor')
	{
		if('tag_watertype' in baseSetup.station)
			delete baseSetup.station.tag_watertype
	}
	else if(!('tag_datatype' in baseSetup.station))
	{
		if('tag_watertype' in baseSetup.station)
			delete baseSetup.station.tag_watertype
	}
	processedJson.amr={"station":{"or":{}},"sensor":{"or":{}}}
	processedJson.sensors={"station":{"or":{}},"sensor":{"or":{}}}
	processedJson.weather={"station":{"or":{}},"sensor":{"or":{}}}
	$.each(baseSetup,function(k,v){
		
		$.each(v,function(l,m){
			
				// processedJson[placeme][k].or[l]=[];
				$.each(m,function(o,p){
					placeme=findmetoplace(l,p,baseSetup);
					p=p.toLowerCase();
					p=p.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,' ');
					if(p.indexOf("-")>-1)
						p=$.trim(p).replace(/-/g, " ");
					else if(p.indexOf("_")>-1)
						p=$.trim(p).replace(/_/g, " ");

					if (p.match(/\s/g)){
					    p=$.trim(p).split(' ');
					}
						
					if(typeof(p)=='string')
						p=p;
					else
						p = $.grep(p,function(n){ return(n) })

					if(placeme!='both')
					{
						if(!(l in processedJson[placeme][k].or))
						{
							processedJson[placeme][k].or[l]=[];
						}
						processedJson[placeme][k].or[l].push(p);
					}
					else
					{

						
						if(('tag_datatype' in baseSetup.station)&&baseSetup.station.tag_datatype.length>1)
						{
							if(!(l in processedJson.amr[k].or))
							{
								processedJson.amr[k].or[l]=[];
							}
							if(!(l in processedJson.sensors[k].or))
							{
								processedJson.sensors[k].or[l]=[];
							}
							processedJson.amr[k].or[l].push(p);
							processedJson.sensors[k].or[l].push(p);
						}
						else if(('tag_datatype' in baseSetup.station)&&baseSetup.station.tag_datatype.length===1)
						{
							if(!(l in processedJson.amr[k].or))
							{
								processedJson.amr[k].or[l]=[];
							}
							if(!(l in processedJson.sensors[k].or))
							{
								processedJson.sensors[k].or[l]=[];
							}

							if('tag_datatype' in processedJson.amr.station.or||(('tag_datatype' in baseSetup.station)&&baseSetup.station.tag_datatype[0]==='amr'))
							{
								processedJson.amr[k].or[l].push(p);
							}
							else if('tag_datatype' in processedJson.sensors.station.or||(('tag_datatype' in baseSetup.station)&&baseSetup.station.tag_datatype[0]==='sensor'))
							{
								processedJson.sensors[k].or[l].push(p);
							}
						}
						else
						{
							if(!(l in processedJson.amr[k].or))
							{
								processedJson.amr[k].or[l]=[];
							}
							if(!(l in processedJson.sensors[k].or))
							{
								processedJson.sensors[k].or[l]=[];
							}
							processedJson.amr[k].or[l].push(p);
							processedJson.sensors[k].or[l].push(p);
						}
					}
				})
			
		})
	});

}
function generateBaseList(type,searchObject)
{
	sensorKey=['sensortype_actual','sensortype_display']
	if(type=='text')
	{
		$.each(searchObject,function(key,value){
			$.each(value,function(k,v){
				altKey=getAltKey(k);
				if(jQuery.inArray( altKey, sensorKey ) ===-1)
				{
					
					if(!(altKey in baseSetup.station))
					{
						baseSetup.station[altKey]=[]
					}
					baseSetup.station[altKey].push($.trim(v))
				}
				else
				{
					
					if(!(altKey in baseSetup.sensor))
					{
						baseSetup.sensor[altKey]=[]
					}
					baseSetup.sensor[altKey].push($.trim(v))
					if(!('tag_datatype' in baseSetup.station))
					{
						baseSetup.station['tag_datatype']=[]
					}
					baseSetup.station['tag_datatype'].push('amr')
					baseSetup.station['tag_datatype'].push('sensor')
				}
			})
				
		})
	}
	else if(type=='settings')
	{
		$.each(searchObject,function(key,value){
			if(key=='zoneSelection')
			{
				if(searchObject.watertype.watertype=='newwater')
				{
					akey='demandSelection'
				}
				else
				{
					akey=key
				}
			}
			else
			{
				akey=key;
			}
			altKey=getAltKey(akey);
			if(key in value)
			{
				if(!(altKey in baseSetup.station))
				{
					baseSetup.station[altKey]=[]
				}
			
				$.each(value[key],function(k,v){
						baseSetup.station[altKey].push(v)
				})
			}	
			
		})
	}
	else if(type=='dropdown')
	{

		$.each(searchObject,function(key,value){
			if(typeof value=== 'object')
			{
				if(key==='sensorparams')
				{
					if(value.length>0)
					{
						altKey=getAltKey(key);
						if(!(altKey in baseSetup.sensor))
						{
							baseSetup.sensor[altKey]=[]
						}
						$.each(value,function(ky,vl){
							baseSetup.sensor[altKey].push(vl)
						});
					}
				}
				else
				{
					if(value.length>0)
					{
						altKey=getAltKey(key);
						if(!(altKey in baseSetup.station))
						{
							baseSetup.station[altKey]=[]
						}
						$.each(value,function(ky,vl){
							baseSetup.station[altKey].push(vl)
							// console.log(vl)
						});
					}
				}
				
			}
			else
			{
				if(!('tag_datatype' in baseSetup.station))
				{
					baseSetup.station['tag_datatype']=[]
				}
				baseSetup.station['tag_datatype'].push(value)
			}
			
			
		})
		
	}
	
}
function findmetoplace(k,v,baseSetup){
	switch(k)
	{
		case 'customerType':
			var placeme='amr'
		break;
		case 'tag_watertype':
			if(v==='newwater')
			{
				var placeme='amr'
			}
			else if(v==='potable')
			{
				var placeme='both'
			}
			
		break;
		case 'zoneSelection':
			placeme=['supply_zone']
		break;
		case 'tag_datatype':
			if(v==='amr')
			{
				var placeme='amr'
			}
			else if(v==='sensor')
			{
				var placeme='sensors';
			}
			else
			{
				var placeme='weather';
			}
			
		break;
		case 'supply_zone':
			// if(baseSetup.station.tag_watertype[0]=='newwater')
			// 	var placeme='amr';
			// else
			// 	var placeme='sensors';
			var placeme='both';
		break;
		case 'commercialType':
			placeme='amr'
		break;
		case 'sensortype_actual':
			if(baseSetup.station.tag_datatype.length>1)
			{
				var placeme='both';
			}
			else
			{
				amrsensorFilter=['consumption','reading'];
				weathersensorFilter=['rain','humidity']
				if(jQuery.inArray( k, amrsensorFilter ) >-1)
				{
					var placeme='amr'
				}
				else if(jQuery.inArray( k, weathersensorFilter ) >-1)
				{
					var placeme='weather'
				}
				else
				{
					var placeme='sensors';
				}
			}
			
		break;
		case 'sensortype_display':
			if(baseSetup.station.tag_datatype.length>1)
			{
				var placeme='both';
			}
			else
			{
				if(baseSetup.station.tag_datatype[0]=='sensor')
					var placeme='sensors'
				else if(baseSetup.station.tag_datatype[0]=='amr')
					var placeme='amr'
				else
					var placeme='weather'
			}	
			
		break;
		case 'ResidentArea':
			var placeme='amr';
		break;
		case 'wemstype':
			var placeme='amr';
		break;
		default:
			amrtextFilter=['tag_sector','tag_subsector','neighbourhood','address','size','meter_type','postcode','bp_name','premise_type','acc_class','tru_ca','device','ssic','site','meter_scheme','billing_class','link_address']
			if(jQuery.inArray( k, amrtextFilter ) ===-1)
			{
				var placeme='both';
			}
			else
			{
				var placeme='amr';
			}
	}
	return placeme;
}
function getAltKey(key)
{
	switch(key)
	{
		case 'customerType':
			var altKey='tag_category'
		break;
		case 'watertype':
			var altKey='tag_watertype'
		break;
		case 'zoneSelection':
			var altKey='supply_zone'
		break;
		case 'demandSelection':
			var altKey='demand_cluster'
		break;
		case 'ZoneCluster':
			var altKey='zonal_cluster'
		break;
		case 'tag_datatype':
			var altKey='tag_datatype'
		break;
		case 'sensorparams':
			var altKey='sensortype_actual'
		break;
		case 'commercialType':
			var altKey='tag_sector'
		break;
		case 'ResidentArea':
			var altKey='meter_scheme';
		break;
		case 'wemstype':
			var altKey='meter_type';
		break;
		default:
			var altKey
			$.each(Parentconfig.allbasictags.search_tags,function(ey,al){
				if(al.label==key)
				{
					altKey=al.value;
					return false;
				}
			})
			if(!altKey)
				var altKey=key;
		break;
	}
	return altKey;
}
