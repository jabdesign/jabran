var sectors;
var wtype_main = 'potable';
var res_sectors = ['Toa Payoh','Punggol'];
var winId,sector;
var addRegister = [];
var mem  ={charts:[],chartsUrl:[],dpid:null,generic:{},reset:function(){
	this.charts = [];
	this.chartsUrl = [];
	this.dpid = null;
	this.generic={};
}};
var reporterSize = 0;
var activeAJAX = {count:0,complete:null};
var queue = [];
var secJson;
var actions={};
var searchtx;
var jsdata;
var rtype = null;
var keepExclude = false;
var active_reports = [];
var tempq = '{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"tag_category":"weather"}}]}]}]}}}},"size":100000}';
function switchwtype(x,el){
	if(x == 0){	//POTABLE
		if(wtype_main == 'potable')
			return;
		wtype_main = 'potable';
	}
	if(x == 1){
	if(wtype_main == 'newwater')
			return;
		wtype_main = 'newwater';	
	}
	$('#overlay').addClass('hide');
	var r_type = $(el).data('type');
	$(el).addClass('zoomOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		$(this).remove();
		active_reports.splice(active_reports.indexOf(r_type),1);
		//$('#overlay').toggleClass('hide');
		report(rtype);
	});
}
function report(x){
	rtype = x;
	if(active_reports.indexOf(x)>=0)
	{
		alert('Report is already running.');
		return;
	}
	active_reports.push(x);
	if(!keepExclude){
	excludeMarkers.splice(0,excludeMarkers.length)
}
	else
	keepExclude = false;
	console.log('check');
	if(st==undefined||et==undefined||st==''||et=='')
	{
		Notify('Date Error','Please select the date range','normal',true)
		return false;
	}
	if(wtype_main=='potable')
		sectors = ['Manufacturing','Retail','Hospital','Airport','Prison','Comm_Bldg','Hotel','Sch','Mixed_Devt'];
	else
		sectors = ['Manufacturing','Wafer_Fab','Comm_Bldg'];
	switch(x){
		case 'sector':
			mem.reset();
			winId = logWindow('report',x);
			actions = getActionParams();
//			console.log(actions)
			tagtext='<div class="tag_titles">Search Tags</div>';
			secJson=actions.Searchjson;
			// finding sector;

			if(actions.searchtxt)
			{
				var tags = actions.searchtxt.split(',');
				searchtx=actions.searchtxt;
				$.each(tags,function(key,value){
					tagtext+='<div class="tags_text">'+value+'</div>';
				});
			}
			else
			{
				searchtx=null;
				var tags= ['Singapore']
				tagtext+='<div class="tags_text">Singapore</div>';
			}
//			console.log(actions.searchtxt)
			for(var i=0;i<tags.length; i++)
			{
				if(sectors.indexOf(tags[i]) >= 0)
				{
					sector = tags[i];
				}
			}



			var html = '<div class="_cross"></div><img src="images/minus.png" width="15" class="minme" style="z-index:9" />';
	//		html += '<div class="report_button"><ul class="inline-list"><li>PW</li><li>NW</li></ul></div>';
			html += '<div class="report_title_section">';//tagtext
			html += '<div class="tsect"><div class="ReportTitle">Sector Report</div><span class="reportage"></span><span onClick="csvSector()"><img src="images/download.png" style="width : 20px; padding-top: 5px;" class="_img pdfdown"></span>'
			html += '<div class="ReportTags" style="padding-right : 16px;">'+tagtext+'</div></div>'

	//		html += '<div class="TabIcons"><ul class="ctabs" id="ctabs" ><li class="tab-title active" id="reportPanel"><a href="#">Report</a></li><li class="tab-title" id="healthPanel"><a href="#">Meterhealth</a></li></ul></div>'
			html += '</div>'
			html += '<div class="contentArea" id="reportPanel"><div class="columns medium-12 reportContainer"><div class="meterList left medium-3" id="meterList"> <h5 style="display:inline-block">Customers</h5><img src="images/refresh.png" style="margin-top: -3px;margin-left: 5px;" onClick="reaggregate()"><ul><ul></div>';
			html += '<div class="left" style="width:53%"><div class="plotcontainer" id="container'+winId+'" ></div><div class="stats"></div></div>';
			html += '<div class="right" style="width:21%"><div class="topNrender" id="topNrender"><select></select><ul style="overflow-y: hidden;margin-bottom: -20px;"><li id="title"><div class="ranknumber">Rank</div><div class="meterlabel" style="color:#fff;">Customer</div><div class="rankIcon" style="padding: 5px;">status</div><div class="ranknumber">changes</div></li></ul><ul style="padding-left:10px"></ul></div></div></div></div>';
			html += '<div class="contentArea" id="healthPanel"></div>';
			$('#'+winId).append(html);
			if(wtype_main == 'potable')
				$('.report_button li:eq(0)').addClass('sec');
			else
				$('.report_button li:eq(1)').addClass('sec');
			$('.report_button li').click(function(){
				var idx = $(this).index();
				switchwtype(idx,$(this).parents('.reporter'));
			});
			sectorPlots2();
			interval=1440;
			pred=false;
			m='CONSUMPTION';
			searchjson=
			$.get('controllers/proxyman.php',{endpoint:"data/search/rankeddata",params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+secJson+"&include=&exclude="+excludeMarkers.toString()+"&interval="+_interval+"&turnoffvee="+veecheck},function(d){
				jsdata=d;
				topNRender();
				loadevent();
			},'json');
		break;

		case 'sbys':
			mem.reset();
			winId = logWindow('report',x);
			actions = getActionParams();
			mem.generic.people={right:[],left:[]};
			//var tag = actions.searchtxt.split(',');
			//sector = tag[tag.length-1];
			var plotsize = (reporterSize-80)/2;
			var html = '<div class="_cross"></div><img src="images/minus.png" width="15" class="minme" style="z-index:9" />';
			html += '<div class="report_button"><ul class="inline-list"><li>PW</li><li>NW</li></ul></div>';
			html += '<div class="eqplots columns medium-5" id="container0'+winId+'"></div><div class="legend" style="position:absolute;font-size:12px;top:0px;left: 17px;"><ul class="inline-list"><li>Weekday<div class="box" style="background-color:#444444"></div></li><li>Weekend<div class="box" style="background-color:#0071C5"></div></li><li>Holiday<div class="box" style="background-color:#66CD00"></div></li></div><div class="eqplots columns medium-7"><div class="columns medium-6" id="container11'+winId+'" style="height:200px"></div><div class="columns medium-4" id="container12'+winId+'" style="height:200px;margin-left:-30px"></div><div id="container13'+winId+'" class="columns medium-2" style="margin-right:30px"></div></div>';
			//ui elements
			html += '<div class="medium-12 _selector row"><img src="images/people.png" width="20px" class="right ppl"><select style="width:40%" id="selectorR" class="right"><option value="0">Right Axis</option>';
			for(var i=0; i<sectors.length;i++)	
				html += '<option value="1">'+sectors[i]+'</option>';
			html += '<option value="1">Residential</option><option value="1">Non-Residential</option><option value="1" selected="selected">Temp</option><option value="1">Rain</option><option value="1">SGDEMAND</option></select>';
			html += '<img src="images/people.png" width="20px" class="ppl left"><select style="width:40%" id="selectorL" class="left"><option value="0">Left Axis</option>';
			for(var i=0; i<sectors.length;i++)	
				html += '<option value="1">'+sectors[i]+'</option>';
			html += '<option value="1">Residential</option><option value="1">Non-Residential</option><option value="1">Temp</option><option value="1">Rain</option><option value="1" selected="selected">SGDEMAND</option></select><img src="images/refresh.png" class="overviewRefresh"></div>';
			html += '<div class="medium-12" id="container2'+winId+'" style="height:300px;min-height:270px;"></div><div class="pplR hide"><ul class="csList"></ul></div><div class="pplL hide"><ul class="csList"></ul></div>';
			html += '';
			$('#'+winId).append(html);
			$('.eqplots').css('height',plotsize);
			if(wtype_main == 'potable')
				$('.report_button li:eq(0)').addClass('sec');
			else
				$('.report_button li:eq(1)').addClass('sec');
			// events
			$('.ppl').click(function(){
				if($(this).hasClass('left'))
					$('.pplL').removeClass('hide');
				else
					$('.pplR').removeClass('hide');
			});
			$('._selector select').on('change',function(){
				if(this.value == 0)
					return;
				var updateAxis = true;
				var op = $(this).find('option:selected').text();
				var type = this.className;
				var serid = op.replace(' ','_')+type;
				var axisFlag;
				if(type == 'right')
					axisFlag = 1;
				else
					axisFlag = 0;
				var serieslist = [];
				var opleft = $('#selectorL option:selected').text();
				var opright = $('#selectorR option:selected').text();
				var isBtn=false;
				//customerList($('#container1'+winId),[opleft,opright]);
				customerList($('.pplL'),[opleft]);
				customerList($('.pplR'),[opright]);
				if(sectors.indexOf(op)>=0 || res_sectors.indexOf(op)>=0)
					isBtn=true;
				var secJson='';
				var sensortype = 'CONSUMPTION';
				var extras = '';
				var endpoint = 'data/search/aggregate';
				var _type = 'column';
				if(op == 'Residential')
					secJson = res_query('all');
				else if(op == 'Non-Residential')
					secJson = res_query('non');
				else if(op == 'SGDEMAND'){
					extras = '&watertype='+wtype_main;
					endpoint = 'singaporedata/demand';
				}
				else if(op == 'Temp')
				{
					secJson = tempq;
					sensortype = 'TEMP';
					endpoint = 'data/search/average';
					_type = 'line';
				}
				else if(op == 'Rain')
				{
					secJson = tempq;
					sensortype = 'RAINFALL';
					endpoint = 'data/search/average';
					_type = 'line';
				}
				else if(op == 'Humidity')
				{
					secJson = tempq;
					sensortype = 'HUMIDITY';
					endpoint = 'data/search/average';
					_type = 'line';
				}
				else if(sectors.indexOf(op)>=0)
					secJson = sectorQuery(op);
				else if(res_sectors.indexOf(op)>=0)
					secJson = res_query(op);
				_.each(mem.spchart.series,function(el,id){
					if(el.yAxis.options.index == axisFlag){
						if(el.name != 'Navigator' && el.name != 'flags')
							el.remove();		
					}
				});
				//function adder(chart,query,m,ep,sTitle,axis,stack,visible,type,extras,id,statfunct,inc,legend,callback){
				adder(mem.spchart,secJson,sensortype,endpoint,op,axisFlag,(mem.spchart.series.length+1),true,_type,extras,null,null,null,null,
					function(){
						console.log('Chart updated');
					});
				if(isBtn){
					$('#container13'+winId+' .rem').remove();
					var sec = op;
					$.get('controllers/proxyman.php',{endpoint:'data/search/aggregate',params:'st='+st+'&et='+et+'&includepredicted='+pred+'&intervalmin='+interval+'&sensortype='+sensortype+'&search='+secJson+'&include=&excludeMarkers='+excludeMarkers.toString()+'&interval='+_interval+'&turnoffvee='+veecheck},
						function(r){
							if(r.errorCode>0)
								return;
							var total=0;
							_.each(r.response.meters[0].data,function(el){
								total = total + (el.value||0);
							});
							var res = (total / mem.generic.wdemand)*100;
							var wt='PW';
							if(wtype_main!='potable')
								wt = 'NW';
							$('#container13'+winId).append('<div class="statlabel rem">'+sec+'/'+wt+'<br/> '+res.toFixed(4)+'%');
						},'json');
				}
				if(typeof(mem.generic.allowScatter)=="undefined" || mem.generic.allowScatter == true)
				processScatter(axisFlag,updateAxis);
				else
					mem.generic.allowScatter == true;

	/*			if(op == 'residential' || op == 'non-residential'){	// COMBINED
					clearScatter();
					if(op == 'residential'){
						mem.spchart.yAxis[axisFlag].axisTitle.attr({
					        text: 'M^3/h (Residential)'
					    });
						_.each(mem.spchart.series,function(el,id){
							if(el.yAxis.options.index == axisFlag){
								if(el.name == 'Toh Payoh' || el.name == 'Punggol'){
									serieslist.push(el);
									el.show();
									el.options.showInLegend = true;
									mem.spchart.legend.renderItem(el);
									mem.spchart.legend.render();
								}
								else
									clearFromLegend(el,mem.spchart);
							}
							/*else
								clearFromLegend(el,mem.spchart);
						});
					}
					if(op == 'non-residential'){
						_.each(mem.spchart.series,function(el,id){
							if(el.yAxis.options.index == axisFlag){
								if(sectors.indexOf(el.name)>=0){
									serieslist.push(el);
									el.show();
									el.options.showInLegend = true;
									mem.spchart.legend.renderItem(el);
									mem.spchart.legend.render();
								}
								else
									clearFromLegend(el,mem.spchart);
							}
							/*else
								clearFromLegend(el,mem.spchart);*/
					/*	});
						mem.spchart.yAxis[axisFlag].axisTitle.attr({
					        text: 'M^3/h (Non-Residential)'
					    });
					}
					processScatter(axisFlag,updateAxis);
				}
				else 	// INDIVIDUAL
				{
					updateAxis = true;
					var thisseries = mem.spchart.get(serid);
					if(!thisseries)
					{
						console.log('ERR: SPCHART');
						return;
					}
					thisseries.show();
					for(var i=0; i<mem.spchart.series.length; i++){
						if(mem.spchart.series[i].yAxis.options.index == axisFlag){
							if(mem.spchart.series[i] != thisseries){
								mem.spchart.series[i].hide();
								clearFromLegend(mem.spchart.series[i],mem.spchart);
							}
						}
						/*else
							clearFromLegend(mem.spchart.series[i],mem.spchart);*/
			/*		}
					thisseries.options.showInLegend = true;
					mem.spchart.legend.renderItem(thisseries);
					mem.spchart.legend.render();
					processScatter(axisFlag,updateAxis);
				}	*/
			});
			$('.report_button li').click(function(){
				var idx = $(this).index();
				switchwtype(idx,$(this).parents('.reporter'));
			});
			$('.csList').on('click','li',function(){
				var data = $(this).data('id');
				var meters = data.split(',');					
					if($(this).hasClass('removeme')){
							_.each(meters,function(e){
								if(excludeMarkers.indexOf(e)>=0)
								excludeMarkers.splice(excludeMarkers.indexOf(e),1);
							})
					}
					else{
					_.each(meters,function(e){
						excludeMarkers.push(e);
					})		}
				$(this).toggleClass('removeme');
			});
			$('.overviewRefresh').click(function(){
				clearScatter();
				mem.generic.allowScatter = false;
				$('._selector select:eq(0)').trigger('change');
				$('._selector select:eq(1)').trigger('change');
				processScatter(0,false);
			});
			$('.csList').mouseleave(function(){
				$(this).parent().addClass('hide');
			});
			// build report
			staticReport();
		break;

		case 'demand':
		mem.reset();
		winId = logWindow('report',x);
		actions = getActionParams();
		var stags = (actions.searchtxt||'Singapore Wide').replace(/,/g,' > ');
		var sectorArr = [];
		var otherParams = ['Toa Payoh','Punggol'];
		sectorArr = sectorArr.concat(sectors,otherParams);
		console.log(sectorArr);
		$('#'+winId).load('controllers/ajaxloader.php',{method:'demand',winid:winId,searchtags:stags,size:reporterSize,data:JSON.stringify(sectorArr)},function(){
		$('.reporter').foundation();
		mem.generic.table = $( "#accordion" ).dataTable({ "paging":false,"info":false,"searching":false,"scrollX":true});
		//$('#resizeme1').resizable();
		//	=======Statistics========
	/*	var ajaxC=0;
		mem.generic.stats = [];
		// Non-Res Sectors
		for(var j=0;j<sectors.length;j++){
			(function(index){
				++ajaxC;
				$.get('controllers/proxyman.php',{endpoint:'data/search/aggregate',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=CONSUMPTION&search="+sectorQuery(sectors[j])+"&include=&exclude="+excludeMarkers.toString()+"&"+"&interval="+_interval+'&turnoffvee='+veecheck},
				function(e){
					--ajaxC;
					console.log(e);
					if(e.errorCode>0)
						return;
					var name = sectors[index];
					var total = 0;
					_.each(e.response.meters[0].data,function(d){
						total = total + (d.value||0);
					});
					mem.generic.stats.push([name,total]);
					if(ajaxC==0){
					addStats();
				}
				},'json');
			})(j);	
		}
		// Res Sectors
		for(var k=0;k<res_sectors.length;k++){
		(function(index){
			++ajaxC;
		$.get('controllers/proxyman.php',{endpoint:'data/search/aggregate',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=CONSUMPTION&search="+res_query(res_sectors[k].toLowerCase())+"&include=&exclude="+excludeMarkers.toString()+"&"+"&interval="+_interval+'&turnoffvee='+veecheck},
		function(e){
			console.log(e);
			--ajaxC;
			if(e.errorCode>0)
				return;
			var total = 0;
			var name = res_sectors[index];
			_.each(e.response.meters[0].data,function(d){
				total = total + (d.value||0);
			});
			mem.generic.stats.push([name,total]);
			if(ajaxC==0){
				addStats();
			}
		},'json');
		})(k);
	}
		// =======Statistics End========*/
		addStats();
		$('#accordion tbody').on('click', 'td.details-control', function (e) {
	        var tr = $(this).closest('tr');
	        var x = $(tr).find('.clickerTD').data('id');
	        var op = $(tr).find('.clickerTD').text();

	        if(sectors.indexOf(op)<0 && res_sectors.indexOf(op)<0 && op!='Non-Residential' && op!='Residential' && x!='All')
	        	return;
	        var row = mem.generic.table.api().row( tr );
	 		
	        if ( row.child.isShown() ) {
	            // This row is already open - close it
	           /* row.child.hide();
	            tr.removeClass('shown');*/
	            //$(tr).find('.clickerTD').trigger('click');
	            //return;
	            row.child.remove();

	        }
	            // Open this row
	            $(this).find('img').attr('src','images/ajax-loader.gif');
	            var innerlist = format(x,row,tr,this);
    	});
			if(wtype_main == 'potable')
				$('.report_button li:eq(0)').addClass('sec');
			else
				$('.report_button li:eq(1)').addClass('sec');
			$('.report_button li').click(function(){
				var idx = $(this).index();
				switchwtype(idx,$(this).parents('.reporter'));
			});
			$('.reporter ._timespan').append($('#date-range1').val());
			demandVar(0,'SGDEMAND');
			$('.reporter').on('click','.csList li',function(){
		//	$('.csList').click(function(){
					var sync = null;
					var sync_el = [];
					var name = $(this).find('._txt').text();
					$('.plotter .container td').each(function(index,el){
						if($(el).text() == name)
							sync = $(el).parents('tr');
					});

					var data = $(this).data('id');
					var meters = data.split(',');					
					if($(this).hasClass('removeme')){
							_.each(meters,function(e){
								if(excludeMarkers.indexOf(e)>=0)
								excludeMarkers.splice(excludeMarkers.indexOf(e),1);
							})
					}
					else{
					_.each(meters,function(e){
						excludeMarkers.push(e);
					})		}
					$(this).toggleClass('removeme');
					if(sync){
						$(sync).toggleClass('removeme');
						$(sync).hide();
						$(sync).get(0).offsetHeight;
						$(sync).show();
					}
			});
			$('#reportControls ul li').click(function(){
				$(this).toggleClass('selected');
			});
			$('#_charttype img').click(function(){
				var types = ['SGDEMAND','TEMP','PHFC_1','PHFC_2','QHPZ_1','MNSR_1','BKSR_1','RAINFALL'];
				if($(this).hasClass('selected'))
					return;
				$(this).parent().find('img').removeClass('selected');
				$(this).addClass('selected');
				var op = $(this).data('id');
				_.each(mem.generic.demandchart.series,function(s){
					if(types.indexOf(s.name)>=0)
					{
						s.update({type:op});
					}
				});
			})
			$('.reporter').on('click','.days_list li',function(){
				var ts = $(this).data('id');
				// sync routine
				
				$('.plotter .container td, .plotter .container th').each(function(index,el){
				try{
					var dateid = $(el).data('id')||null;
					if(dateid && !isNaN(parseInt(dateid))){
					var thists = new Date(dateid);
					var tsreset = thists.setHours(0);
					var _ts = tsreset;
					if(_ts == ts){
						$(this).toggleClass('removeme');
						$(this).hide();
						$(this).get(0).offsetHeight;
						$(this).show();
					}}}
				catch(e){
					console.log('Err - TS');
				}	
				});
				
				if(!mem.generic.hasOwnProperty('dayslist'))
					mem.generic.dayslist=[];
				if(mem.generic.dayslist.indexOf(ts)>=0)
					mem.generic.dayslist.splice(mem.generic.dayslist.indexOf(ts),1);
				else
				mem.generic.dayslist.push(ts);
				$(this).toggleClass('removeme');
			});
			$('.reporter').on('click','.cleardays',function(){
				$(this).parent('.listwrap').remove();
			});
			//$('.cuttoff span').empty().append('CuttOff: <strong>'+parseInt(d.y)+'</strong> on '+d.series.name);
			$('.cuttoff span').click(function(){
				var idx = $(this).index();
				console.log(idx);
				try{
				var txt = $(this).text();
				var val = prompt('Enter CutOff Value');
				if(!val)
					return;
				var explode = txt.split(' on ');
				$('.cuttoff span:eq('+idx+')').empty().append('<strong>'+val+'</strong>');
				if(mem.generic.plotline){
					mem.generic.demandchart.yAxis[0].removePlotLine('plotLine');
					mem.generic.plotline = !mem.generic.plotline;
				}
				mem.generic.demandchart.yAxis[0].addPlotLine({
					value:parseFloat(val),
					color:'red',
					width:1,
					id:'plotLine'
				});
				mem.generic.plotline=true;
				mem.generic.y[idx] = parseFloat(val);
			}
			catch(e){
				alert('Sensor type not set');
			}
			});

		});
		$('#'+winId).css('padding-top','0px !important');
		break;

		//ZONAL REPORT
		case 'zonal':
		mem.reset();
		winId = logWindow('report',x);
		var dmaZones = [];
		var dmaZoneIds = [];
		actions = getActionParams();
		var _dmalist = dma.requestmeta();
		for(var i in _dmalist){
			dmaZones.push(_dmalist[i].display_name);
			dmaZoneIds.push(i);
		}
		$('#'+winId).load('controllers/ajaxloader.php',{method:'zonal',winid:winId,size:reporterSize,data:JSON.stringify([]),dmaZones:dmaZones,dmaZoneIds:dmaZoneIds},function(){
			if(wtype_main == 'potable')
				$('.report_button li:eq(0)').addClass('sec');
			else
				$('.report_button li:eq(1)').addClass('sec');
			$('.report_button li').click(function(){
				var idx = $(this).index();
				switchwtype(idx,$(this).parents('.reporter'));
			});
			var opts = {disable_search:true,
				max_selected_options: 5
			}
			mem.generic.zonalpre = ['group1','group3'];	// PRE SELECTION
			$('.sel0').chosen(opts);
			$('.sel1').chosen(opts);
			$('#'+winId+' select').on('change',function(e,f){

				//console.log($(this).find('option:selected').parent('optgroup').attr('id'));

				var idx;
				if($(this).parent().hasClass('selector0'))
					idx = 0;
				else
					idx = 1;
				//var addtype = parseInt($('input[name=addtype]:radio:checked').val());
				if(f.hasOwnProperty('selected'))
				{
				/*	if(mem.generic.zonalpre[idx] != '' && mem.generic.zonalpre[idx] != f.selected)
					{
						alert('You can only choose within one group at a time');
						return;
					}*/
					var group;	
					var id;
					var dmaZones = [];
                			var dmaZoneIds = [];
                			var _dmalist = dma.requestmeta();
                			for(var i in _dmalist){
                        			dmaZones.push(_dmalist[i].display_name);
                        			dmaZoneIds.push(i);
                			}
					var _opts = $('.sel'+idx+' option:selected');
					_.each(_opts,function(s,r){
						if($(s).text() == f.selected) {
							group = $(_opts[r]).parent().attr('id');
							console.log(dmaZoneIds);
							if(group == 'group1') {
								id = dmaZoneIds[$(s).index()-1];
							} else if (group == 'group2') {
								 id = dmaZoneIds[$(s).index()];
							}
						}
					})
					if(mem.generic.zonalpre[idx] != '' && mem.generic.zonalpre[idx] != group)
						{
						alert('You can only choose within one group at a time');
						return;
					}
					switch(group){
						case 'group1': // DEMAND
						if(f.selected != 'SGDEMAND'){
							console.log(f);
							var zone = f.selected.split('_');
							//wwAdder(mem.generic.zonalchart,zone[0],f.selected,idx,mem.generic.zonalchart.series.length,'column');
							//var explode = sensor.split('_');
				//function adder(chart,query,m,ep,sTitle,axis,stack,visible,type,extras,id,statfunct,inc,legend,callback){
				adder2(mem.generic.zonalchart,'','dmademand','',f.selected,idx,mem.generic.zonalchart.series.length,true,'line','','','',id);
				//adder(mem.generic.zonalchart,'','dmademand','singaporedata/demand/'+wwMap(zone[0]).toLowerCase()+'/1',f.selected,idx,mem.generic.zonalchart.series.length,true,'line','');

							//fetchAnalytics(wwMap(zone[0]),f.selected,'genericanalysis/analyse','','');
							// supplyzone,name,ep,sensor,search
						}
						else{
							adder(mem.generic.zonalchart,'',f.selected,'singaporedata/demand',f.selected,idx,mem.generic.zonalchart.series.length,true,null,'watertype='+wtype_main);
							//fetchAnalytics(wwMap(zone[0]),f.selected,'genericanalysis/analyse','','');
							// supplyzone,name,ep,sensor,search
						}
						break;
						case 'group2': // NIGHT
							var explode = f.selected.split('_');
							var zone = wwMap(explode[1]).toLowerCase();
							var subzone = explode[2];
							//adder(mem.generic.zonalchart,'','ACTUAL_MNF','data/supplyzone/'+zone+'/'+subzone,f.selected,idx,mem.generic.zonalchart.series.length,true,null,null);
							adder2(mem.generic.zonalchart,'','dmaminnightflow','',f.selected,idx,mem.generic.zonalchart.series.length,true,'line','','','',id);
							//fetchAnalytics(zone,f.selected,'genericanalysis/analyse','MIN_NIGHT_FLOW','');
						break;
						case 'group3': // WEATHER
							adder(mem.generic.zonalchart,tempq,f.selected,'data/search/average',f.selected,idx,mem.generic.zonalchart.series.length,true,'column',null,null,null,null,true);
							fetchAnalytics('',f.selected,'genericanalysis/analyse',f.selected,tempq);
						break;
					}
				}
				if(f.hasOwnProperty('deselected')){
					for(var i=0; i<mem.generic.zonalchart.series.length;i++){
						if(mem.generic.zonalchart.series[i].yAxis.options.index == idx)
							if(mem.generic.zonalchart.series[i].name == f.deselected)
								mem.generic.zonalchart.series[i].remove();
					}
					$('.reporter table tbody #'+f.deselected).remove();
					if($('.sel'+idx+' option:selected').length == 0)
						mem.generic.zonalpre[idx] = '';
				}
			});
			var pid3 = plotter('container'+winId,'','','SGDEMAND','SGDEMAND','normal','singaporedata/demand',null,null,0,'watertype='+wtype_main);
			chartqueue('set',pid3,function(){
				var mychart=null;
				for(var x=0; x<mem.charts.length; x++){
					if(mem.charts[x].pid == pid3){
						mychart = mem.charts[x].chart;
						mem.generic.zonalchart = mychart;
						break;
					}
				}
				adder(mychart,tempq,'TEMP','data/search/average','TEMP',1,0,true,'column',null,null,null,null,true);
				fetchAnalytics('','TEMP','genericanalysis/analyse','TEMP',tempq);
			});
		// events
	/*	$('#'+winId+' select').on('change',function(){
			var idx;
			if($(this).parent().hasClass('selector0'))
				idx = 0;
			else
				idx = 1;
			var op = $(this).val();
			var addtype = parseInt($('input[name=addtype]:radio:checked').val());
			if(addtype)
			removeAxis(mem.generic.zonalchart,idx);
			if(idx==0){
				adder(mem.generic.zonalchart,'','SGDEMAND','singaporedata/demand','Demand',0,0,true,null,'watertype='+wtype_main);
			}
			else{
				adder(mem.generic.zonalchart,tempq,op,'data/search/average',op,1,0,true,'line',null,null,null,null,true);
			}
		});*/
		});
	break;
		case 'health':
		mem.reset();
		winId = logWindow('report',x);

		actions = getActionParams();
		$('#'+winId).load('controllers/ajaxloader.php',{method:'health',winid:winId,size:reporterSize,data:JSON.stringify([])},function(){
			console.log('Report Loaded');
			//var _now = new Date();
			//var _then = new Date(_now.getTime() - 86400000);
			var _then = new Date(st);
			//var _now = new Date(_then.getTime()+86400000);
			var _now = new Date(et);
			var now = _now.toString().split('GMT');
			var then = _then.toString().split('GMT');
			mem.generic.health ={"stype":sensor2val()||'CONSUMPTION'};
			mem.generic.health.view = 0;	// show both (1=customers;2=meters)
			mem.generic.health.state=function(x){
				mem.generic.health.view = x;
				mem.generic.health.table.draw();
			}
			$('._timespan').append(then[0] + ' to ' + now[0]);
			$('._timespan').css('color','rgb(0,173,255)').css('top','2px');
			//var m = 
			//var searchall = '{"query":{"filtered":{"query":{"match_all":{}}}},"size":100000}';
			var searchall = actions.Searchjson;
			$.get('controllers/proxyman.php',{endpoint:'meterquality/analyse',params:'st='+_then.getTime()+'&et='+_now.getTime()+'&search='+searchall+'&interval='+_interval+'&sensortype='+mem.generic.health.stype+'&turnoffvee='+veecheck,contenttype:'application/x-www-form-urlencoded',pyt:'true'},
				function(d){
					console.log(d);
					if(d.errorCode>0)
					{
						alert('Error '+d.errorMsg+' (HEALTH)')
						return;
					}
					var html = '<table id="healthtable" class="display" cellspacing="0" width="100%"><thead>';
					var summerize = calculateSummery(d.response.customers);
					html += summerize;
					html += '<tr><th style="min-width:360px">Meter ID</th><th>Meters with spikes</th><th>Customer</th><th>Meters Stop</th><th>Incomplete %</th><th>Backflow</th><th>Sector</th></tr></thead><tbody>';
					for(var i=0; i<d.response.customers.length;i++)
					{
						//html += '<tr class="group"><td colspan="5">'+d.response.customers[i].customer_name+'</td></tr>';
						for(var j=0; j<d.response.customers[i].meters.length; j++)
						{
							if(d.response.customers[i].meters[j]['meter stop']==null)
							{
								var letsay='<div class="na">NA</div>';
							}
							else if(d.response.customers[i].meters[j]['meter stop']==0)
							{
								var letsay='<div class="falsegreen">False</div>';
							}
							else if(d.response.customers[i].meters[j]['meter stop']==100)
							{
								var letsay='<div class="truered">True</div>';
							}
							else
							{
								var letsay=d.response.customers[i].meters[j]['meter stop'];
							}
							if(d.response.customers[i].meters[j].spikes==null)
							{
								spikesay='<div class="na">NA</div>';
							}
							else
							{
								spikesay=d.response.customers[i].meters[j].spikes;
							}
							if(d.response.customers[i].meters[j].backflows==null)
							{
								var backflowsay='<div class="na">NA</div>';
							}
							else
							{
								var backflowsay=d.response.customers[i].meters[j].backflows;
							}
//							var txt = d.response.customers[i].customer_name+':'+d.response.customers[i].spikes+':'+d.response.customers[i]['meter stop']+':'+d.response.customers[i].incomplete+':<span class="right datael"> Meters with no data '+d.response.customers[i]['no data']+'%</span>';
						// Jab Edit-
						//	var txt = d.response.customers[i].customer_name+':'+d.response.customers[i].spikes+':'+d.response.customers[i]['meter stop']+':'+d.response.customers[i].incomplete+':'+d.response.customers[i].backflows+':'+d.response.customers[i].sectors.toString();
						//	html += '<tr class="reg"><td>'+d.response.customers[i].meters[j].mtrid+'</td><td>'+spikesay+'<div class="DClass green">D</div></td><td>'+txt+'</td><td>'+letsay+'</td><td>'+d.response.customers[i].meters[j].incomplete+'<div class="DClass green">D</div></td><td>'+backflowsay+'</td><td>'+d.response.customers[i].sectors.toString()+'</td></tr>';
							var txt = d.response.customers[i].customer_name.replace(/,/g,' ').replace(/\r\n/g, " ").replace(/\n/g," ").replace(":","")+':'+d.response.customers[i].spikes+':'+d.response.customers[i]['meter stop']+':'+d.response.customers[i].incomplete+':'+d.response.customers[i].backflows+':'+d.response.customers[i].sectors.toString();
							html += '<tr class="reg"><td><span class="clickertd">'+d.response.customers[i].meters[j].mtrid+'</span><div class="right heatmapbtn">Detail</div></td><td>'+spikesay+'</td><td>'+txt+'</td><td>'+letsay+'</td><td>'+d.response.customers[i].meters[j].incomplete+'</td><td>'+backflowsay+'</td><td>'+d.response.customers[i].sectors.toString()+'</td></tr>';
						}
					}
					html += '<tbody></table>';
					$('.tablecontainer').append(html);
					mem.generic.health.table = $('#healthtable').DataTable({
        "columnDefs": [
            { "visible": false, "targets": [2] }
        ],
        "order": [[ 2, 'dsc' ]],
        "bPaginate": false,
        //"displayLength": 10,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var myclass='';
 		if(mem.generic.health.view > 1)
 			myclass = 'hide';
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                	var txt = group.split(':');
                    $(rows).eq( i ).before(
                //        '<tr class="group '+myclass+'"><td>'+txt[0]+'</td><td>'+txt[1]+'<div class="MClass blue">M</div></td>'+'<td>'+txt[2]+'</td><td>'+txt[3]+'<div class="MClass blue">M</div></td><td>'+txt[4]+'</td><td>'+txt[5]+'</td></tr>'
                    '<tr class="group '+myclass+'"><td><span class="clickertd">'+txt[0]+'</span><div class="right heatmapbtn">Detail</div></td><td>'+txt[1]+'</td>'+'<td>'+txt[2]+'</td><td>'+txt[3]+'</td><td>'+txt[4]+'</td><td>'+txt[5]+'</td></tr>'
                    );
                    last = group;
                }
            } );
       		if(mem.generic.health.view == 1)
       			$('#healthtable .reg').css('display','none');
       		else
       			$('#healthtable .reg').removeAttr('style');
        }
    } );
		$('.dataTables_wrapper .dataTables_filter input').attr('placeholder','Search');
		$('#healthtable').on( 'click', 'tbody tr td .clickertd', function (event) {
	    var meters = [];
        var parent_tr = $(this).parents('tr');
		var index = $(parent_tr).index();
        if($(parent_tr).hasClass('group')){
        var el = $('#healthtable tbody tr');
        for(var i=index+1;i<el.length;i++ ){
        	if($(el[i]).hasClass('group'))
        		break;
        	meters.push(mid2id($(el[i]).find('td:eq(0) .clickertd').text()));
        	}
    	}
    	else
    		meters.push(mid2id($(parent_tr).find('td:eq(0) .clickertd').text()));

        	$(this).parents('.reporter').find('.minme').trigger('click');
				plotme(meters,null,'CONSUMPTION');
    });

   	$('#healthtable').on( 'click', 'tbody tr td .heatmapbtn', function (event) {
		console.log('child');
		var parent_tr = $(this).parents('tr');
		var index = $(parent_tr).index();
		var meters = [];
		var name = $(this).parents('td').find('.clickertd').text();
		if($(parent_tr).hasClass('group')){
		var el = $('#healthtable tbody tr');
        for(var i=index+1;i<el.length;i++ ){
        	if($(el[i]).hasClass('group'))
        		break;
        	//heatMap(null,$(el[i]).find('td:eq(0) .clickertd').text(),[$(el[i]).find('td:eq(0) .clickertd').text()]);
        	meters.push(mid2id($(el[i]).find('td:eq(0) .clickertd').text()));
        }
    	}
        else
    		meters.push(mid2id($(parent_tr).find('td:eq(0) .clickertd').text()));
       
        heatMap(null,name,meters,true);
        event.stopImmediatePropagation();
        event.preventDefault();

	});
				},'json')
		});
		break;
		// Customer Analysis
		case 'customer':
		mem.reset();
		winId = logWindow('report',x);
		mem.generic.threshold = 1100;
		
		$('#'+winId).load('controllers/ajaxloader.php',{method:'customer',winid:winId,size:reporterSize,data:JSON.stringify([])},function(){
			computeReport2();
		});
		break;
	case 'dailyreport':
		mem.reset();
		winId = logWindow('report',x);
		actions = getActionParams();
		var today = new Date();
		var _et = et;
		//var _st = _et - (14*24*60*60*1000);	// last 2 weeks
		var height = $('#'+winId).height()-10;
	$('#'+winId).append('<div class="_cross"></div><iframe src="apps/dailyreport.php?st='+_et+'&et='+_et+'" width="100%" height="'+height+'" frameborder=0></iframe>');
	break;
	}
}
	$(document).on('click','._cross',function(){
 		var  uniqid    =    $(this).parents('.reporter').data('id');
 		var r_type = $(this).parents('.reporter').data('type');
 		active_reports.splice(active_reports.indexOf(r_type),1);
 		var cont = $(this).parents('.reporter');
		$(cont).addClass('zoomOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			$('.reporter *').off();
			if(!allmode)
			excludeMarkers.splice(0,excludeMarkers.length);
			$(this).remove();
		});

		 $('.minWindow').each(function(x){
                    if($(this).data('id')    ==   uniqid)                         
                         $(this).remove();
               });
		//$('#overlay').addClass('hide');
	});
	
function calculateNumberOfAffectedMeters(data)
{
	data = data.replace(" meters", "");
	return Number(data.split("/")[0]);
}
function calculateSummery(customers)
{
	var spikeMeters = 0;
	var stopMeters = 0;
	var incompleteMeters = 0;
	var backFlowMeters = 0;
	var totalMeters = 0;
	for(var i=0; i<customers.length;i++)
	{
		var spike = customers[i].spikes;
		var stop = customers[i]['meter stop'];
		var incomplete = customers[i].incomplete;
		var backflow = customers[i].backflows;
		var temp = spike.replace(" meters", "");
		
		var numberOfMeter = Number(temp.split("/")[1]);
		var numberOfSpikeMeter = calculateNumberOfAffectedMeters(spike);
		var numberOfStopMeter = calculateNumberOfAffectedMeters(stop);
		var numberOfIncompleteMeter = calculateNumberOfAffectedMeters(incomplete);
		var numberOfBackFlowMeter = calculateNumberOfAffectedMeters(backflow);
		
		spikeMeters += numberOfSpikeMeter;
		stopMeters += numberOfStopMeter;
		incompleteMeters += numberOfIncompleteMeter;
		backFlowMeters += numberOfBackFlowMeter;
		totalMeters += numberOfMeter;
	}
	
	return "<tr><td style='color:orange;'>Overral Status</td><td style='color:orange;'>"+spikeMeters+"/"+totalMeters+" meters</td><td></td><td style='color:orange;'>"+stopMeters+"/"+totalMeters+" meters</td><td style='color:orange;'>"+incompleteMeters+"/"+totalMeters+" meters</td><td style='color:orange;'>"+backFlowMeters+"/"+totalMeters+" meters</td><td></td></tr>";
}
function loadevent()
{
	$('#ctabs li a').click(function(e){
				e.preventDefault();
				
				var currenctActive=$('#ctabs .active').attr('id');
				var clicked=$(this).parent().attr('id');
				$('#ctabs #'+currenctActive).removeClass('active')
				$('#ctabs #'+clicked).addClass('active')
	});

	$('.reporter').on('click','#meterList li ._txt',function(e){
		var parent = $(this).parent();
		id=$(parent).data('id').toString();
		var sel=$('.selactive').data('id');
		var ids = id.split(',');
		var _name = $(this).text();
		idlist = [];
		_.each(ids,function(e){
			idlist.push(mem.generic.idarr[e]);
		});
		if($(parent).hasClass('selactive'))
		{
			var chi=0;
			var cr=[];
			$(parent).removeClass('selactive')

			$.each(mem.rankchart.series,function(ckey,cvalue){
				console.log(id)
				
				if(cvalue.name == _name)
				{
					mem.rankchart.series[chi].remove();
					var statid = '#stats'+_name.trim().replace(/ /g,'').replace(/[^\w\s]/gi, '');
					$(statid).remove();
				}
				chi++;
			})
	}
		else
	{
			console.log(id)
			endpoint="data/search/aggregate";
			adder(mem.rankchart,'','CONSUMPTION',endpoint,_name,0,1,true,'',null,null,
			function(d){ 
				console.table(d);
				_.each(d,function(x,c){
				try{
				html = '<ul id="stats'+_name.trim().replace(/ /g,'').replace(/[^\w\s]/gi, '')+'">';
				html += '<li class="box" style="background-color:'+x.color+';"></li><li>Quality <span class="val">' + x.quality.toFixed(2)+'%</span><li>Demand Change <span class="val">'+x.statistics.diff.toFixed(2)+'%</span></li><li>Max <span class="val">'+x.statistics.max.toFixed(2)+'</span></li><li>Min <span class="val">'+x.statistics.min.toFixed(2)+'</span></li><li>Mean <span class="val">'+x.statistics.mean.toFixed(2)+'</span></li><li>Demand Variation <span class="val">'+x.statistics.sd.toFixed(2)+'</span></li></ul>';
				$('.stats').append(html);
				}
				catch(e){
					console.log("ERR STATS");
				}
				})
			},idlist);
			$(parent).addClass('selactive')
		}
	});
}
function loadrank(rankData,ts,id)
{
	$.each(rankData,function(ky,val){
		if(val.ts==ts)
		{
			//console.log(val)

			$('#topNrender ul:eq(1)').empty();
			rankIterate=0;
			//$('#topNrender ul').append('<li id="title"><div class="ranknumber">Rank</div><div class="meterlabel" style="color:#fff;">Meter</div><div class="rankIcon" style="padding: 5px;">status</div><div class="ranknumber" >changes</div></li><div class="rankwrap">');
			$.each(val.data,function(key,value){
				
					if('customer_name' in value)
					{
						name=value.customer_name;
						id=value.mtrid;
					}
					else if('customername' in value)
					{
						name=value.customername;
						id=value.mtrid;
					}
					else if('display_customer_name' in value)
					{
						name=value.display_customer_name;
						id=value1_id;
					}
					else
					{
						name=value.mtrid;
						id=value.mtrid;
					}
					//if(rankIterate<5)
					{
						var rankjson=checkrank(value.rankingChanged);
						console.log(rankjson)
						$('#topNrender ul:eq(1)').append('<li id="'+id+'"><div class="ranknumber">'+value.ranking+'</div><div class="meterlabel">'+name+'</div><div class="rankIcon"><img src="images/'+rankjson[0]+'.png"></div><div class="ranknumber" class="'+rankjson[0]+'">'+rankjson[1]+'</div></li>');
						rankIterate++;
					}
			});
				//$('#topNRender ul').append('</div>');
		}
	})
}
function topNRender()
{
	
	console.log(jsdata.response.timestamps)
	length=jsdata.response.timestamps.length

	latest=jsdata.response.timestamps[length-1];
	latest.ts;
	loadrank(jsdata.response.timestamps,latest.ts,'#topNrender');
	$('#topNrender select').empty()
	for(ik=length-1;ik>=0;ik--)
	{

		var dateVal ="/Date("+jsdata.response.timestamps[ik].ts+")/";
		var date1 = new Date( parseFloat( dateVal.substr(6 )));
		date=date1.getDate();
		month=date1.getMonth();
		year=date1.getFullYear();
		var myDate = new Date(year,month,date).toString().split('00')
		$('#topNrender select').append('<option value="'+jsdata.response.timestamps[ik].ts+'">'+myDate[0]+'</option>')
	}
	var namearr=[],idarr=[],indexarr=[];
	$('#topNrender select').on('change', function() {
	  loadrank(jsdata.response.timestamps,this.value,'#topNrender');
	});
	$('#meterList ul').empty();
	if(searchtx==null)
	{
		$.each(markers,function(key,value){
			if(value.visible==true)
			{
				$.each(MarkerInfoData.data,function(key1,value1){
					
					if(value1._id==value.id)
					{
						if('bp_name' in value1)
						{
							name=value1.bp_name;
							id=value1._id;
						}
						else if('customer_name' in value1)
						{
							name=value1.customer_name;
							id=value1._id;
						}
						else if('customername' in value1)
						{
							name=value1.customername;
							id=value1._id;
						}
						else if('display_customer_name' in value1)
						{
							name=value1.display_customer_name;
							id=value1._id;
						}
						else
						{
							name=value1._id;
							id=value1._id;
						}
							namearr.push(name);
							idarr.push(id);
						//$('#meterList ul').append('<li id="'+id+'">'+name+'</li>')
					}
					//console.log(value._id)
				});
			}
		})
		
	}
	else
	{
		console.log(MarkerInfoData.data)
		$.each(MarkerInfoData.data,function(key1,value1){

			if('bp_name' in value1)
			{
				name=value1.bp_name;
				id=value1._id;
			}	
			else if('customer_name' in value1)
			{
				name=value1.customer_name;
				id=value1._id;
			}
			else if('customername' in value1)
			{
				name=value1.customername;
				id=value1._id;
			}
			else if('display_customer_name' in value1)
			{
				name=value1.display_customer_name;
				id=value1._id;
			}
			else
			{
				name=value1._id;
				id=value1._id;
			}
			namearr.push(name);
			idarr.push(id);

		});
	}
	//	Combine Customers
	var groupcustomers = _.uniq(namearr);
	_.each(groupcustomers,function(e,i){
		if(namearr.indexOf(e)>=0){
			indexarr.push([namearr.indexOf(e)]);
			namearr[namearr.indexOf(e)]='';
		}
		while(namearr.indexOf(e)>=0){
			indexarr[indexarr.length-1].push(namearr.indexOf(e));
			namearr[namearr.indexOf(e)]='';
		}
	});
	//	Meter Health
	var qualityarr=[];
	var overall = [];
	$.get('controllers/proxyman.php',{endpoint:"data/search/data",params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=CONSUMPTION&search="+secJson+"&include=&exclude="+'&turnoffvee='+veecheck},function(d){
		if(!d || d.errorCode>0){
			console.log('ERR Server Response - MeterHealth');
			return;
		}
		
		for(var cnt=0; cnt<d.response.meters.length; cnt++){
			var thismeter = idarr.indexOf(d.response.meters[cnt].mtrid);
			qualityarr[thismeter] = d.response.meters[cnt].quality;
		}
		_.each(groupcustomers,function(e,i){
			var mquality=0;
			_.each(indexarr[i],function(elm,ilm){
				if(qualityarr[elm] && !isNaN(qualityarr[elm]))
				mquality = mquality + parseFloat(qualityarr[elm]);
			});
			var totalq	=	mquality	/	indexarr[i].length;
			var _color;
			if(totalq >= 50)
				_color = 'orange';
			if(totalq >= 75)
				_color = 'green';
			if(totalq < 50)
				_color = 'red';
			overall.push(totalq);
			$('#meterList ul').append('<li data-id="'+indexarr[i].toString()+'"><div class="_txt">'+e+'</div><img src="images/close.png" onClick="removeme(this)" class="right"><div class="healthbox right" style="background-color:'+_color+'">'+parseInt(totalq)+'%</div></li>');
		});
		var overallscore = 0;
		var overallage = 0;
		_.each(overall,function(elx){
			if(elx)
			overallscore = overallscore + elx;
		});
		overallage = overallscore/overall.length;
		$('.reportage').append(overallage.toFixed(2)+'% Accuracy');
	},'json');

	mem.generic.idarr = idarr;
	
		

}
function sectorPlots2()
{
	var pid2 = plotter('container'+winId,secJson,'Sector Graph','aggregate','CONSUMPTION',null, null, '',function(d){ 
		console.table(d);
		_.each(d,function(x,c){
		try{
			html = '<ul id="__aggregate">';
			html += '<li class="box" style="background-color:'+x.color+';"></li><li>Quality <span class="val">' + x.quality.toFixed(2)+'%</span></li><li>Demand Change <span class="val">'+x.statistics.diff.toFixed(2)+'%</span></li><li>Max <span class="val">'+x.statistics.max.toFixed(2)+'</span></li><li>Min <span class="val">'+x.statistics.min.toFixed(2)+'</span></li><li>Mean <span class="val">'+x.statistics.mean.toFixed(2)+'</span></li><li>Demand Variation <span class="val">'+x.statistics.sd.toFixed(2)+'</span></li></ul>';
			$('.stats').append(html);
		}
		catch(e){
			console.log("ERR STATS");
		}
				})
			},null);
		chartqueue('set',pid2,function(){
			var mychart = null;
			var myquery = null;
			for(var x=0; x<mem.charts.length; x++){
				if(mem.charts[x].pid == pid2){
					mychart = mem.charts[x].chart;
					mem.rankchart=mychart;
					break;
				}
			}
			adder(mychart,tempq,'TEMP','data/search/average','Temperature',1,0,true,'line',null,'',
				function(d){ 
				console.table(d);
				_.each(d,function(x,c){
				try{
				html = '<ul id="__temp">';
				html += '<li class="box" style="background-color:'+x.color+';"></li><li>Temprature Change <span class="val">'+x.statistics.diff.toFixed(2)+'%</span></li><li>Max <span class="val">'+x.statistics.max.toFixed(2)+'</span></li><li>Min <span class="val">'+x.statistics.min.toFixed(2)+'</span></li><li>Mean <span class="val">'+x.statistics.mean.toFixed(2)+'</span></li><li>Temprature Variation <span class="val">'+x.statistics.sd.toFixed(2)+'</span></li></ul>';
				$('.stats').append(html);
				}
				catch(e){
					console.log("ERR STATS");
				}
				})
			});
			/*myquery = secJson;
			myname = sectors[t];
			adder(mychart,myquery,'CONSUMPTION','data/search/aggregate',myname,0,0);*/
			
			//adder(mychart,JSON.stringify(query[0]),'CONSUMPTION','data/search/aggregate','aggregate',1,1);
			//adder(mychart,JSON.stringify(query[0]),'CONSUMPTION','data/search/aggregate','Toh Payoh',0,1);
		});
}
function checkrank(rank)
{
	rkjson=[]
	if(rank<0)
	{
		rkjson.push('red');
		x=Math.abs(rank)
		rkjson.push(x);
	}
	else if(rank>0)
	{
		rkjson.push('green');
		rkjson.push(rank);
	}
	else
	{
		rkjson.push('yellow');
		rkjson.push(0);
	}
	return rkjson;
}
function sectorPlots(){
	var containerId = 'container'+winId;
	var pp = getActionParams();
	var searchjson = pp.Searchjson;
	var m = 'CONSUMPTION';
	//AGGR
	$.get('controllers/proxyman.php',{endpoint:"data/search/aggregate",params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include=&exclude="+excludeMarkers.toString()+'&turnoffvee='+veecheck},
		function(d){
			var stuff = d.response.meters[0].data || null;
				d.data = stuff;
				if(!d.data){
					$('#'+containerId).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>')
					return;
				}
				var	payload	=	[];
				for(var	i=0;	i<d.data.length;	i++)
					{
						var	temp	=	{};
						temp.y	=	d.data[i].value;
						temp.x	=	d.data[i].ts;
						temp.id = 'ID'+i;
						payload.push(temp);
						delete(temp);
					}
				var	chart = new Highcharts.Chart({
					chart: {
					renderTo: containerId,
					type:'column',
					zoomType: 'xy'
				},
				 navigator: {
			    enabled: true,
			    height: 15,
			    xAxis:{
				labels: {
					enabled: false
				}
			    }},
			    rangeSelector: {
		    	allButtonsEnabled: true,
				inputEnabled: true,
		    	selected: 1,
		    	enabled:true
		    	},
				plotOptions: {
            	series: {
                marker: {
                    enabled: false
                		}
		            }
		        },
							xAxis:	{	type:'datetime'},
					yAxis:	[{opposite:false,title:{text:units[m]}},{opposite:true,title:{text:''}}],
					title: {
						text:'Sector Report for '+sector
					},
					series:	[{data:payload,turboThreshold:10000,name:'Aggr'},{data:flags(),width:18,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags'}]
				});
			},'json');
	//TOP - N
	$.get('controllers/proxyman.php',{endpoint:"data/search/data",params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+searchjson+"&include=&exclude="+excludeMarkers.toString()+'&turnoffvee='+veecheck},
		function(d)
			{
				console.log(d);
				var thisobj = [];
				if(d.errorCode>0)
				{
					alert('Server Error');
					return;
				}
				if(d.response.meters.length==0){
					alert('No Data Available');
					return;
				}
				for(var i=0;i<d.response.meters.length;i++){
					var sum = 0;
					var val;
					for(var j=0; j<d.response.meters[i].data.length;j++){
						val = d.response.meters[i].data[j].value || null;
						if(val)
							sum = sum + val;
					}
					thisobj.push({id:d.response.meters[i].mtrid,val:sum});
				}
				thisobj.sort(function(a,b){
					return	a.val - b.val;
				});
				console.log(thisobj);
				for(var l=0; l<thisobj.length; l++)
					$('.toplist ul').append('<li><strong>'+thisobj[l].id+'</strong> : '+thisobj[l].val);
			},'json');
}

function staticReport(){
	//var query = [{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"address":"toa"}},{"term":{"address":"payoh"}}]},{"or":[{"term":{"tag_watertype":wtype_main}}]},{"or":[{"term":{"tag_category":"residential"}}]}]}]}}}},"size":100000},{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"address":"punggol"}}]},{"or":[{"term":{"tag_watertype":wtype_main}}]},{"or":[{"term":{"tag_category":"residential"}}]}]}]}}}},"size":100000}];
	var query = [{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"address":"toh"}},{"term":{"address":"payoh"}}]},{"or":[{"term":{"tag_watertype":wtype_main}}]},{"or":[{"term":{"tag_category":"residential"}}]},{"or":[{"term":{"meter_scheme":["sub"]}},{"term":{"meter_scheme":["normal"]}}]}]}]}}}},"size":100000},{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"address":"punggol"}}]},{"or":[{"term":{"tag_watertype":wtype_main}}]},{"or":[{"term":{"tag_category":"residential"}}]},{"or":[{"term":{"meter_scheme":["sub"]}},{"term":{"meter_scheme":["normal"]}}]}]}]}}}},"size":100000}];
	// set up scatter
	scatter('container0'+winId,'Scatter');
	//NON-RESIDENTIAL
/*try{
	var pid2 = plotter('container1'+winId,sectorQuery(sectors[0]),'Commercial & Residential (Ratio)',sectors[0],'CONSUMPTION','normal');
		chartqueue('set',pid2,function(){
			var mychart = null;
			var myquery = null;
			for(var x=0; x<mem.charts.length; x++){
				if(mem.charts[x].pid == pid2){
					mychart = mem.charts[x].chart;
					break;
				}
			}
			for(var t=1; t<sectors.length; t++){
				myquery = sectorQuery(sectors[t]);
				myname = sectors[t];
				var f = function(){mychart.yAxis[0].axisTitle.attr({	text:'Non-Residential'})};
				adder(mychart,myquery,'CONSUMPTION','data/search/aggregate',myname,0,0,null,null,null,null,null,null,null,f);
			}
			adder(mychart,JSON.stringify(query[1]),'CONSUMPTION','data/search/aggregate','Punggol',1,1,null,null,null,null,null,null,null,function(){
				mychart.yAxis[1].axisTitle.attr({	text:'Residential'});
			});
			adder(mychart,JSON.stringify(query[0]),'CONSUMPTION','data/search/aggregate','Toh Payoh',1,1,null,null,null,null,null,null,null,function(){
				mychart.yAxis[1].axisTitle.attr({	text:'Residential'});
			});
		});
	
}
catch(e){
	alert('Container 2 Error');
	console.log(e);
}*/
	customerList($('.pplR'),['SGDEMAND']);
	var pid3 = plotter('container2'+winId,'','Sector Analysis','SGDEMAND','SGDEMAND','normal','singaporedata/demand','column',null,null,'watertype='+wtype_main,'demandleft');
	chartqueue('set',pid3,function(){
		var mychart = null;
		var myquery = null;
			for(var x=0; x<mem.charts.length; x++){
				if(mem.charts[x].pid == pid3){
					mychart = mem.charts[x].chart;
					console.log(mychart);
					mem.spchart = mychart;
					while(activeAJAX.complete != null)
						$.noop()
					activeAJAX.complete = function(){
						/*mem.spchart.yAxis[0].axisTitle.attr({
							text: 'Temp'
						});
						mem.spchart.yAxis[1].axisTitle.attr({
							text: 'Demand'
						});
*/
						activeAJAX.complete = null;
					}
				//	adder(mychart,tempq,'RAINFALL','data/search/average','Rain',0,0,false,'line',null,'rainleft',null,null,false);
					adder(mychart,tempq,'TEMP','data/search/average','Temp',1,0,true,'line',null,'tempright',null,null,true);
				/*	adder(mychart,tempq,'RAINFALL','data/search/average','Rain',1,0,false,'line',null,'rainright',null,null,false);
					adder(mychart,'','SGDEMAND','singaporedata/demand','Demand',0,0,false,'line','watertype='+wtype_main,'demandleft',null,null,false);
					for(var t=0; t<sectors.length; t++){
					myquery = sectorQuery(sectors[t]);
					myname = sectors[t];
					adder(mychart,myquery,'CONSUMPTION','data/search/aggregate',myname,1,1,false,null,null,myname.toLowerCase()+'right',null,null,false);
					adder(mychart,myquery,'CONSUMPTION','data/search/aggregate',myname,0,1,false,null,null,myname.toLowerCase()+'left',null,null,false);
				}
					adder(mychart,JSON.stringify(query[1]),'CONSUMPTION','data/search/aggregate','Punggol',1,2,false,null,null,'punggolright',null,null,false);
					adder(mychart,JSON.stringify(query[0]),'CONSUMPTION','data/search/aggregate','Toh Payoh',1,2,false,null,null,'to_payohright',null,null,false);
					adder(mychart,JSON.stringify(query[1]),'CONSUMPTION','data/search/aggregate','Punggol',0,2,false,null,null,'punggolleft',null,null,false);
					adder(mychart,JSON.stringify(query[0]),'CONSUMPTION','data/search/aggregate','Toh Payoh',0,2,false,null,null,'toh_payohleft',null,null,false);*/
				//	adder(mychart,'','SGDEMAND','singaporedata/demand','Demand',1,0,true,'line','watertype='+wtype_main,'demandright');
					break;
				}
			}
	});
//	PIE CHARTS
mem.generic.pies=[];
var ajaxC=0;
	for(var j=0;j<sectors.length;j++){
		(function(index){
			++ajaxC;
			$.get('controllers/proxyman.php',{endpoint:'data/search/aggregate',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=CONSUMPTION&search="+sectorQuery(sectors[j])+"&include=&exclude="+excludeMarkers.toString()+"&"+"&interval="+_interval+'&turnoffvee='+veecheck},
			function(e){
				--ajaxC;
				console.log(e);
				if(e.errorCode>0)
					return;
				var name = sectors[index];
				var total = 0;
				_.each(e.response.meters[0].data,function(d){
					total = total + (d.value||0);
				});
				mem.generic.pies.push([name,total]);
				if(ajaxC==0){
				createPie('container11'+winId,mem.generic.pies);
			}
			},'json');
		})(j);	
	}
	for(var k=0;k<res_sectors.length;k++){
		(function(index){
			++ajaxC;
		$.get('controllers/proxyman.php',{endpoint:'data/search/aggregate',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=CONSUMPTION&search="+res_query(res_sectors[k].toLowerCase())+"&include=&exclude="+excludeMarkers.toString()+"&"+"&interval="+_interval+'&turnoffvee='+veecheck},
		function(e){
			console.log(e);
			--ajaxC;
			if(e.errorCode>0)
				return;
			var total = 0;
			var name = res_sectors[index];
			_.each(e.response.meters[0].data,function(d){
				total = total + (d.value||0);
			});
			mem.generic.pies.push([name,total]);
			if(ajaxC==0){
				createPie('container11'+winId,mem.generic.pies);
			}
		},'json');
	})(k);
	}
	var payload = [];
	$.get('controllers/proxyman.php',{endpoint:'data/search/aggregate',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=CONSUMPTION&search="+res_query('all')+"&include=&exclude="+excludeMarkers.toString()+"&"+"&interval="+_interval+'&turnoffvee='+veecheck},
		function(d){
			if(d.errorCode>0)
				return;
			// ALL RESIDENTIAL
		$.get('controllers/proxyman.php',{endpoint:'data/search/aggregate',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=CONSUMPTION&search="+res_query('non')+"&include=&exclude="+excludeMarkers.toString()+"&"+"&interval="+_interval+'&turnoffvee='+veecheck},
			function(f){
				if(f.errorCode>0)
					return;
				// ALL NON RES
				var total_res=0,total_non=0;
				_.each(d.response.meters[0].data,function(el){
					total_res = total_res + (el.value||0);
				});
				mem.generic.dom=total_res;
				_.each(f.response.meters[0].data,function(el){
					total_non = total_non + (el.value||0);
				});
				mem.generic.non=total_non;
				payload.push(['Domestic',total_res],['Non-Domestic',total_non]);
				createPie('container12'+winId,payload);
				$.get('controllers/proxyman.php',{endpoint:'singaporedata/demand',params:'st='+st+'&et='+et+'&includepredicted='+pred+'&intervalmin='+interval+'&sensortype=SGDEMAND&watertype='+wtype_main+'&turnoffvee='+veecheck},
					function(g){
						if(g.errorCode>0)
							return;
						var coef = 0.000264;
						var total_demand=0;
						_.each(g.response.data,function(el){
							total_demand = total_demand + (	(el.value||0)/coef);
						});
						mem.generic.wdemand=total_demand;
						var non = (total_non / total_demand)*100;
						var dom = (total_res / total_demand)*100;
						var wt='PW';
						if(wtype_main!='potable')
							wt = 'NW';
						$('#container13'+winId).append('<div class="statlabel">Non/'+wt+'<br/> '+non.toFixed(4)+'%');
						$('#container13'+winId).append('<div class="statlabel">Dom/'+wt+'<br/> '+dom.toFixed(4)+'%');
					},'json');
			},'json');
		},'json');
}

function plotter(cid,query,title,sTitle,m,stack,ep,type,statfunct,axis,extras,id){
	id = id || null;
	ep = ep || "data/search/aggregate";
	type = type || 'column';
	var zIndex=1;
	if(type=='line')
		zIndex = 2;
	axis = axis || 0;
	statfunct = statfunct || null;
	var chart = null;
	var pid = getRandomInt(0,999);
	$.get('controllers/proxyman.php',{endpoint:ep,params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+query+"&include=&exclude="+excludeMarkers.toString()+"&"+extras+"&interval="+_interval+"&turnoffvee="+veecheck},
		function(d){
			if(d.errorCode>0)
			{
				alert('Error '+d.errorCode+' :: '+d.errorMsg);
				console.log('PLTR '+params);
				return;
			}
			if(!d.response)
				d.response = {meters:[{data:d.data}]};
			if(!d.response.meters)
				d.response.meters = [{data:d.response.data}];
			var stuff = d.response.meters[0].data || null;
				d.data = stuff;
				if(!d.data){
					$('#'+containerId).append('<br/><div class="panel callout radius"><h3>No Data Available</h3></div>')
					return;
				}
				var	payload	=	[];
				for(var	i=0;	i<d.data.length;	i++)
					{
						var	temp	=	{};
						if(!d.data[i].hasOwnProperty('value'))
							temp.y	=	null;
						else
							temp.y	=	d.data[i].value;
						temp.x	=	d.data[i].ts;
						temp.id = 'ID'+i;
						payload.push(temp);
						delete(temp);
					}
					chart = new Highcharts.Chart({
					chart: {
					renderTo: cid,
				/*	type:type,	*/
					zoomType: 'xy'
				},
				 navigator: {
			    enabled: true,
			    height: 15,
			    xAxis:{
				labels: {
					enabled: false
				}
			    }},
			    rangeSelector: {
		    	allButtonsEnabled: false,
				inputEnabled: false,
		    	selected: 1,
		    	enabled:false
		    	},
				plotOptions: {
            	series: {
                marker: {
                    enabled: false,
                     states: {
                select: {
                    fillColor: 'red',
                    lineWidth: 0
	                }
	            }
                		},
                 cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
	                            if(typeof(mem.generic.event)=='function')
	                            	mem.generic.event(this);
	                        }
	                    }
	                }
		            },
		         column: {
                stacking: stack
            	}            
		        },
							xAxis:	{	type:'datetime'},
					yAxis:	[{opposite:false},{opposite:true}],
					title: {
						text:title
					},
					series:	[{data:payload,turboThreshold:10000,name:sTitle,stack:0,type:type,idtype:0,yAxis:axis,id:id,zIndex:zIndex},{data:flags(),width:16,showInLegend:false,type:'flags',shape : 'circlepin',name:'flags'}]
				});
					chart.yAxis[axis].update({
		                title:{               
        					text: units[m]	}
    					});
					mem.charts.push({pid:pid,chart:chart});
					mem.chartsUrl.push({st:"st","et":et,"intervalmin":interval,"includepredicted":pred,type:m,names:["Meter"],sensorType:[m],endpoint:[ep],type2:null,case:1});
					
					chartqueue('get',pid);

					$.each(chart.series,function(ckey,cvalue){
//						console.log(cvalue)
						if(cvalue.name==sTitle)
						{
							color=cvalue.color;
						}
					})
					//
					/*$('#'+statID).css({'background':color})
					
					$('#'+statID).empty();
				try{
					$.each(d.response.meters[0].statistics,function(key,value){
						if(key!='sd'&&key!='variance'&&key!='diff')
						{
							if(key=='mean')
							{
								key='average';
							}
							varl=parseFloat(value)
							var str = varl.toFixed(8);
							str = str.substring(0, str.length-7);
							$('#'+statID).append('<div class="sts '+key+'"><div class="tlabl">'+key+' </div><div class="tval">: '+str+'</div></div>')
						}
					})
				}
				catch(e){
					console.log(e);
				}*/

				if(statfunct	&&	typeof(statfunct)=='function'){
					
					var stats = [];
					_.each(d.response.meters,function(elx,idx){
						var color = chart.series[chart.series.length-1].color;
						stats.push({mtrid:elx.mtrid||null,statistics:elx.statistics||null,quality:elx.quality||null,color:color});
					});
					statfunct(stats);
				}					
			},'json');
	return pid;
}
function wwAdder(chart,zonename,name,axis,stack){
	axis = axis || 0;
	stack = stack || 0;
	zonename = zonename || null;
	if(!zonename){
		alert('ERR ILLEGAL ZONE');
		return;
	}
	var zone = wwMap(zonename).toLowerCase();
	url_a = "https://waterwise.visenti.com/waterwise2/"+zone+"/support/getDemandPredictionData_support.php?op=dailyactual&z=0&st="+st+"&et="+et;
	$.get('controllers/proxyman.php',{params:url_a,ww:'true'},
		function(d){
			var stuff = JSON.parse(d.data);
			console.log(stuff);
			chart.addSeries({
				name:name,
				data:stuff,
				yAxis:axis,
				stack:stack
			});
			chart.yAxis[axis].update({
		        title:{               
        		text: 'M^3/h'	}
    		});
		},'json');

}
function adder2(chart,query,m,ep,sTitle,axis,stack,visible,type,extras,id,statfunct,inc,legend,callback){
        if(visible == undefined)
                visible = true;
        if(visible === false)
                visible = false;
        type = type || 'column';
        if(legend==undefined)
                legend = true;
        inc = inc || '';
        var zIndex=1;
        if(type=='line')
                zIndex = 2;
        if(inc)
                inc = inc.toString();
        statfunct = statfunct || null;
        callback = callback || null;
        extras = extras || 'i=';
        activeAJAX.count++;
	var interval;
	if('dmaminnightflow'==m) {
		interval = '0.5'
	} else {
		interval = '1440';
	}
        //$.get('controllers/proxyman.php',{endpoint:ep,params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+query+"&include="+inc+"&exclude="+excludeMarkers.toString()+"&"+extras+"&interval="+_interval+"&turnoffvee="+veecheck},
	var postUrl = window.location.protocol+"//"+window.location.hostname;
	$.ajax({url:postUrl+'/simulationapi/zonal/analysis/report', type:'post',data:{st:st,et:et,intervalmin:interval,includepredicted:pred,sensortype:m,include:inc,turnoffvee:'false',includeflag:'false',timezone:'Asia/Singapore'},headers:{'Content-Type': 'application/json'},success:
                function(d,e,f){
                                if(d.errorCode>0){
                                        console.log('Error ADDR '+d.errorCode);
                                        console.log(f);
                                        //console.log('ADDR '+params);
                                        return;
                }
                                                var stuff = d.response.data || null;
						
                        			var tr = '<tr id="'+sTitle+'"><td>'+sTitle+'</td>';
                                        	tr += '<td>'+parseFloat(d.response.weekendStatistic.rateChange).toFixed(3)+'</td>';
                                                tr += '<td>'+parseFloat(d.response.weekendStatistic.average).toFixed(3)+'</td>';
                                                tr += '<td>'+parseFloat(d.response.weekdayStatistic.rateChange).toFixed(3)+'</td>';
                                                tr += '<td>'+parseFloat(d.response.weekdayStatistic.average).toFixed(3)+'</td>';

                        			$('.reporter table tbody').append(tr);

                                                d.data = stuff;
                                                var     payload =       [];
                                                for(var i=0;    i<d.data.length;        i++)
                                                        {
                                                                var     temp    =       {};
                                                                if(!d.data[i].hasOwnProperty('value'))
                                                                        temp.y = null;
                                                                else
                                                                        temp.y  =       d.data[i].value;
                                                                temp.x  =       d.data[i].ts;
                                                                payload.push(temp);
                                                                delete(temp);
                                                        }
                                                chart.addSeries({
                                                        name:sTitle,
                                                        data:payload,
                                                        yAxis:axis,
                                                        stack:stack,
                                                        visible:visible,
                                                        type:type,
                                                        showInLegend: legend,
                                                        id:id,
                                                        zIndex:zIndex
                                                });
                                                chart.yAxis[axis].update({
                                title:{
                                                text: units[m]  }
                                        });
                                        
                                        var color = chart.series[chart.series.length-1].color;
                                        /*$.each(chart.series,function(ckey,cvalue){
                                                if(cvalue.name!='flags' && cvalue.name!='Navigator')
                                                colors.push(cvalue.color);
                                        });*/
                                        //statistics callback
                                        if(statfunct    &&      typeof(statfunct)=='function'){
                                                var stats = [];
                                                _.each(d.response.meters,function(elx,idx){
                                                        stats.push({mtrid:elx.mtrid||null,statistics:elx.statistics||null,quality:elx.quality||null,color:color});
                                                });
                                                statfunct(stats);
                                        }
                                        if(typeof(callback) == 'function')
                                                callback();
                                        if (--activeAJAX.count == 0) { if(typeof(activeAJAX.complete) == 'function')
                                                activeAJAX.complete(); }
                }});
}
function adder(chart,query,m,ep,sTitle,axis,stack,visible,type,extras,id,statfunct,inc,legend,callback){
	if(visible == undefined)
		visible = true;
	if(visible === false)
		visible = false;
	type = type || 'column';
	if(legend==undefined)
		legend = true;
	inc = inc || '';
	var zIndex=1;
	if(type=='line')
		zIndex = 2;
	if(inc)
		inc = inc.toString();
	statfunct = statfunct || null;
	callback = callback || null;
	extras = extras || 'i=';
	activeAJAX.count++;
	$.get('controllers/proxyman.php',{endpoint:ep,params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+m+"&search="+query+"&include="+inc+"&exclude="+excludeMarkers.toString()+"&"+extras+"&interval="+_interval+"&turnoffvee="+veecheck},
		function(d,e,f){
				if(d.errorCode>0){
					console.log('Error ADDR '+d.errorCode);
					console.log(f);
					//console.log('ADDR '+params);
					return;
				}
						if(!d.response)
							d.response = {meters:[{data:d.data}]};
						if(!d.response.meters)
							d.response.meters = [{data:d.response.data}];
						for(var dd=0; dd<d.response.meters.length; dd++){
						var stuff = d.response.meters[dd].data || null;
						d.data = stuff;
						var	payload	=	[];
						for(var	i=0;	i<d.data.length;	i++)
							{
								var	temp	=	{};
								if(!d.data[i].hasOwnProperty('value'))
									temp.y = null;
								else
									temp.y	=	d.data[i].value;
								temp.x	=	d.data[i].ts;
								payload.push(temp);
								delete(temp);
							}
						chart.addSeries({
							name:sTitle,
							data:payload,
							yAxis:axis,
							stack:stack,
							visible:visible,
							type:type,
							showInLegend: legend,
							id:id,
							zIndex:zIndex
						});
						chart.yAxis[axis].update({
		                title:{               
        					text: units[m]	}
    					});
					}
					var color = chart.series[chart.series.length-1].color;
					/*$.each(chart.series,function(ckey,cvalue){
						if(cvalue.name!='flags' && cvalue.name!='Navigator')
						colors.push(cvalue.color);	
					});*/
					//statistics callback
					if(statfunct	&&	typeof(statfunct)=='function'){
						var stats = [];
						_.each(d.response.meters,function(elx,idx){
							stats.push({mtrid:elx.mtrid||null,statistics:elx.statistics||null,quality:elx.quality||null,color:color});
						});
						statfunct(stats);
					}
					if(typeof(callback) == 'function')
						callback();
					if (--activeAJAX.count == 0) { if(typeof(activeAJAX.complete) == 'function')
						activeAJAX.complete(); }
		},'json');
}

function scatter(cid,title){
	var ep1 = "data/search/average";
	var ep2 = "singaporedata/demand";
	var pid = getRandomInt(0,100);
	var payload = [];
	var act = getActionParams();
	wtype = wtype_main;
//	SGDATA
	$.get('controllers/proxyman.php',{endpoint:ep2,params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=SGDEMAND&search=&include=&exclude="+excludeMarkers.toString()+"&watertype="+wtype+"&interval="+_interval+'&turnoffvee='+veecheck},
		function(d){
			if(d.errorCode>0)
			{
				alert('ERR:Bad Data');
				return;
			}
			mem.generic.scatterPid = pid;
			console.log('Setting up scatter at '+pid);
			// Get Temp
			$.get('controllers/proxyman.php',{endpoint:ep1,params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype=TEMP&search="+tempq+"&include=&exclude="+excludeMarkers.toString()+"&interval="+_interval+'&turnoffvee='+veecheck},
			function(r){
				if(r.errorCode>0){
					alert('ERR : Bad Data');
					return;
				}
		try{
			var thisday=null;
			for(var i=0; i<r.response.meters[0].data.length;i++){
				if(r.response.meters[0].data[i].ts == d.response.data[i].ts && (r.response.meters[0].data[i].value||null)!= null){
					var symbol = 'circle';
					var color = '#444444';
					thisday = new Date(r.response.meters[0].data[i].ts);
					if(thisday.getDay()==6 || thisday.getDay()==0){
						symbol = 'square';
						color = '#0071C5';
					}
					if(hol.indexOf(r.response.meters[0].data[i].ts)>0){
						symbol = 'triangle';
						color = '#66CD00';
					}
					payload.push({x:r.response.meters[0].data[i].value||null,y:d.response.data[i].value||null,color:color,marker:{symbol:symbol},myData:thisday.toString()});
				}
			}
		}
			catch(e){
				console.log("Err Scatter-" +e);
			}
			// initialize chart
			var chart = new Highcharts.Chart({
					chart: {
					renderTo: cid,
					type:'scatter',
					zoomType: 'xy'
				},
				exporting: {
         		enabled: false
				},
				title: {	text:title	},
				yAxis:{ title:{text:'SGDEMAND'}},
				xAxis:{title:{text:'Temp'}},
				tooltip: {
		        formatter: function () {
		            return '<b>' + this.point.myData + '</b><br/>X: '+this.point.x+'<br/> Y: '+this.point.y;
		        }
		    },
				series:	[{data:payload,name:'SGDEMAND / Temp',visible:true}]
			});
			mem.generic.preScatter = ['SGDEMAND','Temp'];
			mem.charts.push({pid:pid,chart:chart});

			},'json');
		},'json');
}
function cloneObject(x,t){
	var payload = {xData:[],yData:[],tData:[]};
			for(var i=0; i<x.xData.length;i++){
				payload.yData.push(x.yData[i]);
				payload.xData.push(x.xData[i]);
				payload.tData.push(t[i]);
				}
	return payload;
}
function clearScatter(){
	var chart=null;
	for(var i=0; i<mem.charts.length;i++){
		if(mem.charts[i].pid == mem.generic.scatterPid)
		{
			chart = mem.charts[i].chart;
			break;
		}
	}
	if(!chart)
		alert('Chart not found');
	while(chart.series.length>0)
		chart.series[0].remove();
}
function processScatter(axisFlag,updateAxis){
	loader($('.eqplots')[0],true);
	var chart;
	for(var i=0; i<mem.charts.length;i++){
		if(mem.charts[i].pid == mem.generic.scatterPid)
		{
			chart = mem.charts[i].chart;
			break;
		}
//		alert('Chart not found');
	}
	
	for(var x=0; x<chart.series.length;x++)
		chart.series[x].remove();
	
	var payload = [];
	var inter=[];
	var query = ['',''];
	var sensortype = ['CONSUMPTION','CONSUMPTION'];
	var watertype = ['',''];
	var ep = ['data/search/aggregate','data/search/aggregate'];
	var left = $('#selectorL option:selected').text();
	var right = $('#selectorR option:selected').text();
	var title = '';
	var title_units = '';
	//	LEFT
	if(sectors.indexOf(left)>=0)
		query[0]=sectorQuery(left);
	else if(res_sectors.indexOf(left)>0)
		query[0]=res_query(left.toLowerCase());
	else if(left == 'Residential'){
		query[0]=res_query('all');
		ep[0] = 'data/search/average';
	}
	else if(left == 'Non-Residential'){
		query[0]=res_query('non');
		ep[0] = 'data/search/average';
	}
	else if(left == 'Temp'){
		query[0]=tempq;
		sensortype[0] = 'TEMP';
		ep[0] = 'data/search/average';
	}
	else if(left == 'Rain'){
		query[0]=tempq;
		sensortype[0] = 'RAINFALL';
		ep[0] = 'data/search/average';
	}
	else if(left == 'Humidity'){
		query[0] = tempq;
		sensortype[0] = 'HUMIDITY';
		ep[0] = 'data/search/average';
	}
	else if(left == 'SGDEMAND'){
		query[0]='';
		watertype[0] = wtype_main;
		ep[0]='singaporedata/demand';
	}
	// RIGHT
		if(sectors.indexOf(right)>=0)
		query[1]=sectorQuery(right);
	else if(res_sectors.indexOf(right)>0)
		query[1]=res_query(right.toLowerCase());
	else if(right == 'Residential'){
		query[1]=res_query('all');
		ep[1] = 'data/search/average';
	}
	else if(right == 'Non-Residential'){
		query[1]=res_query('non');
		ep[1] = 'data/search/average';
	}
	else if(right == 'Temp'){
		query[1]=tempq;
		sensortype[1] = 'TEMP';
		ep[1] = 'data/search/average';
	}
	else if(right == 'Rain'){
		query[1]=tempq;
		sensortype[1] = 'RAINFALL';
		ep[1] = 'data/search/average';
	}
	else if(right == 'Humidity'){
		query[1] = tempq;
		sensortype[1] = 'HUMIDITY';
		ep[1] = 'data/search/average';
	}
	else if(right == 'SGDEMAND'){
		query[1]='';
		watertype[1] = wtype_main;
		ep[1]='singaporedata/demand';
	}
	var title=null;
	if(updateAxis){
	if(axisFlag){
		title = right;
		chart.xAxis[0].update({
		title:{               
        text: title	}
    });
	}
	else{
		title = left;
		chart.yAxis[0].update({
		title:{               
        text: title	}
    	});
	}
	mem.spchart.yAxis[axisFlag].axisTitle.attr({
		text: units[sensortype[axisFlag]]+' ('+title+')'
	});
}
	if(query.length == 0 || query.length >	2)
		alert('Query Err');

	$.get('controllers/proxyman.php',{endpoint:ep[0],params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+sensortype[0]+"&search="+query[0]+"&include=&exclude="+excludeMarkers.toString()+"&watertype="+watertype[0]+"&interval="+_interval+'&turnoffvee='+veecheck},
		function(d){
			$.get('controllers/proxyman.php',{endpoint:ep[1],params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+sensortype[1]+"&search="+query[1]+"&include=&exclude="+excludeMarkers.toString()+"&watertype="+watertype[1]+"&interval="+_interval+'&turnoffvee='+veecheck},
				function(e){
								var temp = e;
								var temp2 = d;
								d = temp;
								e = temp2;
							loader(null,false);
							if(!d.response.meters)
							d.response.meters = [{data:d.response.data}];
							if(!e.response.meters)
							e.response.meters = [{data:e.response.data}];
					try{
						var thisday = null;
						for(var i=0; i<d.response.meters[0].data.length; i++){
							//if(i>d.response.meters[0].data.length || i>e.response.meters[0].data.length)
							//	break;
							if(d.response.meters[0].data[i].ts == e.response.meters[0].data[i].ts && (e.response.meters[0].data[i].value||null)!=null && (d.response.meters[0].data[i].value||null)!=null){
								var symbol = 'circle';
								var color = '#444444';
								thisday = new Date(e.response.meters[0].data[i].ts);
								if(thisday.getDay()==6 || thisday.getDay()==0){
									symbol = 'square';
									color = '#0071C5';
								}
								if(hol.indexOf(e.response.meters[0].data[i].ts)>0){
									symbol = 'triangle';
									color = '#66CD00';
								}
								payload.push({x:d.response.meters[0].data[i].value,y:e.response.meters[0].data[i].value,color:color,marker:{symbol:symbol},myData:thisday.toString()});
							}
						}
					}
					catch(e){
						console.log('Err-PScatter '+e);
					}
						chart.addSeries({name:left+' / '+right,data:payload});
						chart.redraw();
						if(!mem.generic.allowScatter)
							mem.generic.allowScatter = true;
				},'json');
		},'json');
	//	GET DATA

	
/*	mem.generic.preScatter[axisFlag] = name;
	mem.generic.scatterData[axisFlag] = cloneObject( chart.series[0],tData );*/

	/*if(ser.xData.length != mem.generic.scatterData.length)
		alert('Warning time-range mismatch');
	for(var j=0; j<ser.xData.length;j++){
		if(j>=mem.generic.scatterData.length)
			break;
		if(ser.xData[j] == mem.generic.scatterData[j].ts)
			payload.data.push([mem.generic.scatterData[j].value,ser.yData[j]]);
	}
	chart.addSeries(payload);
	chart.redraw();*/
}

function chartqueue(method,pid,callback,opt){
	callback = callback || null;
	switch(method){
		case 'set':
			queue.push({pid:pid,callback:callback,opt:opt});
		break;
		case 'get':
			for(var i=0; i<queue.length;i++){
				if(queue[i].pid == pid){
					queue[i].callback();
				}
			}
		break;
	}
}
function sectorQuery(s){
	s = s || null;
	if(!s)
		return;
	str = s.toLowerCase();

	var explode = str.replace(/_/,'","');
	return '{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"and":[{"or":[{"term":{"tag_watertype":"'+wtype_main+'"}}]},{"or":[{"term":{"tag_category":"nonresidential"}}]},{"or":[{"term":{"tag_sector":["'+explode.toLowerCase()+'"]}}]}]}]}}}},"size":100000}';
	// var explode = str.split('_');
	// var term = '';
	// _.each(explode,function(e,idx){
	// 	if(idx>0)
	// 		term += '\",\"';
	// 	term += e;
	// });
	//  return JSON.stringify({"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"or":[{"and":[{"term":{"tag_category":"nonresidential"}},{"term":{"tag_watertype":wtype_main}},{"or":[{"term":{"tag_sector":[explode.toString()]}}]}]},{"has_parent":{"type":"station_info","query":{"filtered":{"query":{"match_all":{}},"filter":{"and":[{"term":{"tag_category":"nonresidential"}},{"term":{"tag_watertype":wtype_main}},{"or":[{"term":{"tag_sector":[explode.toString()]}}]}]}}}}}]}]}}}},"size":100000});
}
function demandVar(method,sensor){
	// reporting engine for Demand Variation
	if(winId == null)
		return;
	if(method == 1) //Weather
	{
		if(!mem.generic.dpid){	// New Chart
		var pid = plotter('container'+winId,tempq,'',sensor,sensor,'normal','data/search/average','column',null,0);
		mem.generic.dpid = pid;
		mem.generic.click = true; // turn on click event
		mem.generic.plotline = false;
		mem.generic.event = function(d){
				if(mem.generic.y && mem.generic.y.length>=2)
				return;
				var this_len = (mem.generic.y||[]).length;
			$('.cuttoff span:eq('+this_len+')').empty().append('<strong title="'+d.series.name+'">'+parseInt(d.y)+'</strong>');
			/*	if(mem.generic.plotline){
					mem.generic.demandchart.yAxis[0].removePlotLine('plotLine');
					mem.generic.plotline = !mem.generic.plotline;
				}*/
				mem.generic.demandchart.yAxis[0].addPlotLine({
					value:d.y,
					color:'red',
					width:1,
					id:'plotLine'
				});
				mem.generic.plotline=true;
				if(mem.generic.y)
					mem.generic.y.push(d.y);
				else
					mem.generic.y = [d.y];
				mem.generic.sensortype = d.series.name;
				var _stype = d.series.name;
				if(mem.generic.sensortype)
					mem.generic.sensortype.push(_stype);
				else
					mem.generic.sensortype = [_stype];
		}
		chartqueue('set',pid,function(){
			for(var x=0; x<mem.charts.length; x++){
			if(mem.charts[x].pid == mem.generic.dpid){
				mem.generic.demandchart = mem.charts[x].chart;
				break;
			}
		}
		});
	}
		else{		
			clearAll();
			adder(mem.generic.demandchart,tempq,sensor,'data/search/average',sensor,0,0,true,'line',null);
		}
	}
	if(method == 0)
	{
		var pid;
		if(!mem.generic.dpid){
		if(sensor == 'SGDEMAND')
		pid = plotter('container'+winId,'','',sensor,sensor,'normal','singaporedata/demand','line',null,0,'watertype='+wtype_main);
		mem.generic.dpid = pid;
		mem.generic.click = true; // turn on click event
		mem.generic.plotline = false;
		mem.generic.event = function(d){
			if(mem.generic.y && mem.generic.y.length>=2)
				return;
			var this_len = (mem.generic.y||[]).length;
			$('.cuttoff span:eq('+this_len+')').empty().append('<strong title="'+d.series.name+'">'+parseInt(d.y)+'</strong> ');
			/*	if(mem.generic.plotline){
					mem.generic.demandchart.yAxis[0].removePlotLine('plotLine');
					mem.generic.plotline = !mem.generic.plotline;
				}*/
				mem.generic.demandchart.yAxis[0].addPlotLine({
					value:d.y,
					color:'red',
					width:1,
					id:'plotLine'
				});
				mem.generic.plotline = true;
				if(mem.generic.y)
					mem.generic.y.push(d.y);
				else
					mem.generic.y = [d.y];
				var stp = d.series.name;
				var _stype;
				if(stp.indexOf('_')>0)
				{
					var exp = stp.split('_');
					_stype = wwMap(exp[0]).toLowerCase();
					mem.generic.subzone = exp[1];
				}
				else
				_stype = d.series.name;
			if(mem.generic.sensortype)
				mem.generic.sensortype.push(_stype);
			else
				mem.generic.sensortype = [_stype];			
		}
		chartqueue('set',pid,function(){
			for(var x=0; x<mem.charts.length; x++){
			if(mem.charts[x].pid == mem.generic.dpid){
				mem.generic.demandchart = mem.charts[x].chart;
				break;
			}
		}
		});	
		}
		else
		{
			clearAll();
			//adder(mem.generic.demandchart,'',sensor,'singaporedata/demand','SGDEMAND',0,0,true,'line','watertype='+wtype_main);
			if(sensor == 'SGDEMAND')
				adder(mem.generic.demandchart,'',sensor,'singaporedata/demand','SGDEMAND',0,0,true,'line','watertype='+wtype_main);
			else{
				var explode = sensor.split('_');
				//function adder(chart,query,m,ep,sTitle,axis,stack,visible,type,extras,id,statfunct,inc,legend,callback){
				adder(mem.generic.demandchart,'','ACTUAL','singaporedata/demand/'+wwMap(explode[0]).toLowerCase()+'/'+explode[1],sensor,0,0,true,'line','');
			}
		}
	}
	if(method == 2)	//WW ZONES
	{
				clearAll();
				var explode = sensor.split('_');
				//function adder(chart,query,m,ep,sTitle,axis,stack,visible,type,extras,id,statfunct,inc,legend,callback){
				adder(mem.generic.demandchart,'','ACTUAL','singaporedata/demand/'+wwMap(explode[0]).toLowerCase()+'/'+explode[1],sensor,0,0,true,'line','');
	}
}
function res_query(t){
	t = t.toLowerCase();
	  var query = [{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"or":[{"and":[{"term":{"tag_category":"residential"}},{"term":{"tag_watertype":wtype_main}},{"or":[{"term":{"meter_scheme":"sub"}},{"term":{"meter_scheme":"normal"}}]},{"or":[{"term":{"address":"toa"}},{"term":{"address":"payoh"}}]}]},{"has_parent":{"type":"station_info","query":{"filtered":{"query":{"match_all":{}},"filter":{"and":[{"term":{"tag_category":"residential"}},{"term":{"tag_watertype":wtype_main}},{"or":[{"term":{"meter_scheme":"sub"}},{"term":{"meter_scheme":"normal"}}]},{"or":[{"term":{"address":"toa"}},{"term":{"address":"payoh"}}]}]}}}}}]}]}}}},"size":100000},{"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"or":[{"and":[{"term":{"tag_category":"residential"}},{"term":{"tag_watertype":wtype_main}},{"or":[{"term":{"meter_scheme":"sub"}},{"term":{"meter_scheme":"normal"}}]},{"or":[{"term":{"address":"punggol"}}]}]},{"has_parent":{"type":"station_info","query":{"filtered":{"query":{"match_all":{}},"filter":{"and":[{"term":{"tag_category":"residential"}},{"term":{"tag_watertype":wtype_main}},{"or":[{"term":{"meter_scheme":"sub"}},{"term":{"meter_scheme":"normal"}}]},{"or":[{"term":{"address":"punggol"}}]}]}}}}}]}]}}}},"size":100000}];
	  var queryall = {"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"or":[{"and":[{"term":{"tag_category":"residential"}},{"term":{"tag_watertype":wtype_main}},{"or":[{"term":{"meter_scheme":"sub"}},{"term":{"meter_scheme":"normal"}}]}]},{"has_parent":{"type":"station_info","query":{"filtered":{"query":{"match_all":{}},"filter":{"and":[{"term":{"tag_category":"residential"}},{"term":{"tag_watertype":wtype_main}},{"or":[{"term":{"meter_scheme":"sub"}},{"term":{"meter_scheme":"normal"}}]}]}}}}}]}]}}}},"size":100000};
	if(t == 'all')	
		return JSON.stringify(queryall);
	if(t == 'non')
		return JSON.stringify({"query":{"filtered":{"query":{"match_all":{}},"filter":{"bool":{"must":[{"or":[{"and":[{"term":{"tag_category":"nonresidential"}}]},{"has_parent":{"type":"station_info","query":{"filtered":{"query":{"match_all":{}},"filter":{"and":[{"term":{"tag_category":"nonresidential"}}]}}}}}]}]}}}},"size":100000});
	if(t == 'toa payoh')
		return JSON.stringify(query[0]);
	if(t == 'punggol')
		return JSON.stringify(query[1]);
}

function demandFetch(r,y,x){
	var t = $(y).parent();
	mem.generic.public = null;
	if(!mem.generic.hasOwnProperty('customerList'))
		mem.generic.customerList = [];
	if(!mem.generic.y || !mem.generic.sensortype)
	{
		alert('No CutOff selection found');
		return;
	}
	//	Toggle Series
	if(!mem.generic.noser){
		while(mem.generic.demandchart.series.length>3){
			mem.generic.demandchart.series.forEach(function(ser){
				if(ser.options.id == 'fetched')
					ser.remove();
			});
		}
	}
	var days = [];
	_.each($('#reportControls ul li.selected'),function(el){
		if($(el).text() == 'Public')
			mem.generic.public = true;
		else
		days.push($(el).text());
	});
	var secJson;
	var refJson = '';
	var refwtype = '';
	var stype = 'CONSUMPTION';
	var secwatertype = '';
	var supplyzone = '';
	var subzoneid = '';
	var rsupplyzone = '';
	var rsubzoneid = '';
	switch(mem.generic.sensortype[0]){
		case 'TEMP':
			refJson = tempq;
		break;
		case 'RAIN':
			refJson = tempq;
		break;
		case 'HUMIDITY':
			refJson = tempq;
		break;
		case 'SGDEMAND':
			refwtype = wtype_main;
		break;
	}
	if(x == 'All')
	secJson = actions.Searchjson;
	else if(x == 'Residential')
	secJson = res_query('all');
	else if(x == 'Non-Residential')
	secJson = res_query('non');
	else if(x == 'Temperature' || x == 'Rainfall' || x == 'Humidity'){
	secJson = tempq;
	switch(x){
		case 'Temperature':
		stype = 'TEMP';
		break;
		case 'Rainfall':
		stype = 'RAINFALL';
		break;
		case 'Humidity':
		stype = 'HUMIDITY';
		break;
	}
}
	else if(x == 'SGDEMAND'){
	secJson = '';
	stype = 'SGDEMAND';
	secwatertype = wtype_main;
}
	else if(x == 'FCPH_DEMAND' || x=='QHPZ_DEMAND' || x=='MNSR_DEMAND' || x=='BKSR_DEMAND')
	{
		secJson = '';
		stype = 'ACTUAL';
		explode = x.split('_');
		supplyzone = explode[0].toLowerCase();
		subzoneid = 1;
	}
	else if(sectors.indexOf(x)>=0)
	secJson = sectorQuery(x);
	else if(res_sectors.indexOf(x)>=0)
	secJson = res_query(x.toLowerCase());
	else{
		alert('ERR SEC QUERY');
		return;
	}
	$(t).addClass('selected');

	//var refsearch = '';
	//if(mem.generic.sensortype == 'TEMP' || mem.generic.sensortype == 'RAINFALL' || )
	// find upper and lower cuttoffs;

	var low_cut,up_cut,low_sen,up_sen;
	if(mem.generic.y.length>1){
		if(mem.generic.y[0]<mem.generic.y[1]){
			low_cut = mem.generic.y[0];
			low_sen = mem.generic.sensortype[0];
			up_cut = mem.generic.y[1];
			up_sen = mem.generic.sensortype[1];
		}
		else
		{
			low_cut = mem.generic.y[1];
			low_sen = mem.generic.sensortype[1];
			up_cut = mem.generic.y[0];
			up_sen = mem.generic.sensortype[0];	
		}
	}
	else
	{
		up_cut = mem.generic.y[0];
		up_sen = mem.generic.sensortype[0];
	}
	var rtype = mem.generic.sensortype[0]||mem.generic.sensortype[1];
	if(isZone(mem.generic.sensortype[0]))
		rtype = 'ACTUAL';
	var p = "secst="+st+"&refst="+st+"&secet="+et+"&refet="+et+"&secintervalmin="+interval+"&refintervalmin="+interval+"&secincludepredicted="+pred+"&refincludepredicted="+pred+"&secsensortype="+stype+"&refsensortype="+rtype+"&secsearch="+secJson+"&refsearch="+refJson+"&secinclude=&secexclude="+excludeMarkers.toString()+"&refinclude=&refexclude=&refwatertype="+refwtype+"&secwatertype="+secwatertype+"&cutoff={'y':"+up_cut+",'ySensorType':'"+up_sen+"'}"+"&includedays="+days.toString()+"&interval="+_interval;
	if(mem.generic.y.length>1)
		p+= "&lowercutoff={'y':"+low_cut+",'ySensorType':'"+low_sen+"'}";
	if(supplyzone)
		p+="&secsupplyzone="+supplyzone;
	if(subzoneid)
		p+="&secsubzoneid="+subzoneid;
	if(isZone(mem.generic.sensortype[0])){
			p+="&refsubzoneid="+mem.generic.subzone;
			p+="&refsupplyzone="+mem.generic.sensortype[0];
	}
	if(mem.generic.public)
		p+='&publicholidayonly=true';
	if(!p)
		p+='&turnoffvee=true';
	if(mem.generic.hasOwnProperty('dayslist')){
		var exdates = _.uniq(_.compact(mem.generic.dayslist));
		p += '&excludedates='+exdates.toString();
	}
	
	// $.ajax({
 //    url: "controllers/proxyman.php",
 //    type: "GET",
 //    dataType: "json",
 //    timeout: 120000,
 //    data: {endpoint:"analysis/demandvariation",params:p},
 //    success: function(d) { 

 //    		console.log(d);
	// 		if(d.errorCode>0){
	// 			alert('ERR DVA');
	// 			return;
	// 		}
	// 		//	clear previous data
	// 		_.each(mem.generic.customerList,function(cs,cd){
	// 			if(cs.name == x)
	// 				delete mem.generic.customerList[cd];
	// 		});
	// 		// 	save new customers
	// 		if(d.response.customerDemandList.length==0)
	// 			console.log('No Customers');
	// 		var customer_obj = {name:x,customers:[]};
	// 		_.each(d.response.customerDemandList,function(cs){
	// 			customer_obj.customers.push({name:cs.customerName,stats:cs.statisticsDTO,id:cs.meterIdList});
	// 		});
	// 		mem.generic.customerList.push(customer_obj);
	// 		var payload=[];var change = [];
	// 		for(var i=0; i<d.response.buckets.length; i++){
	// 				var type = 0;
	// 				var clr = null;
	// 				var obj = d.response.buckets[i].content;
	// 				var txt;
	// 				if(d.response.buckets[i].bucketType == 'more'){
	// 					txt = '+ ';
	// 					type=0;
	// 					clr = '#8bbc21';
	// 				}
	// 				else{
	// 					txt = '- ';
	// 					type=1;
	// 					clr = '#c42525';
	// 				}
	// 				txt += x;
	// 			if(!$.isEmptyObject(d.response.buckets[i].content))
	// 			{
	// 				var ser = {name:txt,data:[],type:'column',yAxis:1,id:'fetched',color:clr};
	// 				for(var key in obj)
	// 				{
	// 					if(obj[key]!=null && obj[key].length>0)
	// 					ser.data.push([obj[key][0].ts,obj[key][0].value]);
	// 				}
	// 				if(!mem.generic.noser){
	// 					mem.generic.demandchart.addSeries(ser);
	// 					mem.generic.demandchart.yAxis[1].axisTitle.attr({
	// 					        text: units[stype]
	// 					});
	// 				}
	// 				else{
	// 					if(txt == '+ All' || txt == '- All')
	// 					{
	// 						mem.generic.demandchart.addSeries(ser);
	// 						mem.generic.demandchart.yAxis[1].axisTitle.attr({
	// 					        text: units[stype]
	// 						});
	// 					}
	// 				}
	// 			}
	// 				change[type]=d.response.buckets[i].statistics.mean||0;
				
	// 		}
	// 		if(mem.generic.noser)
	// 			--mem.generic.serlist;
	// 		if(mem.generic.serlist == 0){
	// 			mem.generic.noser = false;
	// 		}
	// 		var totalChange = change[0]-change[1];
	// 		mem.generic.table.fnUpdate(parseInt(d.response.statisticsDTO.changePercent),t,2);
	// 		mem.generic.table.fnUpdate(parseInt(totalChange),t,3);
	// 		// Alerts
	// 		var threshold = $('#reportControls input:eq(0)').val() || null;
	// 		var threshold2 = $('#reportControls input:eq(1)').val() || null;
	// 		var ch = parseInt(d.response.statisticsDTO.changePercent)
	// 		var rem = [true,true];
	// 		if(threshold && !isNaN(ch))
	// 		{
	// 			var th = parseInt(threshold);
	// 			if(ch > th){
	// 				$(t).addClass('blink');
	// 				rem[0] = false;
	// 			}

	// 		}
	// 		if(threshold2 && !isNaN(totalChange))
	// 		{
	// 			var th2 = parseInt(threshold2);
	// 			if(totalChange > th2){
	// 				$(t).addClass('blink');
	// 				rem[1] = false;
	// 			}
	// 		}
	// 		// new changes
	// 		var daily_mean = (change[0]+change[1])/2;
	// 		mem.generic.table.fnUpdate(parseInt(daily_mean),t,5);
	// 		mem.generic.table.fnUpdate(parseInt(d.response.statisticsDTO.contributionPercent),t,6);
	// 		if(rem[0] && rem[1])
	// 			$(t).removeClass('blink');

	// 		$(t).find('img').attr('src','images/refresh.png');




 //    },
 //    error: function(x, t, m) {
 //        if(t==="timeout") {
 //             Notify('Timeout Error','Timeout, please retry again','normal',true)
 //            $(t).find('img').attr('src','images/refresh.png');
 //        } else {
 //             Notify('Search Filter Error','please select the search filter','normal',true)
 //        }
 //    }
	// });
	$.get('controllers/proxyman.php',{endpoint:"analysis/demandvariation",params:p},function(d){		
			console.log(d);
			if(d.errorCode>0){
				alert('ERR DVA');
				return;
			}
			//	clear previous data
			_.each(mem.generic.customerList,function(cs,cd){
				if(cs === undefined || cs === null)
					mem.generic.customerList.splice(cd,1);
				if(cs.name == x)
					mem.generic.customerList.splice(cd,1);
			});
			// 	save new customers
			if(d.response.customerDemandList.length==0)
				console.log('No Customers');
			var customer_obj = {name:x,customers:[]};
			_.each(d.response.customerDemandList,function(cs){
				customer_obj.customers.push({name:cs.customerName,stats:cs.statisticsDTO,id:cs.meterIdList});
			});
			mem.generic.customerList.push(customer_obj);
			var payload=[];var change = [];
			for(var i=0; i<d.response.buckets.length; i++){
					var type = 0;
					var clr = null;
					var obj = d.response.buckets[i].content;
					var txt;
					if(d.response.buckets[i].bucketType == 'more'){
						txt = '+ ';
						type=0;
						clr = '#8bbc21';
					}
					else{
						txt = '- ';
						type=1;
						clr = '#c42525';
					}
					txt += x;
				if(!$.isEmptyObject(d.response.buckets[i].content))
				{
					var ser = {name:txt,data:[],type:'column',yAxis:1,id:'fetched',color:clr};
					for(var key in obj)
					{
						if(obj[key]!=null && obj[key].length>0)
						ser.data.push([obj[key][0].ts,obj[key][0].value]);
					}
					if(!mem.generic.noser){
						mem.generic.demandchart.addSeries(ser);
						mem.generic.demandchart.yAxis[1].axisTitle.attr({
						        text: units[stype]
						});
					}
					else{
						if(txt == '+ All' || txt == '- All')
						{
							mem.generic.demandchart.addSeries(ser);
							mem.generic.demandchart.yAxis[1].axisTitle.attr({
						        text: units[stype]
							});
						}
					}
				}
					change[type]=d.response.buckets[i].statistics.mean||0;
				
			}
			if(mem.generic.noser)
				--mem.generic.serlist;
			if(mem.generic.serlist == 0){
				mem.generic.noser = false;
			}
			var totalChange = change[0]-change[1];
			mem.generic.table.fnUpdate(parseInt(d.response.statisticsDTO.changePercent),t,2);
			mem.generic.table.fnUpdate(parseInt(totalChange),t,3);
			var no_of_customers = d.response.customerDemandList.length;
			mem.generic.table.fnUpdate(no_of_customers,t,4);
			// Alerts
			var threshold = $('#reportControls input:eq(0)').val() || null;
			var threshold2 = $('#reportControls input:eq(1)').val() || null;
			var ch = parseInt(d.response.statisticsDTO.changePercent)
			var rem = [true,true];
			if(threshold && !isNaN(ch))
			{
				var th = parseInt(threshold);
				if(ch > th){
					$(t).addClass('blink');
					rem[0] = false;
				}

			}
			if(threshold2 && !isNaN(totalChange))
			{
				var th2 = parseInt(threshold2);
				if(totalChange > th2){
					$(t).addClass('blink');
					rem[1] = false;
				}
			}
			// new changes
			var daily_mean = (change[0]+change[1])/2;
			mem.generic.table.fnUpdate(parseInt(daily_mean),t,5);
			mem.generic.table.fnUpdate(parseInt(d.response.statisticsDTO.contributionPercent),t,6);
			if(rem[0] && rem[1])
				$(t).removeClass('blink');

			$(t).find('img').attr('src','images/refresh.png');

	},'json');
}
function fetchAll(){
	if(mem.generic.noser&&mem.generic.serlist>0){
		alert('Please wait for the analysis to complete');
		return;
	}
	if(!mem.generic.dpid)
	{
		alert('Select a reference');
		return;
	}
	if(!mem.generic.y || !mem.generic.sensortype)
	{
		alert('Cuttoff not found');
		return;
	}
	for(var i=0;i<mem.generic.demandchart.series.length;i++){
		if(mem.generic.demandchart.series[i].type=='line' || mem.generic.demandchart.series[i].name=='flags' || mem.generic.demandchart.series[i].name=='Navigator')
			continue;
		else{
			mem.generic.demandchart.series[i].remove();
			i=0;
		}
	}
	//$('#accordion_wrapper tr').removeClass('selected');
	mem.generic.noser = true;
	mem.generic.serlist = 0;
	var els = $(mem.generic.table).find('.clickerTD');
	_.each(els,function(e){
		++mem.generic.serlist;
		$(e).trigger('click');
	});
}
function clearAll(){
	if(!mem.generic.dpid)
		return;
	mem.generic.demandchart.yAxis[0].removePlotLine('plotLine');
			while(mem.generic.demandchart.series.length>2){
				for(var i=0; i<mem.generic.demandchart.series.length;i++)
				if(mem.generic.demandchart.series[i].name == 'flags' || mem.generic.demandchart.series[i].name == 'Navigator')
					continue;
				else
				mem.generic.demandchart.series[i].remove();
			}
			$(mem.generic.table).find('tr').removeClass('selected');
			$(mem.generic.table).find('.clearable').empty();
			$('.reporter').removeClass('blink');
			mem.generic.y=null;
			mem.generic.sensortype=null;
			$('.cuttoff span').empty().append('Select Cuttoff');
}
function clearFromLegend(e,c){
	e.options.showInLegend = false;
	e.legendItem = null;
	c.legend.destroyItem(e);
	c.legend.render();
}
function loader(el,method){
	if(el){
		var pos = $(el).offset();
		var w = ((40/100)	*	$(el).width()) + pos.left;
		var h = (40/100)	*	$(el).height() + pos.top;
	}
	if(method)
	{
		var loader = '<div style="position:absolute;top:'+h+'px;left:'+w+'px;" class="loaderdiv">Loading..</div>'
		$('.reporter').append(loader);
	}
	else
		$('.loaderdiv').remove();
}
function loader2(el,method){
	if(el){
		var pos = $(el).offset();
		var w = ((40/100)	*	$(el).width()) + pos.left;
		var h = (40/100)	*	$(el).height() + pos.top;
	}
	if(method)
	{
		var loader = '<div style="position:absolute;top:'+h+'px;left:'+w+'px;" class="loaderdiv">Loading..</div>'
		$(el).parent().append(loader);
	}
	else
		$('.loaderdiv').remove();
}
function formatC ( x,idx,me,refresh ) {
	var p = $(me).parent();
	var html = '<table><thead><td>Weekend Change Rate</td><td>Weekend Average</td><td>Weekday Change Rate</td><td>Weekday Average</td></thead><tbody>';
	// supplyzone,name,ep,search
	var stype = sensor2val()||'CONSUMPTION';
	ep = 'genericanalysis/analyse';
	$.get('controllers/proxyman.php',{endpoint:ep,params:"supplyzone=&candidate_st="+st+"&candidate_et="+et+"&resolution="+interval+"&sensortype="+stype+"&search=&include="+x+"&exclude=&population_st=&population_et=&dayjump=&operations=weekday_change_rate,weekend_change_rate,weekday_average,weekend_average&username=&turnoffvee="+veecheck,pyt:"true",contenttype:"application/x-www-form-urlencoded"},
		function(d){
			if(d.errorCode>0){
				alert('Err - Generic Analytics Api');
				return;
			}
			//var tr = '<tr>';
			var pre = mem.generic.customertable.api().row(idx).data();
			var j=0;
			_.each(d.response,function(el){
				for(var i in el)
			//		tr += '<td>'+el[i]+'</td>';
					pre[8+j] = el[i];
					++j;
					//mem.generic.customertable.fnUpdate(el[i],p,(7+i));
			});
			mem.generic.customertable.api().row(idx).data(pre);
			//tr += '</tr>';
			//html += tr+'</tbody></table>';
		//row.child(html).show();
		
		$(me).find('img').attr('src','images/refresh.png');
		if(refresh)
		mem.generic.customertable.api().draw();
	},'json');
	//tr.addClass('shown');
}

function format ( x, row, tr, me ) {
	if(!mem.generic.hasOwnProperty('customerList'))
	{
			alert('Make sure you have requested a Demand Variation Analysis for this sector');
			$(me).find('img').attr('src','images/plus.png');
			return;
	}
    // `d` is the original data object for the row
    var secJson = '';
    var stype = 'CONSUMPTION';
    if(x == 'SGDEMAND')
    	return;
    if(x == 'All')
	secJson = actions.Searchjson;
	else if(x == 'Residential')
	secJson = res_query('all');
	else if(x == 'Non-Residential')
	secJson = res_query('non');
	else if(x == 'Temperature' || x == 'Rainfall' || x == 'Humidity'){
	secJson = tempq;
	switch(x){
		case 'Temperature':
		stype = 'TEMP';
		break;
		case 'Rainfall':
		stype = 'RAINFALL';
		break;
		case 'Humidity':
		stype = 'HUMIDITY';
		break;
	}
}
	else if(sectors.indexOf(x)>=0)
	secJson = sectorQuery(x);
	else if(res_sectors.indexOf(x)>=0)
	secJson = res_query(x);
	else{
		alert('ERR SEC QUERY');
		return;
	}
	$.get('controllers/proxyman.php',{endpoint:'data/search/data',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+stype+"&search="+secJson+"&include=&exclude=&turnoffvee="+veecheck},
	function(d){	
		if(d.errorCode>0)
		{
			$(me).find('img').attr('src','images/plus.png');
			Notify('Warning',d.errorMsg,'normal',true);
			return;
		}
		var customers = [];
		var groupcustomers = [];
		var cust_id = [];
		var groupids = [];
		var qids = [];
		var quality = [];
		var cust = null, meta=null;
		var total = 0;

		for(var i=0; i<d.response.meters.length; i++){
			//meta = meterInfo(d.response.meters[i].mtrid);
			meta = d.response.meters[i].meta._source;
			cust = meta.bp_name || meta.customer_name || meta.customername || meta.display_customer_name || d.response.meters[i].mtrid;
			customers.push(cust);
			cust_id.push(d.response.meters[i].mtrid);
			quality.push(d.response.meters[i].quality.quality||0)
			// total calculation
			_.each(d.response.meters[i].data,function(e){
				total = total + (e.value||0);
			});
		}
		groupcustomers = _.uniq(customers);
		var len = groupcustomers.length;
		_.each(groupcustomers,function(el,il){
			if(el == ''){
				groupids.push([]);
				return;
			}
			var idx = [];
			var qx = [];
			var cd = customers.indexOf(el);
			if(cd>=0){
				idx.push(cust_id[cd]);
				qx.push(quality[cd]);
				customers[cd] = '';
			}
			while(customers.indexOf(el)>=0){
				idx.push(cust_id[customers.indexOf(el)]);
				qx.push(quality[customers.indexOf(el)]);
				customers[customers.indexOf(el)] = '';
			}
			groupids.push(idx);
			var totalq = 0;
			_.each(qx,function(q){
				totalq = totalq + q;
			})
			qids.push(totalq / (qx.length));
			//idx.splice(0,idx.length);
		});
		/*html += '<div class="csList"><ul>'
		_.each(groupcustomers,function(ed,ld){
			var _color;
			if(qids[ld] >= 50)
				_color = 'orange';
			if(qids[ld] >= 75)
				_color = 'green';
			if(qids[ld] < 50)
				_color = 'red';
			//html += '<li data-id="'+groupids[ld].toString()+'"><div class="_txt">'+ed+'</div><div class="healthbox right '+_color+'">'+parseInt(qids[ld])+'%</div><div style="clear:both"></div></li>';
		});
		html += '</ul></div>';*/
		
		var len = 'Unknown';
		var found = false;
		for(var i=0;i<mem.generic.customerList.length;i++)
		{
			var html = '<div class="csList"><div class="button tiny" onClick="generateHM(\''+x+'\')">Data Availability</div><ul>';
			if(mem.generic.customerList[i].name == x)
			{
				found = true;
				var el = mem.generic.customerList[i].customers;
				len = el.length;
				_.each(el,function(ed){
					var val = ed.stats.contributionPercent||null;
					var per = ed.stats.changePercent||null;
					var abs = ed.stats.changeValue||null;
					if(val){
						val = parseFloat(val);
					if(!isNaN(val))
						val = val.toFixed(2);
				}
					else
						val = '0';
					if(per)
						per = per.toFixed(2);
					else
						per = '0';
					if(abs)
						abs = abs.toFixed(2);
					else
						abs = '0';
					var ld = groupcustomers.indexOf(ed.name)
					var _color = '#eee';
					var _quality = 'unknown';
					if(ld >=0)
					{
						if(qids[ld] >= 50)
							_color = 'orange';
						if(qids[ld] >= 75)
							_color = 'green';
						if(qids[ld] < 50)
							_color = 'red';
						_quality = parseInt(qids[ld]);
					}
					html += '<li data-id="'+ed.id.toString()+'"><div class="_txt">'+ed.name+'</div><div class="healthbox right '+_color+'">'+_quality+'%</div><br/><div class="inline-block"><b>Contribution:</b> '+val+'%</div><div class="inline-block"><b>Change: '+per+'%</b></div><div class="inline-block"><b>Absolute Value: '+abs+'</b></div></li>';
				});
				break;	
			}
		}
		if(!found)
		{
			alert('Make sure you have requested a Demand Variation Analysis for this sector');
			$(me).find('img').attr('src','images/plus.png');
			return;
		}
        row.child(html).show();
	    tr.addClass('shown');
	    //mem.generic.table.fnUpdate(len,tr,4);
	    var total_wtype = (total/mem.generic.wdemand)*100;
	    mem.generic.table.fnUpdate(total_wtype.toFixed(2)+'%',tr,7);
	    $(me).find('img').attr('src','images/refresh.png');
	},'json');
    
}
function removeme(x){ //SECTOR REPORT
	var list = $(x).parent().data('id');
	var meters = list.toString().split(',');
	var txt = $(x).parent().find('._txt');
	if($(txt).hasClass('removeme'))
	{
	_.each(meters,function(e,i){
		if(excludeMarkers.indexOf(mem.generic.idarr[e]>=0))
		excludeMarkers.splice(excludeMarkers.indexOf(mem.generic.idarr[e],1));
	});
		$(x).parent().find('img').attr('src','images/close.png');
	}
	else{
	_.each(meters,function(e){
		excludeMarkers.push(mem.generic.idarr[e]);
	});
		$(x).parent().find('img').attr('src','images/plus.png');
	}
	$(txt).toggleClass('removeme');
}
function customerList(x,types){	// OVERVIEW REPORT
	$(x).find('ul').empty();
	var count = 0;
	var html = '';
	for(var i=0; i<types.length; i++){
	var m = types[i];
	if(sectors.indexOf(m)<0 && res_sectors.indexOf(m)<0){
		continue;
	}

	var secJson='';
	var stype = 'CONSUMPTION';
	if(sectors.indexOf(m)>=0)
		secJson = sectorQuery(m);
	else if(res_sectors.indexOf(m)>=0)
		secJson = res_query(m);
	else if(m == 'residential')
		secJson = res_query('all');
	else if(m == 'non-residential')
		secJson = res_query('non');
	else
	{
		alert('ERR SEC JSON');
		continue;
	}
	++count;
	$.get('controllers/proxyman.php',{endpoint:'data/search/data',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+stype+"&search="+secJson+"&include=&exclude=&turnoffvee="+veecheck},
	function(d){	
		--count;
		if(d.errorCode>0){
			alert('Err '+d.errorMsg);
			return;
		}
		var customers = [];
		var groupcustomers = [];
		var cust_id = [];
		var groupids = [];
		var qids = [];
		var quality = [];
		var removed = [];
		var cust = null, meta=null;
		for(var i=0; i<d.response.meters.length; i++){
			meta = meterInfo(d.response.meters[i].mtrid);
			cust = meta.bp_name || meta.customer_name || meta.customername || meta.display_customer_name || d.response.meters[i].mtrid;
			customers.push(cust);
			cust_id.push(d.response.meters[i].mtrid);
			quality.push(d.response.meters[i].quality.quality||0)
		}
		groupcustomers = _.uniq(customers);
		_.each(groupcustomers,function(el,il){
			if(el == ''){
				groupids.push([]);
				return;
			}
			var idx = []; var qx = [];
			var cd = customers.indexOf(el);
			if(cd>=0){
				idx.push(cust_id[cd]);
				qx.push(quality[cd]);
				customers[cd] = '';
			}
			while(customers.indexOf(el)>=0){
				idx.push(cust_id[customers.indexOf(el)]);
				qx.push(quality[customers.indexOf(el)]);
				customers[customers.indexOf(el)] = '';
			}
			groupids.push(idx);
			var totalq = 0;
			_.each(qx,function(q){
				totalq = totalq + q;
			})
			qids.push(totalq / (qx.length));
			//idx.splice(0,idx.length);
		});
		_.each(groupcustomers,function(ed,ld){
			var _color;
			if(qids[ld] >= 50)
				_color = 'orange';
			if(qids[ld] >= 75)
				_color = 'green';
			if(qids[ld] < 50)
				_color = 'red';
			var extraclass = '';
			if(_.intersection(excludeMarkers,groupids[ld]).length>0)
				extraclass = 'removeme';
			html += '<li class="'+extraclass+'" data-id="'+groupids[ld].toString()+'">'+ed+'<div class="healthbox right '+_color+'">'+parseInt(qids[ld])+'%</li>';
		})
		if(count == 0)
		{
			if(html == '')
				$(x).find('ul').append('No Customers found');
			else
				$(x).find('ul').append(html);
		}
	},'json');
	}	//ALL DONE
	
}
function reloadExc(){
	keepExclude = true;
	$('#overlay').addClass('hide');
	$('.reporter').addClass('zoomOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
		$(this).remove();
		//$('#overlay').toggleClass('hide');
		report(rtype);
	});
}
function reaggregate(){
//function adder(chart,query,m,ep,sTitle,axis,stack,visible,type,extras,id,statfunct,inc,legend,callback){

	for(var i=0; i<mem.rankchart.series.length; i++){
		if(mem.rankchart.series[i].name == 'aggregate')
			{
				mem.rankchart.series[i].remove();
				break;
			}
	}
	$('#__aggregate').remove();
				adder(mem.rankchart,secJson,'CONSUMPTION','data/search/aggregate','aggregate',0,0,true,null,'',null,function(d){
					_.each(d,function(x,c){
						try{
							html = '<ul id="__aggregate">';
							html += '<li class="box" style="background-color:'+x.color+';"></li><li>Quality <span class="val">' + x.quality+'%</span><li>Demand Change <span class="val">'+x.statistics.diff.toFixed(2)+'%</span></li><li>Max <span class="val">'+x.statistics.max.toFixed(2)+'</span></li><li>Min <span class="val">'+x.statistics.min.toFixed(2)+'</span></li><li>Mean <span class="val">'+x.statistics.mean.toFixed(2)+'</span></li><li>Demand Variation <span class="val">'+x.statistics.sd.toFixed(2)+'</span></li></ul>';
							$('.stats').prepend(html);
						}
						catch(e){
							console.log("ERR STATS");
						}
				})
				});
	var newtotal = 0;
	_.each($('.meterList li .healthbox'),function(f,g){
		if($(f).parent().find('.removeme').length>0)
			return;
		var num = $(f).text();

		var val = num.substring(0,num.length-1);
		newtotal = newtotal + parseInt(val);
	});
	var newcount = $('.meterList li').length - $('.meterList .removeme').length;
	var totalavg = newtotal/newcount;
	$('.reportage').empty().append(totalavg.toFixed(2)+'%');
}
function removeAxis(chart,axis){	// ZONAL REPORT
	for(var i=0; i<chart.series.length; i++)
	{
		if(chart.series.name == 'Navigator' || chart.series[i].name == 'flags')
			continue;
		if(chart.series[i].yAxis.options.index == axis){
			chart.series[i].remove();
			i = 0;
		}
	}
}
function sensor2val(x){
	var id = $('#basic input:checked').attr('id')||null;
	var val = null;
	if(!id||id.length==0)
		return val;
	else
	{
		_.each(actionListObj.UnitOptions,function(e){
			if(e.id == id)
				val = e.stype;
		});
		return val;
	}
}
function fetchAnalytics(supplyzone,name,ep,sensor,search){
	// supplyzone,name,ep,search
	$.get('controllers/proxyman.php',{endpoint:ep.toLowerCase(),params:"supplyzone="+supplyzone+"&candidate_st="+st+"&candidate_et="+et+"&resolution="+interval+"&sensortype="+sensor+"&search="+search+"&include=&exclude=&population_st=&population_et=&dayjump=&operations=weekday_change_rate,weekend_change_rate,weekday_average,weekend_average&username=&turnoffvee="+veecheck,pyt:"true",contenttype:"application/x-www-form-urlencoded"},
		function(d){
			console.log(d);
			if(d.errorCode>0){
				alert('Err - Generic Analytics Api');
				return;
			}
			var tr = '<tr id="'+name+'"><td>'+name+'</td>';
			_.each(d.response,function(el){
				for(var i in el)
					tr += '<td>'+el[i]+'</td>';
			});
			$('.reporter table tbody').append(tr);
		},'json');
}
function computeReport2(){
	var html = '<table id="customertable"><thead><th></th><th>Customer</th><th class="selected">Average Consumption</th><th>Min</th><th>Max</th><th>Difference</th><th>Status</th><th>Sector</th><th>Weekend Change</th><th>Weekend Avg</th><th>Weekday Change</th><th>Weekday Avg</th></thead><tbody>'
	actions = getActionParams();
	var stype = sensor2val()||'CONSUMPTION';
	var ids = [];
	var cust = [];
	var groupcust = [];
	var groupcust2 = [];
	var sector = [];
	var infoarr = [];
	var groupcustomers = null;
	_.each(group,function(g,gl){
	/*	var thisid=[];
		var thiscust = [];
		var thissec = []; */
		_.each(g,function(meta,metaid){
		//	thisid.push(meta[2]);
		//	thiscust.push(meta[4]);
		//	thissec.push(meta[5]);
			infoarr.push(meterInfo(meta[2]));
		});
	//	ids.push(thisid);
	//	cust.push(thiscust);
	//	groupcust.push(_.uniq(thiscust));
	});
	groupcustomers = groupMeterbycustomer(infoarr);
		ids = groupcustomers.groupids;
		_.each(groupcustomers.customers,function(el){
			groupcust2.push([el]);
		})
		_.each(ids,function(idarr){
			var sec=[];
			_.each(idarr,function(e){
				var _info = meterInfo(e);
				sec.push(_info.tag_sector);
			});
			sector.push(sec);
		});
		groupcust.splice(0,groupcust.length);
		groupcust = groupcust2;
		console.log(ids);
		console.log(cust);
		console.log(groupcust);
		var ajaxCount=0;
		var totalReq = 0;
		_.each(groupcust,function(el,elid){
			if(el.length>1)
				return;
			if(typeof(el)=='undefined' || el == '' || el == null)
				return;
			++ajaxCount; ++totalReq;
			$.get('controllers/proxyman.php',{endpoint:'data/search/aggregate',params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+stype+"&search=&include="+ids[elid].toString()+"&exclude="+excludeMarkers.toString()+"&interval="+_interval+"&turnoffvee="+veecheck},function(d){
				--ajaxCount;
				// progress bar
				var per = 100-((ajaxCount/totalReq)*100);
				console.log(per);
				$('.reporter .progress .meter').css('width',parseInt(per)+'px');
				if(d.errorCode>0)
					return;
				var data = d.response.meters[0].statistics||null;
		try{
				if(data)
				{
					var checkel = $('#'+winId+' input[name="paramtype"]:checked').val();
					var _class = 'green';
					var _isblinking = '';
					if(!isNaN(data[checkel]) && parseFloat(data[checkel])>=mem.generic.threshold){
						_class = 'blink';
						_isblinking = 'yesblink';
					}
					html += '<tr class="'+_isblinking+'"><td class="details-control"><img src="images/plus.png"></td><td class="clickerTD" data-id="'+ids[elid].toString()+'">'+el.toString()+'</td><td>'+data.mean.toFixed(2)+'</td><td>'+data.min.toFixed(2)+'</td><td>'+data.max.toFixed(2)+'</td><td>'+data.diff.toFixed(2)+'</td><td><div class="box '+_class+'"</td><td>'+_.uniq(sector[elid]).toString()+'</td><td></td><td></td><td></td><td></td></tr>';
				}
			}
		catch(e){
			console.log(e);
		}
				if(ajaxCount == 0 || ajaxCount < 0)
				{
					html += '<tbody></table>';
					$('#'+winId+' .tablecontainer').empty().append(html);
					var table = $('#customertable').dataTable({paging:false,scrollY:"450px",scrollX:true,scrollCollapse: true,});
					new $.fn.dataTable.FixedColumns( table );
					mem.generic.customertable = table;
					//$('#customertable_filter').css({'position':'absolute','top':'0px','z-index': '9','left':'150px'});
					/*$('#customertable tbody').on('click', 'td.details-control', function (e) {
			        var tr = $(this).closest('tr');
			        //console.log($(tr)).index();
			        var x = $(tr).find('.clickerTD').data('id');
			        $(this).find('img').attr('src','images/ajax-loader.gif');
			        formatC(x,tr,this);
			        });*/
					// activate all blinks
					var len = $('#customertable tbody tr').length;
					$('#customertable tbody tr').each(function(id,el){
						//if($(el).hasClass('yesblink')){
							console.log(id);
							$(el).find('img').attr('src','images/ajax-loader.gif');
							var x = $(el).find('.clickerTD').data('id');
							var refresh = false;
							if(id == len-1)
								refresh = true;
							formatC(x,id,this,refresh);
						
					});
				}
			},'json');
		});
}
function computeReport(){
	var html = '<table id="customertable"><thead><th>Customer</th><th>Average Consumption</th><th>Min</th><th>Max</th><th>Status</th><th>Sector</th></thead><tbody>';
			actions = getActionParams();
			var stype = sensor2val()||'CONSUMPTION';
			$.get('controllers/proxyman.php',{endpoint:"data/search/data",params:"st="+st+"&et="+et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+stype+"&search="+actions.Searchjson+"&include=&exclude="+excludeMarkers.toString()+"&interval="+_interval+"&turnoffvee="+veecheck},
				function(d){
					$('#'+winId+' .tablecontainer').empty();
					if(d.errorCode>0)
					{
						alert('Err - Customer');
						return;
					}
					var meters = [];
					var meterStats = [];
					var mdata = [];
					var customers = [];
					var groupcust = [];
					var stats = [];
					var sector = [];
					for(var i=0; i<d.response.meters.length;i++){
						meters.push(d.response.meters[i].mtrid);
						meterStats.push(d.response.meters[i].statistics||{});
					}
					// to customers
					_.each(meters,function(el){
						var meta = meterInfo(el);
						var cust = meta.bp_name || meta.customer_name || meta.customername || meta.display_customer_name || meta.site || el;
						customers.push(cust);
						mdata.push(meta);
					});
					var groupcust = _.uniq(customers);
					_.each(groupcust,function(g){
						var thisarr=[];
						var thissector = [];
						var idx = customers.indexOf(g);
						if(idx>=0)
						{
							thisarr.push(meterStats[idx]);
							thissector.push(mdata[idx].tag_sector||'');
							customers[idx] = '';
						}
						while(customers.indexOf(g)>=0){
							thisarr.push(meterStats[customers.indexOf(g)]);
							thissector.push(mdata[customers.indexOf(g)].tag_sector||'');
							customers[customers.indexOf(g)] = '';	
						}
						stats.push(thisarr);
						sector.push(thissector);
					});
					console.log(groupcust);
					console.log(stats);
					_.each(groupcust,function(el,elid){
						var diff=0,max=0,min=0,mean=0,variance=0,sd=0;
						_.each(stats[elid],function(st){
							mean = mean + st.mean||0;
						});
						mean = mean / stats[elid].length;
						var _class = 'green';
						if(!isNaN(mean) && mean>=mem.generic.threshold)
							_class = 'blink';
						html += '<tr><td>'+el+'</td><td>'+mean+'</td><td>N.A</td><td>N.A</td><td><div class="box '+_class+'"</td><td>'+_.uniq(sector[elid]).toString()+'</td></tr>';
					});
					html += '<tbody></table>';
					$('#'+winId+' .tablecontainer').append(html);

					//mem.generic.customertable = $('#customertable').dataTable({});
			},'json');
}
function recalc(){
	//$(mem.generic.customertable).off();
	mem.generic.customertable.api().destroy();
	$('#'+winId+' .tablecontainer').empty().append('	<br/><br/><div class="small-12 text-center"><img src="images/ajax-loader.gif"></div><br/><div class="progress round large-10"><span class="meter" style="width:2%"></span></div>');
	mem.generic.threshold = $('#'+winId+' #thres').val();
	computeReport2();
}
function createPie(el,payload){
	var chart=new Highcharts.Chart({
			chart: {
			renderTo: el},
			title:{text:''},
			exporting: {
         		enabled: false
			},
			//series:{data:[['mem.generic.pies',50],['oye',50]]}
			  series: [{
            type: 'pie',
            name: 'Sector share',
            data: payload
        }]
});
}
function dailyPdf(){
	var _st = st;
	var _et = st + (3600*24*1000);
	var win = logWindow('report','dpdf');
	loader2('#'+win,1);
	var sensortype = 'CONSUMPTION';
	var meters=[];
	var jsons=[];
	_.each(metadata,function(m){
		var cat = m.tag_category||null;
		if(cat != "nonresidential")
			return;
		meters.push(m._id);
	});
	//	get data
	$.get('controllers/proxyman.php',{endpoint:'data/search/data',params:"st="+_st+"&et="+_et+"&intervalmin="+interval+"&includepredicted="+pred+"&sensortype="+sensortype+"&search=&include="+meters.toString()+"&exclude=&interval="+_interval+"&turnoffvee="+veecheck},
		function(d){
			if(d.errorCode>0){
				alert(d.errorMsg);
				loader2('#'+win,0);
				return;
			}
			loader2('#'+win,0);
			var html = '<ul>';
			var dir = '../uploads/charts/';
			var uniqid = getRandomInt(0,500);
			for(var i=0; i<d.response.meters.length; i++){
				var stuff={outfile:dir+'charts_'+uniqid+'_'+i+'.png',type:'image/png'};
				var payload = [];
				for(var j=0;j<d.response.meters[i].data.length;j++){
					var temp = [];
					temp.push( d.response.meters[i].data[j].ts||null);
					temp.push( d.response.meters[i].data[j].value||null);
					payload.push(temp);
					delete temp;
				}
				html += '<li>'+d.response.meters[i].mtrid+'</li>';
				var options = {
					title: {text:d.response.meters[i].mtrid}, 
					xAxis:	{	type:'datetime',minPadding:0.02,maxPadding:0.02},
					series:[{data:payload}]
				};
				stuff.infile = options;
				jsons.push(stuff);
			}
		html +'</ul><br/><strong>Creating PDF..</strong>';
		$('#'+win).append(html);
		console.log(jsons);
		//json2pdf
		$.post('chartman.php',{req:'json2pdf',json:JSON.stringify(jsons),uniqid:uniqid},
			function(e){
				if(e.errorCode>0){
					alert('Err - PDF');
					$('#'+win).remove();
					return;
				}
				window.location.href= 'push.php?file='+e.response+'&filename='+e.filename;
				$('#'+win).remove();
			},'json');

		},'json');
}
function showDays(el){
	$('.reporter .days_list').remove();
	var html = '<div class="listwrap"><div class="button tiny cleardays text-center">Done</div><ul class="days_list">';
	if(!mem.generic.hasOwnProperty('sensortype'))
	{
		alert('Please select cuttoff first');
		return;
	}
	var sensor = mem.generic.sensortype[0]||null;
	var ts,ds;
	if(!sensor)
	{
		alert('Please select cuttoff first');
		return;
	}
	for(var i=0; i<mem.generic.demandchart.series.length; i++)
	{
		if(mem.generic.demandchart.series[i].name==sensor)
		{
			ts = mem.generic.demandchart.series[i].xData;
		}
	}
	if(typeof(ts)!='object' || ts.length==0)
		return;
	var tempdate=null;
	var _cl = '';
	if(!mem.generic.hasOwnProperty('dayslist'))
		mem.generic.dayslist = [];
	_.each(ts,function(t){
		if(mem.generic.dayslist.indexOf(t)>=0)
			_cl = 'removeme';
		else
			_cl = '';
		tempdate = new Date(t);
		html += '<li data-id="'+t+'" class="'+_cl+'">'+tempdate+'</li>';
	});	
	html += '</ul></div>';
	$(el).parent().append(html);
}
function getRemote(remote_url) {
    return JSON.parse($.ajax({
        type: "GET",
        url: remote_url,
        async: false
    }).responseText);
}
function addStats(){
		$.get('controllers/proxyman.php',{endpoint:'singaporedata/demand',params:'st='+st+'&et='+et+'&includepredicted='+pred+'&intervalmin='+interval+'&sensortype=SGDEMAND&watertype='+wtype_main+'&turnoffvee='+veecheck},
					function(g){
						if(g.errorCode>0)
							return;
						var coef = 0.000264;
						var total_demand=0;
						_.each(g.response.meters[0].data,function(el){
							total_demand = total_demand + (	(el.value||0)/coef);
						});
						mem.generic.wdemand=total_demand;
						var wt='PW';
						if(wtype_main!='potable')
							wt = 'NW';
						mem.generic.wt = wt;
					},'json');
}

function heatMap(searchquery,name,includes,mode){
	if(!searchquery && !includes)
		return;
	mode = mode || null;
	searchquery = searchquery||'';
	includes = includes||'';
	var groupby = 'bp_name';
	$.get('controllers/proxyman.php',{endpoint:'dataavailability/search/dataavailability',params:'st='+st+'&et='+et+'&groupby='+groupby+'&search='+searchquery+'&include='+includes},
		function(d){
			if(d.length==0){
				alert('No information returned');
				return;
			}
		/*	if(mode)
			{
				_.each(includes,function(inc){
					$.get('controllers/proxyman.php',{endpoint:'dataavailability/search/dataavailability',params:'st='+st+'&et='+et+'&groupby=bp_name&search='+searchquery+'&include='+inc},
						function(x){
							childs.push(x);
						},'json');
				});
			}*/
			var uniqid = logWindow('control');
			var plotterdiv = $('#'+uniqid);
			var randomposition = getRandomInt(25,120);
			var currentposition = plotterdiv.position();
			var thistop = currentposition.top;
			$('.plotter:last-child').css({top:parseInt(thistop)+randomposition+'px'});
			var containerid = uniqid+'heatmap';
			$('#'+uniqid).append('<div class="container" id="'+containerid+'"></div>');
			$('#'+uniqid+' .deptitle').empty().append(name);
			//parsing data for series
			var seriesarr = [];
			var yaxis =[];
			var xaxis = [];
			var days = [];
			_.each(d,function(arr,index){
				//sorting by ts
				arr.statuses.sort(function(a,b){
					return a.ts - b.ts;
				});
				yaxis.push(arr.group_by_field)
				_.each(arr.statuses,function(s,g){
					var per = parseInt(s.available.substring(0,s.available.length-1));
					xaxis.push(s.ts);
					xaxis = _.uniq(xaxis);
					var pos = xaxis.indexOf(s.ts);
					var temp = {x:pos,y:index,value:per,reason:'Missing: '+s.data_missing+', Missing Header: '+s.header_missing+' Missing Scada: '+s.scada_missing+' Unknown: '+s.unknown_reason};
					seriesarr.push(temp);
				});
			});
			console.log(seriesarr);
			console.log(yaxis);
			console.log(xaxis);
			var colorScale = new chroma.scale(['red', 'green']).out('hex');
			var html = '<table class="heattable"><thead><tr><th>Customer</th>';
			_.each(xaxis,function(x){
				var thisdate = new Date(x);
				var hours = thisdate.getHours();
				var mins = thisdate.getMinutes();
				var month = thisdate.getMonthName();
				var day = thisdate.getDate();
				var year = thisdate.getFullYear();
				var testts = thisdate.setHours(0);
				var classtype = '';
				if(!mem.generic.hasOwnProperty('dayslist'))
					mem.generic.dayslist =[];
				if(mem.generic.dayslist.indexOf(testts)>=0)
					classtype = 'removeme';
				html += '<th title="'+thisdate.toString()+'" class="'+classtype+'" data-id="'+x+'">'+day+'/'+month+'/'+year+' '+hours+':'+mins+'</th>';
			});
			html += '</tr></thead><tbody>';
			// Data Rows
			_.each(yaxis,function(y,yindex){
				var classtype = '';
				if(checkexclusion(y))
					classtype = 'removeme';
				html += '<tr class="'+classtype+'"><td>'+y+'</td>';
					// xaxis
					_.each(xaxis,function(x,xindex){
						var classtype ='';
						var thisdate = new Date(x);
						var testts = thisdate.setHours(0);
						if(mem.generic.dayslist.indexOf(testts)>=0)
							classtype = 'removeme';
						_.each(seriesarr,function(ser){
							if(ser.x == xindex && ser.y == yindex){
								var clr = colorScale(ser.value / 100);
								html += '<td class="'+classtype+'" style="background-color:'+clr+'" class="heattd" title="'+ser.reason+'" data-id="'+x+'">'+ser.value+'</td>';
							}
						});
					});
				html += '</tr>';
			});
			html += '</tbody></table>';
			$('#'+containerid).append(html).find('table').tooltip();
			$('#'+uniqid).css('zIndex','100');	// force visibility through reports

			// create heatmap
	/*		var	chart = new Highcharts.Chart({
				chart: {
		            type: 'heatmap',
		            marginTop: 10,
		            marginBottom: 0,
		            renderTo:containerid
        		},
        		title: {
          		  text: ''
        		},
        		xAxis:	{
        			categories:xaxis},
        			//type:'datetime'},
        		yAxis: {
            		categories: yaxis,
            		title:null
            	},
        		colorAxis: {
		            min: 0,
		            minColor: '#FFFFFF',
		            maxColor: Highcharts.getOptions().colors[0]
		        },
		         legend: {
			        align: "right",
			        layout: "vertical",
			        margin: 0,
			        verticalAlign: "top"
			        //y: 25
			    },
		        series:[{
		        	rowsize: 24*3600000,
		        	name:'Checking',
		        	data:seriesarr,
		        	turboThreshold:__tr
		        }]
		    });
		    console.log(chart);*/
		    if(mode){
			groupby = 'mtrid';
			$.get('controllers/proxyman.php',{endpoint:'dataavailability/search/dataavailability',params:'st='+st+'&et='+et+'&groupby='+groupby+'&search='+searchquery+'&include='+includes},
			function(e){
				var seriesarr = [];
				var yaxis =[];
				var xaxis = [];
				var days = [];
				_.each(e,function(arr,index){
					//sorting by ts
					arr.statuses.sort(function(a,b){
						return a.ts - b.ts;
					});
					yaxis.push(arr.group_by_field)
					_.each(arr.statuses,function(s,g){
						var per = parseInt(s.available.substring(0,s.available.length-1));
						xaxis.push(s.ts);
						xaxis = _.uniq(xaxis);
						var pos = xaxis.indexOf(s.ts);
						var temp = {x:pos,y:index,value:per,reason:'Missing: '+s.data_missing+', Missing Header: '+s.header_missing+' Missing Scada: '+s.scada_missing+' Unknown: '+s.unknown_reason};
						seriesarr.push(temp);
					});
				});
				// Data Rows
				var html = '';
				_.each(yaxis,function(y,yindex){
					var classtype = '';
					if(checkexclusion(y))
						classtype = 'removeme';
					html += '<tr class="'+classtype+'"><td>'+y+'</td>';
						// xaxis
						_.each(xaxis,function(x,xindex){
							var classtype ='';
							var thisdate = new Date(x);
							var testts = thisdate.setHours(0);
							if(mem.generic.dayslist.indexOf(testts)>=0)
								classtype = 'removeme';
							_.each(seriesarr,function(ser){
								if(ser.x == xindex && ser.y == yindex){
									var clr = colorScale(ser.value / 100);
									html += '<td class="'+classtype+'" style="background-color:'+clr+'" class="heattd" title="'+ser.reason+'" data-id="'+x+'">'+ser.value+'</td>';
								}
							});
						});
					html += '</tr>';
				});
				$('#'+containerid).find('tbody').append(html);
			},'json');
		}
		},'json');
}
function generateHM(el){
	if(typeof(el)=='string')
		var x = el;
	else
	var x = $(el).text();
	var secJson = null;
	if(x == 'SGDEMAND')
    	return;
    if(x == 'All')
	secJson = actions.Searchjson;
	else if(x == 'Residential')
	secJson = res_query('all');
	else if(x == 'Non-Residential')
	secJson = res_query('non');
	else if(x == 'Temperature' || x == 'Rainfall' || x == 'Humidity')
	secJson = tempq;
	else if(sectors.indexOf(x)>=0)
	secJson = sectorQuery(x);
	else if(res_sectors.indexOf(x)>=0)
	secJson = res_query(x);
	else{
		alert('ERR SEC QUERY');
		return;
	}
	heatMap(secJson,x);
}
function checkexclusion(e){
	if(!e)
		return null;
	var retVal = null;
	$('.reporter .removeme').each(function(index,el){
		if($(el).find('._txt').text() == e)
			retVal = true;
	});
	return retVal;
}
function clearExclude(el){
	excludeMarkers = [];
	Notify('Demand Variation','Exclusions have been reset.','normal',true);
	$(el).removeClass('selected');
}
